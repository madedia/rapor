<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_absensiCatatan extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    function get_Detail($id_transKelas)
    {
        $this->db->where('trans_kelas.id_transKelas', $id_transKelas);
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        $this->db->where('trans_kelas.id_guruWali = guru.id_guru');

        $query = $this->db->get('trans_kelas, trans_klsgeneral, guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

	function get_kelasWali($id_guru)
	{
		$this->db->where('id_guruWali', $id_guru);
		$this->db->where('id_periode', $this->session->userdata('periode_aktif'));
		$query = $this->db->get('trans_kelas');

		if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['id_transKelas'];
            return $val;
        } else return null;
	}

	function get_siswaCatatan($id_transKelas)
    {
        $this->db->where('trans_siswa.id_tKelasKey', $id_transKelas);
        $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
        $this->db->where('id_periodeDet', $this->session->userdata('periode_aktifDet'));
        $this->db->where('catatan_walikelas.id_tSisKlsKey = trans_siswa.id_transSisKls');

        $query = $this->db->get('trans_siswa, siswa, catatan_walikelas');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_catatanGuru($id_tSisKlsKey)
    {
    	$this->db->where('nilai_mapel.id_tSisKlsKey', $id_tSisKlsKey);
        $this->db->where('nilai_mapel.id_periodeDet', $this->session->userdata('periode_aktifDet'));
        $this->db->where('nilai_mapel.id_tAjarKey = trans_ajar.id_transAjar');
        $this->db->where('trans_ajar.id_guru = guru.id_guru');
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');

    	$query = $this->db->get('nilai_mapel, trans_ajar, guru, mapel');

    	if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function insert($data, $isUpdate)
    {
    	for ($i=0; $i < count($data['id_tSisKlsKey']); $i++)
    	{ 
            $this->db->set('jml_sakit', $data['jml_sakit'][$i]);
            $this->db->set('jml_izin', $data['jml_izin'][$i]);
            $this->db->set('jml_tketerangan', $data['jml_tketerangan'][$i]);
            $this->db->set('isi_catatan', $data['isi_catatan'][$i]);
            $this->db->set('status_kenaikan', $data['status_kenaikan'][$i]);
            $this->db->set('kelas_lanjut', $data['kelas_lanjut'][$i]);

            if($data['isUpdate'][$i])
            {
            	//$this->db->where('id_tSisKlsKey', $row['id_tSisKlsKey']);
            	$this->db->where('id_catatan', $data['id_catatan'][$i]);
            	$this->db->update('catatan_walikelas');
            }
            else
            {
            	$this->db->set('id_periodeDet', $this->session->userdata('periode_aktifDet'));
            	$this->db->set('id_tSisKlsKey', $data['id_tSisKlsKey'][$i]);
            	$this->db->insert('catatan_walikelas');
           	}
            //echo $this->db->last_query() . '<br>';
            
        }
    }

    function get_angkaKelas($id_transKelas)
    {
        $this->db->where('id_transKelas', $id_transKelas);
        $this->db->where('id_tklsgeneralKey = id_trans_klsgeneral');
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');

        $query = $this->db->get('kelas_general, trans_klsgeneral, trans_kelas');

        //echo $this->db->last_query() . '<br>';

        /*if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['kelas_angka'];
            return $val;
        }
        else return null;*/
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_nextKelas($data_kelas)
    {
        foreach ($data_kelas as $row) {
            //$str= substr($row['nama_kelas'], 0, strpos($row['nama_kelas'], " "))."I";
            $str = strstr($row['nama_kelas'], ' ');
            $this->db->where('kelas_angka >',$row['kelas_angka'] );
            $this->db->like('nama_kelas', $str);
        }
        $this->db->where('kelas_general.id_kelasGen = trans_klsgeneral.id_kelasGen');

        $query = $this->db->get('kelas_general, trans_klsgeneral');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_namaKlsLanjut($id_trans_klsgeneral)
    {
        $this->db->where('id_trans_klsgeneral', $id_trans_klsgeneral);
        $query = $this->db->get('trans_klsgeneral');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['nama_kelas'];
            return $val;
        } else return null;
    }
}