<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Mata Pelajaran
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Nilai Mata Pelajaran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Daftar Mata Pelajaran Enrol</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_enrol" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No.</th>
                    <th style="text-align:center">Kode</th>
            				<th style="text-align:center">Nama Mata Pelajaran</th>
            				<th style="text-align:center">Kelas</th>
            				<th style="text-align:center">Isi Nilai</th>
            				<th style="text-align:center">Isi Nilai Via Excel</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($mapelEnrol) {
						$baris=1;
						foreach ($mapelEnrol as $row) {
							if ($row['id_mapel']!=''){
								echo "</tr>";
								echo "<td style='text-align:center'>".$baris."</td>";
								echo "<td>".$row['id_mapelKey']."</td>";
								echo "<td>".$row['nama_mapel']."</td>";
								echo "<td>".$row['nama_kelas']."</td>";
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('nilai_mapel/kamus_deskripsi/'.$row['id_transAjar']).'" class="btn btn-sm btn-warning"><i class="fa fa-sliders"></i> Deskripsi KD</a>';
                                echo '&nbsp <a href="'.base_url('nilai_mapel/isi/'.$row['id_transAjar']).'" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Daftar Nilai</a></td>';
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('nilai_mapel/ekspor_tabel/'.$row['id_transAjar']).'" class="btn btn-sm btn-default"><i class="fa fa-download"></i> Unduh Format</a>';
                                echo '&nbsp <a href="'.base_url('nilai_mapel/impor_tabel/'.$row['id_transAjar'].'/'.$row['id_tKelasKey']).'" class="btn btn-sm btn-default"><i class="fa fa-upload"></i> Unggah Nilai</a></td>';
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": [4, 5],
        } ]
    });
  });
</script>