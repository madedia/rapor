<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siswa extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 3) {
        	redirect ('');
        }

		$this->load->model('model_siswa');
	}

	function beranda ($periode = null)
	{
		$data['periode'] = $periode;

		$data['siswa']= $this->model_siswa->get_struktur('siswa', null);
		$data['kelas']=$this->model_siswa->get_struktur('kelas', null);
		$data['agama']=$this->model_siswa->get_struktur('agama' , null);

		$this->template->display('siswa/siswa', $data);
	}

	function tambah_siswa ()
	{
		$data1 = array(
			'id_siswa' => $this->input->post('id_siswa'),
			'nama_siswa' => $this->input->post('nama_siswa'),
			'id_agama' => $this->input->post('id_agama'),
			'status_siswa' => $this->input->post('status'),
			'tahun_masuk' => $this->input->post('tahun_masuk'),
		);
		/*$data2 = array(
			'id_siswa' => $this->input->post('id_siswa'),
			'id_tkelasKey' => $this->input->post('id_kelasKey'),
		);*/

		$this->model_siswa->tambah_siswa($data1);
		//$this->model_siswa->tambah_transSiswa($data2);
		redirect('siswa');
	}

	function detail ($id_siswa)
	{
		$data['siswa'] = $this->model_siswa->get_detail($id_siswa);
	}

	function get_detail()
	{
		$id_siswa=$this->input->post('id_siswa');

		$data = $this->model_siswa->detail_siswaX($id_siswa);

		$riwayat_kelas = null;
		foreach ($data as $row) {
			$riwayat_kelas .= '<br><b>&nbsp;</b><a class="pull-right">'.$row['id_periode']." : ".$row['nama_kelas'].'</a>';
		}

		echo '<img class="profile-user-img img-responsive img-circle" src="'.base_url('assets/images/siswa2.png').'" alt="User profile picture">
              <h3 class="profile-username text-center">'.$data[0]['nama_siswa'].'</h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>NIP</b> <a class="pull-right">'.$data[0]['id_siswa'].'</a>
                </li>
                <li class="list-group-item">
                  <b>Tahun Diterima</b> <a class="pull-right">'.$data[0]['tahun_masuk'].'</a>
                </li>
                <li class="list-group-item">
                  <b>Riwayat Kelas</b>
                  '.$riwayat_kelas.'
                </li>
                <li class="list-group-item">
                  <b>Status</b> <a class="pull-right">'.$data[0]['nama_status'].'</a>
                </li>
              </ul>';
	}

	function edit_detail()
	{
		$id_siswa=$this->input->post('id_siswa');

		$data = $this->model_siswa->detail_siswaX($id_siswa);

		$riwayat_kelas = null;
		foreach ($data as $row) {
			$riwayat_kelas .= '<br><b>&nbsp;</b><a class="pull-right">'.$row['id_periode']." : ".$row['nama_kelas'].'</a>';
		}
        echo "<img class='profile-user-img img-responsive img-circle' src='".base_url('assets/images/siswa2.png')."' alt='User profile picture'>
              <input type='text' name='nama_siswa' class='profile-username text-center form-control' value='".$data[0]['nama_siswa']."'></input>

              <ul class='list-group list-group-unbordered'>
                <li class='list-group-item'>
                  <b>NIP</b> <a class='pull-right'>".$data[0]['id_siswa']."</a>
                </li>
                <li class='list-group-item'>
                  <b>Riwayat Kelas</b>
                  ".$riwayat_kelas."
                </li>
                
                <li class='list-group-item'>
                  <b>Status</b> <a class='pull-right'>".$data[0]['nama_status']."</a>
                </li>
              </ul>
              <input style='display:none' name='id_siswa' value='".$data[0]['id_siswa']."'>";
	}

	function do_edit_detail()
	{
		$data['id_siswa']=$this->input->post('id_siswa');
		$data['nama_siswa']=$this->input->post('nama_siswa');
		$this->model_siswa->update_detail($data);
		redirect('siswa');
	}

	function update($id_siswa = null)
	{
		$data['id_siswa'] = $id_siswa;
		$data['siswa']= $this->model_siswa->get_struktur('siswa', $data);
		$data['status_siswa']= $this->model_siswa->get_struktur('status_siswa', null);

		$this->template->display('siswa/update_siswa', $data);
	}

	function do_updateAll ()
	{
		$data = array(
			'id_siswa' => $this->input->post('id_siswa'),
			'status_siswa' => $this->input->post('status_siswa'),
		);

		$this->model_siswa->update_status($data);
		redirect('siswa');
	}

	function update_now ($id_siswa, $pilih)
	{
		$data['id_siswa'] = $id_siswa;
		if ($pilih == 'active') {
			$data['status_siswa'] = 1;
		} else if ($pilih == 'non-active') {
			$data['status_siswa'] = 2;
		} else if ($pilih == 'graduated') {
			$data['status_siswa'] = 3;
		}
		$this->model_siswa->update_status1($data);
		redirect('siswa');
	}

	function delete ($id_siswa)
	{
		$data['id_siswa'] = $id_siswa;
		$data['status_siswa'] = 4;
		$this->model_siswa->delete_siswa($data);
		redirect('siswa');
	}
}

?>