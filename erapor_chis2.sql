-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2016 at 10:10 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erapor_chis2`
--
CREATE DATABASE IF NOT EXISTS `erapor_chis2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `erapor_chis2`;

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE `agama` (
  `id_agama` int(11) NOT NULL,
  `nama_agama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id_agama`, `nama_agama`) VALUES
(1, 'Buddha'),
(2, 'Hindu'),
(3, 'Islam'),
(4, 'Katolik'),
(5, 'Khonghucu'),
(6, 'Kristen');

-- --------------------------------------------------------

--
-- Table structure for table `catatan_walikelas`
--

CREATE TABLE `catatan_walikelas` (
  `id_catatan` int(11) NOT NULL,
  `id_tSisKlsKey` int(11) NOT NULL,
  `id_periodeDet` varchar(20) NOT NULL,
  `jml_sakit` int(11) NOT NULL,
  `jml_izin` int(11) NOT NULL,
  `jml_tketerangan` int(11) NOT NULL,
  `isi_catatan` text NOT NULL,
  `status_kenaikan` int(11) DEFAULT NULL,
  `kelas_lanjut` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catatan_walikelas`
--

INSERT INTO `catatan_walikelas` (`id_catatan`, `id_tSisKlsKey`, `id_periodeDet`, `jml_sakit`, `jml_izin`, `jml_tketerangan`, `isi_catatan`, `status_kenaikan`, `kelas_lanjut`) VALUES
(1, 3, '2015-2016-uts2', 1, 2, 0, 'Belajar lebih tekun lagi!.', 1, ''),
(2, 4, '2015-2016-uts2', 0, 0, 1, 'tes lagi', 0, ''),
(3, 5, '2015-2016-uts2', 0, 1, 0, 'Belajar lebih tekun lagi!.', 0, ''),
(4, 6, '2015-2016-uts2', 1, 0, 0, '', 0, ''),
(5, 7, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(6, 8, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(7, 9, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(8, 10, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(9, 11, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(10, 12, '2015-2016-uts2', 0, 0, 0, '', 0, ''),
(31, 3, '2015-2016-uas2', 1, 2, 3, '', 2, '8'),
(32, 4, '2015-2016-uas2', 0, 0, 0, '', 1, '8'),
(33, 5, '2015-2016-uas2', 1, 2, 7, 'Tingkatkan lagi prestasinya', 1, '8'),
(34, 6, '2015-2016-uas2', 0, 0, 0, '', 2, ''),
(35, 7, '2015-2016-uas2', 0, 0, 0, '', 0, ''),
(36, 8, '2015-2016-uas2', 0, 0, 0, '', 0, ''),
(37, 9, '2015-2016-uas2', 0, 0, 0, '', 0, ''),
(38, 10, '2015-2016-uas2', 0, 0, 0, '', 0, ''),
(39, 11, '2015-2016-uas2', 0, 0, 0, '', 0, ''),
(40, 12, '2015-2016-uas2', 0, 0, 0, '', 1, ''),
(41, 57, '2015-2016-uas2', 0, 0, 1, '', 1, '8'),
(43, 58, '2015-2016-uas2', 0, 0, 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `deskripsi_nketerampilan`
--

CREATE TABLE `deskripsi_nketerampilan` (
  `id_keterampilan` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deskripsi_nketerampilan`
--

INSERT INTO `deskripsi_nketerampilan` (`id_keterampilan`, `sikap`, `des_ket`) VALUES
(1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `deskripsi_npengetahuan`
--

CREATE TABLE `deskripsi_npengetahuan` (
  `id_pengetahuan` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_peng` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deskripsi_npengetahuan`
--

INSERT INTO `deskripsi_npengetahuan` (`id_pengetahuan`, `sikap`, `des_peng`) VALUES
(1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `deskripsi_nsikap`
--

CREATE TABLE `deskripsi_nsikap` (
  `id_sikap` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_sikap` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deskripsi_nsikap`
--

INSERT INTO `deskripsi_nsikap` (`id_sikap`, `sikap`, `des_sikap`) VALUES
(1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` varchar(20) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `nama_pendek` varchar(100) DEFAULT NULL,
  `foto_profil` varchar(100) DEFAULT NULL,
  `tipe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `nama_pendek`, `foto_profil`, `tipe`) VALUES
('001', 'admin', 'min', NULL, 0),
('123', 'Guru Tes', NULL, NULL, 3),
('196509141993032003', 'Made Sukses', 'made', NULL, 2),
('196509141993032005', 'Dewi Salamah, S.Pd', 'dewi', NULL, 2),
('196509141993032007', 'Dra. Maria Hari Suryawati, M.Pd', 'maria', NULL, 1),
('197112011995011001', 'Akhirul Riyad', 'riyad', NULL, 2),
('198411222009031004', 'Nuke Kurniawati Lestari, S.T.', 'nuke', NULL, 0),
('198609102009032011', 'I Nyoman Sudarsana, S.Sn', 'nyoman', NULL, 2),
('198609102009032012', 'A', '', NULL, 4),
('198609102009032013', 'B', '', NULL, NULL),
('198609102009032014', 'C', '', NULL, 4),
('198609102009032015', 'D', '', NULL, 3),
('198609102009032016', 'E', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_general`
--

CREATE TABLE `kelas_general` (
  `id_kelasGen` varchar(20) NOT NULL,
  `nama_kelasGen` varchar(100) NOT NULL,
  `kelas_angka` int(11) NOT NULL,
  `smt_ganjil` int(11) DEFAULT NULL,
  `smt_genap` int(11) DEFAULT NULL,
  `status_aksel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_general`
--

INSERT INTO `kelas_general` (`id_kelasGen`, `nama_kelasGen`, `kelas_angka`, `smt_ganjil`, `smt_genap`, `status_aksel`) VALUES
('X', 'Sepuluh', 10, 1, 2, 0),
('X-A', 'Sepuluh Akselerasi', 10, NULL, NULL, 1),
('XI', 'Sebelas', 11, 3, 4, 0),
('XI-A', 'Sebelas Akselerasi', 11, NULL, NULL, 1),
('XII', 'Dua Belas', 12, 5, 6, 0),
('XII-A', 'Dua Belas Akselerasi', 12, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kelompok_mapel`
--

CREATE TABLE `kelompok_mapel` (
  `id_kelompok` varchar(20) NOT NULL,
  `nama_kelompok` varchar(100) NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompok_mapel`
--

INSERT INTO `kelompok_mapel` (`id_kelompok`, `nama_kelompok`, `detail`) VALUES
('A', 'KELOMPOK A', ''),
('B', 'KELOMPOK B', ''),
('C_IPA', 'KELOMPOK C PEMINATAN', 'Kelompok Peminatan Matematika dan Ilmu Alam'),
('C_IPS', 'KELOMPOK C PEMINATAN', 'Kelompok Peminatan Ilmu-Ilmu Sosial (IIS)'),
('D', 'KELOMPOK LINTAS PEMINATAN', ''),
('E', 'MUATAN SEKOLAH', ''),
('Z_COBA', 'KELOMPOK COBA', 'kELOMPOK coBA tES');

-- --------------------------------------------------------

--
-- Table structure for table `konversi_nilai`
--

CREATE TABLE `konversi_nilai` (
  `id_predikat` varchar(10) NOT NULL,
  `batas_atas` float NOT NULL,
  `batas_bawah` float NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `detail_sikap` varchar(20) NOT NULL,
  `nilai_ekskul` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konversi_nilai`
--

INSERT INTO `konversi_nilai` (`id_predikat`, `batas_atas`, `batas_bawah`, `sikap`, `detail_sikap`, `nilai_ekskul`) VALUES
('A', 4, 3.67, 'SB', 'Sangat Baik ( SB )', 'Sangat Memuaskan'),
('A-', 3.66, 3.34, 'SB', 'Sangat Baik ( SB )', 'Sangat Memuaskan'),
('B', 3, 2.67, 'B', 'Baik ( B )', 'Memuaskan'),
('B+', 3.33, 3.01, 'B', 'Baik ( B )', 'Memuaskan'),
('B-', 2.66, 2.34, 'B', 'Baik ( B )', 'Memuaskan'),
('C', 2, 1.67, 'C', 'Cukup ( C )', 'Cukup Memuaskan'),
('C+', 2.33, 2.01, 'C', 'Cukup ( C )', 'Cukup Memuaskan'),
('C-', 1.66, 1.34, 'C', 'Cukup ( C )', 'Cukup Memuaskan'),
('D', 1, 0, 'K', 'Kurang ( K )', 'Kurang Memuaskan'),
('D+', 1.33, 1.01, 'K', 'Kurang ( K )', 'Kurang Memuaskan');

-- --------------------------------------------------------

--
-- Table structure for table `log_backupdb`
--

CREATE TABLE `log_backupdb` (
  `id_backupdb` int(11) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `keterangan` text,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_backupdb`
--

INSERT INTO `log_backupdb` (`id_backupdb`, `id_user`, `keterangan`, `waktu`) VALUES
(1, '198411222009031004', 'tes', '2016-02-05 03:05:44'),
(2, '198411222009031004', 'tes2', '2016-02-05 03:05:51'),
(3, '198411222009031004', 'siswa ok', '2016-02-05 03:06:03'),
(4, '198411222009031004', 'guru ok', '2016-02-05 03:06:08'),
(5, '198411222009031004', 'mapel ok', '2016-02-05 03:06:12'),
(6, '198411222009031004', 'kepsek ok', '2016-02-05 03:06:19'),
(7, '198411222009031004', 'v1', '2016-02-05 03:06:24'),
(8, '198411222009031004', 'v2', '2016-02-05 03:06:29'),
(9, '198411222009031004', 'tes', '2016-02-05 03:10:44'),
(10, '198411222009031004', 'tes2', '2016-02-05 03:12:01'),
(11, '198411222009031004', 'tes3', '2016-02-05 03:12:34'),
(12, '198411222009031004', 'tes4', '2016-02-05 03:12:48'),
(13, '198411222009031004', 'tes lagi', '2016-02-05 03:15:38'),
(14, '198411222009031004', 'backup', '2016-02-05 03:17:42'),
(15, '198411222009031004', 'mau', '2016-02-05 03:21:27');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` varchar(20) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL,
  `id_kelompok` varchar(20) NOT NULL,
  `id_kelasGen` varchar(20) NOT NULL,
  `kkm100` float NOT NULL,
  `kkm_aksel100` float NOT NULL,
  `kkm` float NOT NULL,
  `kkm_aksel` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`, `id_kelompok`, `id_kelasGen`, `kkm100`, `kkm_aksel100`, `kkm`, `kkm_aksel`) VALUES
('BAL_X', 'Bahasa Bali*', 'B', 'X', 80, 83, 3, 3.2),
('BAL_XI', 'Bahasa Bali*', 'B', 'XI', 80, 83, 3, 3.2),
('BAL_XII', 'Bahasa Bali*', 'B', 'XII', 80, 83, 3, 3.2),
('CHE_ENG_X', 'Chemistry', 'E', 'X', 0, 0, 20, 20.75),
('CHE_ENG_XI', 'Chemistry', 'E', 'XI', 0, 0, 20, 20.75),
('CHE_ENG_XII', 'Chemistry', 'E', 'XII', 0, 0, 20, 20.75),
('COB_X', 'Coba', 'Z_COBA', 'X', 80, 85, 3.2, 3.4),
('IND_X', 'Bahasa Indonesia', 'A', 'X', 80, 83, 3, 3.2),
('IND_XI', 'Bahasa Indonesia', 'A', 'XI', 80, 83, 3, 3.2),
('IND_XII', 'Bahasa Indonesia', 'A', 'XII', 80, 83, 3, 3.2),
('KEW_X', 'Pendidikan Kewarnegaraan', 'A', 'X', 80, 83, 3, 3.2),
('MAN_ENG_X', 'Bahasa Mandarin', 'E', 'X', 0, 0, 80, 83),
('MAN_ENG_XI', 'Bahasa Mandarin', 'E', 'XI', 0, 0, 80, 83),
('MAN_ENG_XII', 'Bahasa Mandarin', 'E', 'XII', 0, 0, 80, 83),
('MAT_ENG_X', 'Mathemathics', 'E', 'X', 80, 83, 3, 3.2),
('MAT_ENG_XI', 'Mathemathics', 'E', 'XI', 80, 83, 3, 3.2),
('MAT_ENG_XII', 'Mathemathics', 'E', 'XII', 80, 83, 3, 3.2),
('MAT_X', 'Matematika', 'A', 'X', 80, 83, 3, 3.2),
('MAT_XI', 'Matematika', 'A', 'XI', 80, 83, 3, 3.2),
('MAT_XII', 'Matematika', 'A', 'XII', 80, 83, 3, 3.2),
('TIK_X', 'Teknologi Informasi dan Komunikasi', 'D', 'X', 80, 83, 3, 3.2),
('TIK_XI', 'Teknologi Informasi dan Komunikasi', 'D', 'XI', 80, 83, 3, 3.2),
('TIK_XII', 'Teknologi Informasi dan Komunikasi', 'D', 'XII', 80, 83, 3, 3.2);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_mapel`
--

CREATE TABLE `nilai_mapel` (
  `id_nilaiMapel` int(11) NOT NULL,
  `id_tSisKlsKey` int(11) NOT NULL,
  `id_tAjarKey` int(11) NOT NULL,
  `id_periodeDet` varchar(20) NOT NULL,
  `pengetahuan` float NOT NULL,
  `keterampilan` float NOT NULL,
  `sikap` float NOT NULL,
  `nilai_pengetahuan` float NOT NULL,
  `nilai_keterampilan` float NOT NULL,
  `nilai_sikap` float NOT NULL,
  `predikat_pengetahuan` varchar(20) NOT NULL,
  `predikat_keterampilan` varchar(20) NOT NULL,
  `predikat_sikap` varchar(20) NOT NULL,
  `status_manualdes` int(11) DEFAULT NULL,
  `des_peng` text,
  `des_ket` text,
  `des_sik` text,
  `id_input` varchar(20) NOT NULL,
  `komentar` text,
  `tangal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_mapel`
--

INSERT INTO `nilai_mapel` (`id_nilaiMapel`, `id_tSisKlsKey`, `id_tAjarKey`, `id_periodeDet`, `pengetahuan`, `keterampilan`, `sikap`, `nilai_pengetahuan`, `nilai_keterampilan`, `nilai_sikap`, `predikat_pengetahuan`, `predikat_keterampilan`, `predikat_sikap`, `status_manualdes`, `des_peng`, `des_ket`, `des_sik`, `id_input`, `komentar`, `tangal`) VALUES
(499, 3, 13, '2015-2016-uas2', 80, 71, 85, 3.2, 2.84, 3.4, 'B+', 'B', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'terampil', '2016-01-16 04:21:46'),
(500, 4, 13, '2015-2016-uas2', 81, 72, 85, 3.24, 2.88, 3.4, 'B+', 'B', 'SB', 1, 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'pintar', '2016-02-15 04:57:40'),
(501, 5, 13, '2015-2016-uas2', 82, 73, 85, 3.28, 2.92, 3.4, 'B+', 'B', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.', '198411222009031004', 'mahir', '2016-02-15 04:57:40'),
(502, 6, 13, '2015-2016-uas2', 83, 74, 85, 3.32, 2.96, 3.4, 'B+', 'B', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'terampil', '2016-01-16 04:21:46'),
(503, 7, 13, '2015-2016-uas2', 84, 75, 85, 3.36, 3, 3.4, 'A-', 'B', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'pintar', '2016-02-15 04:57:40'),
(504, 8, 13, '2015-2016-uas2', 85, 76, 85, 3.4, 3.04, 3.4, 'A-', 'B+', 'SB', 1, 'Sangat Baik', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'mahir', '2016-02-15 04:57:40'),
(505, 9, 13, '2015-2016-uas2', 86, 77, 85, 3.44, 3.08, 3.4, 'A-', 'B+', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'terampil', '2016-02-15 04:57:40'),
(506, 10, 13, '2015-2016-uas2', 87, 78, 85, 3.48, 3.12, 3.4, 'A-', 'B+', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'pintar', '2016-01-16 04:21:46'),
(507, 11, 13, '2015-2016-uas2', 88, 79, 85, 3.52, 3.16, 3.4, 'A-', 'B+', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', NULL, '198411222009031004', 'mahir', '2016-02-15 04:57:40'),
(508, 12, 13, '2015-2016-uas2', 89, 80, 85, 3.56, 3.2, 3.4, 'A-', 'B+', 'SB', 1, 'Kemampuan siswa pada semua KI dan KD sangat baik dan tuntas', 'Keterampilan siswa semua KI dan KD sangat baik dan tuntas', 'Sikap sosial dan spiritual siswa baik', '198411222009031004', 'terampil', '2016-01-16 04:21:46'),
(509, 3, 14, '2015-2016-uas2', 71, 75, 81, 2.84, 3, 3.24, 'B', 'B', 'B', 0, '', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(510, 4, 14, '2015-2016-uas2', 72, 76, 82, 2.88, 3.04, 3.28, 'B', 'B+', 'B', 0, 'berilmu', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:38:18'),
(511, 5, 14, '2015-2016-uas2', 73, 77, 83, 2.92, 3.08, 3.32, 'B', 'B+', 'B', 0, 'berilmu', '', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(512, 6, 14, '2015-2016-uas2', 74, 78, 84, 2.96, 3.12, 3.36, 'B', 'B+', 'SB', 0, 'berilmu', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:38:18'),
(513, 7, 14, '2015-2016-uas2', 75, 79, 85, 3, 3.16, 3.4, 'B', 'B+', 'SB', 0, 'berilmu', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:38:18'),
(514, 8, 14, '2015-2016-uas2', 76, 80, 86, 3.04, 3.2, 3.44, 'B+', 'B+', 'SB', 0, 'berilmu', 'terampil', '', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(515, 9, 14, '2015-2016-uas2', 77, 81, 87, 3.08, 3.24, 3.48, 'B+', 'B+', 'SB', 0, 'berilmu', 'terampil', '', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(516, 10, 14, '2015-2016-uas2', 78, 82, 88, 3.12, 3.28, 3.52, 'B+', 'B+', 'SB', 0, 'berilmu', 'terampil', '', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(517, 11, 14, '2015-2016-uas2', 79, 83, 89, 3.16, 3.32, 3.56, 'B+', 'B+', 'SB', 0, 'berilmu', 'terampil', '', '198411222009031004', 'komentar bagus', '2016-02-06 06:42:56'),
(518, 12, 14, '2015-2016-uas2', 80, 84, 90, 3.2, 3.36, 3.6, 'B+', 'A-', 'SB', 0, 'berilmu', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:38:18'),
(519, 3, 16, '2015-2016-uas2', 80, 90, 95, 3.2, 3.6, 3.8, 'B+', 'A-', 'SB', 0, 'Penguasaan frasa masih perlu ditingkatkan', 'Terampil menulis puisi', 'Sangat tertib waktu pelajaran', '198411222009031004', '', '2016-01-26 00:39:29'),
(520, 4, 16, '2015-2016-uas2', 85, 85, 0, 3.4, 3.4, 0, 'A-', 'A-', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(521, 5, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(522, 6, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(523, 7, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(524, 8, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(525, 9, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(526, 10, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(527, 11, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(528, 12, 16, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-26 00:36:53'),
(530, 36, 25, '2015-2016-uas2', 85, 0, 0, 3.4, 0, 0, 'A-', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:19:23'),
(531, 37, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(532, 38, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(533, 39, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(534, 40, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(535, 41, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(536, 42, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(537, 43, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(538, 44, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(539, 45, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(540, 46, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(541, 47, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(542, 48, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:36'),
(543, 49, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:37'),
(544, 50, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:37'),
(545, 51, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:37'),
(546, 52, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:37'),
(547, 53, 25, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-16 05:18:37'),
(548, 3, 14, '2015-2016-uts2', 80, 90, 90, 3.2, 3.6, 3.6, 'B+', 'A-', 'SB', 0, NULL, NULL, NULL, '198411222009031004', 'Bagus', '2016-01-24 09:32:15'),
(549, 4, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(550, 5, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(551, 6, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(552, 7, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(553, 8, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(554, 9, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(555, 10, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(556, 11, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(557, 12, 14, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:32:15'),
(558, 3, 16, '2015-2016-uts2', 80, 80, 80, 3.2, 3.2, 3.2, 'B+', 'B+', 'B', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:16'),
(559, 4, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:16'),
(560, 5, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(561, 6, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(562, 7, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(563, 8, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(564, 9, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(565, 10, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(566, 11, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(567, 12, 16, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:17'),
(568, 3, 13, '2015-2016-uts2', 100, 100, 90, 4, 4, 3.6, 'A', 'A', 'SB', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(569, 4, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(570, 5, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(571, 6, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(572, 7, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(573, 8, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(574, 9, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(575, 10, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(576, 11, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(577, 12, 13, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-01-24 09:34:35'),
(578, 54, 26, '2015-2016-uas2', 90, 90, 80, 3.6, 3.6, 3.2, 'A-', 'A-', 'B', 0, 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.', NULL, '198411222009031004', '', '2016-02-15 04:46:14'),
(579, 55, 26, '2015-2016-uas2', 98, 0, 0, 3.92, 0, 0, 'A', 'D', 'K', 0, 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.', NULL, NULL, '198411222009031004', '', '2016-02-15 04:36:49'),
(580, 56, 26, '2015-2016-uas2', 80, 0, 0, 3.2, 0, 0, 'B+', 'D', 'K', 0, 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.', NULL, NULL, '198411222009031004', '', '2016-02-15 04:45:38'),
(581, 13, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.', '', '', '198411222009031004', '', '2016-01-29 04:50:02'),
(582, 14, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(583, 15, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(584, 16, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(585, 17, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(586, 18, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(587, 19, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:25'),
(588, 20, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(589, 21, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(590, 22, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(591, 23, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(592, 24, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(593, 26, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(594, 27, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(595, 28, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(596, 29, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(597, 30, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(598, 31, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(599, 32, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(600, 33, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(601, 34, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(602, 35, 17, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, '', '', '', '198411222009031004', '', '2016-01-29 02:18:26'),
(603, 3, 15, '2015-2016-uas2', 78, 79, 80, 3.12, 3.16, 3.2, 'B+', 'B+', 'B', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(604, 4, 15, '2015-2016-uas2', 79, 80, 81, 3.16, 3.2, 3.24, 'B+', 'B+', 'B', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(605, 5, 15, '2015-2016-uas2', 80, 81, 82, 3.2, 3.24, 3.28, 'B+', 'B+', 'B', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(606, 6, 15, '2015-2016-uas2', 81, 82, 83, 3.24, 3.28, 3.32, 'B+', 'B+', 'B', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(607, 7, 15, '2015-2016-uas2', 82, 83, 84, 3.28, 3.32, 3.36, 'B+', 'B+', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(608, 8, 15, '2015-2016-uas2', 83, 84, 85, 3.32, 3.36, 3.4, 'B+', 'A-', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(609, 9, 15, '2015-2016-uas2', 84, 85, 86, 3.36, 3.4, 3.44, 'A-', 'A-', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(610, 10, 15, '2015-2016-uas2', 85, 86, 87, 3.4, 3.44, 3.48, 'A-', 'A-', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(611, 11, 15, '2015-2016-uas2', 86, 87, 88, 3.44, 3.48, 3.52, 'A-', 'A-', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(612, 12, 15, '2015-2016-uas2', 87, 88, 89, 3.48, 3.52, 3.56, 'A-', 'A-', 'SB', 1, 'Pengetahuan Baik', 'Keterampilan Baik', 'Sikap Baik', '198609102009032011', 'Komentar guru baik', '2016-01-30 07:14:07'),
(613, 3, 27, '2015-2016-uas2', 60, 70, 80, 2.4, 2.8, 3.2, 'B-', 'B', 'B', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(614, 4, 27, '2015-2016-uas2', 61, 71, 81, 2.44, 2.84, 3.24, 'B-', 'B', 'B', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(615, 5, 27, '2015-2016-uas2', 62, 72, 82, 2.48, 2.88, 3.28, 'B-', 'B', 'B', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(616, 6, 27, '2015-2016-uas2', 63, 73, 83, 2.52, 2.92, 3.32, 'B-', 'B', 'B', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(617, 7, 27, '2015-2016-uas2', 64, 74, 84, 2.56, 2.96, 3.36, 'B-', 'B', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(618, 8, 27, '2015-2016-uas2', 65, 75, 85, 2.6, 3, 3.4, 'B-', 'B', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(619, 9, 27, '2015-2016-uas2', 66, 76, 86, 2.64, 3.04, 3.44, 'B-', 'B+', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(620, 10, 27, '2015-2016-uas2', 67, 77, 87, 2.68, 3.08, 3.48, 'B', 'B+', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(621, 11, 27, '2015-2016-uas2', 68, 78, 88, 2.72, 3.12, 3.52, 'B', 'B+', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(622, 12, 27, '2015-2016-uas2', 69, 79, 89, 2.76, 3.16, 3.56, 'B', 'B+', 'SB', 1, NULL, NULL, NULL, '198609102009032011', NULL, '2016-01-30 07:20:23'),
(623, 3, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(624, 4, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(625, 5, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(626, 6, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(627, 7, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(628, 8, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(629, 9, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(630, 10, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(631, 11, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(632, 12, 14, '2015-2016-uts1', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', 0, NULL, NULL, NULL, '198411222009031004', '', '2016-02-04 04:45:53'),
(633, 57, 14, '2015-2016-uas2', 81, 85, 91, 3.24, 3.4, 3.64, 'B+', 'A-', 'SB', NULL, 'berilmu', 'terampil', 'catatan baik', '198411222009031004', 'komentar bagus', '2016-02-06 06:38:18'),
(634, 57, 13, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', NULL, NULL, NULL, NULL, '198411222009031004', '', '2016-02-15 04:57:40'),
(635, 58, 13, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 'D', 'D', 'K', NULL, NULL, NULL, NULL, '198411222009031004', '', '2016-02-15 04:57:40');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_pede`
--

CREATE TABLE `nilai_pede` (
  `id_nilaiPede` int(11) NOT NULL,
  `id_tPedeSiswaKey` int(11) NOT NULL,
  `id_periodeDet` varchar(20) NOT NULL,
  `nilai_pede` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `id_input` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_pede`
--

INSERT INTO `nilai_pede` (`id_nilaiPede`, `id_tPedeSiswaKey`, `id_periodeDet`, `nilai_pede`, `keterangan`, `id_input`) VALUES
(5, 32, '2015-2016-uts2', 'A', 'Programmer', ''),
(6, 33, '2015-2016-uts2', 'B', 'Gamer', ''),
(7, 34, '2015-2016-uts2', '', '', ''),
(8, 35, '2015-2016-uts2', 'A', 'Pemimpin', ''),
(9, 36, '2015-2016-uts2', 'B', 'Sekretaris', ''),
(10, 37, '2015-2016-uts2', 'C', 'Sie Kerohanian', ''),
(11, 42, '2015-2016-uts2', 'A', 'Tendangan bagus, tingkatkan', ''),
(12, 43, '2015-2016-uts2', '', '', ''),
(13, 44, '2015-2016-uts2', '', '', ''),
(14, 45, '2015-2016-uts2', '', '', ''),
(15, 46, '2015-2016-uts2', '', '', ''),
(16, 38, '2015-2016-uas2', 'A', 'Sangat Baik', ''),
(17, 39, '2015-2016-uas2', 'B', 'Baik', ''),
(18, 38, '2015-2016-uts2', 'A', 'Pintar dalam memimpin pleton', ''),
(19, 39, '2015-2016-uts2', 'B', '', ''),
(20, 50, '2015-2016-uts2', 'C', '', ''),
(21, 32, '2015-2016-uas2', 'A', 'Sangat Memuaskan', ''),
(22, 33, '2015-2016-uas2', 'A', 'Sangat Memuaskan', ''),
(23, 34, '2015-2016-uas2', 'A', 'Sangat Memuaskan', ''),
(24, 35, '2015-2016-uas2', 'A', 'Sangat Memuaskan', ''),
(25, 36, '2015-2016-uas2', 'B', 'Sekretaris Kegiatan Ulang Tahun Sekolah', ''),
(26, 37, '2015-2016-uas2', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pede`
--

CREATE TABLE `pede` (
  `id_pede` int(11) NOT NULL,
  `nama_pede` varchar(100) NOT NULL,
  `tipe_pede` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='pengembangan diri';

--
-- Dumping data for table `pede`
--

INSERT INTO `pede` (`id_pede`, `nama_pede`, `tipe_pede`) VALUES
(1, 'Pramuka', 'Ekstrakurikuler'),
(2, 'Karate', 'Ekstrakurikuler'),
(3, 'Komputer', 'Ekstrakurikuler'),
(4, 'OSIS', 'Organisasi'),
(5, 'Sepak Bola', 'Ekstrakurikuler');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id_periode` varchar(20) NOT NULL,
  `tahun_awal` year(4) NOT NULL,
  `tahun_akhir` year(4) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id_periode`, `tahun_awal`, `tahun_akhir`, `status`) VALUES
('2012-2013', 2012, 2013, 0),
('2013-2014', 2013, 2014, 0),
('2014-2015', 2014, 2015, 0),
('2015-2016', 2015, 2016, 1),
('2016-2017', 2016, 2017, 0),
('2017-2018', 2017, 2018, 0),
('2019-2020', 2019, 2020, 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` varchar(20) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `tahun_masuk` year(4) NOT NULL,
  `tahun_keluar` year(4) DEFAULT NULL,
  `id_agama` int(11) NOT NULL,
  `status_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nama_siswa`, `tahun_masuk`, `tahun_keluar`, `id_agama`, `status_siswa`) VALUES
('206_del', 'Audi Padilangga', 2014, 2015, 2, 4),
('207_del', 'Caren Angellina Mimaki', 2014, 2015, 3, 4),
('208', 'Cindy Mak', 2014, 2014, 4, 2),
('209', 'Dewa Ayu Putu Dinda Junianti', 2014, NULL, 5, 1),
('210', 'Fandy Kurniawan Susanto', 2014, NULL, 4, 1),
('211', 'Florencia Aprilia Prasetyo', 2014, 2015, 1, 1),
('212', 'Graciela Vijjadevi Suwangsa', 2014, NULL, 1, 1),
('213', 'I Komang Suvarmana Wisuta', 2014, NULL, 2, 1),
('214', 'Leonardo Tobias', 2014, NULL, 3, 1),
('215', 'Mathias Siurjaya', 2014, NULL, 6, 1),
('216', 'Maria Elizabeth Sabanari', 2014, NULL, 2, 1),
('217', 'Metta Chindy Conerlia', 2014, NULL, 3, 1),
('219', 'Renaldy Pramana', 2014, NULL, 2, 1),
('220', 'Risi Parwati Dewi', 2014, NULL, 2, 1),
('221', 'Selly Juliana', 2014, NULL, 6, 1),
('222', 'Vincent Gunawan', 2014, NULL, 6, 1),
('223', 'Wesli Tong', 2014, NULL, 6, 1),
('237', 'Jason Iskandar', 2014, NULL, 5, 1),
('238', 'A.A. NGR ANANTA P', 2014, NULL, 2, 1),
('239', 'A.A. SG. NATASYA INTAN S', 2014, NULL, 2, 1),
('240', 'BRANDON B PERKASA ', 2014, NULL, 6, 1),
('241', 'CELLINA JOVINANDI', 2014, NULL, 6, 1),
('242', 'CINDY ARIESTANTI', 2014, NULL, 6, 1),
('243', 'ERICKO HARTANTO S', 2014, NULL, 5, 1),
('245', 'INDAH AYU GRENDE', 2014, NULL, 4, 1),
('246', 'JENIFER', 2014, NULL, 1, 1),
('247', 'STEVEN CHANDRA', 2014, NULL, 6, 1),
('248', 'WAYAN ALEXANDER', 2014, NULL, 2, 1),
('249', 'YENNY PURNOMOSARI', 2014, NULL, 3, 1),
('286', 'IMMANUELA YAHYA', 2014, NULL, 4, 1),
('287', 'Alda Anabela Adelina', 2014, NULL, 2, 1),
('288', 'Audrey Aurelia Hadisuwito', 2014, NULL, 2, 1),
('289', 'Ariel Siloam', 2014, NULL, 6, 1),
('290', 'Diana', 2014, NULL, 6, 1),
('291', 'Farra Almeira Suyanto', 2014, NULL, 6, 1),
('292', 'Frederic', 2014, NULL, 5, 1),
('293', 'Gabriella Santoso Gui', 2014, NULL, 4, 1),
('294', 'I Putu Agus Prayoga Mahaendra', 2014, NULL, 1, 1),
('295', 'I Gst Lanang Ganesha', 2014, NULL, 6, 1),
('296', 'I Made Indra Yana Wibawa', 2014, NULL, 2, 1),
('297', 'I Wayan Wedana Taro Guna', 2014, NULL, 3, 1),
('298', 'Jerrel Audric Theos', 2014, NULL, 4, 1),
('299', 'Kelvin Anderson', 2014, NULL, 5, 1),
('300', 'Mulyani Moeliono', 2014, NULL, 4, 1),
('301', 'Nabila Amara Zahwa', 2014, NULL, 1, 1),
('302', 'Nadine Nathania Amaris', 2014, NULL, 6, 1),
('303', 'Regina Gunawan', 2014, NULL, 2, 1),
('304', 'Reyvaldo Yung Chung Lay', 2014, NULL, 3, 1),
('305', 'Taryn Agatha Hartono', 2014, NULL, 4, 1),
('306', 'Tengku Vania Andila', 2014, NULL, 5, 1),
('307', 'Vanni Natasha', 2014, NULL, 4, 1),
('308', 'Velisia Putri Nathalie', 2014, NULL, 1, 1),
('310', 'Wellson Archun Harend', 2014, NULL, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `status_siswa`
--

CREATE TABLE `status_siswa` (
  `id_status` int(11) NOT NULL,
  `nama_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_siswa`
--

INSERT INTO `status_siswa` (`id_status`, `nama_status`) VALUES
(1, 'Aktif'),
(2, 'Non Aktif'),
(3, 'Lulus');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_user`
--

CREATE TABLE `tipe_user` (
  `id_tipeUser` int(11) NOT NULL,
  `hak_akses` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_user`
--

INSERT INTO `tipe_user` (`id_tipeUser`, `hak_akses`) VALUES
(0, 'Admin'),
(1, 'Kepala Sekolah'),
(2, 'Guru Wali'),
(3, 'Guru'),
(4, 'Guru Ekstrakurikuler / Organisasi');

-- --------------------------------------------------------

--
-- Table structure for table `total_nilaimapel`
--

CREATE TABLE `total_nilaimapel` (
  `id_transTotalNilai` int(11) NOT NULL,
  `id_tSisKlsKey` int(11) NOT NULL,
  `id_tKelasKey` int(11) NOT NULL,
  `id_periodeDet` varchar(20) NOT NULL,
  `status_muatan` int(11) NOT NULL,
  `jml_pengetahuan` float NOT NULL,
  `jml_keterampilan` float NOT NULL,
  `jml_sikap` float NOT NULL,
  `jml_nilai_pengetahuan` float NOT NULL,
  `jml_nilai_keterampilan` float NOT NULL,
  `rata2_pengetahuan` float NOT NULL,
  `rata2_keterampilan` float NOT NULL,
  `rata2_nilai_pengetahuan` float NOT NULL,
  `rata2_nilai_keterampilan` float NOT NULL,
  `sikap_antarMapel` varchar(10) NOT NULL,
  `ranking` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_nilaimapel`
--

INSERT INTO `total_nilaimapel` (`id_transTotalNilai`, `id_tSisKlsKey`, `id_tKelasKey`, `id_periodeDet`, `status_muatan`, `jml_pengetahuan`, `jml_keterampilan`, `jml_sikap`, `jml_nilai_pengetahuan`, `jml_nilai_keterampilan`, `rata2_pengetahuan`, `rata2_keterampilan`, `rata2_nilai_pengetahuan`, `rata2_nilai_keterampilan`, `sikap_antarMapel`, `ranking`) VALUES
(393, 3, 20, '2015-2016-uas2', 0, 298, 310, 340, 11.92, 12.4, 74.5, 77.5, 2.98, 3.1, 'SB', 2),
(394, 8, 20, '2015-2016-uas2', 0, 233, 235, 255, 9.32, 9.4, 58.25, 58.75, 2.33, 2.35, 'SB', 7),
(395, 5, 20, '2015-2016-uas2', 0, 224, 226, 249, 8.96, 9.04, 56, 56.5, 2.24, 2.26, 'SB', 10),
(396, 10, 20, '2015-2016-uas2', 0, 239, 241, 259, 9.56, 9.64, 59.75, 60.25, 2.39, 2.41, 'SB', 5),
(397, 7, 20, '2015-2016-uas2', 0, 230, 232, 253, 9.2, 9.28, 57.5, 58, 2.3, 2.32, 'SB', 8),
(398, 12, 20, '2015-2016-uas2', 0, 245, 247, 263, 9.8, 9.88, 61.25, 61.75, 2.45, 2.47, 'SB', 3),
(399, 4, 20, '2015-2016-uas2', 0, 306, 308, 247, 12.24, 12.32, 76.5, 77, 3.06, 3.08, 'SB', 1),
(400, 9, 20, '2015-2016-uas2', 0, 236, 238, 257, 9.44, 9.52, 59, 59.5, 2.36, 2.38, 'SB', 6),
(401, 6, 20, '2015-2016-uas2', 0, 227, 229, 251, 9.08, 9.16, 56.75, 57.25, 2.27, 2.29, 'SB', 9),
(402, 11, 20, '2015-2016-uas2', 0, 242, 244, 261, 9.68, 9.76, 60.5, 61, 2.42, 2.44, 'SB', 4),
(403, 3, 20, '2015-2016-uas2', 1, 71, 75, 81, 2.84, 3, 71, 75, 2.84, 3, 'B', 0),
(404, 5, 20, '2015-2016-uas2', 1, 73, 77, 83, 2.92, 3.08, 73, 77, 2.92, 3.08, 'B', 0),
(405, 10, 20, '2015-2016-uas2', 1, 78, 82, 88, 3.12, 3.28, 78, 82, 3.12, 3.28, 'SB', 0),
(406, 7, 20, '2015-2016-uas2', 1, 75, 79, 85, 3, 3.16, 75, 79, 3, 3.16, 'SB', 0),
(407, 12, 20, '2015-2016-uas2', 1, 80, 84, 90, 3.2, 3.36, 80, 84, 3.2, 3.36, 'SB', 0),
(408, 4, 20, '2015-2016-uas2', 1, 72, 76, 82, 2.88, 3.04, 72, 76, 2.88, 3.04, 'B', 0),
(409, 9, 20, '2015-2016-uas2', 1, 77, 81, 87, 3.08, 3.24, 77, 81, 3.08, 3.24, 'SB', 0),
(410, 6, 20, '2015-2016-uas2', 1, 74, 78, 84, 2.96, 3.12, 74, 78, 2.96, 3.12, 'SB', 0),
(411, 11, 20, '2015-2016-uas2', 1, 79, 83, 89, 3.16, 3.32, 79, 83, 3.16, 3.32, 'SB', 0),
(412, 8, 20, '2015-2016-uas2', 1, 76, 80, 86, 3.04, 3.2, 76, 80, 3.04, 3.2, 'SB', 0),
(413, 36, 22, '2015-2016-uas2', 0, 85, 0, 0, 3.4, 0, 85, 0, 3.4, 0, 'K', 1),
(414, 50, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 8),
(415, 37, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 9),
(416, 42, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 10),
(417, 47, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 11),
(418, 52, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 12),
(419, 39, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 13),
(420, 44, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 14),
(421, 49, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 15),
(422, 41, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 16),
(423, 46, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 17),
(424, 51, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 18),
(425, 38, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 2),
(426, 43, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 3),
(427, 48, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 4),
(428, 53, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 5),
(429, 40, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 6),
(430, 45, 22, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 7),
(431, 3, 20, '2015-2016-uts2', 1, 80, 90, 90, 3.2, 3.6, 80, 90, 3.2, 3.6, 'SB', 0),
(432, 8, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(433, 5, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(434, 10, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(435, 7, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(436, 12, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(437, 4, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(438, 9, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(439, 6, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(440, 11, 20, '2015-2016-uts2', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(441, 3, 20, '2015-2016-uts2', 0, 180, 180, 170, 7.2, 7.2, 90, 90, 3.6, 3.6, 'SB', 1),
(442, 7, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 5),
(443, 12, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 6),
(444, 4, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 7),
(445, 9, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 8),
(446, 6, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 9),
(447, 11, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 10),
(448, 8, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 2),
(449, 5, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 3),
(450, 10, 20, '2015-2016-uts2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 4),
(451, 55, 36, '2015-2016-uas2', 0, 98, 0, 0, 3.92, 0, 98, 0, 3.92, 0, 'K', 2),
(452, 54, 36, '2015-2016-uas2', 0, 90, 90, 80, 3.6, 3.6, 90, 90, 3.6, 3.6, 'B', 1),
(453, 56, 36, '2015-2016-uas2', 0, 80, 0, 0, 3.2, 0, 80, 0, 3.2, 0, 'K', 3),
(454, 23, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 1),
(455, 29, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 2),
(456, 34, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 3),
(457, 15, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 4),
(458, 20, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 5),
(459, 26, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 6),
(460, 31, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 7),
(461, 17, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 8),
(462, 22, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 9),
(463, 28, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 10),
(464, 33, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 11),
(465, 14, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 12),
(466, 19, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 13),
(467, 24, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 14),
(468, 30, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 15),
(469, 35, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 16),
(470, 16, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 17),
(471, 21, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 18),
(472, 27, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 19),
(473, 32, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 20),
(474, 13, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 21),
(475, 18, 21, '2015-2016-uas2', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 22),
(476, 5, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(477, 10, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(478, 7, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(479, 12, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(480, 4, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(481, 9, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(482, 6, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(483, 11, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(484, 3, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0),
(485, 8, 20, '2015-2016-uts1', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'K', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trans_ajar`
--

CREATE TABLE `trans_ajar` (
  `id_transAjar` int(11) NOT NULL,
  `id_guru` varchar(20) NOT NULL,
  `id_mapelKey` varchar(20) NOT NULL,
  `kkm_ajar` float NOT NULL,
  `kkm_ajar100` float NOT NULL,
  `id_tKelasKey` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_ajar`
--

INSERT INTO `trans_ajar` (`id_transAjar`, `id_guru`, `id_mapelKey`, `kkm_ajar`, `kkm_ajar100`, `id_tKelasKey`) VALUES
(13, '196509141993032003', 'MAT_XI', 3, 80, 20),
(14, '198411222009031004', 'MAT_ENG_XI', 3, 80, 20),
(15, '196509141993032005', 'TIK_XI', 3, 80, 20),
(16, '197112011995011001', 'IND_XI', 3, 80, 20),
(17, '001', 'MAT_X', 3, 80, 21),
(18, '198411222009031004', 'TIK_X', 3, 80, 33),
(19, '198609102009032012', 'MAT_XII', 3, 80, 22),
(20, '198609102009032011', 'IND_XII', 3, 80, 22),
(25, '198609102009032011', 'BAL_XII', 3, 80, 22),
(26, '196509141993032003', 'MAT_X', 3, 80, 36),
(27, '198609102009032013', 'BAL_XI', 3, 80, 20),
(28, '196509141993032007', 'MAT_X', 3, 80, 25),
(29, '198609102009032015', 'MAT_ENG_X', 3, 80, 36);

-- --------------------------------------------------------

--
-- Table structure for table `trans_kelas`
--

CREATE TABLE `trans_kelas` (
  `id_transKelas` int(11) NOT NULL,
  `id_tklsgeneralKey` int(11) NOT NULL,
  `id_guruWali` varchar(20) NOT NULL,
  `id_periode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_kelas`
--

INSERT INTO `trans_kelas` (`id_transKelas`, `id_tklsgeneralKey`, `id_guruWali`, `id_periode`) VALUES
(20, 5, '198609102009032011', '2015-2016'),
(22, 7, '196509141993032005', '2015-2016'),
(25, 10, '197112011995011001', '2015-2016'),
(30, 5, '198609102009032011', '2016-2017'),
(31, 1, '198411222009031004', '2016-2017'),
(32, 7, '196509141993032005', '2016-2017'),
(33, 10, '197112011995011001', '2016-2017'),
(36, 2, '196509141993032003', '2015-2016'),
(37, 5, '198609102009032011', '2017-2018'),
(38, 1, '198411222009031004', '2017-2018'),
(39, 7, '196509141993032005', '2017-2018'),
(40, 10, '197112011995011001', '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `trans_kepsek`
--

CREATE TABLE `trans_kepsek` (
  `id_transKepsek` int(11) NOT NULL,
  `id_kepsek` varchar(20) NOT NULL,
  `tahun_mulai` int(11) DEFAULT NULL,
  `tahun_berakhir` int(11) DEFAULT NULL,
  `status_kepsek` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_kepsek`
--

INSERT INTO `trans_kepsek` (`id_transKepsek`, `id_kepsek`, `tahun_mulai`, `tahun_berakhir`, `status_kepsek`) VALUES
(1, '196509141993032007', 2014, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `trans_klsgeneral`
--

CREATE TABLE `trans_klsgeneral` (
  `id_trans_klsgeneral` int(11) NOT NULL,
  `id_kelasGen` varchar(20) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_klsgeneral`
--

INSERT INTO `trans_klsgeneral` (`id_trans_klsgeneral`, `id_kelasGen`, `nama_kelas`) VALUES
(1, 'X', 'X IPA'),
(2, 'X', 'X IPS'),
(3, 'X', 'X Bahasa'),
(4, 'XI', 'XI IPA'),
(5, 'XI', 'XI IPS'),
(6, 'XI', 'XI Bahasa'),
(7, 'XII', 'XII IPA'),
(8, 'XII', 'XII IPS'),
(9, 'XII', 'XII Bahasa'),
(10, 'X-A', 'X IPA Akselerasi');

-- --------------------------------------------------------

--
-- Table structure for table `trans_nketerampilan`
--

CREATE TABLE `trans_nketerampilan` (
  `id_transKet` int(11) NOT NULL,
  `id_tAjarKey` int(11) NOT NULL,
  `id_nketerampilan` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_nketerampilan`
--

INSERT INTO `trans_nketerampilan` (`id_transKet`, `id_tAjarKey`, `id_nketerampilan`, `sikap`, `des_ket`) VALUES
(1, 13, 1, 'A', 'keterampilan'),
(2, 13, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(3, 13, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(4, 14, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(5, 14, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(6, 14, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(7, 15, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(8, 15, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(9, 15, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(10, 16, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(11, 16, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(12, 16, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(13, 17, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(14, 17, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(15, 17, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(16, 18, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(17, 18, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(18, 18, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(19, 19, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(20, 19, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(21, 19, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(22, 20, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(23, 20, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(24, 20, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(25, 21, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(26, 21, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(27, 21, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(37, 25, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(38, 25, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(39, 25, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(40, 26, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(41, 26, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(42, 26, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(43, 27, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(44, 27, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(45, 27, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(46, 28, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(47, 28, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(48, 28, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(49, 29, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(50, 29, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(51, 29, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(52, 30, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(53, 30, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(54, 30, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(55, 31, 1, 'A', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) sangat baik.'),
(56, 31, 2, 'B', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik.'),
(57, 31, 3, 'C', 'Kemampuan siswa pada KI4, KD (1,2 dst.....) baik akan tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `trans_npengetahuan`
--

CREATE TABLE `trans_npengetahuan` (
  `id_transPeng` int(11) NOT NULL,
  `id_tAjarKey` int(11) NOT NULL,
  `id_npengetahuan` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_peng` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_npengetahuan`
--

INSERT INTO `trans_npengetahuan` (`id_transPeng`, `id_tAjarKey`, `id_npengetahuan`, `sikap`, `des_peng`) VALUES
(1, 13, 1, 'A', 'Sangat Baik'),
(2, 13, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(3, 13, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(4, 14, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(5, 14, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(6, 14, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(7, 15, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(8, 15, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(9, 15, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(10, 16, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(11, 16, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(12, 16, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(13, 17, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(14, 17, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(15, 17, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(16, 18, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(17, 18, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(18, 18, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(19, 19, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(20, 19, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(21, 19, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(22, 20, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(23, 20, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(24, 20, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(25, 0, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(26, 21, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(27, 21, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(37, 25, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(38, 25, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(39, 25, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(40, 26, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(41, 26, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(42, 26, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(43, 27, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(44, 27, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(45, 27, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(46, 28, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(47, 28, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(48, 28, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(49, 29, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(50, 29, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(51, 29, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(52, 30, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(53, 30, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(54, 30, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.'),
(55, 31, 1, 'A', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik.'),
(56, 31, 2, 'B', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) baik.'),
(57, 31, 3, 'C', 'Kemampuan siswa pada KI3, KD (1,2 dst.....) sangat baik akan tetapi perlu ditingkatkan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `trans_nsikap`
--

CREATE TABLE `trans_nsikap` (
  `id_transSik` int(11) NOT NULL,
  `id_tAjarKey` int(11) NOT NULL,
  `id_nsikap` int(11) NOT NULL,
  `sikap` varchar(10) NOT NULL,
  `des_sik` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_nsikap`
--

INSERT INTO `trans_nsikap` (`id_transSik`, `id_tAjarKey`, `id_nsikap`, `sikap`, `des_sik`) VALUES
(1, 13, 1, 'SB', 'sikap'),
(2, 13, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(3, 13, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(4, 14, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(5, 14, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(6, 14, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(7, 15, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(8, 15, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(9, 15, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(10, 25, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(11, 25, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(12, 25, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(13, 26, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(14, 26, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(15, 26, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(16, 27, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(17, 27, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(18, 27, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(19, 28, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(20, 28, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(21, 28, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(22, 29, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(23, 29, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(24, 29, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(25, 30, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(26, 30, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(27, 30, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.'),
(28, 31, 1, 'SB', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa sangat baik.'),
(29, 31, 2, 'B', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik.'),
(30, 31, 3, 'C', 'Sikap spiritual (K1) dan sikap sosial (K2) siswa baik tetapi perlu ditingkatan lagi dengan pendampingan guru dan orang tua.');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pedeguru`
--

CREATE TABLE `trans_pedeguru` (
  `id_transPedeGuru` int(11) NOT NULL,
  `id_tPede` int(11) NOT NULL,
  `id_pembina` varchar(20) NOT NULL,
  `id_periode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pedeguru`
--

INSERT INTO `trans_pedeguru` (`id_transPedeGuru`, `id_tPede`, `id_pembina`, `id_periode`) VALUES
(1, 1, '198609102009032011', '2015-2016'),
(2, 2, '196509141993032003', '2015-2016'),
(3, 3, '198411222009031004', '2015-2016'),
(4, 4, '196509141993032007', '2015-2016'),
(5, 5, '198609102009032012', '2015-2016');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pedesiswa`
--

CREATE TABLE `trans_pedesiswa` (
  `id_transPedeSiswa` int(11) NOT NULL,
  `id_siswa` varchar(20) NOT NULL,
  `id_tPedeGuru` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_pedesiswa`
--

INSERT INTO `trans_pedesiswa` (`id_transPedeSiswa`, `id_siswa`, `id_tPedeGuru`) VALUES
(32, '240', 3),
(33, '241', 3),
(34, '242', 3),
(35, '240', 4),
(36, '242', 4),
(37, '243', 4),
(39, '308', 1),
(42, '240', 5),
(43, '241', 5),
(44, '242', 5),
(45, '243', 5),
(46, '245', 5),
(47, '307', 5),
(48, '308', 5),
(49, '310', 5),
(50, '310', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trans_periode`
--

CREATE TABLE `trans_periode` (
  `id_transPeriode` varchar(20) NOT NULL,
  `urutan` int(11) NOT NULL,
  `id_periode` varchar(20) NOT NULL,
  `nama_tes` varchar(20) NOT NULL,
  `semester` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `rapat_dewanGuru` date DEFAULT NULL,
  `tanggal_rapor` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_periode`
--

INSERT INTO `trans_periode` (`id_transPeriode`, `urutan`, `id_periode`, `nama_tes`, `semester`, `status`, `rapat_dewanGuru`, `tanggal_rapor`) VALUES
('2012-2013-uas1', 2, '2012-2013', 'UAS Semester Gasal', 1, 0, NULL, NULL),
('2012-2013-uas2', 4, '2012-2013', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2012-2013-uts1', 1, '2012-2013', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2012-2013-uts2', 3, '2012-2013', 'UTS Semester Genap', 1, 0, NULL, NULL),
('2013-2014-uas1', 2, '2013-2014', 'UAS Semester Gasal', 1, 0, NULL, NULL),
('2013-2014-uas2', 4, '2013-2014', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2013-2014-uts1', 1, '2013-2014', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2013-2014-uts2', 3, '2013-2014', 'UTS Semester Genap', 2, 0, NULL, NULL),
('2014-2015-uas1', 3, '2014-2015', 'UAS Semester Gasal', 2, 0, NULL, NULL),
('2014-2015-uas2', 4, '2014-2015', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2014-2015-uts1', 1, '2014-2015', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2014-2015-uts2', 2, '2014-2015', 'UTS Semester Genap', 1, 0, NULL, NULL),
('2015-2016-uas1', 2, '2015-2016', 'UAS Semester Gasal', 1, 0, '2015-12-08', '2015-12-10'),
('2015-2016-uas2', 4, '2015-2016', 'UAS Semester Genap', 2, 1, '2016-06-16', '2016-06-17'),
('2015-2016-uts1', 1, '2015-2016', 'UTS Semester Gasal', 1, 0, '2015-10-02', '2015-10-03'),
('2015-2016-uts2', 3, '2015-2016', 'UTS Semester Genap', 2, 0, '2016-03-24', '2016-03-26'),
('2016-2017-uas1', 2, '2016-2017', 'UAS Semester Gasal', 1, 0, NULL, NULL),
('2016-2017-uas2', 4, '2016-2017', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2016-2017-uts1', 1, '2016-2017', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2016-2017-uts2', 3, '2016-2017', 'UTS Semester Genap', 2, 0, NULL, NULL),
('2017-2018-uas1', 3, '2017-2018', 'UAS Semester Gasal', 2, 0, NULL, NULL),
('2017-2018-uas2', 4, '2017-2018', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2017-2018-uts1', 1, '2017-2018', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2017-2018-uts2', 2, '2017-2018', 'UTS Semester Genap', 1, 0, NULL, NULL),
('2019-2020-uas1', 3, '2019-2020', 'UAS Semester Gasal', 2, 0, NULL, NULL),
('2019-2020-uas2', 4, '2019-2020', 'UAS Semester Genap', 2, 0, NULL, NULL),
('2019-2020-uts1', 1, '2019-2020', 'UTS Semester Gasal', 1, 0, NULL, NULL),
('2019-2020-uts2', 2, '2019-2020', 'UTS Semester Genap', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trans_siswa`
--

CREATE TABLE `trans_siswa` (
  `id_transSisKls` int(11) NOT NULL,
  `id_siswa` varchar(20) NOT NULL,
  `id_tKelasKey` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_siswa`
--

INSERT INTO `trans_siswa` (`id_transSisKls`, `id_siswa`, `id_tKelasKey`) VALUES
(4, '241', 20),
(5, '242', 20),
(6, '243', 20),
(7, '245', 20),
(8, '246', 20),
(9, '247', 20),
(10, '248', 20),
(11, '249', 20),
(12, '286', 20),
(13, '287', 21),
(14, '288', 21),
(15, '289', 21),
(16, '290', 21),
(17, '291', 21),
(18, '292', 21),
(19, '293', 21),
(20, '294', 21),
(21, '295', 21),
(22, '296', 21),
(23, '297', 21),
(24, '298', 21),
(26, '300', 21),
(27, '301', 21),
(28, '302', 21),
(29, '303', 21),
(30, '304', 21),
(31, '305', 21),
(32, '306', 21),
(33, '307', 21),
(34, '308', 21),
(35, '310', 21),
(36, '206', 22),
(37, '207', 22),
(38, '208', 22),
(39, '209', 22),
(40, '210', 22),
(41, '211', 22),
(42, '212', 22),
(43, '213', 22),
(44, '214', 22),
(45, '215', 22),
(46, '216', 22),
(47, '217', 22),
(48, '219', 22),
(49, '220', 22),
(50, '221', 22),
(51, '222', 22),
(52, '223', 22),
(53, '237', 22),
(54, '307', 36),
(55, '308', 36),
(56, '310', 36),
(57, '292', 20),
(58, '240', 20);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(20) NOT NULL,
  `tipe_backup` int(11) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `tipe_backup`, `password`) VALUES
('001', 0, 'admin'),
('196509141993032003', 2, 'password'),
('196509141993032005', 2, 'password'),
('196509141993032007', 1, 'password'),
('197112011995011001', 3, 'password'),
('198411222009031004', 0, 'password'),
('198609102009032011', 2, 'password'),
('198609102009032012', 4, 'password'),
('198609102009032014', 4, 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `catatan_walikelas`
--
ALTER TABLE `catatan_walikelas`
  ADD PRIMARY KEY (`id_catatan`);

--
-- Indexes for table `deskripsi_nketerampilan`
--
ALTER TABLE `deskripsi_nketerampilan`
  ADD PRIMARY KEY (`id_keterampilan`);

--
-- Indexes for table `deskripsi_npengetahuan`
--
ALTER TABLE `deskripsi_npengetahuan`
  ADD PRIMARY KEY (`id_pengetahuan`);

--
-- Indexes for table `deskripsi_nsikap`
--
ALTER TABLE `deskripsi_nsikap`
  ADD PRIMARY KEY (`id_sikap`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `kelas_general`
--
ALTER TABLE `kelas_general`
  ADD PRIMARY KEY (`id_kelasGen`);

--
-- Indexes for table `kelompok_mapel`
--
ALTER TABLE `kelompok_mapel`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `konversi_nilai`
--
ALTER TABLE `konversi_nilai`
  ADD PRIMARY KEY (`id_predikat`);

--
-- Indexes for table `log_backupdb`
--
ALTER TABLE `log_backupdb`
  ADD PRIMARY KEY (`id_backupdb`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `nilai_mapel`
--
ALTER TABLE `nilai_mapel`
  ADD PRIMARY KEY (`id_nilaiMapel`);

--
-- Indexes for table `nilai_pede`
--
ALTER TABLE `nilai_pede`
  ADD PRIMARY KEY (`id_nilaiPede`);

--
-- Indexes for table `pede`
--
ALTER TABLE `pede`
  ADD PRIMARY KEY (`id_pede`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `status_siswa`
--
ALTER TABLE `status_siswa`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tipe_user`
--
ALTER TABLE `tipe_user`
  ADD PRIMARY KEY (`id_tipeUser`);

--
-- Indexes for table `total_nilaimapel`
--
ALTER TABLE `total_nilaimapel`
  ADD PRIMARY KEY (`id_transTotalNilai`);

--
-- Indexes for table `trans_ajar`
--
ALTER TABLE `trans_ajar`
  ADD PRIMARY KEY (`id_transAjar`);

--
-- Indexes for table `trans_kelas`
--
ALTER TABLE `trans_kelas`
  ADD PRIMARY KEY (`id_transKelas`);

--
-- Indexes for table `trans_kepsek`
--
ALTER TABLE `trans_kepsek`
  ADD PRIMARY KEY (`id_transKepsek`);

--
-- Indexes for table `trans_klsgeneral`
--
ALTER TABLE `trans_klsgeneral`
  ADD PRIMARY KEY (`id_trans_klsgeneral`);

--
-- Indexes for table `trans_nketerampilan`
--
ALTER TABLE `trans_nketerampilan`
  ADD PRIMARY KEY (`id_transKet`);

--
-- Indexes for table `trans_npengetahuan`
--
ALTER TABLE `trans_npengetahuan`
  ADD PRIMARY KEY (`id_transPeng`);

--
-- Indexes for table `trans_nsikap`
--
ALTER TABLE `trans_nsikap`
  ADD PRIMARY KEY (`id_transSik`);

--
-- Indexes for table `trans_pedeguru`
--
ALTER TABLE `trans_pedeguru`
  ADD PRIMARY KEY (`id_transPedeGuru`);

--
-- Indexes for table `trans_pedesiswa`
--
ALTER TABLE `trans_pedesiswa`
  ADD PRIMARY KEY (`id_transPedeSiswa`);

--
-- Indexes for table `trans_periode`
--
ALTER TABLE `trans_periode`
  ADD PRIMARY KEY (`id_transPeriode`);

--
-- Indexes for table `trans_siswa`
--
ALTER TABLE `trans_siswa`
  ADD PRIMARY KEY (`id_transSisKls`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agama`
--
ALTER TABLE `agama`
  MODIFY `id_agama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `catatan_walikelas`
--
ALTER TABLE `catatan_walikelas`
  MODIFY `id_catatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `deskripsi_nketerampilan`
--
ALTER TABLE `deskripsi_nketerampilan`
  MODIFY `id_keterampilan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `deskripsi_npengetahuan`
--
ALTER TABLE `deskripsi_npengetahuan`
  MODIFY `id_pengetahuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `deskripsi_nsikap`
--
ALTER TABLE `deskripsi_nsikap`
  MODIFY `id_sikap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log_backupdb`
--
ALTER TABLE `log_backupdb`
  MODIFY `id_backupdb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `nilai_mapel`
--
ALTER TABLE `nilai_mapel`
  MODIFY `id_nilaiMapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=636;
--
-- AUTO_INCREMENT for table `nilai_pede`
--
ALTER TABLE `nilai_pede`
  MODIFY `id_nilaiPede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `pede`
--
ALTER TABLE `pede`
  MODIFY `id_pede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `status_siswa`
--
ALTER TABLE `status_siswa`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipe_user`
--
ALTER TABLE `tipe_user`
  MODIFY `id_tipeUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `total_nilaimapel`
--
ALTER TABLE `total_nilaimapel`
  MODIFY `id_transTotalNilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;
--
-- AUTO_INCREMENT for table `trans_ajar`
--
ALTER TABLE `trans_ajar`
  MODIFY `id_transAjar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `trans_kelas`
--
ALTER TABLE `trans_kelas`
  MODIFY `id_transKelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `trans_kepsek`
--
ALTER TABLE `trans_kepsek`
  MODIFY `id_transKepsek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trans_klsgeneral`
--
ALTER TABLE `trans_klsgeneral`
  MODIFY `id_trans_klsgeneral` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `trans_nketerampilan`
--
ALTER TABLE `trans_nketerampilan`
  MODIFY `id_transKet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `trans_npengetahuan`
--
ALTER TABLE `trans_npengetahuan`
  MODIFY `id_transPeng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `trans_nsikap`
--
ALTER TABLE `trans_nsikap`
  MODIFY `id_transSik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `trans_pedeguru`
--
ALTER TABLE `trans_pedeguru`
  MODIFY `id_transPedeGuru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `trans_pedesiswa`
--
ALTER TABLE `trans_pedesiswa`
  MODIFY `id_transPedeSiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `trans_siswa`
--
ALTER TABLE `trans_siswa`
  MODIFY `id_transSisKls` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
