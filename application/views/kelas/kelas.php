<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kelas
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Kelas</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<?php if ($this->session->userdata('tipe') <= 1) { ?>
		<!-- Small boxes (Stat box) -->
	      <div class="row">
	        <div class="col-lg-3 col-xs-6">
	          <a data-toggle="modal" href="#tambahKelasGen">
	              <!-- small box -->
	              <div class="small-box bg-red">
	                <div class="inner">
	                  <h3><?php echo count($kelas_general) ?></h3>
	                  <p>Tambah Kategori Kelas</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-pricetags"></i>
	                </div>
	              </div>
	          </a>
	        </div><!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <a data-toggle="modal" href="#tambahTransKelas">
	              <!-- small box -->
	              <div class="small-box bg-yellow">
	                <div class="inner">
	                  <h3><?php echo count($trans_klsgeneral) ?></h3>
	                  <p>Tambah Katalog Kelas</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-quote"></i>
	                </div>
	              </div>
	          </a>
	        </div><!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <a data-toggle="modal" href="#enrolKelas">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3><?php echo count($kelas) ?></h3>
	                  <p>Enrol Kelas</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-pin"></i>
	                </div>
	              </div>
	          </a>
	        </div><!-- ./col -->
	      </div><!-- /.row -->
	<?php } ?>
          
  <!-- Custom Tabs (Pulled to the right) -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
      <li class="active"><a href="#tab_1-1" data-toggle="tab">Enrol</a></li>
      <li><a href="#tab_2-2" data-toggle="tab">Katalog</a></li>
      <li><a href="#tab_3-2" data-toggle="tab">Kategori</a></li>
      <li class="pull-left header"><i class="fa fa-th"></i> </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1-1">
      	<table id="tabel_enrol" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center" class="col-sm-1">No</th>
				<th style="text-align:center">Kelas</th>
				<th style="text-align:center">Wali Kelas</th>
				<th style="text-align:center" class="col-sm-2">Opsi</th>
				<th style="text-align:center">Leger</th>
				<th style='display:none'></th>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($kelas) {
					$baris=1;
					foreach ($kelas as $row) {
						if ($row['id_transKelas']!=''){
							echo "<tr>";
							echo "<td style='text-align:center'>".($baris)."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo "<td>".$row['nama_guru']."</td>";
							echo '<td style="text-align:center">&nbsp <a href="'.base_url('kelas/detail/'.$row['id_transKelas']).'" class="btn btn-sm btn-info" data-toggle="tooltip" title="Daftar Siswa"><i class="fa fa-group"></i></a>';
							echo '&nbsp <a href="#editEnrol" class="edit-enrol btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
							echo '&nbsp <a href="'.base_url('kelas/delete/enrol/'.$row['id_transKelas']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							echo '<td style="text-align:center"><a href="'.base_url('output/leger/'.$row['id_transKelas']).'" class="btn btn-sm btn-success"> Lihat </a></td>';
							echo "<td class='nr' style='display:none'>".$row['id_transKelas']."</td>";
							echo "</tr>";
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2-2">
        <table id="tabel_katalog" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center" class="col-sm-1">No</th>
                <th style="text-align:center" class="col-sm-2">Kategori</th>
				<th style="text-align:center">Nama Kelas</th>
				<th style="text-align:center" class="col-sm-12">Opsi</th>
				<th style="display:none"></th>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($trans_klsgeneral) {
					$baris=1;
					foreach ($trans_klsgeneral as $row) {
						if ($row['id_trans_klsgeneral']!=''){
							echo "<tr>";
							echo "<td style='text-align:center'>".($baris)."</td>";
							echo "<td style='text-align:center'>".$row['id_kelasGen']."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo '<td style="text-align:center">&nbsp <a href="#editKatalog" class="edit-katalog btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
							echo '&nbsp <a href="'.base_url('kelas/delete/katalog/'.$row['id_trans_klsgeneral']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							echo "<td class='nr2' style='display:none'>".$row['id_trans_klsgeneral']."</td>";
							echo "</tr>";
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3-2">
        <table id="tabel_kategori" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center" class="col-sm-1">No</th>
                <th style="text-align:center">Kode</th>
				<th style="text-align:center">Nama Kategori</th>
				<th style="text-align:center" class="col-sm-12">Opsi</th>
				<th style="display:none"></th>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($kelas_general) {
					$baris=1;
					foreach ($kelas_general as $row) {
						if ($row['id_kelasGen']!=''){
							echo "<tr>";
							echo "<td style='text-align:center'>".($baris)."</td>";
							echo "<td style='text-align:center'>".$row['id_kelasGen']."</td>";
							echo "<td>".$row['nama_kelasGen']."</td>";
							echo '<td style="text-align:center">&nbsp <a href="#editKategori" class="edit-kategori btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
							echo '&nbsp <a href="'.base_url('kelas/delete/kategori/'.$row['id_kelasGen']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							echo "<td class='nr3' style='display:none'>".$row['id_kelasGen']."</td>";
							echo "</tr>";
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->

</section><!-- /.content -->

<!-- Modal1 -->
<div class="modal fade" id="tambahKelasGen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Kategori Kelas</h4>
      </div>
      <div class="modal-body">
	<?php 
		echo form_open('kelas/tambah_kategori');?>

		<div class="form-group">
			<?php 
			echo form_label('Kode Kelas', 'id_kelasGen1', array(
			    'class' => 'control-label',
			));
			?>
			<input style="width:20%" type="text" required name="id_kelasGen1" id="id_kelasGen1" class="form-control"> </input>

			<?php 
			echo form_label('dalam angka', 'kelas_angka', array(
			    'class' => 'control-label',
			));
			?>
			<input style="width:20%" type="number" min="0" required name="kelas_angka" id="kelas_angka" class="form-control"> </input>

			<input type="checkbox" class="minimal" name="status_aksel" value="1">
              <b>Kelas Akselerasi</b>
            </input>
            
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama Kategori Kelas', 'nama_kelasGen', array(
			    'class' => 'control-label',
			));
			?>
			<input type="text" required name="nama_kelasGen" id="nama_kelasGen" class="form-control"> </input>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Semester Ganjil', 'smt_ganjil', array(
			    'class' => 'control-label',
			));
			?>
			<input style="width:20%" type="number" min="0" required name="smt_ganjil" id="smt_ganjil" class="form-control"> </input>
			<?php 
			echo form_label('Semester Genap', 'smt_genap', array(
			    'class' => 'control-label',
			));
			?>
			<input style="width:20%" type="number" min="0" required name="smt_genap" id="smt_genap" class="form-control"> </input>
		</div>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-danger'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal1 -->

<!-- Modal2 -->
<div class="modal fade" id="tambahTransKelas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Katalog Kelas</h4>
      </div>
      <div class="modal-body">
	<?php 
		echo form_open('kelas/tambah_katalog');?>

		<div class="form-group">
			<?php 
			echo form_label('Kategori Kelas', 'id_kelasGen2', array(
			    'class' => 'control-label',
			));
			?>

			<select required name="id_kelasGen2" id="id_kelasGen2" class="form-control">
				<option value='' disabled selected>- Pilih -</option>
				<?php 
					if ($kelas_general){
						foreach ($kelas_general as $row) {
							echo "<option value=".$row['id_kelasGen'];
							echo ">".$row['id_kelasGen'].' | '.$row['nama_kelasGen']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama Kelas', 'nama_kelas', array(
			    'class' => 'control-label',
			));
			?>
			<input type="text" required name="nama_kelas" id="nama_kelas" class="form-control"> </input>
		</div>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-warning'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal2 -->

<!-- Modal3 -->
<div class="modal fade" id="enrolKelas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enrol Kelas</h4>
      </div>
      <div class="modal-body">
	<?php 
		echo form_open('kelas/enrol_kelas');?>

		<div class="form-group">
			<?php 
			echo form_label('Kelompok Kelas', 'id_kelasGen', array(
			    'class' => 'control-label',
			));
			?>

			<select name="id_kelasGen" id="id_kelasGen" class="form-control" onChange=get_tkelasGen()>
				<option value='' disabled selected>- Pilih -</option>
				<?php 
					if ($kelas_general){
						foreach ($kelas_general as $row) {
							echo "<option value=".$row['id_kelasGen'];
							echo ">".$row['id_kelasGen'].' | '.$row['nama_kelasGen']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama Kelas', 'tkelasGen', array(
			    'class' => 'control-label',
			));
			?>

			<select name="tkelasGen" id="tkelasGen" class="form-control">
				<option value='' disabled selected>- Kosong -</option>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Wali Kelas', 'id_guruWali', array(
			    'class' => 'control-label',
			));
			?>

			<select name="id_guruWali" id="id_guruWali" class="form-control">
				<option value=''disabled selected>- Pilih -</option>
				<?php 
					if ($guru){
						foreach ($guru as $row) {
							echo "<option value='".$row['id_guru']."'";
							echo ">".$row['nama_guru']."</option>";
						}
					}	
				?>
			</select>
		</div>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal3 -->

<!-- Modal4 -->
<div class="modal fade" id="editEnrol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Kelas Enrol</h4>
      </div>
      <?php echo form_open('kelas/do_edit_enrol');?>
      <div class="modal-body" id="edit_enrol">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal4 -->

<!-- Modal5 -->
<div class="modal fade" id="editKatalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Katalog</h4>
      </div>
      <?php echo form_open('kelas/do_edit_katalog');?>
      <div class="modal-body" id="edit_katalog">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal5 -->

<!-- Modal6 -->
<div class="modal fade" id="editKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
      </div>
      <?php echo form_open('kelas/do_edit_kategori');?>
      <div class="modal-body" id="edit_kategori">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal6 -->


<!-- page script -->
<script type="text/javascript">
	function get_tkelasGen(){
		var id_kelasGen =$('#id_kelasGen').val();
		//alert(id_kelasGen);
		var url = '<?php echo base_url('kelas/get_tkelasGen/');?>'

		$.ajax
			({
			type: "POST",
			url: url,
			data: {"id_kelasGen":id_kelasGen},
			cache: false,
			success: function(html)
			{
				$("#tkelasGen").html(html);
			}
		});
	}

	$(".edit-enrol").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_transKelas = $row.find(".nr").text(); // Find the text
      var $url = '<?php echo base_url('kelas/edit_enrol');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_transKelas":$id_transKelas},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_enrol").html(html);
	      }
	  });

      $('#editEnrol').modal('show');
    });

    $(".edit-katalog").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_trans_klsgeneral = $row.find(".nr2").text(); // Find the text
      var $url = '<?php echo base_url('kelas/edit_katalog');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_trans_klsgeneral":$id_trans_klsgeneral},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_katalog").html(html);
	      }
	  });

      $('#editKatalog').modal('show');
    });

    $(".edit-kategori").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_kelasGen = $row.find(".nr3").text(); // Find the text
      var $url = '<?php echo base_url('kelas/edit_kategori');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_kelasGen":$id_kelasGen},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_kategori").html(html);
	      }
	  });

      $('#editKategori').modal('show');
    });
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 4
        } ]
    }*/);
  });
  $(function () {
    $("#tabel_katalog").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 5
        } ]
    }*/);
  });
  $(function () {
    $("#tabel_kategori").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    }*/);
  });
</script>