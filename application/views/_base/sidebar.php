      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url('assets/images/user7-128x128.jpg') ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('nama_pendek'); ?></p>
              <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <!-- open tipe Administrator -->
            <?php if($this->session->userdata('tipe') <= 0) {?> 
              <li><a href="<?php echo base_url('siswa');?>"><i class="fa fa-users"></i> <span>Siswa</span></a></li>
              <li><a href="<?php echo base_url('guru');?>"><i class="fa fa-black-tie"></i> <span>Guru</span></a></li>
              <li><a href="<?php echo base_url('kelas');?>"><i class="fa fa-building"></i> <span>Kelas</span></a></li>
              <li><a href="<?php echo base_url('mapel');?>"><i class="fa fa-book"></i> <span>Mata Pelajaran</span></a></li>
              <li><a href="<?php echo base_url('pede');?>"><i class="fa fa-bicycle"></i> <span>Pengembangan Diri</span></a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-pencil"></i> <span>Penilaian</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('nilai_mapel');?>"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
                  <li><a href="<?php echo base_url('nilai_pede');?>"><i class="fa fa-circle-o"></i> Pengembangan Diri</a></li>
                  <li><a href="<?php echo base_url('absensi_catatan');?>"><i class="fa fa-circle-o"></i> Absensi dan Catatan</a></li>
                </ul>
              </li>
              <li><a href="<?php echo base_url('periode');?>"><i class="fa fa-calendar"></i> <span>Periode</span></a></li>
              <!-- <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-expeditedssl"></i> <span>Administrator</span></a></li> -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-expeditedssl"></i> <span>Administrator</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('admin/kepsek');?>"><i class="fa fa-circle-o"></i> Kepala Sekolah</a></li>
                  <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-circle-o"></i> User</a></li>
                  <li><a href="<?php echo base_url('admin/aturan_konversi');?>"><i class="fa fa-circle-o"></i> Atutan Konversi</a></li>
                  <li><a href="<?php echo base_url('admin/ki_kd');?>"><i class="fa fa-circle-o"></i> KI/KD Umum</a></li>
                  <li><a href="<?php echo base_url('admin/backup_db');?>"><i class="fa fa-circle-o"></i> Database</a></li>
                </ul>
              </li>
            <?php }?> <!-- close tipe Administrator -->

            <!-- open tipe Kepala Sekolah / Yayasan -->
            <?php if($this->session->userdata('tipe') == 1) {?> 
              <li><a href="<?php echo base_url('siswa');?>"><i class="fa fa-users"></i> <span>Siswa</span></a></li>
              <li><a href="<?php echo base_url('guru');?>"><i class="fa fa-black-tie"></i> <span>Guru</span></a></li>
              <li><a href="<?php echo base_url('kelas');?>"><i class="fa fa-building"></i> <span>Kelas</span></a></li>
              <li><a href="<?php echo base_url('mapel');?>"><i class="fa fa-book"></i> <span>Mata Pelajaran</span></a></li>
              <li><a href="<?php echo base_url('pede');?>"><i class="fa fa-bicycle"></i> <span>Pengembangan Diri</span></a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-pencil"></i> <span>Penilaian</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('nilai_mapel');?>"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
                  <li><a href="<?php echo base_url('nilai_pede');?>"><i class="fa fa-circle-o"></i> Pengembangan Diri</a></li>
                  <li><a href="<?php echo base_url('absensi_catatan');?>"><i class="fa fa-circle-o"></i> Absensi dan Catatan</a></li>
                </ul>
              </li>
              <li><a href="<?php echo base_url('periode');?>"><i class="fa fa-calendar"></i> <span>Periode</span></a></li>
              <li><a href="<?php echo base_url('admin');?>"><i class="fa fa-expeditedssl"></i> <span>Administrator</span></a></li>
            <?php }?> <!-- close tipe Kepala Sekolah / Yayasan -->

            <!-- open tipe Guru Wali -->
            <?php if($this->session->userdata('tipe') == 2) {?> 
              <li><a href="<?php echo base_url('siswa');?>"><i class="fa fa-users"></i> <span>Siswa</span></a></li>
              <li><a href="<?php echo base_url('kelas');?>"><i class="fa fa-building"></i> <span>Kelas</span></a></li>
              <li><a href="<?php echo base_url('mapel');?>"><i class="fa fa-book"></i> <span>Mata Pelajaran</span></a></li>
              <li><a href="<?php echo base_url('pede');?>"><i class="fa fa-bicycle"></i> <span>Pengembangan Diri</span></a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-pencil"></i> <span>Penilaian</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('nilai_mapel');?>"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
                  <li><a href="<?php echo base_url('nilai_pede');?>"><i class="fa fa-circle-o"></i> Pengembangan Diri</a></li>
                  <li><a href="<?php echo base_url('absensi_catatan');?>"><i class="fa fa-circle-o"></i> Absensi dan Catatan</a></li>
                </ul>
              </li>
            <?php }?> <!-- close tipe Guru Wali -->
            
            <!-- open tipe Guru Biasa -->
            <?php if($this->session->userdata('tipe') == 3) {?> 
              <li class="treeview active">
                <a href="#">
                  <i class="fa fa-pencil"></i> <span>Penilaian</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('nilai_mapel');?>"><i class="fa fa-circle-o"></i> Mata Pelajaran</a></li>
                  <li><a href="<?php echo base_url('nilai_pede');?>"><i class="fa fa-circle-o"></i> Pengembangan Diri</a></li>
                </ul>
              </li>
            <?php }?> <!-- close tipe Guru Biasa -->

            <!-- open tipe Guru Ekstrakurikuler -->
            <?php if($this->session->userdata('tipe') == 4) {?> 
              <li class="treeview active">
                <a href="#">
                  <i class="fa fa-pencil"></i> <span>Penilaian</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('nilai_pede');?>"><i class="fa fa-circle-o"></i> Pengembangan Diri</a></li>
                </ul>
              </li>
            <?php }?> <!-- close tipe Guru Ekstrakurikuler -->
            <li><a data-toggle="modal" data-target="#aturanKonversi"><i class="fa fa-info-circle"></i> <span>Aturan Konversi</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">