<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Absensi dan Catatan Wali
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('absensi_catatan'); ?>"> Absensi dan Catatan Wali</a></li>
    <li class="active">Kelas XXX</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-warning">
        <div class="box-header with-border">
        	<h3 class="box-title">Daftar Kelas Enrol</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
          <?php echo form_open(base_url().'absensi_catatan/insert/');?>
          <table id="tabel_catatan" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th style="text-align:center; vertical-align:middle" rowspan='2'>No</th>
                  <th style="text-align:center; vertical-align:middle" rowspan='2'>NIS</th>               
                  <th style="text-align:center; vertical-align:middle" rowspan='2'>Nama Siswa</th>
                  <th style="text-align:center" colspan='3'>Ketidakhadiran</th>
                  <th style="text-align:center; vertical-align:middle;" rowspan='2'>Catatan Wali Kelas</th>
                  <?php if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false)
                    echo '<th style="text-align:center" colspan="2">Kenaikan</th>'; ?>

                  <!-- ID Kelas, ID Catatan, dan Keterangan Insert/Update -->
                  <th style="display:none" rowspan='2'></th>
                  <th style='display:none' rowspan='2'></th>
                  <th style='display:none' rowspan='2'></th>
                </tr>
                <tr>
                  <th style="text-align:center">Sakit</th>
                  <th style="text-align:center">Izin</th>
                  <th style="text-align:center">T. Ket.</th>
                  <?php if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false) 
                  echo '<th style="text-align:center">Status</th>
                        <th style="text-align:center">Ke Kelas</th>'; ?>
                </tr>
              </thead>
              <tbody>
                <?php
                  if ($siswa_kelas) {
                    $baris=1;
                    foreach ($siswa_kelas as $row) {
                      if ($row['id_siswa']!=''){
                        echo "<tr>";
                        echo "<td style='text-align:center'>".($baris)."</td>";
                        echo "<td>".$row['id_siswa']."</td>";
                        
                        echo "<td>".$row['nama_siswa']."</td>";

                        if ($row['isUpdate'])
                        {
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_sakit[]' value='".$row['jml_sakit']."'></td>";
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_izin[]' value='".$row['jml_izin']."'></td>";
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_tketerangan[]' value='".$row['jml_tketerangan']."'></td>";
                          echo "<td><textarea style='width:100%' rows='2' type='text' class='form-control' name='isi_catatan[]'>".$row['isi_catatan']."</textarea>";
                          echo "<a onClick=get_catatanGuru(".$row['id_transSisKls'].") data-toggle='modal' data-target='#get_komentar' style='width:100%' class='btn btn-sm btn-info'><i class='fa fa-file-text'></i>&nbsp Catatan Guru Mata Pelajaran</button></a></td>"; 

                          if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false)
                          {
                            echo "<td>
                                <select name='status_kenaikan[]' class='form-control'>";
                                 if ($row['status_kenaikan']==0)
                                 {
                                    echo "<option value='0' readonly>- Pilih -</option>;
                                    <option value='1'>Naik Kelas</option>
                                    <option value='2'>Tidak Naik Kelas</option>";
                                 }
                                 if ($row['status_kenaikan']=='1')
                                 {
                                    echo "<option value='0' readonly>- Pilih -</option>;
                                    <option value='1' selected>Naik Kelas</option>
                                    <option value='2'>Tidak Naik Kelas</option>";
                                 }
                                 else if ($row['status_kenaikan']==2)
                                 {
                                    echo "<option value='0' readonly>- Pilih -</option>;
                                    <option value='1'>Naik Kelas</option>
                                    <option value='2' selected>Tidak Naik Kelas</option>";
                                 }
                            echo "</select></td>";

                            echo "<td>
                              <select name='kelas_lanjut[]' id='kelas_lanjut' class='form-control'>
                                <option value='' readonly>- Pilih -</option>";
                                  if ($next_kelas){
                                    foreach ($next_kelas as $row2) {
                                      if ($row['kelas_lanjut']) {
                                          echo "<option selected value='".$row2['id_trans_klsgeneral']."'>";
                                          echo $row2['nama_kelas']."</option>";
                                      }
                                      else {
                                          echo "<option value='".$row2['id_trans_klsgeneral']."'>";
                                          echo $row2['nama_kelas']."</option>";
                                      }
                                      
                                    }
                                  }
                            echo "</select> </td>";
                          }
                          echo "<td style='display:none'><input type='text' class='form-control' name='id_catatan[]' value='".$row['id_catatan']."'></td>";
                        }
                        else
                        {
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_sakit[]'></td>";
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_izin[]'></td>";
                          echo "<td><input type='number' style='width:70px' min='0' class='form-control' name='jml_tketerangan[]'></td>";
                          echo "<td><textarea style='width:100%' rows='2' type='text' class='form-control' name='isi_catatan[]'></textarea>";
                          echo "<a onClick=get_catatanGuru(".$row['id_transSisKls'].") data-toggle='modal' data-target='#get_komentar' style='width:100%' class='btn btn-sm btn-info'><i class='fa fa-file-text'></i>&nbsp Catatan Guru Mata Pelajaran</button></a></td>"; 
                          
                          echo "<td>
                             <select name='status_kenaikan[]' id='status_kenaikan' class='form-control'>
                             <option value='' readonly selected>- Pilih -</option>
                             <option value='1'>Naik Kelas</option>
                             <option value='2'>Tidak Naik Kelas</option>
                          </select></td>";

                          echo "<td>
                            <select name='kelas_lanjut[]' id='kelas_lanjut' class='form-control'>
                                <option value='' readonly selected>- Pilih -</option>";
                                  if ($next_kelas){
                                    foreach ($next_kelas as $row2) {
                                      echo "<option value='".$row2['id_trans_klsgeneral']."'>";
                                      echo $row2['nama_kelas']."</option>";
                                    }
                                  }
                          echo "</select> </td>";
                          echo "<td style='display:none'><input type='text' class='form-control' name='id_catatan[]' value='".null."'></td>";
                        }
                        echo "<td style='display:none'><input type='text' class='form-control' name='isUpdate[]' value='".$row['isUpdate']."'></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_transSisKls[]' value='".$row['id_transSisKls']."'></td>";
                        echo "</tr>";
                        $baris++;
                      }
                    }
                  }
                ?>
              </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-warning'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->


<!-- Modal2 -->
<div class="modal fade" id="get_komentar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Catatan</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive" style="width:auto; height:auto">
            <table name="catatan_guru" id="catatan_guru" class="table table-striped table-bordered">
                <tr>
                    <th>No</th>
                    <th>Catatan</th>
                </tr>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>     
      </div>
    </div>
  </div>
</div> <!-- end -->

<script type="text/javascript">

        function get_catatanGuru(id_transSisKls){
            //var id_transSisKls =$('#id_transSisKls').val();
            var str1 = '<?php echo base_url('absensi_catatan/catatanGuru/');?>'
            var url = str1.concat('/',id_transSisKls);
            //window.alert (url);

            $.ajax
                ({
                type: "POST",
                url: url,
                data: {"id_transSisKls":id_transSisKls},
                cache: false,
                success: function(html)
                {
                    $("#catatan_guru").html(html);
                }
            });
        }
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_catatan").DataTable({
      scrollY: "300px",
      scrollX: true,
      "bPaginate": false,
      "scrollCollapse": true,
      "bAutoWidth" : false,
      //"fixedColumns": {leftColumns: 1}
    });
  });

  $('body').addClass('sidebar-collapse');
</script>