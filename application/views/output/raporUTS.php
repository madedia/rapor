<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Rapor Siswa
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('siswa'); ?>"> Siswa</a></li>
    <li class="active">Rapor</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">       
  	<!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
        	<div class="row">
			  <div class="col-sm-4 col-md-2">
			  	<div class="dropdown">
		          <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Tahun Ajaran
		          <span class="caret"></span></button>
		          <ul class="dropdown-menu">
		            <?php 
		              if ($thn_ajaran){
		                foreach ($thn_ajaran as $row) {
		                  echo "<li><a href=".base_url('output/rapor/'.$id_siswa.'/'.$row['id_periode'].'/'.$row['id_transPeriode'].'/'.$mode).">".$row['id_periode'].' | '.$row['nama_tes']."</a></li>";
		                }
		              } 
		            ?>
		          </ul>
		        </div>
			  </div><!-- /.col -->
			  <div class="col-sm-4 col-md-2">
			  	<div class="dropdown">
		          <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Format Rapor
		          <span class="caret"></span></button>
		          <ul class="dropdown-menu">
		            <li><a href="<?php echo base_url('output/rapor/'.$id_siswa.'/'.$id_periode.'/'.$id_periodeDet.'/mode_ip'); ?>">Skala Nilai 0-4</a></li>
		            <li><a href="<?php echo base_url('output/rapor/'.$id_siswa.'/'.$id_periode.'/'.$id_periodeDet.'/mode_reg'); ?>">Skala Nilai 0-100</a></li>
		          </ul>
		        </div>
			  </div><!-- /.col -->
			  <div class="col-sm-4 col-md-2">
	  	        <div>
		          <div>
		          <a href=<?php echo base_url('output/rapor/'.$id_siswa.'/'.$id_periode.'/'.$id_periodeDet.'/'.$mode.'/1')?> type="button" class="btn btn-danger">
		            	<i class="fa fa-file-pdf-o"></i> &nbsp Simpan PDF </a>
		        </div>
		        </div>
			  </div><!-- /.col -->
			</div><!-- /.row -->
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
        	<div class="row">
				<div class="col-md-1"> </div>

				<div class="col-md-5">
					<br>Nama Peserta Didik : <?php echo $nama_siswa; ?>
					<br>Nomor Induk : <?php echo $id_siswa; ?>
					<br>Nama Sekolah : SMA CHIS Denpasar
				</div>
				<div class="col-md-5">
					<br>Kelas/Semester : <?php echo $nama_kelas; ?> / <?php echo $semester; ?>
					<br>Tahun Pelajaran : <?php echo $id_periode; ?>
				</div>
				<div class="col-md-1"></div>
			</div>

			<br/>Capaian Kompetensi Peserta Didik
			<br/>
			<br/>
            <table class="table table-striped table-bordered">
			  <tr>
			    <th style='text-align:center; vertical-align:middle' rowspan="3">No.</th>
			    <th style='text-align:center; vertical-align:middle' rowspan="3">Mata Pelajaran</th>
			    <th style='text-align:center; vertical-align:middle' colspan="7">Nilai Hasil Belajar</th>
			  </tr>
			  <tr>
			    <th style='text-align:center; vertical-align:middle' colspan="3">Pengetahuan</th>
			    <th style='text-align:center; vertical-align:middle' colspan="2">Keterampilan</th>
			    <th style='text-align:center; vertical-align:middle' colspan="2">Sikap</th>
			  </tr>
			  <tr>
			    <th style='text-align:center; vertical-align:middle'>KKM</th>
			    <th style='text-align:center; vertical-align:middle'>Nilai</th>
			    <th style='text-align:center; vertical-align:middle'>Predikat</th>
			    <th style='text-align:center; vertical-align:middle'>Nilai</th>
			    <th style='text-align:center; vertical-align:middle'>Predikat</th>
			    <th style='text-align:center; vertical-align:middle'>Dalam Mapel</th>
			    <th style='text-align:center; vertical-align:middle'>Antar Mapel</th>
			  </tr>

			  <tr>
			  	<th colspan="8">Kelompok A</th>
			  	<td style='text-align:center; vertical-align:middle' rowspan="<?php echo count($nilai)+11 ?>">
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          echo $row['sikap_antarMapel'];
			        }
			      }
			      ?>
			    </td>
			  </tr>

			  <tr>
			    <?php
			      if ($nilai) {
			        $baris=1;
			        foreach ($nilai as $row) {
			            if ($row['id_kelompok']=='A')
			            {
			              echo "</tr>";
			              echo "<td>".($baris)."</td>";
			              echo "<td>".$row['nama_mapel']."</td>";
			              
			              if ($mode=="mode_ip")
			              {
			              	/*if (strpos($nama_kelas,'Akselerasi') !== false)
				                echo "<td><b>".number_format($row['kkm_aksel'],2,'.','')."</b></td>";
				              else
				                echo "<td><b>".number_format($row['kkm'],2,'.','')."</b></td>";*/
				            echo "<td><b>".number_format($row['kkm_ajar'],2,'.','')."</b></td>";
			              	echo "<td>".number_format($row['nilai_pengetahuan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".number_format($row['nilai_keterampilan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              elseif ($mode=="mode_reg")
			              {
			              	/*if (strpos($nama_kelas,'Akselerasi') !== false)
				                echo "<td><b>".$row['kkm_aksel100']."</b></td>";
				              else
				                echo "<td><b>".$row['kkm100']."</b></td>";*/
				            echo "<td><b>".$row['kkm_ajar100']."</b></td>";
			              	echo "<td>".$row['pengetahuan']."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".$row['keterampilan']."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              $baris++;
			            }
			          }
			        }
			    ?>
			  </tr>

			  <tr>
			    <th colspan="8">Kelompok B</th>
			  </tr>

			  <tr>
			    <?php
			      if ($nilai) {
			        $baris=1;
			        foreach ($nilai as $row) {
			          if ($row['id_kelompok']=='B'){
			            echo "</tr>";
			            echo "<td>".($baris)."</td>";
			            echo "<td>".$row['nama_mapel']."</td>";

			            if ($mode=="mode_ip")
			              {
			              	echo "<td><b>".number_format($row['kkm_ajar'],2,'.','')."</b></td>";
			              	echo "<td>".number_format($row['nilai_pengetahuan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".number_format($row['nilai_keterampilan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              elseif ($mode=="mode_reg")
			              {
			              	echo "<td><b>".$row['kkm_ajar100']."</b></td>";
			              	echo "<td>".$row['pengetahuan']."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".$row['keterampilan']."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			            $baris++;
			          }
			        }
			      }
			    ?>
			  </tr>

			  <tr>
			    <th colspan="8">Kelompok C PEMINATAN</th>
			  </tr>

			  <tr>
			    <?php
			        if (strpos($nama_kelas,"IPA") !== false)
			          echo "<th colspan='8'>Kelompok Peminatan Matematika dan Ilmu Alam</th>";
			        if (strpos($nama_kelas,"IPS") !== false)
			          echo "<th colspan='8'>Kelompok Peminatan Ilmu Sosial</th>";
			        if (strpos($nama_kelas,"Bahasa") !== false)
			          echo "<th colspan='8'>Kelompok Peminatan Ilmu Bahasa</th>";
			    ?>
			  </tr>

			  <tr>
			    <?php
			      if ($nilai) {
			        $baris=1;
			        foreach ($nilai as $row) {
			          if (strpos($row['id_kelompok'],"C") !== false){
			            echo "</tr>";
			            echo "<td>".($baris)."</td>";
			            echo "<td>".$row['nama_mapel']."</td>";
			            
			            if ($mode=="mode_ip")
			              {
			              	echo "<td><b>".number_format($row['kkm_ajar'],2,'.','')."</b></td>";
			              	echo "<td>".number_format($row['nilai_pengetahuan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".number_format($row['nilai_keterampilan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              elseif ($mode=="mode_reg")
			              {
			              	echo "<td><b>".$row['kkm_ajar100']."</b></td>";
			              	echo "<td>".$row['pengetahuan']."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".$row['keterampilan']."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }

			            $baris++;
			          }
			        }
			      }
			    ?>
			  </tr>

			  <tr>
			    <th colspan="8">Kelompok Lintas Peminatan</th>
			  </tr>

			  <tr>
			    <?php
			      if ($nilai) {
			        $baris=1;
			        foreach ($nilai as $row) {
			          if ($row['id_kelompok']=='D'){
			            echo "</tr>";
			            echo "<td>".($baris)."</td>";
			            echo "<td>".$row['nama_mapel']."</td>";
			            
			            if ($mode=="mode_ip")
			              {
			              	echo "<td><b>".number_format($row['kkm_ajar'],2,'.','')."</b></td>";
			              	echo "<td>".number_format($row['nilai_pengetahuan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".number_format($row['nilai_keterampilan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              elseif ($mode=="mode_reg")
			              {
				            echo "<td><b>".$row['kkm_ajar100']."</b></td>";
			              	echo "<td>".$row['pengetahuan']."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".$row['keterampilan']."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }

			            $baris++;
			          }
			        }
			      }
			    ?>
			  </tr>

			  <tr>
			  	<th colspan="2">JUMLAH</th>
			  	<td></td>
			  	<td>
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          if ($mode=="mode_ip")
			          	echo number_format($row['jml_nilai_pengetahuan'],2,'.','');
			          elseif ($mode=="mode_reg")
			          	echo $row['jml_pengetahuan'];
			        }
			      }
			      ?>
			    </td>
			  	<td></td>
			  	<td>
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          if ($mode=="mode_ip")
			          	echo number_format($row['jml_nilai_keterampilan'],2,'.','');
			          elseif ($mode=="mode_reg")
			          	echo $row['jml_keterampilan'];
			        }
			      }
			      ?>
			    </td>
			  	<td></td>
			  	<td></td>
			  </tr>
			  <tr>
			  	<th colspan="2">RATA-RATA</th>
			  	<td></td>
			  	<td>
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          if ($mode=="mode_ip")
			          	echo number_format($row['rata2_nilai_pengetahuan'],2,'.','');
			          elseif ($mode=="mode_reg")
			          	echo $row['rata2_pengetahuan'];
			        }
			      }
			      ?>
			    </td>
			  	<td></td>
			  	<td>
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          if ($mode=="mode_ip")
			          	echo number_format($row['rata2_nilai_keterampilan'],2,'.','');
			          elseif ($mode=="mode_reg")
			          	echo $row['rata2_keterampilan'];
			        }
			      }
			      ?>
			    </td>
			  	<td></td>
			  	<td></td>
			  </tr>
			  <tr>
			  	<th colspan="2">RANKING</th>
			  	<td style='text-align:center; vertical-align:middle' colspan="7">
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 0) {
			          echo $row['ranking'];
			        }
			      }
			      ?>
			    </td>
			  </tr>
			</table> <br>

			<table class="table table-striped table-bordered">
			  <tr>
			    <th style='text-align:center; vertical-align:middle' rowspan="3">No.</th>
			    <th style='text-align:center; vertical-align:middle' rowspan="3">Mata Pelajaran</th>
			    <th style='text-align:center; vertical-align:middle' colspan="7">Nilai Hasil Belajar</th>
			  </tr>
			  <tr>
			    <th style='text-align:center; vertical-align:middle' colspan="3">Pengetahuan</th>
			    <th style='text-align:center; vertical-align:middle' colspan="2">Keterampilan</th>
			    <th style='text-align:center; vertical-align:middle' colspan="2">Sikap</th>
			  </tr>
			  <tr>
			    <th style='text-align:center; vertical-align:middle'>KKM</th>
			    <th style='text-align:center; vertical-align:middle'>Nilai</th>
			    <th style='text-align:center; vertical-align:middle'>Predikat</th>
			    <th style='text-align:center; vertical-align:middle'>Nilai</th>
			    <th style='text-align:center; vertical-align:middle'>Predikat</th>
			    <th style='text-align:center; vertical-align:middle'>Dalam Mapel</th>
			    <th style='text-align:center; vertical-align:middle'>Antar Mapel</th>
			  </tr>

			  <tr>
			    <th colspan="8">MUATAN SEKOLAH</th>
			    <td style='text-align:center; vertical-align:middle' rowspan="<?php echo count($nilai)+10 ?>">
			      <?php
			      if($total)
			      foreach ($total as $row) {
			        if ($row['status_muatan'] == 1) {
			          echo $row['sikap_antarMapel'];
			        }
			      }
			      ?>
			    </td>
			  </tr>

			  <tr>
			    <?php
			      if ($nilai) {
			        $baris=1;
			        foreach ($nilai as $row) {
			            if ($row['id_kelompok']=='E'){
			              echo "</tr>";
			              echo "<td>".($baris)."</td>";
			              echo "<td>".$row['nama_mapel']."</td>";
			              
			              if ($mode=="mode_ip")
			              {
			              	echo "<td><b>".number_format($row['kkm_ajar'],2,'.','')."</b></td>";
			              	echo "<td>".number_format($row['nilai_pengetahuan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".number_format($row['nilai_keterampilan'],2,'.','')."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }
			              elseif ($mode=="mode_reg")
			              {
			              	echo "<td><b>".$row['kkm_ajar100']."</b></td>";
			              	echo "<td>".$row['pengetahuan']."</td>";
				            echo "<td>".$row['predikat_pengetahuan']."</td>";
				            echo "<td>".$row['keterampilan']."</td>";
				            echo "<td>".$row['predikat_keterampilan']."</td>";
				            echo "<td>".$row['predikat_sikap']."</td>";
			              }

			              $baris++;
			            }
			          }
			        }
			    ?>
			  </tr>
			</table>

			<div class="row">
			  <div class="col-md-4"> *) Muatan Lokal Provinsi Bali </div>
			  <div class="col-md-3"> </div>
			  <div class="col-md-3"> </div>
			  <div class="col-md-1"></div>
			</div>

			<br> Pengembangan Diri
			<table class="table table-striped table-bordered">
			  <tr>
			    <th>No.</th>
			    <th colspan="2">Jenis Kegiatan</th>
			    <th>Nilai</th>
			    <th>Keterangan</th>
			  </tr>
			  <tr>
			    <td >A</td>
			    <td colspan="2">Kegiatan Ekstrakurikuler</td>
			    <td colspan=""></td>
			    <td colspan=""></td>
			  </tr>
			  <?php
			  if($pede)
			  {
			    $baris = 1;
			    foreach ($pede as $row) {
			      if ($row['tipe_pede'] == 'Ekstrakurikuler') {
			        echo "<tr>";
			        echo "<td></td>";
			        echo "<td>".$baris."</td>";
			        echo "<td>".$row['nama_pede']."</td>";
			        echo "<td>".$row['nilai_pede']."</td>";
			        echo "<td>".$row['keterangan']."</td>";
			        echo "</tr>";
			        $baris++;
			      }
			    }
			  }
			  ?>
			  <tr>
			    <td >B</td>
			    <td colspan="2">Keikutsertaan dalam Organisasi / Kegiatan Sekolah</td>
			    <td colspan=""></td>
			    <td colspan=""></td>
			  </tr>
			  <?php
			  if($pede)
			  {
			    $baris = 1;
			    foreach ($pede as $row) {
			      if ($row['tipe_pede'] == 'Organisasi') {
			        echo "<tr>";
			        echo "<td></td>";
			        echo "<td>".$baris."</td>";
			        echo "<td>".$row['nama_pede']."</td>";
			        echo "<td>".$row['nilai_pede']."</td>";
			        echo "<td>".$row['keterangan']."</td>";
			        echo "</tr>";
			        $baris++;
			      }
			    }
			  }
			  ?>
			</table>

			<br> Ketidakhadiran
			<table class="table table-striped table-bordered">
			  <tr>
			    <th>No.</th>
			    <th>Alasan Ketidakhadiran</th>
			    <th>Keterangan</th>
			  </tr>
			  <tr>
			    <td>1</td>
			    <td>Sakit</td>
			    <td><?php echo $jml_sakit; ?></td>
			  </tr>
			  <tr>
			    <td>2</td>
			    <td>Izin</td>
			    <td><?php echo $jml_izin; ?></td>
			  </tr>
			  <tr>
			    <td>3</td>
			    <td>Tanpa Keterangan</td>
			    <td><?php echo $jml_tketerangan; ?></td>
			  </tr>
			</table>

			<table class="table table-striped table-bordered">
			  <tr>
			    <th rowspan='5'>Catatan Wali Kelas <br> <?php echo $isi_catatan; ?> </th>
			  </tr>
			</table>

			<div class="row">
			  <div class="col-md-4"> </div>
			  <div class="col-md-3"> </div>
			  <div class="col-md-3">
			    <br>
			    <br> Denpasar, <?php //$today = date("d M Y"); echo $today;
			    echo $tanggal_rapor;?>
			  </div>
			  <div class="col-md-1"></div>
			</div>

			<div class="row">
			  <div class="col-md-1"> </div>
			  <div class="col-md-3">
			    <br>Orang Tua/Wali
			    <br>Peserta Didik
			    <br>
			    <br>
			    <br>
			    <br>_________________
			  </div>
			  <div class="col-md-3">
			    <br>Wali Kelas
			    <br>
			    <br>
			    <br>
			    <br>
			    <br><u><?php echo $nama_wali; ?></u>
			    <br>NIP. <?php echo $id_wali; ?>
			  </div>
			  <div class="col-md-3">
			    <br>Kepala Sekolah
			    <br>
			    <br>
			    <br>
			    <br>
			    <br><u><?php echo $nama_kepsek; ?></u>
			    <br>NIP. <?php echo $id_kepsek; ?>
			  </div>
			  <div class="col-md-1"></div>
			</div>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->