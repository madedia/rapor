<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_mapel extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();

		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }

		$this->load->model('model_nilaiMapel');
	}


	function index()
	{
		$result = $this->model_nilaiMapel->get_mapelEnrol();
		$data['mapelEnrol']=$result;

		$this->template->display('nilai_mapel/mapel_enrol', $data);
	}

	public function ekspor_tabel($id_tAjar)
	{
		//$objWorkSheet = $this->excel;
		$result = $this->model_nilaiMapel->get_siswaKelas($id_tAjar);
		$result2 = $this->model_nilaiMapel->get_nilaiKelasSiswa($id_tAjar, $this->session->userdata('periode_aktifDet'));
		$detail_mapel = $this->model_nilaiMapel->get_Detail($id_tAjar);

		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();

		$sheet->setCellValue('A1', 'Daftar Nilai Mata Pelajaran '.$detail_mapel[0]['nama_mapel'].' Kelas '.$detail_mapel[0]['nama_kelas']);
		$sheet->setCellValue('A2', 'Periode '.$this->session->userdata('periode_tes').' Tahun Ajaran '.$this->session->userdata('periode_aktif'));
    	$sheet->mergeCells('A1:I1');
    	$sheet->mergeCells('A2:I2');
    	$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    	$sheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    	$sheet->setCellValue('A4', 'Guru Pengampu : '.$detail_mapel[0]['nama_guru']);

		//name the worksheet
		//$this->excel->getActiveSheet()->setTitle('Daftar Nilai Mapel ........');

		$sheet
			->setCellValue('A6', 'NIS')
			->setCellValue('B6', 'Nama Lengkap')
			->setCellValue('C6', 'Nilai Pengetahuan')
			->setCellValue('D6', 'Deskripsi')
			->setCellValue('E6', 'Nilai Keterampilan')
			->setCellValue('F6', 'Deskripsi')
			->setCellValue('G6', 'Nilai Sikap')
			->setCellValue('H6', 'Deskripsi')
			->setCellValue('I6', 'Komentar thd Peserta Didik');

		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(40);
		$sheet->getColumnDimension('C')->setWidth(10);
		$sheet->getColumnDimension('D')->setWidth(30);
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->getColumnDimension('F')->setWidth(30);
		$sheet->getColumnDimension('G')->setWidth(10);
		$sheet->getColumnDimension('H')->setWidth(30);
		$sheet->getColumnDimension('I')->setWidth(50);
		$sheet->getStyle('A6:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$styleArray = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('argb' => '000000'),
		),
		),
		);

		$sheet->getStyle('A6:I6')->applyFromArray($styleArray);

		$baris=6;
		if($result)
		{
			foreach ($result as $res)
			{
				$temp = 0;
				if($result2)
				{
					foreach ($result2 as $res2) {
						if ($res['id_transSisKls']==$res2['id_tSisKlsKey'])
						{
							$this->excel->getActiveSheet()
								->setCellValue('A'.($baris+1), $res2['id_siswa'])
								->setCellValue('B'.($baris+1), $res2['nama_siswa'])
								->setCellValue('C'.($baris+1), $res2['pengetahuan'])
								->setCellValue('D'.($baris+1), $res2['des_peng'])
								->setCellValue('E'.($baris+1), $res2['keterampilan'])
								->setCellValue('F'.($baris+1), $res2['des_ket'])
								->setCellValue('G'.($baris+1), $res2['sikap'])
								->setCellValue('H'.($baris+1), $res2['des_sik'])
								->setCellValue('I'.($baris+1), $res2['komentar']);
							$temp = 1;				
							break;
						}
					}
					if($temp == 0) {
						$this->excel->getActiveSheet()
							->setCellValue('A'.($baris+1), $res['id_siswa'])
							->setCellValue('B'.($baris+1), $res['nama_siswa']);
					}
					$sheet->getStyle('A'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('B'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('C'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('D'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('E'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('F'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('G'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('H'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('I'.($baris+1))->applyFromArray($styleArray);
					$baris++;
				}
			}
		}
		
		$sheet->getDefaultRowDimension()->setRowHeight(20);

		$filename= 'Daftar Nilai Mapel '.$detail_mapel[0]['nama_mapel'].' Kls '.$detail_mapel[0]['nama_kelas'].' periode '.$this->session->userdata('periode_aktifDet').'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function impor_tabel($id_tAjar, $id_tKelasKey)
	{
		$data_imp = array(
			'id_tAjar' => $id_tAjar,
			'id_tKelasKey' => $id_tKelasKey
		);

		$mapelKelas_detail= $this->model_nilaiMapel->get_Detail($id_tAjar);
		foreach ($mapelKelas_detail as $row) {
			$data_imp['nama_mapel'] = $row['nama_mapel'];
			$data_imp['status_aksel'] = $row['status_aksel'];
			$data_imp['nama_kelas'] = $row['nama_kelas'];
		}

		$config['upload_path'] = './files/';
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$data_imp['error'] = $this->upload->display_errors();
			//echo $data_imp['error'];
			$this->template->display('nilai_mapel/upload_nilaiMapel', $data_imp);
		}
		else
		{
			$import_res['upload_data'] = $this->upload->data();

			$this->insert_nilaiImpor($id_tAjar, $id_tKelasKey, $import_res['upload_data']['file_name']);
		}
	}

	public function insert_nilaiImpor($id_tAjar, $id_tKelasKey, $file_name)
	{
		$data = array();
		$data['id_input'] = $this->session->userdata('id_guru');
		$data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');

		$init = $this->model_nilaiMapel->get_siswaKelas($id_tAjar);
		$result = $this->model_nilaiMapel->get_nilaiKelasSiswa($id_tAjar, $this->session->userdata('periode_aktifDet'));

		/*
		Pengecekan  apakah nilai yang akan di import apakah sebelunya telah terisi apa belum
		Jika sudah maka update
		Jika belum maka insert
		*/
		if($init)
		{
			$barisExcel = 0;
			foreach ($init as $row1)
			{
				$temp = 0;
				if ($result)
				{
					foreach ($result as $row2)
					{
						if($row1['id_transSisKls'] == $row2['id_tSisKlsKey'])
						{
							$data['id_nilaiMapel'][$barisExcel] = $row2['id_nilaiMapel'];
							$data['isUpdate'][$barisExcel] = 1;
							$temp = 1;				
							break;
						}
					}
				}
				if($temp == 0) {
					$data['isUpdate'][$barisExcel] = 0;
				}
				$barisExcel++;
			}
		}

		$this->load->library('excel');
		$eksekusi = 0;

		$inputFileName = FCPATH . 'files/'. $file_name;
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		$objPHPExcel->setActiveSheetIndex(0);

		// Validasi format antara yang di-export dengan yang di-import
		$worksheet = $objPHPExcel->getActiveSheet();
		$jmlRowExcel = ($worksheet->getHighestRow())-6;
		if(count($init) == $jmlRowExcel)
		{
	        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

	        //remove header until row=6
	        array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);

	        $A = $worksheet->getCell( "A6" ) ->getValue();
	        $B = $worksheet->getCell( "B6" ) ->getValue();
	        $C = $worksheet->getCell( "C6" ) ->getValue();
	        $D = $worksheet->getCell( "D6" ) ->getValue();
	        $E = $worksheet->getCell( "E6" ) ->getValue();
	        $F = $worksheet->getCell( "F6" ) ->getValue();
	        $G = $worksheet->getCell( "G6" ) ->getValue();
	        $H = $worksheet->getCell( "H6" ) ->getValue();
	        $I = $worksheet->getCell( "I6" ) ->getValue();

			if(strcasecmp($A,"NIS")==0)
	            if(strcasecmp($B,"Nama Lengkap")==0)
	                if(strcasecmp($C,"Nilai Pengetahuan")==0)
	                    if(strcasecmp($D,"Deskripsi")==0)
	                        if(strcasecmp($E,"Nilai Keterampilan")==0)
	                        	if(strcasecmp($F,"Deskripsi")==0)
	                        		if(strcasecmp($G,"Nilai Sikap")==0)
	                        			if(strcasecmp($H,"Deskripsi")==0)
	                        				if(strcasecmp($I,"Komentar thd Peserta Didik")==0)
	                        					$eksekusi = 1;
		}

        if($eksekusi)
        {
        	$baris = 0;
        	foreach ($sheetData as $rowCol)
        	{
				$data['id_siswa'][$baris] = $rowCol['A'];
				$data['nama_siswa'][$baris] = $rowCol['B'];
				$data['pengetahuan'][$baris] = $rowCol['C'];
				$data['des_peng'][$baris] = $rowCol['D'];
				$data['keterampilan'][$baris] = $rowCol['E'];
				$data['des_ket'][$baris] = $rowCol['F'];
				$data['sikap'][$baris] = $rowCol['G'];
				$data['des_sik'][$baris] = $rowCol['H'];
				$data['komentar'][$baris] = $rowCol['I'];
        		$data['id_tSisKlsKey'][$baris] = $this->model_nilaiMapel->get_idTSisKls($data['id_siswa'][$baris], $id_tKelasKey);

        		$baris++;
        	}
        	$data['id_tAjarKey'] = $id_tAjar;

        	unlink($inputFileName);

        	$this->tambahNilai($id_tAjar, $data, 1);
        }
	}

	public function isi($id_tAjar)
	{
		$data['id_tAjar']=$id_tAjar;
		$data['mapelKelas_detail']= $this->model_nilaiMapel->get_Detail($id_tAjar);
		foreach ($data['mapelKelas_detail'] as $row) {
			$data['nama_mapel'] = $row['nama_mapel'];
			$data['status_aksel'] = $row['status_aksel'];
			$data['nama_kelas'] = $row['nama_kelas'];
		}
		if(strpos($this->session->userdata('periode_aktifDet'),"uas") !== false) 
		{
			$data['statusUAS'] = 1;
			$data['kamus_peng'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 1);
			$data['kamus_ket'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 2);
			$data['kamus_sik'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 3);
		}
		else
			$data['statusUAS'] = 0;

		/*
		Pengecekan apakah data nilai sudah pernah diisi dan disimpan apa belum
		*/
		$data['siswa_mapel'] = array();
		$siswa_enrol= $this->model_nilaiMapel->get_siswaKelas($id_tAjar);
		$siswa_nilai= $this->model_nilaiMapel->get_nilaiKelasSiswa($id_tAjar, $this->session->userdata('periode_aktifDet'));
		if($siswa_enrol)
		{
			foreach ($siswa_enrol as $row1) {
				$temp=0;
				if ($siswa_nilai)
				{
					foreach ($siswa_nilai as $row2) {
						if($row1['id_transSisKls'] == $row2['id_tSisKlsKey'])
						{
							$row2['isUpdate'] = 1;
							array_push($data['siswa_mapel'], $row2);
							$temp = 1;				
							break;
						}
					}
				}
				if($temp == 0) {
					$row1['isUpdate'] = 0;
					array_push($data['siswa_mapel'], $row1);
				}
			}
			//$this->template->display('nilai_mapel/mapel_nilai', $data);
		}
		$this->template->display('nilai_mapel/mapel_nilai', $data);
	}

	public function tambahNilai($id_tAjar, $data=null, $impor=null)
	{
		if($impor == null)
		{
			$data = array(
				'id_periodeDet' => $this->session->userdata('periode_aktifDet'),
				'id_tSisKlsKey' => $this->input->post('id_transSisKls'),
				'id_tAjarKey' => $id_tAjar,
				'pengetahuan' => $this->input->post('nPeng'),
				'keterampilan' => $this->input->post('nKet'),
				'sikap' => $this->input->post('nSik'),
				'des_peng' => $this->input->post('des_peng'),
				'des_ket' => $this->input->post('des_ket'),
				'des_sik' => $this->input->post('des_sik'),
				'komentar' => $this->input->post('komentar'),
				'id_input' => $this->session->userdata('id_guru'),
				'id_nilaiMapel' => $this->input->post('id_nilai'),
				'isUpdate' => $this->input->post('isUpdate'),
			);
		}

		for ($baris=0; $baris<count($data['id_tSisKlsKey']); $baris++) {
            if (!$data['pengetahuan'][$baris]) $data['pengetahuan'][$baris]=0;
            if (!$data['keterampilan'][$baris]) $data['keterampilan'][$baris]=0;
            if (!$data['sikap'][$baris]) $data['sikap'][$baris]=0;

            $data['nilai_pengetahuan'][$baris] = ($data['pengetahuan'][$baris] * 4) / 100;
            $data['predikat_pengetahuan'][$baris] = $this->model_nilaiMapel->get_predikat($data['nilai_pengetahuan'][$baris], 0);

            if ($data['predikat_pengetahuan'][$baris] != null)
				if ($data['des_peng'][$baris] == null)
					$data['des_peng'][$baris]= $this->model_nilaiMapel->get_deskripsi(1, $id_tAjar, $data['predikat_pengetahuan'][$baris]);

            $data['nilai_keterampilan'][$baris]= ($data['keterampilan'][$baris] * 4) / 100;
            $data['predikat_keterampilan'][$baris] = $this->model_nilaiMapel->get_predikat($data['nilai_keterampilan'][$baris], 0);

            if ($data['predikat_keterampilan'][$baris] != null)
				if ($data['des_ket'][$baris] == null)
					$data['des_ket'][$baris]= $this->model_nilaiMapel->get_deskripsi(2, $id_tAjar, $data['predikat_keterampilan'][$baris]);

            $data['nilai_sikap'][$baris]= ($data['sikap'][$baris] * 4) / 100;
            $data['predikat_sikap'][$baris] = $this->model_nilaiMapel->get_predikat($data['nilai_sikap'][$baris], 1);

            if ($data['predikat_sikap'][$baris] != null)
				if ($data['des_sik'][$baris] == null)
					$data['des_sik'][$baris]= $this->model_nilaiMapel->get_deskripsi(3, $id_tAjar, $data['predikat_keterampilan'][$baris]);
		}

		$this->model_nilaiMapel->insert_nilaiMapel($data, $id_tAjar);

		/*
		Cari jumlah nilai sama rata-rata
		*/
		$data2 = array();
		$data2['id_periodeDet'] = $data['id_periodeDet'];
		$data2['kelas'] = $this->model_nilaiMapel->get_kelasNilai($id_tAjar);

		/*
		cek apakah mapel yang di cek apakah mapel muatan
		*/
		$isMuatan = $this->model_nilaiMapel->is_muatan($id_tAjar);
		$data2['id_totalNilai'] = $this->model_nilaiMapel->get_idTotalNilai($data['id_periodeDet'], $data2['kelas'], $isMuatan);

		if($data2['id_totalNilai'])
		{
			$isUpdateTotal = 1;
		}
		else
		{
			$isUpdateTotal = 0;
		}
		
		if ($isMuatan)
		{
			$data2['total-muatan'] = $this->model_nilaiMapel->get_rankNilaiMuatan($data['id_periodeDet'], $data2['kelas']);
			$this->model_nilaiMapel->insert_totalNilaiM($data2, $isUpdateTotal);
		}
		else
		{
			$data2['total-umum'] = $this->model_nilaiMapel->get_rankNilai($data['id_periodeDet'], $data2['kelas']);
			$this->model_nilaiMapel->insert_totalNilai($data2, $isUpdateTotal);
		}
		redirect('nilai_mapel');
	}

	public function kamus_deskripsi($id_tAjar)
	{
		$data['id_tAjar']=$id_tAjar;
		$data['mapelKelas_detail']= $this->model_nilaiMapel->get_Detail($id_tAjar);
		foreach ($data['mapelKelas_detail'] as $row) {
			$data['nama_mapel'] = $row['nama_mapel'];
			$data['status_aksel'] = $row['status_aksel'];
			$data['nama_kelas'] = $row['nama_kelas'];
		}
		
		$data['pengetahuan'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 1);
		$data['keterampilan'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 2);
		$data['sikap'] = $this->model_nilaiMapel->get_kamusDeskripsi($id_tAjar, 3);
		$this->template->display('nilai_mapel/kamus_deskripsi', $data);
	}

	public function update_deskripsi($id_tAjar)
	{
		$data = array(
			'id_nilaiMapel' => $this->input->post('id_nilai'),
			
			//'id_tSisKlsKey' => $this->input->post('id_transSisKls'),
			'des_peng' => $this->input->post('des_peng'),
			'des_ket' => $this->input->post('des_ket'),
			'des_sik' => $this->input->post('des_sik'),
			'id_input' => $this->session->userdata('id_guru'),
		);

		$this->model_nilaiMapel->update_deskripsi($data);
		
		redirect('nilai_mapel');
	}

	function update_kamusDeskripsi($id_tAjar)
    {
    	$data1 = array(
    		'id_tAjar' => $this->input->post('id_tAjar'),
			'id_transPeng' => $this->input->post('id_transPeng'),
			'des_peng' => $this->input->post('des_peng'),
		);
		$this->model_nilaiMapel->update_kamusDeskripsi($data1, 1);

		$data2 = array(
    		'id_tAjar' => $this->input->post('id_tAjar'),
			'id_transKet' => $this->input->post('id_transKet'),
			'des_ket' => $this->input->post('des_ket'),
		);
		$this->model_nilaiMapel->update_kamusDeskripsi($data2, 2);

		$data3 = array(
    		'id_tAjar' => $this->input->post('id_tAjar'),
			'id_transSik' => $this->input->post('id_transSik'),
			'des_sik' => $this->input->post('des_sik'),
		);
    	$this->model_nilaiMapel->update_kamusDeskripsi($data3, 3);
    	redirect('nilai_mapel');
    }
}

?>