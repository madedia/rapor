<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_nilaiPede extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    public function get_pedeEnrol()
    {
        if($this->session->userdata('tipe')>=3){
            $this->db->where('id_pembina', $this->session->userdata('id_guru'));
        }

        $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
        $this->db->where('id_pede = id_tPede');
        $query = $this->db->get('pede, trans_pedeguru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function insert($data)
    {
        for ($i=0; $i < count($data['id_tPedeSiswaKey']); $i++) { 
            $this->db->set('nilai_pede', $data['nilai_pede'][$i]);
            $this->db->set('keterangan', $data['keterangan'][$i]);

            if($data['isUpdate'][$i])
            {
                $this->db->where('id_nilaiPede', $data['id_nilaiPede'][$i]);
                $this->db->update('nilai_pede');
                //echo $this->db->last_query() . '<br>';
            }
            else
            {
                $this->db->set('id_tPedeSiswaKey', $data['id_tPedeSiswaKey'][$i]);
                $this->db->set('id_periodeDet', $this->session->userdata('periode_aktifDet'));
                $this->db->insert('nilai_pede');
                //echo $this->db->last_query() . '<br>';
            }
        }
    }

    function get_siswa($id_transPedeGuru, $id_pembina)
    {
        $this->db->where('id_tPedeGuru', $id_transPedeGuru);
        $this->db->where('id_tPedeGuru = id_transPedeGuru');
        $this->db->where('id_transPedeSiswa = id_tPedeSiswaKey');
        
        $this->db->where('trans_pedesiswa.id_siswa = siswa.id_siswa');
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));
        $this->db->where('nilai_pede.id_periodeDet', $this->session->userdata('periode_aktifDet'));

        if(($this->session->userdata('tipe')==2) && ($id_pembina != $this->session->userdata('id_guru')))
        {
            $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
            $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
            $this->db->where('trans_kelas.id_guruWali', $this->session->userdata('id_guru'));
            $query = $this->db->get('nilai_pede, siswa, trans_siswa, trans_pedeguru, trans_pedesiswa, trans_kelas');
        }
        else
        {
            $query = $this->db->get('nilai_pede, siswa, trans_pedeguru, trans_pedesiswa');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswa2($id_tPedeGuru, $id_pembina)
    {
        $this->db->where('id_tPedeGuru', $id_tPedeGuru);
        $this->db->where('id_tPedeGuru = id_transPedeGuru');
        $this->db->where('trans_pedesiswa.id_siswa = siswa.id_siswa');
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));

        if(($this->session->userdata('tipe')==2) && ($id_pembina != $this->session->userdata('id_guru')))
        {
            $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
            $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
            $this->db->where('trans_kelas.id_guruWali', $this->session->userdata('id_guru'));
            $query = $this->db->get('siswa, trans_siswa, trans_pedeguru, trans_pedesiswa, trans_kelas');
        }
        else
        {
            $query = $this->db->get('trans_pedeguru, trans_pedesiswa, siswa');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_keterangan($id_predikat)
    {
        $this->db->where('id_predikat', $id_predikat);
        $query = $this->db->get('konversi_nilai');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['nilai_ekskul'];
            return $val;
        }
        else return null;
    }

}