<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Absensi dan Catatan Wali
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Absensi dan Catatan Wali</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-warning">
        <div class="box-header with-border">
        	<h3 class="box-title">Daftar Kelas Enrol</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_enrol" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">Kelas</th>
                    <th style="text-align:center">Wali Kelas</th>
                    <th style="text-align:center">Isi Data</th>
                    <th style="text-align:center">Laporan Excel</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($kelas) {
						$baris=1;
						foreach ($kelas as $row) {
							if ($row['id_transKelas']!=''){
								echo "<tr>";
                                echo "<td>".($baris)."</td>";
                                echo "<td>".$row['nama_kelas']."</td>";
                                echo "<td>".$row['nama_guru']."</td>";
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('absensi_catatan/kelas/'.$row['id_transKelas']).'" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Daftar Nilai</a></td>';
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('absensi_catatan/ekspor_tabel/'.$row['id_transKelas']).'" class="btn btn-sm btn-default"><i class="fa fa-download"></i> Unduh</a></td>';

                                echo "</tr>";
                                $baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": [3, 4],
        } ]
    });
  });
</script>