<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Rapor Siswa
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('siswa'); ?>"> Siswa</a></li>
    <li class="active">Rapor</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">       
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
          <div class="row">
        <div class="col-sm-4 col-md-2">
          <div class="dropdown">
              <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Tahun Ajaran
              <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <?php 
                  if ($thn_ajaran){
                    foreach ($thn_ajaran as $row) {
                      echo "<li><a href=".base_url('output/leger/'.$id_transKelas.'/'.$row['id_transPeriode']).">".$row['id_periode'].' | '.$row['nama_tes']."</a></li>";
                    }
                  } 
                ?>
              </ul>
            </div>
        </div><!-- /.col -->
        <div class="col-sm-4 col-md-2">
          <div class="dropdown">
              <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown">Format Leger
              <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('output/leger/'.$id_transKelas.'/'.$id_periodeDet.'/mode_ip'); ?>">Skala Nilai 0-4</a></li>
                <li><a href="<?php echo base_url('output/leger/'.$id_transKelas.'/'.$id_periodeDet.'/mode_reg'); ?>">Skala Nilai 0-100</a></li>
              </ul>
            </div>
        </div><!-- /.col -->
        <div class="col-sm-4 col-md-2">
              <div>
                <!-- <button type="button" onClick="window.open('<?php echo base_url('output/leger/'.$id_transKelas.'/'.$id_periodeDet.'/1'); ?>')" class="btn btn-success">
                  Simpan EXCEL
                </button> -->
                <a href=<?php echo base_url('output/leger/'.$id_transKelas.'/'.$id_periodeDet.'/'.$mode.'/1'); ?> type="button" class="btn btn-success">
                  <i class="fa fa-file-excel-o"></i> &nbsp Simpan Excel </a>
            </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
          <div>
            <h4>
              <center><?php echo "DAFTAR NILAI RAPOR ". $nama_tes. " SEMESTER ".$semester." KELAS ". $nama_kelas; ?>
              <br> SMA CHIS DENPASAR                                                                                                                                                                                
              <br><?php echo "TAHUN PELAJARAN ". $id_periode; ?></center> 
            </h4>
            <b>
            <?php
            echo " Kelas      : ".$nama_kelas;
            echo "<br> Semester   : ".$semester;
            ?></b>
          </div>

          <!--<div class="table-responsive">-->
          <table id="tabel_leger" class="table table-striped table-bordered">
            <thead>
              <tr valign="middle">
                <th style="text-align:center; vertical-align:middle" rowspan="3">No.</th>
                <th style="text-align:center; vertical-align:middle" rowspan="3">NIS</th>
                <th style="text-align:center; vertical-align:middle" rowspan="3">NAMA</th>
                
                <?php
                $kel_a = $kel_b = $kel_c = $kel_d = $kel_e = 0;

                if($mapel)
                foreach ($mapel as $row)
                {
                  if ($row['id_kelompok'] == 'A') $kel_a++;
                  if ($row['id_kelompok'] == 'B') $kel_b++;
                  if ($row['id_kelompok'] == 'C_IPA' || $row['id_kelompok'] == 'C_IPS' || $row['id_kelompok'] == 'C_IB') $kel_c++;
                  if ($row['id_kelompok'] == 'D') $kel_d++;
                  if ($row['id_kelompok'] == 'E') $kel_e++;
                }
                $kel_a = $kel_a*5; $kel_b = $kel_b*5; $kel_c = $kel_c*5; $kel_d = $kel_d*5; $kel_e = $kel_e*5;

                if ($kel_a) echo "<th style='text-align:center; vertical-align:middle' colspan=".$kel_a.">MAPEL WAJIB KELOMPOK A</th>";
                if ($kel_b) echo "<th style='text-align:center; vertical-align:middle' colspan=".$kel_b.">MAPEL WAJIB KELOMPOK B</th>";
                if ($kel_c) echo "<th style='text-align:center; vertical-align:middle' colspan=".$kel_c.">MAPEL KELOMPOK PEMINATAN</th>";
                if ($kel_d) echo "<th style='text-align:center; vertical-align:middle' colspan=".$kel_d.">MAPEL KELOMPOK LINTAS PEMINATAN</th>";
                ?>  

                

                <th style='text-align:center; vertical-align:middle' rowspan="2" colspan="2">JUMLAH</th>
                <th style='text-align:center; vertical-align:middle' rowspan="2" colspan="2">RATA-RATA</th>
                <th style='text-align:center; vertical-align:middle' rowspan="3">RK</th>
                
                <?php
                  if ($kel_e) echo "<th style='text-align:center; vertical-align:middle' colspan=".$kel_e.">MUATAN SEKOLAH</th>";
                ?>
              </tr>

              <tr>
                <?php
                  $baris = 1;
                  if($mapel)
                  foreach ($mapel as $row)
                  { 
                    echo "<th style='text-align:center; vertical-align:middle' colspan='5'>".$row['nama_mapel']."</th>";
                  }  
                ?>
              </tr>

              <tr>
                <?php
                  if ($mapel)
                  foreach ($mapel as $row)
                  {
                    if ($row['id_kelompok'] != 'E')
                    {
                      echo "<th style='text-align:center; vertical-align:middle'>P</th>
                          <th style='text-align:center; vertical-align:middle'>pr</th>
                          <th style='text-align:center; vertical-align:middle'>K</th>
                          <th style='text-align:center; vertical-align:middle'>pr</th>
                          <th style='text-align:center; vertical-align:middle'>S</th>";
                    }
                    
                  }
                ?>
                <th style="text-align:center">P</th>
                <th style="text-align:center">K</th>
                <th style="text-align:center">P</th>
                <th style="text-align:center">K</th>
                <?php
                  if ($mapel)
                  foreach ($mapel as $row)
                  {
                    if ($row['id_kelompok'] == 'E')
                    {
                      echo "<th style='text-align:center; vertical-align:middle'>P</th>
                          <th style='text-align:center; vertical-align:middle'>pr</th>
                          <th style='text-align:center; vertical-align:middle'>K</th>
                          <th style='text-align:center; vertical-align:middle'>pr</th>
                          <th style='text-align:center; vertical-align:middle'>S</th>";
                    }
                    
                  }
                ?>
              </tr>
            </thead>
            
            <tbody>
              <?php
                $baris = 1;
                if($siswa)
                foreach ($siswa as $row) {
                  echo "<tr>";
                  echo "<td>".$baris."</td>";
                  echo "<td>".$row['id_siswa']."</td>";
                  echo "<td>".$row['nama_siswa']."</td>";
                  
                  if($mapel)
                  foreach ($mapel as $row1) {
                    if ($row1['id_kelompok'] != 'E')
                    {
                      if($nilai[$row['id_siswa']][$row1['id_mapel']])
                      {
                        foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                          if ($mode=="mode_ip")
                          {
                            echo "<td>".$row2['nilai_pengetahuan']."</td>";
                            echo "<td>".$row2['predikat_pengetahuan']."</td>";
                            echo "<td>".$row2['nilai_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_sikap']."</td>";
                          }
                          elseif ($mode=="mode_reg")
                          {
                            echo "<td>".$row2['pengetahuan']."</td>";
                            echo "<td>".$row2['predikat_pengetahuan']."</td>";
                            echo "<td>".$row2['keterampilan']."</td>";
                            echo "<td>".$row2['predikat_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_sikap']."</td>";
                          }
                        }
                      }
                      else
                      {
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                      }
                    }
                  }

                  $temp=1;
                  if($total)
                  foreach ($total as $row3) {
                    if ($row3['id_tSisKlsKey'] == $row['id_transSisKls'] && $row3['status_muatan'] == 0)
                    {
                      if ($mode=="mode_ip")
                      {
                        echo "<td>".$row3['jml_nilai_pengetahuan']."</td>";
                        echo "<td>".$row3['jml_nilai_keterampilan']."</td>";
                        echo "<td>".$row3['rata2_nilai_pengetahuan']."</td>";
                        echo "<td>".$row3['rata2_nilai_keterampilan']."</td>";
                      }
                      elseif ($mode=="mode_reg")
                      {
                        echo "<td>".$row3['jml_pengetahuan']."</td>";
                        echo "<td>".$row3['jml_keterampilan']."</td>";
                        echo "<td>".$row3['rata2_pengetahuan']."</td>";
                        echo "<td>".$row3['rata2_keterampilan']."</td>";
                      }
                        echo "<td>".$row3['ranking']."</td>";
                        $temp=0;
                    }
                  }
                  if($temp)
                  {
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                  }


                  if($mapel)
                  foreach ($mapel as $row1) {
                    if ($row1['id_kelompok'] == 'E')
                    {
                      if($nilai[$row['id_siswa']][$row1['id_mapel']])
                      {
                        foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                          if ($mode=="mode_ip")
                          {
                            echo "<td>".$row2['nilai_pengetahuan']."</td>";
                            echo "<td>".$row2['predikat_pengetahuan']."</td>";
                            echo "<td>".$row2['nilai_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_sikap']."</td>";
                          }
                          elseif ($mode=="mode_reg")
                          {
                            echo "<td>".$row2['pengetahuan']."</td>";
                            echo "<td>".$row2['predikat_pengetahuan']."</td>";
                            echo "<td>".$row2['keterampilan']."</td>";
                            echo "<td>".$row2['predikat_keterampilan']."</td>";
                            echo "<td>".$row2['predikat_sikap']."</td>";
                          }
                        }
                      }
                      else
                      {
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                      }
                    }
                  }

                  echo "</tr>";
                  $baris++;
                }
              ?>
            </tbody>
          </table>
          <!--</div>-->

          <div class="row">
            <div class="col-md-4"> </div>
            <div class="col-md-3"> </div>
            <div class="col-md-3">
              <br>
              <br> Denpasar, <?php //$today = date("d M Y"); echo $today;
              echo $tanggal_rapor;?>
            </div>
            <div class="col-md-1"></div>
          </div>

          <div class="row">
            <div class="col-md-1"> </div>
            <div class="col-md-3">
              <br>Wali Kelas
              <br>
              <br>
              <br>
              <br>
              <br><u><?php echo $nama_wali; ?></u>
              <br>NIP. <?php echo $id_wali; ?>
            </div>
            <div class="col-md-3">
              
            </div>
            <div class="col-md-3">
              <br>Kepala Sekolah
              <br>
              <br>
              <br>
              <br>
              <br><u><?php echo $nama_kepsek; ?></u>
              <br>NIP. <?php echo $id_kepsek; ?>
            </div>
            <div class="col-md-1"></div>
          </div>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->


<script>
  $(function () {
    $("#tabel_leger").DataTable({
      scrollY: "300px",
      scrollX: true,
      "bPaginate": false,
      "scrollCollapse": true,
      "bAutoWidth" : false,
      "fixedColumns": {leftColumns: 3}
    });
  });

  $('body').addClass('sidebar-collapse');
</script>