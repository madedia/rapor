<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Identitas Peserta Didik</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
          <?php echo form_open('siswa/edit_siswa');?>
          <table id="tabel_siswa" class="table table-responsive">
            <tr>
              <td width="20%">1. Nama Peserta Didik (lengkap)</td>
              <td width="1%">:</td>
              <td><input></input></td>
            </tr>
            <tr>
              <td>2. No Induk</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>3. Nomor Induk Siswa Nasional</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>4. Tempat dan Tanggal Lahir</td>
              <td>:</td>
              <td><input type='text'></input></td>
              <td><input type='date'></input></td>
            </tr>
            <tr>
              <td>5. Jenis Kelamin</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>6. Agama</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>7. Anak ke</td>
              <td>:</td>
              <td><input type='number' min='1'></input></td>
            </tr>
            <tr>
              <td>8. Status dalam Keluarga</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>9. Alamat Peserta Didik</td>
              <td>:</td>
              <td><input type='text'></input></td>
              <td width="15%">Telepon</td>
              <td width="1%">:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>10. Diterima di Sekolah ini <br> a.  Di Kelas</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
              <td><br>b.  Pada Tanggal</td>
              <td><br>:</td>
              <td><br><input type='date'></input></td>
              <td width="15%"><br>c.  Semester</td>
              <td><br>:</td>
              <td><br><input type='number'></input></td>
            </tr>
            <tr>
              <td>11. Sekolah Asal <br> a.  Nama Sekolah</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
              <td><br>b.  Alamat  </td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
            </tr>
            <tr>
              <td>12. Ijasah SMP / MTs / Paket B <br>Tahun</td>
              <td><br>:</td>
              <td><br><input type='year'></input></td>
              <td><br>Nomor</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
            </tr>
            <tr>
              <td>14. Orang Tua <br>a.  Ayah</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
              <td><br>b.  Ibu</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
            </tr>
            <tr>
              <td>15. Alamat Orang Tua</td>
              <td>:</td>
              <td><input type='text'></input></td>
              <td>Telepon</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>16. Pekerjaan Orang Tua <br>a.  Ayah</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
              <td><br>b.  Ibu</td>
              <td><br>:</td>
              <td><br><input type='text'></input></td>
            </tr>
            <tr>
              <td>17. Nama Wali</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>18. Alamat Wali</td>
              <td>:</td>
              <td><input type='email'></input></td>
              <td>Telepon</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
            <tr>
              <td>19. Pekerjaan Wali</td>
              <td>:</td>
              <td><input type='text'></input></td>
            </tr>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->


<div class="modal modal-danger fade" id="modal-delete" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
                    <p>Are you sure to delete this data ?</p>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
            <button id="btn-delete" type="button" class="btn btn-outline"><i class="fa fa-check"></i> Yes</button>
          </div>
        </div>
      </div>

<script type="text/javascript">
      $(document).ready(function() {

        $('#btn-delete-trig').click(function(){
            $('#modal-delete').modal('show');
        });
      });
</script>

<script type="text/javascript">
  $(document).ready(function() {

    $('#btnDelete').click(function() {
      bootbox.confirm("Are you sure want to delete?", function(result) {
        alert("Confirm result: " + result);
      });
    });
  });
</script>