<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kelas extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 3) {
        	redirect ('');
        }

		$this->load->model('model_kelas');
	}

	function index ()
	{
		$data['kelas']=$this->model_kelas->get_struktur('kelas');
		$data['kelas_general']=$this->model_kelas->get_struktur('kelas_general');
		$data['trans_klsgeneral']=$this->model_kelas->get_struktur('trans_klsgeneral');
		$data['guru']=$this->model_kelas->get_struktur('guru');
		$data['periode']=$this->model_kelas->get_struktur('periode');

		$this->template->display('kelas/kelas', $data);
	}

	function get_tkelasGen($id_kelasGen)
	{
		$id_kelasGen=$this->input->post('id_kelasGen');

		$res=$this->model_kelas->get_tkelasGen($id_kelasGen);

		echo '<option value="">- Pilih Kelas -</option>';
		if($res)
		{
			foreach ($res as $row) {
				echo "<option value=".$row['id_trans_klsgeneral'];
				echo ">".$row['nama_kelas']."</option>";
			}
		}
	}

	function enrol_kelas ()
	{
		$data = array(
			'id_tklsgeneralKey' => $this->input->post('tkelasGen'),
			'id_guruWali' => $this->input->post('id_guruWali'),
			'id_periode' => $this->session->userdata('periode_aktif'),
		);

		$this->model_kelas->tambahKelas($data);
		$this->model_kelas->update_tipeEnrol($data['id_guruWali']);
		redirect('kelas');
	}

	function tambah_katalog ()
	{
		$data = array(
			'id_kelasGen' => $this->input->post('id_kelasGen2'),
			'nama_kelas' => $this->input->post('nama_kelas'),
		);
		$this->model_kelas->tambahKatalog($data);
		redirect('kelas');
	}

	function tambah_kategori ()
	{
		$data = array(
			'id_kelasGen' => $this->input->post('id_kelasGen1'),
			'nama_kelasGen' => $this->input->post('nama_kelasGen'),
			'kelas_angka' => $this->input->post('kelas_angka'),
			'smt_ganjil' => $this->input->post('smt_ganjil'),
			'smt_genap' => $this->input->post('smt_genap'),
			'status_aksel' => $this->input->post('status_aksel'),
		);
		$this->model_kelas->tambahKategori($data);
		redirect('kelas');
	}

	function detail ($id_transKelas)
	{
		$data['detail']=$this->model_kelas->get_detail($id_transKelas, 'enrol');
		$data['siswa']=$this->model_kelas->get_siswa($id_transKelas);
		
		$siswaAktif = $this->model_kelas->get_siswaAktif();
		$data['siswa_available']=$this->model_kelas->get_siswaFree($siswaAktif);
		$data['id_transKelas'] = $id_transKelas;

		$this->template->display('kelas/kelas_detail', $data);
	}

	function delete ($pilihan, $key)
	{
		$data['pilihan']=$pilihan;
		$data['key']=$key;

		$this->model_kelas->delete_kelas($data);

		redirect('kelas');
	}

	function delete_siswa ($id_transKelas, $id_transSisKls)
	{
		$data['siswa']=$this->model_kelas->delete_siswa($id_transSisKls);

		redirect("kelas/detail/".$id_transKelas);
	}

	function tambah_siswa ($id_transKelas)
	{
		$data = $this->input->post('pilih');

		$this->model_kelas->insert_siswa($data, $id_transKelas);
		redirect("kelas/detail/".$id_transKelas);
	}

	function edit_enrol()
	{
		$id_transKelas = $this->input->post('id_transKelas');
		$detail=$this->model_kelas->get_detail($id_transKelas, 'enrol');
		$guru=$this->model_kelas->get_struktur('guru');
		
		/*Kelompok Kelas*/
		echo '<div class="form-group">';
		echo form_label('Kelompok Kelas', '', array(
			    'class' => 'control-label',
			));

		echo '<select disabled name="" id="" class="form-control">
				<option value="'.$detail[0]['id_kelasGen'].'">'.$detail[0]['id_kelasGen'].'</option>
				</select>
		</div>';

		/*Nama Kelas*/
		echo '<div class="form-group">';
		echo form_label('Nama Kelas', '', array(
			    'class' => 'control-label',
			));

		echo '<select disabled name="" id="" class="form-control">
				<option value="'.$detail[0]['nama_kelas'].'">'.$detail[0]['nama_kelas'].'</option>
				</select>
		</div>';

		/*Guru Wali*/
		echo '<div class="form-group">';
		echo form_label('Wali Kelas', 'id_guruWaliEdit', array(
			    'class' => 'control-label',
			));
		echo '<select name="id_guruWaliEdit" id="id_guruWaliEdit" class="form-control">';
		if ($guru){
			foreach ($guru as $row) {
				if($row['id_guru']==$detail[0]['id_guruWali'])
					echo '<option selected value="'.$row['id_guru'].'">'.$row['nama_guru'].'</option>';
				else
					echo '<option value="'.$row['id_guru'].'">'.$row['nama_guru'].'</option>';
			}
		}

		echo '</select>
				<input style="display:none" name="id_guruWaliAwal" value="'.$detail[0]['id_guruWali'].'">
				<input style="display:none" name="id_transKelas" value="'.$detail[0]['id_transKelas'].'">
				</div>';

	}

	function do_edit_enrol()
	{
		$data['id_transKelas'] = $this->input->post('id_transKelas');
		$data['id_guruWaliEdit'] = $this->input->post('id_guruWaliEdit');
		$data['id_guruWaliAwal'] = $this->input->post('id_guruWaliAwal');

		if ($data['id_guruWaliEdit']!=$data['id_guruWaliAwal'])
		{
			$data['guru_awalKelasLain'] = $this->model_kelas->get_kelasLain($data['id_guruAwal'], $data['id_transKelas']);
			$data['guru_awalMapel'] = $this->model_kelas->get_ajarMapel($data['id_guruAwal']);
	        $data['guru_awalPede'] = $this->model_kelas->get_ajarPede($data['id_guruAwal']);

			$this->model_kelas->update_enrol($data);
		}
		redirect('kelas');
	}

	function edit_katalog()
	{
		$id_trans_klsgeneral = $this->input->post('id_trans_klsgeneral');
		$detail=$this->model_kelas->get_detail($id_trans_klsgeneral, 'katalog');

		/*Kategori Kelas*/
		echo '<div class="form-group">';
		echo form_label('Kategori', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input disabled type="text" name="" value="'.$detail[0]['id_kelasGen'].'">
			</div>';

		/*Nama Kelas*/
		echo '<div class="form-group">';
		echo form_label('Nama Kelas', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input type="text" name="nama_kelas" value="'.$detail[0]['nama_kelas'].'">
			<input style="display:none" name="id_trans_klsgeneral" value="'.$detail[0]['id_trans_klsgeneral'].'">
			</div>';
	}

	function do_edit_katalog()
	{
		$data['id_trans_klsgeneral'] = $this->input->post('id_trans_klsgeneral');
		$data['nama_kelas'] = $this->input->post('nama_kelas');
		$this->model_kelas->update_katalog($data);
		redirect('kelas');
	}

	function edit_kategori()
	{
		$id_kelasGen = $this->input->post('id_kelasGen');
		$detail=$this->model_kelas->get_detail($id_kelasGen, 'kategori');

		/*Kategori Kelas*/
		echo '<div class="form-group">';
		echo form_label('Kategori', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input disabled type="text" name="" value="'.$detail[0]['id_kelasGen'].'"><br>';
		echo form_label('dalam angka', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input style="width:20%" type="number" min="0" name="kelas_angka" value="'.$detail[0]['kelas_angka'].'">';

		if($detail[0]['status_aksel'] == 1)
		{
			echo '<br><input type="checkbox" checked class="minimal" name="status_aksel" value="1">
	              <b>Kelas Akselerasi</b>
	            </input>';
		}
		else
		{
			echo '<br><input type="checkbox" class="minimal" name="status_aksel" value="1">
	              <b>Kelas Akselerasi</b>
	            </input>';
		}
		echo '</div>';

		/*Nama Kategori Kelas*/
		echo '<div class="form-group">';
		echo form_label('Nama Kategori Kelas', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input type="text" name="nama_kelasGen" value="'.$detail[0]['nama_kelasGen'].'">
			</div>';

		/*Semester*/
		echo '<div class="form-group">';
		echo form_label('Semester Ganjil', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input style="width:20%" type="number" min="0" name="smt_ganjil" value="'.$detail[0]['smt_ganjil'].'"><br>';
		echo form_label('Semester Genap', '', array(
			    'class' => 'control-label',
			));
		echo '<br><input style="width:20%" type="number" min="0" name="smt_genap" value="'.$detail[0]['smt_genap'].'">
			<input style="display:none" type="text" name="id_kelasGen" value="'.$detail[0]['id_kelasGen'].'">
			</div>';
	}

	function do_edit_kategori()
	{
		$data['id_kelasGen'] = $this->input->post('id_kelasGen');
		$data['kelas_angka'] = $this->input->post('kelas_angka');
		$data['status_aksel'] = $this->input->post('status_aksel');
		$data['nama_kelasGen'] = $this->input->post('nama_kelasGen');
		$data['smt_ganjil'] = $this->input->post('smt_ganjil');
		$data['smt_genap'] = $this->input->post('smt_genap');
		$this->model_kelas->update_kategori($data);
		//echo $data['id_kelasGen'];
		redirect('kelas');
	}
}

?>