<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Siswa
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Siswa</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

	<?php if ($this->session->userdata('tipe') <= 1) { ?>
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#tambahSiswa">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3><?php echo count($siswa) ?></h3>
	                  <p>Tambah Siswa</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-person-add"></i>
	                </div>
	              </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
    <?php } ?>
          
  	<!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
        	<h3 class="box-title">Data Siswa</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_siswa" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
					<th style="text-align:center">NIS</th>
					<th style="text-align:center">Nama</th>
					<?php if ($this->session->userdata('tipe') == 0) { ?>
						<th style="text-align:center">Status</th>
						<th style="text-align:center">Opsi</th>
					<?php } ?>
					<th style="text-align:center">Rapor</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($siswa) {
						$baris=1;
						foreach ($siswa as $row) {
							if ($row['id_siswa']!=''){
								echo "<tr>";
								echo "<td style='text-align:center'>".($baris)."</td>";
								echo "<td style='text-align:center'><a onClick=get_detail(".$row['id_siswa'].") data-toggle='modal' data-target='#detailSiswa'>".$row['id_siswa']."</a></td>";
								echo "<td>".$row['nama_siswa']."</td>";;


								if ($this->session->userdata('tipe') == 0) {
									echo "<td>".$row['nama_status']."";
									if ($row['status_siswa']==1) {
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/non-active');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-ban-circle' data-toggle='tooltip' data-placement='top' title='Non Aktifkan'></span></a>";
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/graduated');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-education' data-toggle='tooltip' data-placement='top' title='Lulus'></span></a>";
									}
									else if ($row['status_siswa']==2) {
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/active');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-ok-circle disabled' data-toggle='tooltip' data-placement='top' title='Aktifkan'></span></a>";
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/graduated');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-education' data-toggle='tooltip' data-placement='top' title='Lulus'></span></a>";
									}
									else if ($row['status_siswa']==3) {
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/active');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-ok-circle disabled' data-toggle='tooltip' data-placement='top' title='Aktifkan'></span></a>";
										echo "<a class='pull-right' href='"; echo base_url('siswa/update_now/'.$row['id_siswa'].'/non-active');
										echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-ban-circle' data-toggle='tooltip' data-placement='top' title='Non Aktifkan'></span></a>";
									}
									echo"</td>";
									echo '<td style="text-align:center">&nbsp <span data-toggle="modal" data-target="#editSiswa"> <a onClick=edit_detail('.$row['id_siswa'].') class="btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a></span>';
									echo '&nbsp <a href="'.base_url('siswa/delete/'.$row['id_siswa']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
								}

								//echo "<td><a href='"; echo base_url('output/rapor/'.$row['id_siswa']); echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-print'></span></a></td>";
								echo '<td style="text-align:center"><a href="'.base_url('output/rapor/'.$row['id_siswa']).'" class="btn btn-sm btn-danger"> Lihat </a></td>';

								echo "</tr>";
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="tambahSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Siswa</h4>
      </div>
      <div class="modal-body">
	<?php 
		//$attributes = array('id' => 'konduite', 'name' => 'konduite');
		echo form_open('siswa/tambah_siswa');?>
		<div class="form-group">
			<?php 
			echo form_label('NIS', 'id_siswa');
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'id_siswa',
		              'id'    => 'id_siswa',
		            ));
			?>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama Lengkap', 'nama_siswa', array(
			    'class' => 'control-label',
			));
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'nama_siswa',
		              'id'    => 'nama_siswa',
		            ));
			?>
		</div>			

		<div class="form-group">
			<?php 
			echo form_label('Agama', 'id_agama', array(
			    'class' => 'control-label',
			));
			?>

			<select name="id_agama" id="id_agama" class="form-control">
				<option selected disabled value=''>- Pilih Agama -</option>
				<?php 
					if ($agama){
						foreach ($agama as $row) {
							echo "<option value='".$row['id_agama']."'";
							echo ">".$row['nama_agama']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Tahun Masuk', 'tahun_masuk', array(
			    'class' => 'control-label',
			));
			?>

			<select name="tahun_masuk" id="tahun_masuk" class="form-control">
				<?php 
					for ($i=2010; $i <= 2100; $i++) { 
						if ($i==date("Y")){
							echo "<option selected disabled value='".$i."'";
							echo ">".$i."</option>";
						}
						echo "<option value='".$i."'";
						echo ">".$i."</option>";
					}	
				?>
			</select>
		</div>

		<!--<div class="form-group">
			<?php 
			echo form_label('Kelas', 'id_kelas', array(
			    'class' => 'control-label',
			));
			?>

			<select name="id_kelasKey" id="id_kelasKey" class="form-control">
				<option value=''>- Pilih Kelas -</option>
				<?php 
					if ($kelas){
						foreach ($kelas as $row) {
							echo "<option value='".$row['id_transKelas']."'";
							echo ">".$row['nama_kelas']."</option>";
						}
					}	
				?>
			</select>
		</div>-->

		<div class="form-group">
			<?php 
			echo form_label('Status', 'status', array(
			    'class' => 'control-label',
			));
			?>

			<select name="status" id="status" class="form-control">
				<option selected disabled value=''>- Pilih Status -</option>
			    <option value="1" >Aktif</option>
			    <option value="2" >Non Aktif</option>
			    <option value="3" >Lulus</option>
			</select>
		</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- end -->

<!-- Modal2 -->
<div class="modal fade" id="detailSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Profil Siswa</h4>
      </div>
      <div class="modal-body">
      	<!-- Profile Image -->
        <div class="box-body box-profile" id='detail_siswa'>
        </div><!-- /.box-body -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div> 
<!-- end2 -->

<!-- Modal3 -->
<div class="modal fade" id="editSiswa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Profil Siswa</h4>
      </div>
      <div class="modal-body">
      	<?php echo form_open('guru/do_edit_detail'); ?>
      	<!-- Profile Image -->
        <div class="box-body box-profile" id='edit_siswa'>
        </div><!-- /.box-body -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> 
<!-- end3 -->

<script type="text/javascript">
	function get_detail($nilai){
		var id_siswa =  $nilai;
		var url = '<?php echo base_url('siswa/get_detail');?>'
		//window.alert (id_siswa);

		$.ajax
			({
			type: "POST",
			url: url,
			data: {"id_siswa":id_siswa},
			cache: false,
			success: function(html)
			{
				$("#detail_siswa").html(html);
			}
		});
	}

	function edit_detail($nilai){
		var id_siswa =  $nilai;
		var url = '<?php echo base_url('siswa/edit_detail');?>'
		//window.alert (id_siswa);

		$.ajax
			({
			type: "POST",
			url: url,
			data: {"id_siswa":id_siswa},
			cache: false,
			success: function(html)
			{
				$("#edit_siswa").html(html);
			}
		});
	}
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_siswa").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 4
        } ]
    }*/);
  });
</script>