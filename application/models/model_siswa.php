<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_siswa extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_struktur($pilihan, $data)
    {
        if ($pilihan == 'siswa')
        {
            $this->db->where('periode.id_periode', $this->session->userdata('periode_aktif'));
            $this->db->where('siswa.status_siswa = status_siswa.id_status');
            $this->db->where('siswa.tahun_masuk <= periode.tahun_awal');

            $where = "(`siswa`.`tahun_keluar` = `periode`.`tahun_awal` OR `siswa`.`tahun_keluar` IS NULL)";
            $this->db->where($where);

            //$this->db->where('siswa.tahun_keluar', NULL);
            //$this->db->or_where('siswa.tahun_keluar >= periode.tahun_awal');

            if($data['id_siswa'])
                $this->db->where('siswa.id_siswa', $data['id_siswa']);

            if ($this->session->userdata('tipe') == 2) {
                $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
                $this->db->where('trans_siswa.id_tKelasKey', $this->session->userdata('kelas_wali'));
                $query = $this->db->get('siswa, periode, status_siswa, trans_siswa');
            }
            else
                $query = $this->db->get('siswa, periode, status_siswa');
        }
        if ($pilihan == 'kelas')
        {
            $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
            $this->db->where('id_trans_klsgeneral = id_tklsgeneralKey');
            $this->db->order_by('nama_kelas', 'ASC');

            $query = $this->db->get('trans_kelas, trans_klsgeneral');
        }
        if ($pilihan == 'agama')
        {
            $query = $this->db->get('agama');
        }
        if ($pilihan == 'status_siswa')
        {
            $query = $this->db->get('status_siswa');
        }

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambah_siswa($data)
    {
        $this->db->insert('siswa', $data);
    }

    function tambah_transSiswa($data2)
    {
        $this->db->insert('trans_siswa', $data2);
    }

    function detail_siswa($data)
    {
        $this->db->where('trans_siswa.id_tkelasKey = trans_kelas.id_transKelas');
        $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
        $this->db->where('trans_kelas.id_periode = periode.id_periode');
        $this->db->where('siswa.id_agama = agama.id_agama');

        $query = $this->db->get('siswa, trans_siswa, trans_kelas, agama, periode');
    }

    function detail_siswaX($id_siswa)
    {
        $this->db->where('siswa.id_siswa', $id_siswa);
        $this->db->where('siswa.status_siswa = status_siswa.id_status');
        $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');

        
        $this->db->where('trans_siswa.id_tkelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        
        $this->db->where('trans_kelas.id_periode = periode.id_periode');
        $this->db->where('siswa.id_agama = agama.id_agama');

        $this->db->where('siswa.tahun_masuk <= periode.tahun_awal');

        $where = "(`siswa`.`tahun_keluar` <= `periode`.`tahun_akhir` OR `siswa`.`tahun_keluar` IS NULL)";
        $this->db->where($where);

        $query = $this->db->get('siswa, trans_siswa, trans_kelas, trans_klsgeneral, agama, periode, status_siswa');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function update_status($data)
    {
        for ($i=0; $i < count($data['id_siswa']); $i++) { 
            $this->db->set('status_siswa', $data['status_siswa'][$i]);
            $this->db->where('id_siswa', $data['id_siswa'][$i]);
            $this->db->update('siswa');
        }
    }

    function update_status1($data)
    {
        $this->db->set('status_siswa', $data['status_siswa']);
        $this->db->where('id_siswa', $data['id_siswa']);
        $this->db->set('tahun_keluar', substr($this->session->userdata('periode_aktif'), 0, 4));
        $this->db->update('siswa');
        //echo $this->db->last_query() . '<br>';
    }

    function update_detail($data)
    {
        $this->db->set('nama_siswa', $data['nama_siswa']);
        $this->db->where('id_siswa', $data['id_siswa']);
        $this->db->update('siswa');
    }

    function delete_siswa($data)
    {
        $this->db->set('status_siswa', $data['status_siswa']);
        $this->db->where('id_siswa', $data['id_siswa']);
        $id_del = $data['id_siswa'].'_del';
        $this->db->set('id_siswa', $id_del);
        $this->db->update('siswa');
    }
}
