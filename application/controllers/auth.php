<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Auth extends CI_Controller
{
	function __construct()
    {
        parent:: __construct();
        $this->load->model('model_auth');
    }

	public function index()
	{
		if(($this->session->userdata('logged_in')))
  		{
		   if ($this->session->userdata('tipe') == 0) 
		   {
		   	redirect ('siswa');
		   }
		   elseif ($this->session->userdata('tipe') == 1) 
		   {
		   	redirect ('siswa');
		   }
		   elseif ($this->session->userdata('tipe') == 2) 
		   {
		   	redirect ('siswa');
		   }
		   elseif ($this->session->userdata('tipe') == 3) 
		   {
		   	redirect ('nilai_mapel');
		   }
		   elseif ($this->session->userdata('tipe') == 4) 
		   {
		   	redirect ('nilai_pede');
		   }
		}
		else {
		   $this->login();
		}
	}
        
	public function login()
	{
        $data=array();
    	$data['Error'] = null;
    	
		if ($this->input->post('id_user') & $this->input->post('password')){
				$data2 = array(
					'id_user' => $this->input->post('id_user'),
					//'Password' => md5($this->input->post('password')),
					'password' => ($this->input->post('password')),
					);
				$user = $this->model_auth->get_user($data2);

				$periode_aktif = $this->model_auth->get_periodeAktif();
				$res = $this->model_auth->get_periodeAktifDet();
				foreach ($res as $row) {
					$periode_aktifDet = $row['id_transPeriode'];
					$periode_tes = $row['nama_tes'];
				}
				$footer = $this->model_auth->get_footer();
				$konversi = $this->model_auth->get_konversi();

				if ($user){
					$newdata = array(
					   'id_guru'    => $user[0]['id_guru'],
					   'nama_guru'   => $user[0]['nama_guru'],
					   'nama_pendek'   => $user[0]['nama_pendek'],
					   'tipe'   => $user[0]['tipe'],
					   'periode_aktif' => $periode_aktif,
					   'periode_aktifDet' => $periode_aktifDet,
					   'periode_tes' => $periode_tes,
					   'footer' => $footer,
					   'konversi' => $konversi,
					   'logged_in' => TRUE,
					);
					$this->session->set_userdata($newdata);

					if ($user[0]['tipe']==0){
                        redirect ('siswa');
                    }
					if ($user[0]['tipe']==1){
                        redirect ('siswa');
                    }
                    else if ($user[0]['tipe']==2){
                    	$kelas_wali = $this->model_auth->get_kelasWali($user[0]['id_guru'], $periode_aktif);
                    	$this->session->set_userdata('kelas_wali', $kelas_wali[0]['id_transKelas']);
                    	$this->session->set_userdata('kelas_gen', $kelas_wali[0]['id_kelasGen']);
                    	$this->session->set_userdata('tkelas_gen', $kelas_wali[0]['id_trans_klsgeneral']);
                        redirect ('siswa');
                    }
                    else if ($user[0]['tipe']==3){
                        redirect ('nilai_mapel');
                    }
                    else if ($user[0]['tipe']==4){
                        redirect ('nilai_pede');
                    }
				}
				else{
					$data['Error']='Username atau password salah!';
					$this->load->view('login', $data);
				}
			}
			else
				$this->load->view('login', $data);
	}
        
    public function logout()
	{
        $newdata = array(
		'user_id'   =>'',
		'user_name'  =>'',
		'user_email'     => '',
		'logged_in' => FALSE,
		);
		//$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		//session_destroy();;
        redirect('login');
	}
}

?>