<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Output extends CI_Controller
{	
  function __construct()
  {
      parent:: __construct();
      
      $this->load->model('model_output');
  }

	function index ()
	{
        
	}

  function rapor ($id_siswa, $id_periode=null, $id_periodeDet=null, $mode=null, $pilihan=null)
  {
      $data['id_siswa'] = $id_siswa;
      $data['thn_ajaran'] = $this->model_output->get_thnAjaran($data['id_siswa']);

      if ($mode == null) {
        $data['mode'] = "mode_ip";
      }
      else {
        $data['mode'] = $mode;
      }

      if ($id_periode == null) {
          $data['id_periode'] = $this->session->userdata('periode_aktif');
          $data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');
      }
      else
      {
          $data['id_periode'] = $id_periode;
          $data['id_periodeDet'] = $id_periodeDet;
      }
      
      $data['nilai'] = $this->model_output->get_nilai($data);
      $data['total'] = $this->model_output->get_totalNilaiR($data);
      $data['pede'] = $this->model_output->get_pede($data);
      $data['catatan'] = $this->model_output->get_catatan($data);

      $data['identitas'] = $this->model_output->get_identitas($data);

      if ($data['identitas'])
      {
          foreach ($data['identitas'] as $row) {
              $data['id_siswa'] = $row['id_siswa'];
              $data['nama_siswa'] = $row['nama_siswa'];
              $data['nama_kelas'] = $row['nama_kelas'];
              $data['id_wali'] = $row['id_wali'];
              $data['nama_wali'] = $row['nama_wali'];
              $data['id_kepsek'] = $row['id_kepsek'];
              $data['nama_kepsek'] = $row['nama_kepsek'];
              $data['id_periode'] = $row['id_periode'];

              if((strpos($data['id_periodeDet'],"uts1") || strpos($data['id_periodeDet'],"uas1")) !== false)
                $data['semester'] = $row['smt_ganjil'];
              else if((strpos($data['id_periodeDet'],"uts2") || strpos($data['id_periodeDet'],"uas2")) !== false)
                $data['semester'] = $row['smt_genap'];

              $tanggal_rapor = strtotime ($row['tanggal_rapor']);
              $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
              $data['tanggal_rapor'] = date("j", $tanggal_rapor). " " . $bulan[date("n", $tanggal_rapor)] . " " . date("Y", $tanggal_rapor);
              //$data['tanggal_rapor'] = date("j  Y", $tanggal_rapor);
          }
      }
      else
      {
          $data['id_siswa'] = null;
          $data['nama_siswa'] = null;
          $data['nama_kelas'] = null;
          $data['id_wali'] = null;
          $data['nama_wali'] = null;
          $data['id_kepsek'] = null;
          $data['nama_kepsek'] = null;
          $data['id_periode'] = null;
          $data['semester'] = null;
          $data['tanggal_rapor'] = null;
      }

      if($data['catatan'])
      {
        foreach ($data['catatan'] as $row) {
          $data['jml_sakit'] = $row['jml_sakit'];
          $data['jml_izin'] = $row['jml_izin'];
          $data['jml_tketerangan'] = $row['jml_tketerangan'];
          $data['isi_catatan'] = $row['isi_catatan'];
          $data['status_kenaikan'] = $row['status_kenaikan'];
          $data['kelas_lanjut'] = $row['kelas_lanjut'];
          $rapat_dewanGuru = strtotime ($row['rapat_dewanGuru']);
          $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
          $data['rapat_dewanGuru'] = date("j", $rapat_dewanGuru). " " . $bulan[date("n", $rapat_dewanGuru)] . " " . date("Y", $rapat_dewanGuru);
          if(strpos($data['id_periodeDet'],"uas2") !== false)
            $data['kelas_lanjut'] = $row['nama_kelas'];
        }
      }
      else
      {
        $data['jml_sakit'] = null;
        $data['jml_izin'] = null;
        $data['jml_tketerangan'] = null;
        $data['isi_catatan'] = null;
      }

      if($pilihan)
      {
          $this->rapor_pdf($data);
      }

      if(strpos($data['id_periodeDet'],"uts") !== false) {
          $this->template->display('output/raporUTS', $data);
      } else if(strpos($data['id_periodeDet'],"uas1") !== false) {
          $this->template->display('output/raporUAS1', $data);
      } else if(strpos($data['id_periodeDet'],"uas2") !== false) {
          $this->template->display('output/raporUAS2', $data);
      }
  }

  function rapor_pdf($data)
  {
      $this->load->library('pdf');

      $pdf = new TCPDF();

      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('CHIS');
      $pdf->SetTitle('Rapor');
      $pdf->SetSubject('Rapor');
      //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
       
      // remove default header/footer
      $pdf->setPrintHeader(false);
      $pdf->setPrintFooter(false);
       
      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
       
      //set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
       
      //set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
       
      //set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
       
      //set some language-dependent strings
      //$pdf->setLanguageArray(null);
       
      // ---------------------------------------------------------
       
      // set font
      $pdf->SetFont('times', '', 12);
       
      // add a page
      $pdf->AddPage();

      $header = null;
      $body = null;
      $footer = null;

      $header .= '<b><table width="100%"  border="0" cellpadding="2" cellspacing="1">                
                <tr>
                  <td width="22%">Nama Peserta Didik</td>
                  <td width="34%">: '.$data['nama_siswa'].' </td>
                  <td width="20%">Kelas/Semester</td>
                  <td width="22%">: '.$data['nama_kelas'].' / '.$data['semester'].' </td>
                </tr>
                <tr>
                  <td>Nomor Induk</td>
                  <td>: '.$data['id_siswa'].' </td>
                  <td>Tahun Pelajaran</td>
                  <td>: '.$data['id_periode'].' </td>
                </tr>
                <tr>
                  <td>Nama Sekolah</td>
                  <td>: SMA CHIS Denpasar</td>
                  <td></td>
                  <td></td>
                </tr>
              </table></b>';

      $footer .= '<b><table width="100%"  border="0" cellpadding="2" cellspacing="5">
                <tr>
                  <td width="28%" >&nbsp;</td>
                  <td width="32%" >&nbsp;</td>
                  <td width="40%" >&nbsp;</td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td> Denpasar, '.$data['tanggal_rapor'].'</td>
                </tr>
                <tr>
                  <td> Orang Tua/Wali <br> Peserta Didik
                  </td>
                  <td align="center"> Wali Kelas </td>
                  <td> Kepala Sekolah </td>
                </tr>
                <tr>
                  <td colspan="3" height="40px"></td>
                </tr>
                <tr>
                  <td> __________________</td>
                  <td> <u>'.$data['nama_wali'].'</u></td>
                  <td> <u>'.$data['nama_kepsek'].'</u></td>
                </tr>
                <tr nobr="true">
                  <td> </td>
                  <td> NIP. '.$data['id_wali'].'</td>
                  <td> NIP. '.$data['id_kepsek'].'</td>
                </tr>
              </table></b>';

      $barisSikap = $barisSikapMuatan = 1;
      $sikap_antarMapel = null;
      $jml_nilai_pengetahuan = null;
      $jml_nilai_keterampilan = null;
      $rata2_nilai_pengetahuan = null;
      $rata2_nilai_keterampilan = null;
      $ranking = null;
      $sikap_antarMapelMuatan = null;
      $tabelA = null;
      $tabelB = null;
      $tabelC = null;
      $tabelD = null;
      $tabelE = null;

      if($data['total']) {
        foreach ($data['total'] as $row) {
            if ($row['status_muatan'] == 0) {
              if ($data['mode']=="mode_ip")
              {
                $jml_nilai_pengetahuan = $row['jml_nilai_pengetahuan'];
                $jml_nilai_keterampilan = $row['jml_nilai_keterampilan'];
                $rata2_nilai_pengetahuan = $row['rata2_nilai_pengetahuan'];
                $rata2_nilai_keterampilan = $row['rata2_nilai_keterampilan'];
              }
              elseif ($data['mode']=="mode_reg")
              {
                $jml_nilai_pengetahuan = $row['jml_pengetahuan'];
                $jml_nilai_keterampilan = $row['jml_keterampilan'];
                $rata2_nilai_pengetahuan = $row['rata2_pengetahuan'];
                $rata2_nilai_keterampilan = $row['rata2_keterampilan'];
              }
                $ranking = $row['ranking'];
                $sikap_antarMapel = $row['sikap_antarMapel'];
            }
            else {
                $sikap_antarMapelMuatan = $row['sikap_antarMapel'];
            }
        }
      }
      if (strpos($data['nama_kelas'],"IPA") !== false)
          $tabelC.= '<tr nobr="true"><td colspan="8"><b>Kelompok Peminatan Matematika dan Ilmu Alam</b></td></tr>';
      if (strpos($data['nama_kelas'],"IPS") !== false)
          $tabelC.= '<tr nobr="true"><td colspan="8"><b>Kelompok Peminatan Ilmu Sosial</b></td></tr>';
      if (strpos($data['nama_kelas'],"Bahasa") !== false)
          $tabelC.= '<tr nobr="true"><td colspan="8"><b>Kelompok Peminatan Ilmu Bahasa</b></td></tr>';

      if ($data["nilai"]) {
          $barisA = $barisB = $barisC = $barisD = $barisE = 1;
          foreach ($data["nilai"] as $row) {
              if ($row["id_kelompok"]=="A"){
                $tabelA .= '<tr nobr="true">';
                $tabelA .= '<td align="center" width="6%">'.($barisA).'</td>';
                $tabelA .= '<td width="31%">'.$row["nama_mapel"].'</td>';
                
                if ($data["mode"]=="mode_ip")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelA .= '<td align="center" width="8%"><b>'.number_format($row["kkm_aksel"],2,".","").'</b></td>';
                  else
                    $tabelA .= '<td align="center" width="8%"><b>'.number_format($row["kkm"],2,".","").'</b></td>';

                  $tabelA .= '<td align="center" width="8%">'.number_format($row["nilai_pengetahuan"],2,".","").'</td>';
                  $tabelA .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelA .= '<td align="center" width="8%">'.number_format($row["nilai_keterampilan"],2,".","").'</td>';
                  $tabelA .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelA .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                elseif ($data["mode"]=="mode_reg")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelA .= '<td align="center" width="8%"><b>'.$row["kkm_aksel100"].'</b></td>';
                  else
                    $tabelA .= '<td align="center" width="8%"><b>'.$row["kkm100"].'</b></td>';

                  $tabelA .= '<td align="center" width="8%">'.$row["pengetahuan"].'</td>';
                  $tabelA .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelA .= '<td align="center" width="8%">'.$row["keterampilan"].'</td>';
                  $tabelA .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelA .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                $tabelA .= '</tr>';
                $barisA++;
                $barisSikap++;
              }
              if ($row["id_kelompok"]=="B"){
                $tabelB .= '<tr nobr="true">';
                $tabelB .= '<td align="center" width="6%">'.($barisB).'</td>';
                $tabelB .= '<td width="31%">'.$row["nama_mapel"].'</td>';
                
                if ($data["mode"]=="mode_ip")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelB .= '<td align="center" width="8%"><b>'.number_format($row["kkm_aksel"],2,".","").'</b></td>';
                  else
                    $tabelB .= '<td align="center" width="8%"><b>'.number_format($row["kkm"],2,".","").'</b></td>';

                  $tabelB .= '<td align="center" width="8%">'.number_format($row["nilai_pengetahuan"],2,".","").'</td>';
                  $tabelB .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelB .= '<td align="center" width="8%">'.number_format($row["nilai_keterampilan"],2,".","").'</td>';
                  $tabelB .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelB .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                elseif ($data["mode"]=="mode_reg")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelB .= '<td align="center" width="8%"><b>'.$row["kkm_aksel100"].'</b></td>';
                  else
                    $tabelB .= '<td align="center" width="8%"><b>'.$row["kkm100"].'</b></td>';

                  $tabelB .= '<td align="center" width="8%">'.$row["pengetahuan"].'</td>';
                  $tabelB .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelB .= '<td align="center" width="8%">'.$row["keterampilan"].'</td>';
                  $tabelB .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelB .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                $tabelB .= '</tr>';
                $barisB++;
                $barisSikap++;
              }
              if (strpos($row["id_kelompok"],"C") !== false){
                $tabelC .= '<tr nobr="true">';
                $tabelC .= '<td align="center" width="6%">'.($barisC).'</td>';
                $tabelC .= '<td width="31%">'.$row["nama_mapel"].'</td>';
                
                if ($data["mode"]=="mode_ip")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelC .= '<td align="center" width="8%"><b>'.number_format($row["kkm_aksel"],2,".","").'</b></td>';
                  else
                    $tabelC .= '<td align="center" width="8%"><b>'.number_format($row["kkm"],2,".","").'</b></td>';

                  $tabelC .= '<td align="center" width="8%">'.number_format($row["nilai_pengetahuan"],2,".","").'</td>';
                  $tabelC .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelC .= '<td align="center" width="8%">'.number_format($row["nilai_keterampilan"],2,".","").'</td>';
                  $tabelC .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelC .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                elseif ($data["mode"]=="mode_reg")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelC .= '<td align="center"><b>'.$row["kkm_aksel100"].'</b></td>';
                  else
                    $tabelC .= '<td align="center"><b>'.$row["kkm100"].'</b></td>';

                  $tabelC .= '<td align="center" width="8%">'.$row["pengetahuan"].'</td>';
                  $tabelC .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelC .= '<td align="center" width="8%">'.$row["keterampilan"].'</td>';
                  $tabelC .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelC .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                $tabelC .= '</tr>';
                $barisC++;
                $barisSikap++;
              }
              if (strpos($row["id_kelompok"],"D") !== false){
                $tabelD .= '<tr nobr="true">';
                $tabelD .= '<td align="center" width="6%">'.($barisD).'</td>';
                $tabelD .= '<td width="31%">'.$row["nama_mapel"].'</td>';
                
                if ($data["mode"]=="mode_ip")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelD .= '<td align="center" width="8%"><b>'.number_format($row["kkm_aksel"],2,".","").'</b></td>';
                  else
                    $tabelD .= '<td align="center" width="8%"><b>'.number_format($row["kkm"],2,".","").'</b></td>';

                  $tabelD .= '<td align="center" width="8%">'.number_format($row["nilai_pengetahuan"],2,".","").'</td>';
                  $tabelD .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelD .= '<td align="center" width="8%">'.number_format($row["nilai_keterampilan"],2,".","").'</td>';
                  $tabelD .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelD .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                elseif ($data["mode"]=="mode_reg")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelD .= '<td align="center"><b>'.$row["kkm_aksel100"].'</b></td>';
                  else
                    $tabelD .= '<td align="center"><b>'.$row["kkm100"].'</b></td>';

                  $tabelD .= '<td align="center" width="8%">'.$row["pengetahuan"].'</td>';
                  $tabelD .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelD .= '<td align="center" width="8%">'.$row["keterampilan"].'</td>';
                  $tabelD .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelD .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                $tabelD .= '</tr>';
                $barisD++;
                $barisSikap++;
              }
              if (strpos($row["id_kelompok"],"E") !== false){
                $tabelE .= '<tr nobr="true">';
                $tabelE .= '<td align="center" width="6%">'.($barisE).'</td>';
                $tabelE .= '<td width="31%">'.$row["nama_mapel"].'</td>';
                
                if ($data["mode"]=="mode_ip")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelE .= '<td align="center" width="8%"><b>'.number_format($row["kkm_aksel"],2,".","").'</b></td>';
                  else
                    $tabelE .= '<td align="center" width="8%"><b>'.number_format($row["kkm"],2,".","").'</b></td>';

                  $tabelE .= '<td align="center" width="8%">'.number_format($row["nilai_pengetahuan"],2,".","").'</td>';
                  $tabelE .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelE .= '<td align="center" width="8%">'.number_format($row["nilai_keterampilan"],2,".","").'</td>';
                  $tabelE .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelE .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                elseif ($data["mode"]=="mode_reg")
                {
                  if (strpos($data["nama_kelas"],"Akselerasi") !== false)
                    $tabelE .= '<td align="center" width="8%"><b>'.$row["kkm_aksel100"].'</b></td>';
                  else
                    $tabelE .= '<td align="center" width="8%"><b>'.$row["kkm100"].'</b></td>';

                  $tabelE .= '<td align="center" width="8%">'.$row["pengetahuan"].'</td>';
                  $tabelE .= '<td align="center" width="11%">'.$row["predikat_pengetahuan"].'</td>';
                  $tabelE .= '<td align="center" width="8%">'.$row["keterampilan"].'</td>';
                  $tabelE .= '<td align="center" width="11%">'.$row["predikat_keterampilan"].'</td>';
                  $tabelE .= '<td align="center" width="9%">'.$row["predikat_sikap"].'</td>';
                }
                $tabelE .= '</tr>';
                $barisE++;
                $barisSikapMuatan++;
              }
            }
      }

      //Mencetak br agar baris antar mapel di tengah
      $barisSikap += 12; $barisSikapMuatan += 2;
      $brAntarMapel = $brAntarMapelMuatan = null;
      for ($i=0; $i < (intval($barisSikap/2)); $i++) { 
        $brAntarMapel .= '<br>';
      }
      for ($i=0; $i < (intval($barisSikapMuatan/2)); $i++) { 
        $brAntarMapelMuatan .= '<br>';
      }

      // Halaman nilai ranking
      $body .= '<br><b>Capaian Kompetensi Peserta Didik</b><br><table border="0.1" cellspacing="0" cellpadding="5" width="100%">
                    <thead>
                      <tr>
                        <td align="center" width="6%" rowspan="3"><b><br><br>No.</b></td>
                        <td align="center" width="31%" rowspan="3"><b><br><br>Mata Pelajaran</b></td>
                        <td align="center" width="63%" colspan="7"><b>Nilai Hasil Belajar</b></td>
                      </tr>
                      <tr>
                          <td align="center" width="27%" colspan="3"><b>Pengetahuan</b></td>
                          <td align="center" width="19%" colspan="2"><b>Keterampilan</b></td>
                          <td align="center" width="17%" colspan="2"><b>Sikap</b></td>
                      </tr>
                      <tr>
                          <td align="center" width="8%"><b>KKM</b></td>
                          <td align="center" width="8%"><b>Nilai</b></td>
                          <td align="center" width="11%"><b>Predikat</b></td>
                          <td align="center" width="8%"><b>Nilai</b></td>
                          <td align="center" width="11%"><b>Predikat</b></td>
                          <td align="center" width="9%"><b>Dalam Mapel</b></td>
                          <td align="center" width="8%"><b>Antar Mapel</b></td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr nobr="true">
                          <td width="92%" colspan="8"><b>KELOMPOK A</b></td>
                          <td align="center" width="8%" rowspan="12">'.$brAntarMapel.$sikap_antarMapel.'
                          </td>
                      </tr>
                      '.$tabelA.$tabelA.$tabelA.$tabelA.$tabelA.$tabelA.$tabelA.$tabelA.$tabelA.'
                      <tr>
                          <td colspan="8"><b>KELOMPOK B</b></td>
                      </tr>
                      '.$tabelB.'
                      <tr>
                          <td colspan="8"><b>KELOMPOK C Peminatan</b></td>
                      </tr>
                      '.$tabelC.'                    
                      <tr>
                          <td colspan="8"><b>KELOMPOK LINTAS PEMINATAN</b></td>
                      </tr>
                      '.$tabelD.'                    
                      <tr>
                          <td colspan="2"><b>JUMLAH</b></td>
                          <td></td>
                          <td align="center">'.$jml_nilai_pengetahuan.'</td>
                          <td></td>
                          <td align="center">'.$jml_nilai_keterampilan.'</td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td colspan="2"><b>RATA-RATA</b></td>
                          <td></td>
                          <td align="center">'.$rata2_nilai_pengetahuan.'</td>
                          <td></td>
                          <td align="center">'.$rata2_nilai_keterampilan.'</td>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                          <td colspan="2"><b>RANKING</b></td>
                          <td></td>
                          <td align="center" colspan="5">'.$ranking.'</td>
                      </tr>
                    </tbody>
                  </table>
              <br>&nbsp;&nbsp;*) Muatan Lokal Provinsi Bali<br><br>';

      // output the HTML content
      $pdf->writeHTML($header, true, false, true, false, '');
      $pdf->writeHTML($body, true, false, true, false, '');
      $pdf->writeHTML($footer, true, false, true, false, '');
      $pdf->AddPage();

      // Halaman nilai Muatan Lokal
      $body = '<table border="0.1" cellspacing="0" cellpadding="5" width="100%">
                <thead>
                    <tr>
                      <td align="center" width="6%" rowspan="3"><b><br><br>No.</b></td>
                      <td align="center" width="31%" rowspan="3"><b><br><br>Mata Pelajaran</b></td>
                      <td align="center" width="63%" colspan="7"><b>Nilai Hasil Belajar</b></td>
                    </tr>
                    <tr>
                        <td align="center" width="27%" colspan="3"><b>Pengetahuan</b></td>
                        <td align="center" width="19%" colspan="2"><b>Keterampilan</b></td>
                        <td align="center" width="17%" colspan="2"><b>Sikap</b></td>
                    </tr>
                    <tr>
                        <td align="center" width="8%"><b>KKM</b></td>
                        <td align="center" width="8%"><b>Nilai</b></td>
                        <td align="center" width="11%"><b>Predikat</b></td>
                        <td align="center" width="8%"><b>Nilai</b></td>
                        <td align="center" width="11%"><b>Predikat</b></td>
                        <td align="center" width="9%"><b>Dalam Mapel</b></td>
                        <td align="center" width="8%"><b>Antar Mapel</b></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td width="92%" colspan="8"><b>MUATAN LOKAL</b></td>
                      <td align="center" width="8%" rowspan="'.$barisSikapMuatan.'">'.$brAntarMapelMuatan.$sikap_antarMapelMuatan.'</td>
                    </tr>
                    '.$tabelE.'
                  </tbody>
              </table><br><br>';

      // output the HTML content
      $pdf->writeHTML($header, true, false, true, false, '');
      $pdf->writeHTML($body, true, false, true, false, '');
      $pdf->writeHTML($footer, true, false, true, false, '');
      $pdf->AddPage();

      if(strpos($data['id_periodeDet'],"uas") !== false) {
        $halA = $halB = $halC= $halD = $halE = null;
        if ($data['nilai']) {
        $barisA= $barisB= $barisC= $barisD= $barisE= 1;
          foreach ($data['nilai'] as $row) {
            if ($row['id_kelompok']=='A'){
              $halA .= '<tr>';
              $halA .= '<td align="center" width="6%"  rowspan="3">'.($barisA).'</td>';
              $halA .= '<td width="31%" rowspan="3">'.$row['nama_mapel'].'</td>';
              $halA .= '<td width="25%">Pengetahuan</td>';
              $halA .= '<td width="38%">'.$row['des_peng'].'</td>';
              $halA .= "</tr>";

              $halA .= '<tr>';
              $halA .= '<td>Keterampilan</td>';
              $halA .= '<td>'.$row['des_ket'].'</td>';
              $halA .= '</tr>';

              $halA .= '<tr>';
              $halA .= '<td>Sikap sosial dan spiritual</td>';
              $halA .= '<td>'.$row['des_sik'].'</td>';
              $halA .= '</tr>';
              $barisA++;
            }
            if ($row['id_kelompok']=='B'){
              $halB .= '<tr>';
              $halB .= '<td align="center" width="6%" rowspan="3">'.($barisB).'</td>';
              $halB .= '<td width="31%" rowspan="3">'.$row['nama_mapel'].'</td>';
              $halB .= '<td width="25%">Pengetahuan</td>';
              $halB .= '<td width="38%">'.$row['des_peng'].'</td>';
              $halB .= '</tr>';

              $halB .= '<tr>';
              $halB .= '<td>Keterampilan</td>';
              $halB .= '<td>'.$row['des_ket'].'</td>';
              $halB .= '</tr>';

              $halB .= '<tr>';
              $halB .= '<td>Sikap sosial dan spiritual</td>';
              $halB .= '<td>'.$row['des_sik'].'</td>';
              $halB .= '</tr>';
              $barisB++;
            }
            if ($row['id_kelompok']=='C'){
              $halC .= '<tr>';
              $halC .= '<td align="center" width="6%" rowspan="3">'.($barisC).'</td>';
              $halC .= '<td width="31%" rowspan="3">'.$row['nama_mapel'].'</td>';
              $halC .= '<td width="25%">Pengetahuan</td>';
              $halC .= '<td width="38%">'.$row['des_peng'].'</td>';
              $halC .= '</tr>';

              $halC .= '<tr>';
              $halC .= '<td>Keterampilan</td>';
              $halC .= '<td>'.$row['des_ket'].'</td>';
              $halC .= '</tr>';

              $halC .= '<tr>';
              $halC .= '<td>Sikap sosial dan spiritual</td>';
              $halC .= '<td>'.$row['des_sik'].'</td>';
              $halC .= '</tr>';
              $barisC++;
            }
            if ($row['id_kelompok']=='D'){
              $halD .= '<tr>';
              $halD .= '<td align="center" width="6%" rowspan="3">'.($barisD).'</td>';
              $halD .= '<td width="31%" rowspan="3">'.$row['nama_mapel'].'</td>';
              $halD .= '<td width="25%">Pengetahuan</td>';
              $halD .= '<td width="38%">'.$row['des_peng'].'</td>';
              $halD .= '</tr>';

              $halD .= '<tr>';
              $halD .= '<td>Keterampilan</td>';
              $halD .= '<td>'.$row['des_ket'].'</td>';
              $halD .= '</tr>';

              $halD .= '<tr>';
              $halD .= '<td>Sikap sosial dan spiritual</td>';
              $halD .= '<td>'.$row['des_sik'].'</td>';
              $halD .= '</tr>';
              $barisD++;
            }
            if ($row['id_kelompok']=='E'){
              $halE .=  "<tr>";
              $halE .= '<td align="center" width="6%" rowspan="3">'.($barisE).'</td>';
              $halE .= '<td width="31%" rowspan="3">'.$row['nama_mapel'].'</td>';
              $halE .= '<td width="25%">Pengetahuan</td>';
              $halE .= '<td width="38%">'.$row['des_peng'].'</td>';
              $halE .= "</tr>";

              $halE .= "<tr>";
              $halE .= "<td>Keterampilan</td>";
              $halE .= "<td>".$row['des_ket']."</td>";
              $halE .= "</tr>";

              $halE .= "<tr>";
              $halE .= "<td>Sikap sosial dan spiritual</td>";
              $halE .= "<td>".$row['des_sik']."</td>";
              $halE .= "</tr>";
              $barisE++;
            }
          }
        }
        // Halaman Kompetensi Mapel Umum
        $body = '<br><b>Deskripsi Kompetensi Dasar Peserta Didik</b><br><table border="0.1px" cellspacing="0" cellpadding="2">
                  <thead>
                    <tr>
                        <td align="center" width="6%"><b>No.</b></td>
                        <td align="center" width="31%"><b>Mata Pelajaran</b></td>
                        <td align="center" width="25%"><b>Kompetensi</b></td>
                        <td align="center" width="38%"><b>Catatan</b></td>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                        <td width="100%" colspan="4"><b>Kelompok A</b></td>
                    </tr>
                    '.$halA.'
                    <tr>
                        <td colspan="4"><b>Kelompok B</b></td>
                    </tr>
                    '.$halB.'
                    <tr>
                        <td colspan="4"><b>Kelompok C Peminatan</b></td>
                    </tr>
                    '.$halC.'
                    <tr>
                        <td colspan="4"><b>Kelompok Lintas Peminatan</b></td>
                    </tr>
                    '.$halD.'
                  </tbody>
                </table>
                <br>&nbsp;&nbsp;*) Muatan Lokal Provinsi Bali<br><br>';

        // output the HTML content
        $pdf->writeHTML($header, true, false, true, false, '');
        $pdf->writeHTML($body, true, false, true, false, '');
        $pdf->writeHTML($footer, true, false, true, false, '');
        $pdf->AddPage();

        // Halaman Kompetensi Mapel Muatan
        $body = '<table border="0.1px" cellspacing="0" cellpadding="2">
                    <tr>
                        <td align="center" width="6%"><b>No.</b></td>
                        <td align="center" width="31%"><b>Mata Pelajaran</b></td>
                        <td align="center" width="25%"><b>Kompetensi</b></td>
                        <td align="center" width="38%"><b>Catatan</b></td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="4"><b>Muatan Lokal</b></td>
                    </tr>
                    '.$halE.'
                </table>';

        // output the HTML content
        $pdf->writeHTML($header, true, false, true, false, '');
        $pdf->writeHTML($body, true, false, true, false, '');
        $pdf->writeHTML($footer, true, false, true, false, '');
        $pdf->AddPage();
      }

      // Halaman Pengembangan Diri
      $ekskul = $organisasi = null;
      if($data['pede'])
      {
        $barisE = 1;
        $barisO = 1;
        foreach ($data['pede'] as $row) {
          if ($row['tipe_pede'] == 'Ekstrakurikuler') {
            $ekskul .= "<tr>";
            $ekskul .= '<td width="5%"></td>';
            $ekskul .= '<td align="center" width="5%">'.$barisE.'</td>';
            $ekskul .= '<td width="35%">'.$row['nama_pede'].'</td>';
            $ekskul .= '<td align="center" width="10%">'.$row['nilai_pede'].'</td>';
            $ekskul .= '<td width="45%">'.$row['keterangan'].'</td>';
            $ekskul .= '</tr>';
            $barisE++;
          }
          if ($row['tipe_pede'] == 'Organisasi') {
            $organisasi .= "<tr>";
            $organisasi .= "<td></td>";
            $organisasi .= '<td align="center" width="5%">'.$barisO.'</td>';
            $organisasi .= '<td width="35%">'.$row['nama_pede'].'</td>';
            $organisasi .= '<td align="center" width="10%">'.$row['nilai_pede'].'</td>';
            $organisasi .= '<td width="45%">'.$row['keterangan'].'</td>';
            $organisasi .= '</tr>';
            $barisO++;
          }
        }
      }

      $body = '<br><br><b>Pengembangan Diri</b><br><table border="0.1px" cellspacing="0" cellpadding="2">
                  <tr>
                      <td align="center" width="5%">No.</td>
                      <td align="center" width="40%" colspan="2">Jenis Kegiatan</td>
                      <td align="center" width="10%">Nilai</td>
                      <td align="center" width="45%">Keterangan</td>
                  </tr>
                  <tr>
                      <td align="center">A</td>
                      <td colspan="2">Kegiatan Ekstrakurikuler</td>
                      <td></td>
                      <td></td>
                  </tr>
                  '.$ekskul.'
                  <tr>
                      <td align="center">B</td>
                      <td colspan="2">Keikutsertaan dalam Organisasi / Kegiatan Sekolah</td>
                      <td></td>
                      <td></td>
                  </tr>
                  '.$organisasi.'
              </table>';

      // Halaman Ketidakhadiran
      $body .= '<br><br><b>Ketidakhadiran</b><br><table border="0.1px" cellspacing="0" cellpadding="2">
                  <tr>
                      <td align="center" width="5%"><b>No.</b></td>
                      <td align="center" width="45%"><b>Alasan Ketidakhadiran</b></td>
                      <td align="center" width="50%"><b>Keterangan</b></td>
                  </tr>
                  <tr>
                      <td align="center">1</td>
                      <td>Sakit</td>
                      <td align="center">'.$data['jml_sakit'].'</td>
                  </tr>
                  <tr>
                      <td align="center">2</td>
                      <td>Izin</td>
                      <td align="center">'.$data['jml_izin'].'</td>
                  </tr>
                  <tr>
                      <td align="center">3</td>
                      <td>Tanpa Keterangan</td>
                      <td align="center">'.$data['jml_tketerangan'].'</td>
                  </tr>
              </table>';

      // Halaman Catatan Wali Kelas
      $body .= '<br><br><table border="0.1px" cellspacing="0" cellpadding="2">
                  <tr>
                      <td align="center">
                      <b>Catatan Wali Kelas</b>
                      <br>
                      <br>
                      '.$data['isi_catatan'].'
                      <br>
                      </td>
                  </tr>
              </table>';

      if(strpos($data['id_periodeDet'],"uas2") !== false) {
        if($data['catatan'])
          {
            $rapat_dewanGuru = $data['rapat_dewanGuru'];
            if($data['status_kenaikan'])
              $status_kenaikan = "Naik/<s>Tidak Naik</s>*)";
            else
              $status_kenaikan = "<s>Naik</s>/Tidak Naik*)";
            $kelas_lanjut = $data['kelas_lanjut'];
          }
        else
        {
          $rapat_dewanGuru =  $status_kenaikan = $kelas_lanjut = null;
        }

        $kenaikan = "Dengan memperhatikan persyaratan kenaikan kelas dan hasil keputusan rapat dewan guru pada tanggal ".$rapat_dewanGuru." Peserta Didik dinyatakan : ".$status_kenaikan." ke kelas: ".$kelas_lanjut;
        // Halaman Kenaikan khusus untuk UAS Semester Genap
        $body .= '<br><b></b><br><table border="0.1px" cellspacing="0" cellpadding="2">
                    <tr>
                        <td align="center">
                        <br><br>
                        '.$kenaikan.'
                        <br>
                        </td>
                    </tr>
                </table>';
      }

      // output the HTML content
      $pdf->writeHTML($header, true, false, true, false, '');
      $pdf->writeHTML($body, true, false, true, false, '');
      $pdf->writeHTML($footer, true, false, true, false, '');
       
      //Close and output PDF document
      $pdf->Output('example_002.pdf', 'I');
       
      //============================================================+
      // END OF FILE
      //============================================================+
  }

  function leger ($id_transKelas, $id_periodeDet=null, $mode=null, $pilihan=null)
  {

      $data['id_transKelas'] = $id_transKelas;
      $data['thn_ajaran'] = $this->model_output->get_thnAjaranL($id_transKelas);

      if ($id_periodeDet == null) {
        $data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');
      }
      else
      {
          $data['id_periodeDet'] = $id_periodeDet;
      }
      if ($mode == null) {
        $data['mode'] = "mode_ip";
      }
      else {
        $data['mode'] = $mode;
      }

      $data['identitas'] = $this->model_output->get_identitasL($data);
      if ($data['identitas'])
      {
          foreach ($data['identitas'] as $row) {
              $data['nama_kelas'] = $row['nama_kelas'];
              $data['id_wali'] = $row['id_wali'];
              $data['nama_wali'] = $row['nama_wali'];
              $data['id_kepsek'] = $row['id_kepsek'];
              $data['nama_kepsek'] = $row['nama_kepsek'];
              $data['id_periode'] = $row['id_periode'];
              $data['nama_tes'] = $row['nama_tes'];
              if((strpos($data['id_periodeDet'],"uts1") || strpos($data['id_periodeDet'],"uas1")) !== false)
                $data['semester'] = $row['smt_ganjil'];
              else if((strpos($data['id_periodeDet'],"uts2") || strpos($data['id_periodeDet'],"uas2")) !== false)
                $data['semester'] = $row['smt_genap'];


              $tanggal_rapor = strtotime ($row['tanggal_rapor']);
              $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
              $data['tanggal_rapor'] = date("j", $tanggal_rapor). " " . $bulan[date("n", $tanggal_rapor)] . " " . date("Y", $tanggal_rapor);
          }
      }
      else
      {
          $data['nama_kelas'] = null;
          $data['id_wali'] = null;
          $data['nama_wali'] = null;
          $data['id_kepsek'] = null;
          $data['nama_kepsek'] = null;
          $data['id_periode'] = null;
          $data['nama_tes'] = null;
          $data['tanggal_rapor'] = null;
          $data['semester'] = null;
      }


      $data['mapel'] = $this->model_output->get_mapelL($data);
      $data['siswa'] = $this->model_output->get_siswaL($data);
      $data['total'] = $this->model_output->get_totalNilai($data);

      if($data['siswa'])
        if($data['mapel'])
          foreach ($data['siswa'] as $row1) {
              foreach ($data['mapel'] as $row2) {
                   $result = $this->model_output->get_nilaiL($row1['id_transSisKls'], $row2['id_transAjar'], $data['id_periodeDet']);
                   $data['nilai'][$row1['id_siswa']][$row2['id_mapel']] = $result;
              }
          }

      if($pilihan)
      {
        $this->leger_excel($data);
      }
      else
        $this->template->display('output/leger', $data);
  }

  function leger_excel ($data)
  {
    $this->load->library('excel');

    /*
      Sheet1
    */
    $this->excel->setActiveSheetIndex(0);
    $sheet = $this->excel->getActiveSheet();

    //name the worksheet
    $sheet->setTitle('Mata Pelajaran Reguler');

    $sheet
      ->getPageSetup()
      ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    $sheet
      ->getPageSetup()
      ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

    /*
      Inisialisasi variabel dan mencari jumlah kolom mapel
      row 1-4 header, sehingga mulai dari 5
    */
    $kel_a = $kel_b = $kel_c = $kel_d = $kel_e = 0; $max_row = 1; $max_col = 1; $row_i = 5; $start_row = 5;
    if($data['mapel'])
    foreach ($data['mapel'] as $row)
    {
      if ($row['id_kelompok'] == 'A') $kel_a++;
      if ($row['id_kelompok'] == 'B') $kel_b++;
      if ($row['id_kelompok'] == 'C_IPA' || $row['id_kelompok'] == 'C_IPS' || $row['id_kelompok'] == 'C_IB') $kel_c++;
      if ($row['id_kelompok'] == 'D') $kel_d++;
      if ($row['id_kelompok'] == 'E') $kel_e++;
    }
    if($kel_a) $kel_a = ($kel_a*5)-1; if($kel_b) $kel_b = ($kel_b*5)-1; if($kel_c) $kel_c = ($kel_c*5)-1; if($kel_d) $kel_d = ($kel_d*5)-1; if($kel_e) $kel_e = ($kel_e*5)-1;
    
    /*
      Mencetak Header leger
    */
    $sheet->setCellValue('A1', 'DAFTAR NILAI RAPOR '. $data['nama_tes']. ' SEMESTER '.$data['semester'].' TAHUN AJARAN '.$data['id_periode']);
    $sheet->setCellValue('A2', 'SMA CHIS DENPASAR');

    $sheet->setCellValue('A4', 'Kelas '.$data['nama_kelas']);

    $sheet
      ->setCellValue('A'.$row_i, 'No')
      ->setCellValue('B'.$row_i, 'NIS')
      ->setCellValue('C'.$row_i, 'NISN')
      ->setCellValue('D'.$row_i, 'Nama');

    $sheet->mergeCells('A'.$row_i.':A'.($row_i+2));
    $sheet->mergeCells('B'.$row_i.':B'.($row_i+2));
    $sheet->mergeCells('C'.$row_i.':C'.($row_i+2));
    $sheet->mergeCells('D'.$row_i.':D'.($row_i+2));

    $temp='D';
    if($kel_a) {
      $cel_a = ++$temp;
      for ($i=0; $i < $kel_a; $i++) { 
        ++$temp;
      }
      //$temp = chr(ord($temp) + $kel_a);
      $cel_a1 = $temp;
      
      $sheet->setCellValue($cel_a.$row_i, 'MAPEL WAJIB KELOMPOK A');
      $sheet->mergeCells($cel_a.$row_i.':'.$cel_a1.$row_i);
    }
    if($kel_b) {
      $cel_b = ++$temp;
      //$temp = chr(ord($temp) + $kel_b);
      for ($i=0; $i < $kel_b; $i++) { 
        ++$temp;
      }
      $cel_b1 = $temp;

      $sheet->setCellValue($cel_b.$row_i, 'MAPEL WAJIB KELOMPOK B');
      $sheet->mergeCells($cel_b.$row_i.':'.$cel_b1.$row_i);
    }
    if($kel_c) {
      $cel_c = ++$temp;
      //$temp = chr(ord($temp) + $kel_c);
      for ($i=0; $i < $kel_c; $i++) { 
        ++$temp;
      }
      $cel_c1 = $temp;

      $sheet->setCellValue($cel_c.$row_i, 'MAPEL KELOMPOK PEMINATAN');
      $sheet->mergeCells($cel_c.$row_i.':'.$cel_c1.$row_i);
    } 
    if($kel_d) {
      $cel_d = ++$temp;
      //$temp = chr(ord($temp) + $kel_d);
      for ($i=0; $i < $kel_d; $i++) { 
        ++$temp;
      }
      $cel_d1 = $temp;

      $sheet->setCellValue($cel_d.$row_i, 'MAPEL KELOMPOK LINTAS PEMINATAN');
      $sheet->mergeCells($cel_d.$row_i.':'.$cel_d1.$row_i);
    } 

    $jml = ++$temp;
    $sheet->setCellValue($jml.$row_i, 'JUMLAH');
    $sheet->setCellValue($temp.($row_i+2), 'P');
    $sheet->setCellValue(++$temp.($row_i+2), 'K');
    $jml1 = $temp;
    $sheet->mergeCells($jml.$row_i.':'.$jml1.($row_i+1));

    $rt2 = ++$temp;
    $sheet->setCellValue($rt2.$row_i, 'RATA-RATA');
    $sheet->setCellValue($temp.($row_i+2), 'P');
    $sheet->setCellValue(++$temp.($row_i+2), 'K');
    $rt22 = $temp;
    $sheet->mergeCells($rt2.$row_i.':'.$rt22.($row_i+1));

    if(strpos($data['id_periodeDet'],"uas"))
    {
      $rk = ++$temp;
      $sheet->setCellValue($rk.$row_i, 'RK');
      $sheet->mergeCells($rk.$row_i.':'.$rk.($row_i+2));
    }

    $sheet->getStyle('E'.$row_i.':'.$temp.$row_i)->getAlignment()->setWrapText(true);
    $sheet->getStyle('E'.($row_i+1).':'.$temp.($row_i+1))->getAlignment()->setWrapText(true);

    $kol = 'D'; ++$row_i; $pindah_e=0;
    if($data['mapel'])
    foreach ($data['mapel'] as $row)
    {
      if ($row['id_kelompok'] != 'E')
      {
        $kiri = ++$kol;
        $sheet->setCellValue($kiri.$row_i, $row['nama_mapel']);
        $sheet->setCellValue($kol.($row_i+1), 'P');
        $sheet->setCellValue(++$kol.($row_i+1), 'pr');
        $sheet->setCellValue(++$kol.($row_i+1), 'K');
        $sheet->setCellValue(++$kol.($row_i+1), 'pr');
        $sheet->setCellValue(++$kol.($row_i+1), 'S');
        $kanan=$kol;
        $sheet->mergeCells($kiri.$row_i.':'.$kanan.$row_i);
      }
    }

    $kol = 'A'; $row_i = $row_i + 2; $max_rowHead = $row_i -1;
    $baris = 1;
    if ($data['siswa']) {
      foreach ($data['siswa'] as $row) {
        $sheet->setCellValue($kol++.$row_i, $baris); 
        $sheet->setCellValue($kol++.$row_i, $row['id_siswa']); 
        $sheet->setCellValue($kol++.$row_i, ""); 
        $sheet->setCellValue($kol++.$row_i, $row['nama_siswa']); 
      
        if($data['mapel']) {
          foreach ($data['mapel'] as $row1) {
            if ($row1['id_kelompok'] != 'E') 
            {
              if($data['nilai'])
              {
                $nilai = $data['nilai'];
                if($nilai[$row['id_siswa']][$row1['id_mapel']])
                {
                  foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                    if ($data['mode']=="mode_ip")
                    {
                      $sheet->setCellValue($kol++.$row_i, $row2['nilai_pengetahuan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_pengetahuan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['nilai_keterampilan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_keterampilan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_sikap']); 
                    }
                    elseif ($data['mode']=="mode_reg")
                    {
                      $sheet->setCellValue($kol++.$row_i, $row2['pengetahuan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_pengetahuan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['keterampilan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_keterampilan']); 
                      $sheet->setCellValue($kol++.$row_i, $row2['predikat_sikap']); 
                    }
                  }
                }
                else
                {
                  $sheet->setCellValue($kol++.$row_i, ""); 
                  $sheet->setCellValue($kol++.$row_i, ""); 
                  $sheet->setCellValue($kol++.$row_i, ""); 
                  $sheet->setCellValue($kol++.$row_i, ""); 
                  $sheet->setCellValue($kol++.$row_i, ""); 
                }
              }
            }
          }
        }
        $flag_total=1;
        if($data['total'])
        {
          foreach ($data['total'] as $row3) {
            if ($row3['id_tSisKlsKey'] == $row['id_transSisKls'] && $row3['status_muatan'] == 0)
            {
              if ($data['mode']=="mode_ip")
              {
                $sheet->setCellValue($kol++.$row_i, $row3['jml_nilai_pengetahuan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['jml_nilai_keterampilan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['rata2_nilai_pengetahuan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['rata2_nilai_keterampilan']); 
              }
              elseif ($data['mode']=="mode_reg")
              {
                $sheet->setCellValue($kol++.$row_i, $row3['jml_pengetahuan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['jml_keterampilan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['rata2_pengetahuan']); 
                $sheet->setCellValue($kol++.$row_i, $row3['rata2_keterampilan']); 
              }
              if(strpos($data['id_periodeDet'],"uas"))
              {
                $sheet->setCellValue($kol++.$row_i, $row3['ranking']); 
              }
              $flag_total=0;
            }
          }
          if($flag_total)
            {
              $sheet->setCellValue($kol++.$row_i, ""); 
              $sheet->setCellValue($kol++.$row_i, ""); 
              $sheet->setCellValue($kol++.$row_i, ""); 
              $sheet->setCellValue($kol++.$row_i, ""); 
              $sheet->setCellValue($kol++.$row_i, ""); 
            }
        }
        $kol = 'A';
        $row_i++;
        $baris++;
      }
      
    }

    /*
      Mendapatkan nilai max_row dan max_col untuk styling tabel
    */
    $max_col = $temp;
    $max_row = $row_i - 1; 
    
    // Merge judul Leger agar di tengah
    $sheet->mergeCells('A1:'.$max_col.'1');
    $sheet->mergeCells('A2:'.$max_col.'2');
    $sheet->getStyle('A1:'.$max_col.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A2:'.$max_col.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    /*
    Footer Excel untuk Tanda tangan
    */
    //$foot_col = chr(ord($max_col) - 5);
    /*for ($i=0; $i < $kel_a; $i++) { 
        ++$temp;
      }*/
    $foot_row = $max_row + 2;
    // Bagian Wali Kelas
    $sheet->setCellValue('C'.($foot_row+1), 'Wali Kelas'); 
    $sheet->setCellValue('C'.($foot_row+4), $data['nama_wali']); 
    $sheet->setCellValue('C'.($foot_row+5), 'NIP. '.$data['id_wali']); 


    // Bagian Kepala Sekolah
    $colNumber = PHPExcel_Cell::columnIndexFromString($max_col);
    $colNumber1 = $colNumber - 10;
    $foot_col = PHPExcel_Cell::stringFromColumnIndex($colNumber1);
    //echo $colNumber." menjadi ".$colString;
    $sheet->setCellValue($foot_col.($foot_row), 'Denpasar, '.$data['tanggal_rapor']); 
    $sheet->setCellValue($foot_col.($foot_row+1), 'Kepala Sekolah'); 
    $sheet->setCellValue($foot_col.($foot_row+4), $data['nama_kepsek']); 
    $sheet->setCellValue($foot_col.($foot_row+5), 'NIP. '.$data['id_kepsek']);


    $sheet->getStyle('A'.$start_row.':'.$max_col.$max_rowHead)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A'.$start_row.':'.$max_col.$max_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => '000000'),
        ),
      ),
    );
    $sheet->getStyle('A'.$start_row.':'.$max_col.$max_row)->applyFromArray($styleArray);

    $sheet->getDefaultRowDimension()->setRowHeight(15);
    $sheet->getDefaultColumnDimension()->setWidth(3.75);
    $sheet->getRowDimension(($start_row))->setRowHeight(50);
    $sheet->getRowDimension(($start_row+1))->setRowHeight(40);
    $sheet->getColumnDimension('A')->setWidth(5);
    $sheet->getColumnDimension('B')->setWidth(7);
    $sheet->getColumnDimension('C')->setWidth(7);
    $sheet->getColumnDimension('D')->setWidth(30);

    /*
      Sheet2
    */
    if($kel_e)
    {
      $sheet2 = $this->excel->createSheet(2);
          $sheet2->setTitle('Mata Pelajaran Muatan');
    
          $sheet2
            ->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
          $sheet2
            ->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    
          /*
            Inisialisasi variabel dan mencari jumlah kolom mapel
            row 1-4 header, sehingga mulai dari 5
          */
          $max_row = 1; $max_col = 1; $row_i = 5; $start_row = 5;
        
          /*
            Mencetak Header leger
          */
          $sheet2->setCellValue('A1', 'DAFTAR NILAI RAPOR '. $data['nama_tes']. ' SEMESTER '.$data['semester'].' TAHUN AJARAN '.$data['id_periode']);
          $sheet2->setCellValue('A2', 'SMA CHIS DENPASAR');
    
          $sheet2->setCellValue('A4', 'Kelas '.$data['nama_kelas']);
    
          $sheet2
            ->setCellValue('A'.$row_i, 'No')
            ->setCellValue('B'.$row_i, 'NIS')
            ->setCellValue('C'.$row_i, 'NISN')
            ->setCellValue('D'.$row_i, 'Nama');
    
          $sheet2->mergeCells('A'.$row_i.':A'.($row_i+2));
          $sheet2->mergeCells('B'.$row_i.':B'.($row_i+2));
          $sheet2->mergeCells('C'.$row_i.':C'.($row_i+2));
          $sheet2->mergeCells('D'.$row_i.':D'.($row_i+2));
    
          $temp='D';
          if($kel_e) {
            $cel_e = ++$temp;
            $temp = chr(ord($temp) + $kel_e);
            $cel_e1 = $temp;
            
            $sheet2->setCellValue($cel_e.$row_i, 'MATA PELAJARAN MUATAN SEKOLAH');
            $sheet2->mergeCells($cel_e.$row_i.':'.$cel_e1.$row_i);
          }
          
          $jml = ++$temp;
          $sheet2->setCellValue($jml.$row_i, 'JUMLAH');
          $sheet2->setCellValue($temp.($row_i+2), 'P');
          $sheet2->setCellValue(++$temp.($row_i+2), 'K');
          $jml1 = $temp;
          $sheet2->mergeCells($jml.$row_i.':'.$jml1.($row_i+1));
    
          $rt2 = ++$temp;
          $sheet2->setCellValue($rt2.$row_i, 'RATA-RATA');
          $sheet2->setCellValue($temp.($row_i+2), 'P');
          $sheet2->setCellValue(++$temp.($row_i+2), 'K');
          $rt22 = $temp;
          $sheet2->mergeCells($rt2.$row_i.':'.$rt22.($row_i+1));
    
          $sheet2->getStyle('E'.$row_i.':'.$temp.$row_i)->getAlignment()->setWrapText(true);
          $sheet2->getStyle('E'.($row_i+1).':'.$temp.($row_i+1))->getAlignment()->setWrapText(true);
    
          $kol = 'D'; ++$row_i; $pindah_e=0;
          if($data['mapel'])
          foreach ($data['mapel'] as $row)
          {
            if ($row['id_kelompok'] == 'E')
            {
              $kiri = ++$kol;
              $sheet2->setCellValue($kiri.$row_i, $row['nama_mapel']);
              $sheet2->setCellValue($kol.($row_i+1), 'P');
              $sheet2->setCellValue(++$kol.($row_i+1), 'pr');
              $sheet2->setCellValue(++$kol.($row_i+1), 'K');
              $sheet2->setCellValue(++$kol.($row_i+1), 'pr');
              $sheet2->setCellValue(++$kol.($row_i+1), 'S');
              $kanan=$kol;
              $sheet2->mergeCells($kiri.$row_i.':'.$kanan.$row_i);
            }
          }
    
          $kol = 'A'; $row_i = $row_i + 2; $max_rowHead = $row_i -1;
          $baris = 1;
          if ($data['siswa']) {
            foreach ($data['siswa'] as $row) {
              $sheet2->setCellValue($kol++.$row_i, $baris); 
              $sheet2->setCellValue($kol++.$row_i, $row['id_siswa']); 
              $sheet2->setCellValue($kol++.$row_i, ""); 
              $sheet2->setCellValue($kol++.$row_i, $row['nama_siswa']); 
            
              if($data['mapel']) {
                foreach ($data['mapel'] as $row1) {
                  if ($row1['id_kelompok'] == 'E') 
                  {
                    if($data['nilai'])
                    {
                      $nilai = $data['nilai'];
                      if($nilai[$row['id_siswa']][$row1['id_mapel']])
                      {
                        foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                          if ($data['mode']=="mode_ip")
                          {
                            $sheet2->setCellValue($kol++.$row_i, $row2['nilai_pengetahuan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_pengetahuan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['nilai_keterampilan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_keterampilan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_sikap']); 
                          }
                          elseif ($data['mode']=="mode_reg")
                          {
                            $sheet2->setCellValue($kol++.$row_i, $row2['pengetahuan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_pengetahuan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['keterampilan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_keterampilan']); 
                            $sheet2->setCellValue($kol++.$row_i, $row2['predikat_sikap']); 
                          }
                        }
                      }
                      else
                      {
                        $sheet2->setCellValue($kol++.$row_i, ""); 
                        $sheet2->setCellValue($kol++.$row_i, ""); 
                        $sheet2->setCellValue($kol++.$row_i, ""); 
                        $sheet2->setCellValue($kol++.$row_i, ""); 
                        $sheet2->setCellValue($kol++.$row_i, ""); 
                      }
                    }
                  }
                }
              }
              $flag_total=1;
              if($data['total'])
              {
                foreach ($data['total'] as $row3) {
                  if ($row3['id_tSisKlsKey'] == $row['id_transSisKls'] && $row3['status_muatan'] == 1)
                  {
                    if ($data['mode']=="mode_ip")
                    {
                      $sheet2->setCellValue($kol++.$row_i, $row3['jml_nilai_pengetahuan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['jml_nilai_keterampilan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['rata2_nilai_pengetahuan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['rata2_nilai_keterampilan']); 
                    }
                    elseif ($data['mode']=="mode_reg")
                    {
                      $sheet2->setCellValue($kol++.$row_i, $row3['jml_pengetahuan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['jml_keterampilan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['rata2_pengetahuan']); 
                      $sheet2->setCellValue($kol++.$row_i, $row3['rata2_keterampilan']); 
                    }
                    $flag_total=0;
                  }
                }
                if($flag_total)
                  {
                    $sheet2->setCellValue($kol++.$row_i, ""); 
                    $sheet2->setCellValue($kol++.$row_i, ""); 
                    $sheet2->setCellValue($kol++.$row_i, ""); 
                    $sheet2->setCellValue($kol++.$row_i, ""); 
                    $sheet2->setCellValue($kol++.$row_i, ""); 
                  }
              }
              $kol = 'A';
              $row_i++;
              $baris++;
            }
            
          }
    
          /*
            Mendapatkan nilai max_row dan max_col untuk styling tabel
          */
          $max_col = $temp;
          $max_row = $row_i - 1; 
          
          // Merge judul Leger agar di tengah
          $sheet2->mergeCells('A1:'.$max_col.'1');
          $sheet2->mergeCells('A2:'.$max_col.'2');
          $sheet2->getStyle('A1:'.$max_col.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $sheet2->getStyle('A2:'.$max_col.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
          /*
          Footer Excel untuk Tanda tangan
          */
          $foot_col = chr(ord($max_col) - 5);
          $foot_row = $max_row + 2;
          // Bagian Wali Kelas
          $sheet2->setCellValue('C'.($foot_row+1), 'Wali Kelas'); 
          $sheet2->setCellValue('C'.($foot_row+4), $data['nama_wali']); 
          $sheet2->setCellValue('C'.($foot_row+5), 'NIP. '.$data['id_wali']); 
    
          // Bagian Kepala Sekolah
          $sheet2->setCellValue($foot_col.($foot_row), 'Denpasar, '.$data['tanggal_rapor']); 
          $sheet2->setCellValue($foot_col.($foot_row+1), 'Kepala Sekolah'); 
          $sheet2->setCellValue($foot_col.($foot_row+4), $data['nama_kepsek']); 
          $sheet2->setCellValue($foot_col.($foot_row+5), 'NIP. '.$data['id_kepsek']); 
    
    
          $sheet2->getStyle('A'.$start_row.':'.$max_col.$max_rowHead)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
          $sheet2->getStyle('A'.$start_row.':'.$max_col.$max_row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
          $styleArray = array(
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
              ),
            ),
          );
          $sheet2->getStyle('A'.$start_row.':'.$max_col.$max_row)->applyFromArray($styleArray);
    
          $sheet2->getDefaultRowDimension()->setRowHeight(15);
          $sheet2->getDefaultColumnDimension()->setWidth(3.75);
          $sheet2->getRowDimension(($start_row))->setRowHeight(50);
          $sheet2->getRowDimension(($start_row+1))->setRowHeight(40);
          $sheet2->getColumnDimension('A')->setWidth(5);
          $sheet2->getColumnDimension('B')->setWidth(7);
          $sheet2->getColumnDimension('C')->setWidth(7);
          $sheet2->getColumnDimension('D')->setWidth(30);
    }
    
    $filename='leger.xls'; //save our workbook as this file name
    header('Content-Type: application/vnd.ms-excel'); //mime type
    header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    header('Cache-Control: max-age=0'); //no cache

    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //if you want to save it as .XLSX Excel 2007 format
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
    //force user to download the Excel file without writing it to server's HD
    $objWriter->save('php://output');
  }
}

?>