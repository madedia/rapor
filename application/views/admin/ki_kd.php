<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrator
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrator</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Master Deskripsi Nilai Kompetensi Pengetahuan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <?php echo form_open(base_url().'admin/edit_KIKD'); ?>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style='text-align:center; vertical-align:middle' class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Nilai Deskripsi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($pengetahuan) {
                    $baris=1;
                    foreach ($pengetahuan as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_pengetahuan[]' value='".$row['id_pengetahuan']."'></td>";
                      echo "<td>".$baris."</td>";
                      echo "<td>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_peng[]'>".$row['des_peng']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Master Deskripsi Nilai Kompetensi Keterampilan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Nilai Deskripsi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($keterampilan) {
                    $baris=1;
                    foreach ($keterampilan as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_keterampilan[]' value='".$row['id_keterampilan']."'></td>";
                      echo "<td>".$baris."</td>";
                      echo "<td>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_ket[]'>".$row['des_ket']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Master Deskripsi Nilai Kompetensi Sikap</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Nilai Deskripsi</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($sikap) {
                    $baris=1;
                    foreach ($sikap as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_sikap[]' value='".$row['id_sikap']."'></td>";
                      echo "<td>".$baris."</td>";
                      echo "<td>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_sik[]'>".$row['des_sikap']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->