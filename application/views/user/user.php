<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Data Pengguna
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">user</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  	<div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                  <div class="widget-user-image">
                    <img class="img-circle" src="<?php echo base_url('assets/images/user7-128x128.jpg'); ?>" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username"><?php echo $this->session->userdata('nama_guru'); ?></h3>
                  <h5 class="widget-user-desc"></h5>
                </div>
                <?php echo form_open(base_url().'user/do_edit'); ?>
                <div class="box-body">
                  <ul class="nav nav-stacked">
                  	<li><a>Username (NIP) <input type="text" class="pull-right" value="<?php echo $this->session->userdata('id_guru'); ?>">  </a></li>
                  	<li><a>Username (Alias) <input type="text" class="pull-right" value="<?php echo $this->session->userdata('id_guru'); ?>">  </a></li>
                  	<li><a>Nama Pendek <input type="text" class="pull-right" value="<?php echo $this->session->userdata('nama_pendek'); ?>">  </a></li>
                    <!-- <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
                  </ul>

                  <br><b>UBAH PASSWORD</b>
                  <ul class="nav nav-stacked">
                  	<li><a>Password sebelumnya<input type="password" class="pull-right" value="">  </a></li>
                  	<li><a>Password baru<input type="password" class="pull-right" value="">  </a></li>
                  	<li><a>ulangi Password baru <input type="password" class="pull-right" value="">  </a></li>
                    <!-- <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li> -->
                  </ul>
                </div>
                <div class="box-footer">
                  <?php echo "<td><button type='submit' class='btn btn-info pull-right'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
                </div>
                <?php echo form_close();?>
              </div><!-- /.widget-user -->
            </div><!-- /.col -->
</section><!-- /.content