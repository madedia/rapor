<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrator
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrator</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#backup_ui">
	              <!-- small box -->
	              <div class="small-box bg-red">
	                <div class="inner">
	                  <h3>&nbsp;</h3>
	                  <p>Backup Database</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-clipboard"></i>
	                </div>
	              </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Log Backup Database</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_user" name="tabel_user" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
					<th style="text-align:center">User ID</th>
					<th style="text-align:center">Keterangan</th>
					<th style="text-align:center">Waktu</th>
					<th style="text-align:center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($log_backup) {
						$baris=1;
						foreach ($log_backup as $row) {
							if ($row['id_backupdb']!=''){
								echo "<tr>";
								echo "<td>".($baris)."</td>";
								echo "<td>".$row['id_user']."</td>";
								echo "<td>".$row['keterangan']."</td>";
								echo "<td>".$row['waktu']."</td>";
								//echo "<td><a href='"; echo base_url('admin/edit_user/'.$row['id_user']); echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-pencil'></span>Edit</a></td>";
								//echo "<td><a name='id_user2' id='id_user2' data-nilai=".$row['id_user']." onClick=edit_user(".$row['id_user'].") data-toggle='modal' data-target='#editUser'>".$row['id_user']."</a></td>";
								echo "<td></td>";
								echo "</tr>";
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="backup_ui" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Backup Database</h4>
      </div>
      <div class="modal-body">
		<?php 
			echo form_open('admin/do_backupDB');?>
		<div class="form-group">
			<?php 
			echo form_label('Keterangan Backup', 'keterangan', array(
			    'class' => 'control-label',
			));
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'keterangan',
		              'id'    => 'keterangan',
		            ));
			?>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-danger'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Backup Sekarang</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- end -->