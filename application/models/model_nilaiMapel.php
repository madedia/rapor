<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_nilaiMapel extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    public function get_mapelEnrol()
    {
        if($this->session->userdata('tipe')==2)
        {
            $this->db->where('id_guruWali', $this->session->userdata('id_guru'));

        }
        if($this->session->userdata('tipe')==3)
        {
            $this->db->where('id_guru', $this->session->userdata('id_guru'));
        }

        $this->db->where('id_mapelKey = id_mapel');
        $this->db->where('id_tKelasKey = id_transKelas');
        $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        $this->db->order_by('trans_klsgeneral.nama_kelas, mapel.nama_mapel', 'ASC');
         
        $query = $this->db->get('trans_ajar, mapel, trans_kelas, trans_klsgeneral');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_idTSisKls($id_siswa, $id_tKelasKey)
    {
        $this->db->where('id_siswa', $id_siswa);
        $this->db->where('id_tKelasKey', $id_tKelasKey);
        $query = $this->db->get('trans_siswa');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['id_transSisKls'];
            return $val;
        }
        else return null;
    }

    function get_Detail($id_tAjar)
    {
        $this->db->where('trans_ajar.id_transAjar', $id_tAjar);
        $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->where('trans_ajar.id_guru = guru.id_guru');

        $query = $this->db->get('trans_ajar, trans_kelas, trans_klsgeneral, kelas_general, mapel, guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_siswaKelas($id_tAjar)
    {
        $this->db->where('trans_ajar.id_transAjar', $id_tAjar);
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->where('trans_siswa.id_tKelasKey = trans_ajar.id_tKelasKey');
        $this->db->where('trans_siswa.id_siswa = siswa.id_siswa');

        $query = $this->db->get('siswa, trans_siswa, trans_ajar, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_nilaiKelasSiswa($id_tAjar, $id_periodeDet)
    {
        $this->db->where('id_tAjarKey', $id_tAjar);
        
        $this->db->where('nilai_mapel.id_tAjarKey = trans_ajar.id_transAjar');
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');

        $this->db->where('nilai_mapel.id_periodeDet', $id_periodeDet);
        $this->db->where('nilai_mapel.id_tSisKlsKey = trans_siswa.id_transSisKls');
        $this->db->where('trans_siswa.id_siswa = siswa.id_siswa');

        $query = $this->db->get('siswa, trans_siswa, nilai_mapel, mapel, trans_ajar');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_predikat($nilai, $pilihan)
    {   
        $this->db->where('batas_atas >=', $nilai); 
        $this->db->where('batas_bawah <=', $nilai);

        $query = $this->db->get('konversi_nilai');

        if($query->num_rows()>0){
            $res = $query->row_array();
            if ($pilihan == 0)
                $val = $res['id_predikat'];
            elseif ($pilihan == 1) {
                $val = $res['sikap'];
            }
            return $val;
        }
        else return null;
    }

    public function get_deskripsi($pilihan, $id_tAjar, $sikap)
    {
        if ($pilihan ==1) {
            $this->db->where('id_tAjarKey', $id_tAjar);
            $this->db->where('sikap', preg_replace("/[^a-zA-Z]/", "", $sikap));
            $query = $this->db->get('trans_npengetahuan');
        }
        elseif ($pilihan ==2) {
            $this->db->where('id_tAjarKey', $id_tAjar);
            $this->db->where('sikap', preg_replace("/[^a-zA-Z]/", "", $sikap));
            $query = $this->db->get('trans_nketerampilan');
        }
        elseif ($pilihan ==3) {
            $this->db->where('id_tAjarKey', $id_tAjar);
            $this->db->where('sikap', $sikap);
            $query = $this->db->get('trans_nsikap');
        }

        if($query->num_rows()>0){
            $res = $query->row_array();
            if ($pilihan ==1) {
                $val = $res['des_peng'];
            }
            elseif ($pilihan ==2) {
                $val = $res['des_ket'];
            }
            elseif ($pilihan ==3) {
                $val = $res['des_sik'];
            }    
            return $val;
        }
        else return null;
    }

    public function insert_nilaiMapel($data, $id_tAjar)
    {
        for ($baris=0; $baris<count($data['id_tSisKlsKey']); $baris++) {
            $this->db->set('id_tSisKlsKey', $data['id_tSisKlsKey'][$baris]);
            $this->db->set('id_tAjarKey', $data['id_tAjarKey']);
            $this->db->set('pengetahuan', $data['pengetahuan'][$baris]);
            $this->db->set('keterampilan', $data['keterampilan'][$baris]);
            $this->db->set('sikap', $data['sikap'][$baris]);
            $this->db->set('nilai_pengetahuan', $data['nilai_pengetahuan'][$baris]);
            $this->db->set('nilai_keterampilan', $data['nilai_keterampilan'][$baris]);
            $this->db->set('nilai_sikap', $data['nilai_sikap'][$baris]);
            $this->db->set('predikat_pengetahuan', $data['predikat_pengetahuan'][$baris]);
            $this->db->set('predikat_keterampilan', $data['predikat_keterampilan'][$baris]);
            $this->db->set('predikat_sikap', $data['predikat_sikap'][$baris]);
            //$this->db->set('status_manualdes', $data['status_manualdes'][$baris]);
            $this->db->set('des_peng', $data['des_peng'][$baris]);
            $this->db->set('des_ket', $data['des_ket'][$baris]);
            $this->db->set('des_sik', $data['des_sik'][$baris]);
            $this->db->set('id_input', $data['id_input']);
            $this->db->set('komentar', $data['komentar'][$baris]);

            if ($data['isUpdate'][$baris])
            {
                $this->db->where('id_nilaiMapel', $data['id_nilaiMapel'][$baris]);
                $this->db->update('nilai_mapel');
            }
            else
            {
                $this->db->set('id_periodeDet', $data['id_periodeDet']);
                $this->db->insert('nilai_mapel');
            }
        }
    }

    public function get_totalNilai($id_tSisKlsKey, $id_periodeDet)
    {
        $this->db->select('id_tSisKlsKey, id_periodeDet, sum(pengetahuan), sum(keterampilan), sum(sikap), sum(nilai_pengetahuan), sum(nilai_keterampilan),
            avg(pengetahuan), avg(keterampilan), avg(sikap), avg(nilai_pengetahuan), avg(nilai_keterampilan), max(predikat_sikap)');

        $this->db->where('id_tSisKlsKey', $id_tSisKlsKey);
        $this->db->where('id_periodeDet', $id_periodeDet);
        $query = $this->db->get('nilai_mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_rankNilai($id_periodeDet, $id_transKelas)
    {
        $string       = "SELECT id_tSisKlsKey, @rank := @rank + 1 AS ranking, score, jml_pengetahuan, jml_keterampilan, jml_sikap, jml_nilai_pengetahuan, jml_nilai_keterampilan, rat_pengetahuan, rat_keterampilan, rat_nilai_pengetahuan, rat_nilai_keterampilan, rat_predikat_sikap FROM
                        (
                          SELECT u.id_tSisKlsKey, sum(u.pengetahuan+u.keterampilan) as score, sum(pengetahuan) as jml_pengetahuan, sum(keterampilan) as jml_keterampilan, sum(sikap) as jml_sikap, sum(nilai_pengetahuan) as jml_nilai_pengetahuan, sum(nilai_keterampilan) as jml_nilai_keterampilan, avg(pengetahuan) as rat_pengetahuan, avg(keterampilan) as rat_keterampilan, avg(sikap) as rat_sikap, avg(nilai_pengetahuan) as rat_nilai_pengetahuan, avg(nilai_keterampilan) as rat_nilai_keterampilan, max(predikat_sikap) as rat_predikat_sikap
                          FROM nilai_mapel u, trans_kelas k, trans_ajar a, mapel m
                          WHERE u.id_tAjarKey = a.id_transAjar 
                          AND a.id_mapelKey = m.id_mapel 
                          AND m.id_kelompok != 'E'      
                          AND a.id_tKelasKey = k.id_transKelas 
                          AND k.id_transKelas = ".$id_transKelas." 
                          AND u.id_periodeDet = '".$id_periodeDet."'
                          GROUP BY id_tSisKlsKey
                          ORDER BY score DESC
                        ) zz, (SELECT @rank := 0) z";

        $query = $this->db->query($string);

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_rankNilaiMuatan($id_periodeDet, $id_transKelas)
    {
        $string       = "SELECT id_tSisKlsKey, score, jml_pengetahuan, jml_keterampilan, jml_sikap, jml_nilai_pengetahuan, jml_nilai_keterampilan, rat_pengetahuan, rat_keterampilan, rat_nilai_pengetahuan, rat_nilai_keterampilan, rat_predikat_sikap FROM
                        (
                          SELECT u.id_tSisKlsKey, sum(u.pengetahuan+u.keterampilan) as score, sum(pengetahuan) as jml_pengetahuan, sum(keterampilan) as jml_keterampilan, sum(sikap) as jml_sikap, sum(nilai_pengetahuan) as jml_nilai_pengetahuan, sum(nilai_keterampilan) as jml_nilai_keterampilan, avg(pengetahuan) as rat_pengetahuan, avg(keterampilan) as rat_keterampilan, avg(sikap) as rat_sikap, avg(nilai_pengetahuan) as rat_nilai_pengetahuan, avg(nilai_keterampilan) as rat_nilai_keterampilan, max(predikat_sikap) as rat_predikat_sikap
                          FROM nilai_mapel u, trans_kelas k, trans_ajar a, mapel m
                          WHERE u.id_tAjarKey = a.id_transAjar 
                          AND a.id_mapelKey = m.id_mapel
                          AND m.id_kelompok = 'E'      
                          AND a.id_tKelasKey = k.id_transKelas 
                          AND k.id_transKelas = ".$id_transKelas." 
                          AND u.id_periodeDet = '".$id_periodeDet."'
                          GROUP BY id_tSisKlsKey
                          ORDER BY score DESC
                        ) zz";

        $query = $this->db->query($string);

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    public function get_idTotalNilai($id_periodeDet, $id_transKelas, $status_muatan)
    {
        $this->db->select('id_transTotalNilai, id_tSisKlsKey, status_muatan');
        $this->db->where('id_periodeDet', $id_periodeDet);
        $this->db->where('id_tKelasKey', $id_transKelas);
        $this->db->where('status_muatan', $status_muatan);

        $query = $this->db->get('total_nilaimapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }
    
    public function get_kelasNilai($id_tAjar)
    {
        $this->db->select('id_transKelas');
        $this->db->where('id_transAjar', $id_tAjar);
        $this->db->where('id_tKelasKey = id_transKelas');
        $this->db->group_by('id_transKelas');
        $query = $this->db->get('trans_ajar, trans_kelas');

       // echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['id_transKelas'];
            return $val;
        }
        else return null;
    }

    public function is_muatan($id_tAjar)
    {
        $this->db->where('trans_ajar.id_transAjar', $id_tAjar);
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->where('mapel.id_kelompok <>', 'E');

        $query = $this->db->get('trans_ajar, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0){
            return 0;
        }
        else return 1;
    }

    public function insert_totalNilai($data2, $isUpdate) {           
        foreach ($data2['total-umum'] as $row2) {
            $this->db->set('id_tSisKlsKey', $row2['id_tSisKlsKey']);
            $this->db->set('id_tKelasKey', $data2['kelas']);
            $this->db->set('id_periodeDet', $data2['id_periodeDet']);

            $this->db->set('status_muatan', 0);
            $this->db->set('jml_pengetahuan', $row2['jml_pengetahuan']);
            $this->db->set('jml_keterampilan', $row2['jml_keterampilan']);
            $this->db->set('jml_sikap', $row2['jml_sikap']);
            $this->db->set('jml_nilai_pengetahuan', $row2['jml_nilai_pengetahuan']);
            $this->db->set('jml_nilai_keterampilan', $row2['jml_nilai_keterampilan']);
            $this->db->set('rata2_pengetahuan', $row2['rat_pengetahuan']);
            $this->db->set('rata2_keterampilan', $row2['rat_keterampilan']);
            $this->db->set('rata2_nilai_pengetahuan', $row2['rat_nilai_pengetahuan']);
            $this->db->set('rata2_nilai_keterampilan', $row2['rat_nilai_keterampilan']);
            $this->db->set('sikap_antarMapel', $row2['rat_predikat_sikap']);
            $this->db->set('ranking', $row2['ranking']);
            
            if ($isUpdate)
            {
                foreach ($data2['id_totalNilai'] as $row1) {
                    if ($row2['id_tSisKlsKey'] == $row1['id_tSisKlsKey']) 
                    {
                        $this->db->set('id_transTotalNilai', $row1['id_transTotalNilai']);
                        $this->db->where('id_transTotalNilai', $row1['id_transTotalNilai']);
                        $this->db->update('total_nilaimapel');
                    }
                }
            }   
            else
            {
                $this->db->insert('total_nilaimapel');
            }        
        }        
    }

    public function insert_totalNilaiM($data2, $isUpdate) {           
        foreach ($data2['total-muatan'] as $row2) {
            $this->db->set('id_tSisKlsKey', $row2['id_tSisKlsKey']);
            $this->db->set('id_tKelasKey', $data2['kelas']);
            $this->db->set('id_periodeDet', $data2['id_periodeDet']);
            $this->db->set('status_muatan', 1);
            $this->db->set('jml_pengetahuan', $row2['jml_pengetahuan']);
            $this->db->set('jml_keterampilan', $row2['jml_keterampilan']);
            $this->db->set('jml_sikap', $row2['jml_sikap']);
            $this->db->set('jml_nilai_pengetahuan', $row2['jml_nilai_pengetahuan']);
            $this->db->set('jml_nilai_keterampilan', $row2['jml_nilai_keterampilan']);
            $this->db->set('rata2_pengetahuan', $row2['rat_pengetahuan']);
            $this->db->set('rata2_keterampilan', $row2['rat_keterampilan']);
            $this->db->set('rata2_nilai_pengetahuan', $row2['rat_nilai_pengetahuan']);
            $this->db->set('rata2_nilai_keterampilan', $row2['rat_nilai_keterampilan']);
            $this->db->set('sikap_antarMapel', $row2['rat_predikat_sikap']);
            //$this->db->set('ranking', null);

            if ($isUpdate)
            {
                foreach ($data2['id_totalNilai'] as $row1) {
                    if ($row2['id_tSisKlsKey'] == $row1['id_tSisKlsKey']) 
                    {
                        $this->db->set('id_transTotalNilai', $row1['id_transTotalNilai']);
                        $this->db->where('id_transTotalNilai', $row1['id_transTotalNilai']);
                        $this->db->update('total_nilaimapel');
                        echo $this->db->last_query() . '<br>';
                        break;
                    }
                }
            }   
            else
            {
                $this->db->insert('total_nilaimapel');
                echo $this->db->last_query() . '<br>';
            }        
        }
    }

    public function update_deskripsi($data)
    { 
        for ($baris=0; $baris<count($data['id_nilaiMapel']); $baris++)
        {
            $this->db->set('des_peng', $data['des_peng'][$baris]);
            $this->db->set('des_ket', $data['des_ket'][$baris]);
            $this->db->set('des_sik', $data['des_sik'][$baris]);
            $this->db->set('status_manualdes', 1);
            $this->db->set('id_inputdes', $data['id_input']);
            $this->db->where('id_nilaiMapel', $data['id_nilaiMapel'][$baris]);
            $this->db->update('nilai_mapel');
        }
        
    }

    function get_kamusDeskripsi($id_tAjar, $pilihan)
    {
        $this->db->where('id_tAjarKey', $id_tAjar);

        if($pilihan == 1)
            $query = $this->db->get('trans_npengetahuan');
        else if($pilihan == 2)
            $query = $this->db->get('trans_nketerampilan');
        else if($pilihan == 3)
            $query = $this->db->get('trans_nsikap');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function update_kamusDeskripsi($data, $pilihan)
    {
        for ($baris=0; $baris<count($data['id_tAjar']); $baris++)
        {
            $this->db->where('id_tAjarKey', $data['id_tAjar'][$baris]);
            //$this->db->where('id_tAjarKey', $data['id_tAjar']);

            if($pilihan == 1)
            {
                $this->db->set('des_peng', $data['des_peng'][$baris]);
                $this->db->where('id_transPeng', $data['id_transPeng'][$baris]);
                $query = $this->db->update('trans_npengetahuan');
            }
            else if($pilihan == 2)
            {
                $this->db->set('des_ket', $data['des_ket'][$baris]);
                $this->db->where('id_transKet', $data['id_transKet'][$baris]);
                $query = $this->db->update('trans_nketerampilan');
            }
            else if($pilihan == 3)
            {
                $this->db->set('des_sik', $data['des_sik'][$baris]);
                $this->db->where('id_transSik', $data['id_transSik'][$baris]);
                $query = $this->db->update('trans_nsikap');
            }
            //echo $this->db->last_query() . '<br>';
        }
    }
}