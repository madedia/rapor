<button class="btn btn-sm btn-danger" id="btn-delete-trig"><i class="fa fa-times"></i></button>

<div class="modal modal-danger fade" id="modal-delete" data-backdrop="static">
      <div class="modal-dialog" style="width: 350px;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-book"></i> Delete Book Data </h4>
          </div>
          <div class="modal-body">
            <div class="box-body table-responsive">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <input type="hidden" id="delete-id" name="delete-id" />
                    <input type="hidden" id="delete-title" name="delete-title" />
                    <p>Are you sure to delete this data ?</p>
                    <div class="callout callout-danger">
                      <p>Title: <span class="delete-title"> </span></p>
                      <p>Author: <span class="delete-author"> </span></p>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- /.box-body -->
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
            <button id="btn-delete" type="button" class="btn btn-primary"><i class="fa fa-check"></i> Yes</button>
          </div>
        </div>
      </div>
    </div>


<script type="text/javascript">
      $(document).ready(function() {

        $('#btn-delete-trig).click(function(){
            $('#modal-delete').modal('show');
        });
      });
</script>