<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrator
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrator</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#tambahUser">
	              <!-- small box -->
	              <div class="small-box bg-green">
	                <div class="inner">
	                  <h3><?php echo count($user) ?></h3>
	                  <p>Tambah User</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-pound"></i>
	                </div>
	              </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Data User</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_user" name="tabel_user" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
					<th style="text-align:center">User ID</th>
					<th style="text-align:center">Nama</th>
					<th style="text-align:center">Hak Akses</th>
					<th style="text-align:center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($user) {
						$baris=1;
						foreach ($user as $row) {
							if ($row['id_user']!=''){
								echo "<tr>";
								echo "<td style='text-align:center'>".($baris)."</td>";
								echo "<td class='nr'>".$row['id_user']."</td>";
								echo "<td>".$row['nama_guru']."</td>";
								echo "<td>".$row['hak_akses']."</td>";
								echo '<td style="text-align:center">&nbsp <a href="#editUser" class="edit-user btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
								echo '&nbsp <a href="'.base_url('admin/hapus_user/'.$row['id_user']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
								echo "</tr>";
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="tambahUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Pengguna</h4>
      </div>
      <div class="modal-body">
		<?php 
			echo form_open('admin/tambah_user');?>
		<div class="form-group">
			<?php 
			echo form_label('USER ID', 'id_user', array(
			    'class' => 'control-label',
			));
			?>

			<select name="id_user" id="id_user" class="form-control" onChange=get_tipeUser()>
				<option value=''disabled selected>- Pilih -</option>
				<?php 
					if ($guru){
						foreach ($guru as $row) {
							echo "<option value='".$row['id_guru']."'";
							echo ">".$row['id_guru'].' | '.$row['nama_guru']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('HAK AKSES', 'tipe_user', array(
			    'class' => 'control-label',
			));
			?>

			<select name="tipe_user" id="tipe_user" class="form-control">
				<option value='' disabled selected>- Kosong -</option>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('PASSWORD', 'password', array(
			    'class' => 'control-label',
			));
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'password',
		              'id'    => 'password',
		            ));
			?>
		</div>		
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- end -->

<!-- Modal2 -->
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit User</h4>
      </div>
      <?php echo form_open('admin/do_edit_user'); ?>
      <div class="modal-body" id="detail_user">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div> 
<!-- end2 -->

<script type="text/javascript">
	function get_tipeUser(){
		var id_user =$('#id_user').val();
		//alert(id_kelasGen);
		var url = '<?php echo base_url('admin/get_tipeUser/');?>'

		$.ajax
			({
			type: "POST",
			url: url,
			data: {"id_user":id_user},
			cache: false,
			success: function(html)
			{
				$("#tipe_user").html(html);
			}
		});
	}

	$(".edit-user").click(function() {
	    var $row = $(this).closest("tr");    // Find the row
	    var $id_user = $row.find(".nr").text(); // Find the text
	    var $url = '<?php echo base_url('admin/edit_user');?>'
	    // Let's test it out
	    //alert($id_user);

	    $.ajax
			({
			type: "POST",
			url: $url,
			data: {"id_user":$id_user},
			cache: false,
			success: function(html)
			{
				$("#detail_user").html(html);
			}
		});

	    $('#editUser').modal('show');
    });
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_user").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 4
        } ]
    });
  });
</script>