<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_kelas extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_struktur($pilihan)
    {
        if ($pilihan == 'kelas')
        {
            $this->db->where('trans_kelas.id_guruWali = guru.id_guru');
            $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
            $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
            if ($this->session->userdata('tipe') == 2) {
                $this->db->where('trans_kelas.id_transKelas', $this->session->userdata('kelas_wali'));
            }
            $this->db->order_by('trans_klsgeneral.nama_kelas', 'ASC');
            $query = $this->db->get('trans_kelas, trans_klsgeneral, guru');
        }
        elseif ($pilihan == 'guru') {
            $query = $this->db->get('guru');
        }
        elseif ($pilihan == 'periode') {
            $query = $this->db->get('periode');
        }
        elseif ($pilihan == 'kelas_general') {
            $query = $this->db->get('kelas_general');
        }
        elseif ($pilihan == 'trans_klsgeneral') {
            $query = $this->db->get('trans_klsgeneral');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswaAktif()
    {
        $this->db->select('trans_siswa.id_siswa');
        $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_siswa, trans_kelas');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswaFree($siswaAktif)
    {
        $this->db->where('status_siswa', '1');
        foreach ($siswaAktif as $row)
        {
            $this->db->where_not_in('id_siswa', $row['id_siswa']);
        }
        $query = $this->db->get('siswa');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_tkelasGen($id_kelasGen)
    {
        $this->db->where('id_kelasGen', $id_kelasGen);

        $query = $this->db->get('trans_klsgeneral');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambahKelas($data)
    {
        $this->db->insert('trans_kelas', $data);
    }

    function update_tipeEnrol($id_guru)
    {
        $this->db->set('tipe', 2);
        $this->db->where('id_guru', $id_guru);
        $where = "(`tipe` > 2 OR `tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function tambahKatalog($data)
    {
        $this->db->insert('trans_klsgeneral', $data);
    }

    function tambahKategori($data)
    {
        $this->db->insert('kelas_general', $data);
    }

    function insert_siswa($data, $id_transKelas)
    {
        foreach ($data as $row) {
            $this->db->set('id_siswa', $row);
            $this->db->set('id_tKelasKey', $id_transKelas);
            $this->db->insert('trans_siswa');
        }
    }

    function get_detail($key, $pilihan)
    {
        if($pilihan == 'enrol')
        {
            $this->db->where('trans_kelas.id_transKelas', $key);
            $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
            $this->db->where('trans_kelas.id_guruWali = guru.id_guru');
            $query = $this->db->get('trans_kelas, trans_klsgeneral, guru');
        }
        elseif($pilihan == 'katalog')
        {
            $this->db->where('id_trans_klsgeneral', $key);
            $query = $this->db->get('trans_klsgeneral');
        }
        elseif($pilihan == 'kategori')
        {
            $this->db->where('id_kelasGen', $key);
            $query = $this->db->get('kelas_general');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswa($id_transKelas)
    {
        $this->db->where('trans_siswa.id_tKelasKey', $id_transKelas);
        $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');

        $query = $this->db->get('trans_siswa, siswa');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function delete_kelas($data)
    {
        if($data['pilihan']=='enrol')
        {
            $this->db->where('id_transKelas', $data['key']);
            $this->db->delete('trans_kelas');
        }
        elseif($data['pilihan']=='katalog')
        {
            $this->db->where('id_trans_klsgeneral', $data['key']);
            $this->db->delete('trans_klsgeneral');
        }
        elseif($data['pilihan']=='kategori')
        {
            $this->db->where('id_kelasGen', $data['key']);
            $this->db->delete('kelas_general');
        }
    }

    function delete_siswa($id_transSisKls)
    {
        $this->db->where('id_transSisKls', $id_transSisKls);

        $query = $this->db->delete('trans_siswa');
    }

    function get_kelasLain($id_guru, $id_transKelas)
    {
        $this->db->where('id_guruWali', $id_guru);
        $this->db->where('id_transKelas !=', $id_transKelas);
        $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_kelas');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function get_ajarMapel($id_guru)
    {
        $this->db->where('trans_ajar.id_guru', $id_guru);
        $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_ajar, trans_kelas');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function get_ajarPede($id_guru)
    {
        $this->db->where('trans_pedeguru.id_pembina', $id_guru);
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_pedeguru');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function update_enrol($data)
    {
        $this->db->set('id_guruWali', $data['id_guruWaliEdit']);
        $this->db->where('id_transKelas', $data['id_transKelas']);
        $this->db->update('trans_kelas');

        if($data['guru_awalKelasLain'] == null)
        {
            if($data['guru_awalMapel'] == null)
            {
                if($data['guru_awalPede'] == null)
                {
                    $this->db->set('tipe', 'NULL', false);
                    $this->db->where('id_guru', $data['id_guruWaliAwal']);
                    $this->db->where('tipe > 1');
                    $this->db->update('guru');
                }
                else
                {
                    $this->db->set('tipe', 4);
                    $this->db->where('id_guru', $data['id_guruWaliAwal']);
                    $this->db->where('tipe > 1');
                    $this->db->update('guru');
                }
            }
            else
            {
                $this->db->set('tipe', 3);
                $this->db->where('id_guru', $data['id_guruWaliAwal']);
                $this->db->where('tipe > 1');
                $this->db->update('guru');
            }
        }

        $this->db->set('tipe', 2);
        $this->db->where('id_guru', $data['id_guruWaliEdit']);
        $where = "(`tipe` > 2 OR `tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function update_katalog($data)
    {
        $this->db->set('nama_kelas', $data['nama_kelas']);
        $this->db->where('id_trans_klsgeneral', $data['id_trans_klsgeneral']);
        $this->db->update('trans_klsgeneral');
    }

    function update_kategori($data)
    {
        $this->db->set('kelas_angka', $data['kelas_angka']);
        $this->db->set('nama_kelasGen', $data['nama_kelasGen']);
        $this->db->set('status_aksel', $data['status_aksel']);
        $this->db->set('smt_ganjil', $data['smt_ganjil']);
        $this->db->set('smt_genap', $data['smt_genap']);
        $this->db->where('id_kelasGen', $data['id_kelasGen']);
        $this->db->update('kelas_general');
        echo $this->db->last_query() . '<br>';
    }
}