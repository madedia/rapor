<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Periode extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();

		if ($this->session->userdata('logged_in') == null)
        {
            $this->session->sess_destroy();
            redirect ('login');
        }
        $this->load->model('model_periode');
	}

	function index ($id_periode = null)
	{
		$data = array();
		
		$data['periode']= $this->model_periode->get_periode();

		if($id_periode==null)
			$id_periode = $this->session->userdata('periode_aktif');
		$data['id_periode'] = $id_periode;

		$data['periodeDet']= $this->model_periode->get_periodeDet($id_periode);

		//$data['aktif_periode']= $this->model_periode->get_periodeAktif();
		//$data['aktif_periodeDet']= $this->model_periode->get_periodeAktifDet();		

		$this->template->display('periode/periode', $data);
		
		//$this->template->display('coba', $data);
	}

	function data_periode()
	{
		$id_periode=$this->input->post('id_periode');
		$periodeDet=$this->model_periode->get_periodeDet3($id_periode);

		echo "<label>Tahun Ajaran ".$id_periode."</label>";
		echo "<table class='table table-bordered table-striped'>
              <thead>
                <tr>
                  <th>Nama Periode</th>
                  <th>Tanggal Rapat Dewan Guru</th>
                  <th>Tanggal Pembagian Rapor</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>";
              
        if ($periodeDet) {
            foreach ($periodeDet as $row) {
            	echo "<tr>";
            	echo "<td class='nr' style='display:none'>".$row['id_transPeriode']."</td>";
	            echo "<td>".$row['nama_tes']."</td>";
	            echo "<td>".$row['rapat_dewanGuru']."</td>";
	            echo "<td>".$row['tanggal_rapor']."</td>";
	            //echo "<td><a class='edit-periode' data-toggle='modal' href='#tambahPeriode'><span style='margin-right:3px;' class='glyphicon glyphicon-edit'></span></a></td>";
	            echo "<td><a class='edit-periode'><span style='margin-right:3px;' class='glyphicon glyphicon-edit'></span></a></td>";
	            echo "</tr>";
            }
        }
        echo "</tbody>
              </table>";
              
	}

	function aktivasi_periode($kontrol)
	{
		$data = array(
			'new_periode' => $this->input->post('id_periodePilih'), 
			'detail' => $this->input->post('id_tesPilih'),
		);
		//echo $kontrol." = ". $this->input->post('controller_aktif');

		$data['new_periode_detail'] = $data['new_periode'] . $data['detail'];

		if($data['detail']=="-uts1")
			$nama_tes = "UTS Semester Gasal";
		else if($data['detail']=="-uas1")
			$nama_tes = "UAS Semester Gasal";
		else if($data['detail']=="-uts2")
			$nama_tes = "UTS Semester Genap";
		else if($data['detail']=="-uas2")
			$nama_tes = "UAS Semester Genap";

		$this->model_periode->set_periodeAktif($data);
		
		$this->session->set_userdata('periode_aktif', $data['new_periode']);
		$this->session->set_userdata('periode_aktifDet', $data['new_periode_detail']);
		$this->session->set_userdata('periode_tes', $nama_tes);
		
		if($kontrol == "output")
			redirect('');
		else
			redirect($kontrol);
	}

	function tambah_periode()
	{
		$data = array(
			'tahun_awal' => $this->input->post('tahun_mulai'), 
			'tahun_akhir' => $this->input->post('tahun_akhir'),
		);
		$statusKelasOtomatis = $this->input->post('statusKelasOtomatis');

		$data['id_periode'] = $data['tahun_awal'] . "-" . $data['tahun_akhir'];

		$this->model_periode->insert_periode($data);

		$data['id_transPeriode'][0] = $data['id_periode'] . "-uts1";
		$data['nama_tes'][0] = "UTS Semester Gasal";
		$data['urutan'][0] = 1;
		$data['semester'][0] = 1;

		$data['id_transPeriode'][1] = $data['id_periode'] . "-uts2";
		$data['nama_tes'][1] = "UTS Semester Genap";
		$data['urutan'][1] = 2;
		$data['semester'][1] = 1;

		$data['id_transPeriode'][2] = $data['id_periode'] . "-uas1";
		$data['nama_tes'][2] = "UAS Semester Gasal";
		$data['urutan'][2] = 3;
		$data['semester'][2] = 2;

		$data['id_transPeriode'][3] = $data['id_periode'] . "-uas2";
		$data['nama_tes'][3] = "UAS Semester Genap";
		$data['urutan'][3] = 4;
		$data['semester'][3] = 2;

		$this->model_periode->insert_periodeDet($data);

		if($statusKelasOtomatis)
		{
			$data['periode_sebelum'] = ($data['tahun_awal']-1) . "-" . ($data['tahun_akhir']-1);
			$this->model_periode->insert_kelas($data);
		}
	
		redirect('periode');
	}

	function edit()
	{
		$id_transPeriode=$this->input->post('id_transPeriode');
		$detail= $this->model_periode->get_periodeDet2($id_transPeriode);

		if($detail)
		{
			echo "<input type='hidden' name='id_transPeriode' value='".$id_transPeriode."'>";
			echo "<table class='table'>";
			foreach ($detail as $row)
			{
				echo "<input type='hidden' name='id_periode' value='".$row['id_periode']."'>";
				echo "<tr>
					<th>ID</th>
					<td>".$row['id_transPeriode']."</td>
					</tr>";
				echo "<tr>
					<th>Nama Tes</th>
					<td>".$row['nama_tes']."</td>
					</tr>";
				echo "<tr>
					<th>Rapat Dewan Guru</th>
					<td>
	                    <div class='input-group'>
	                      <input type='date' name='rapat_dewanGuru' id='rapat_dewanGuru' class='form-control' value=".$row['rapat_dewanGuru'].">
	                      <div class='input-group-addon'>
	                        <i class='fa fa-calendar'></i>
	                      </div>
	                    </div>
					<td>";
				echo "<tr>
					<th>Tanggal Rapor</th>
					<td>
	                    <div class='input-group'>
	                      <input type='date' name='tanggal_rapor' id='tanggal_rapor' class='form-control' value=".$row['tanggal_rapor'].">
	                      <div class='input-group-addon'>
	                        <i class='fa fa-calendar'></i>
	                      </div>
	                    </div>
					<td>";
				echo "</tr>";
			}
		}
	}

	function do_edit()
	{
		$id_periode = $this->input->post('id_periode');
		$data = array(
			'id_transPeriode' => $this->input->post('id_transPeriode'),
			'rapat_dewanGuru' => $this->input->post('rapat_dewanGuru'), 
			'tanggal_rapor' => $this->input->post('tanggal_rapor'), 
		);

		$this->model_periode->edit_periode($data);

		redirect('periode/index/'.$id_periode);
	}
}

?>