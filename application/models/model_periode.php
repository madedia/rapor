<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_Periode extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_periode ()
	{
		$query = $this->db->get('periode');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
	}

	function get_periodeDet ($id_periode)
	{
        $this->db->where('id_periode', $id_periode);
        $this->db->order_by('urutan', 'asc');
		$query = $this->db->get('trans_periode');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
	}

    function get_periodeDet2 ($pilihan)
    {
        $this->db->where('id_transPeriode', $pilihan);
        $query = $this->db->get('trans_periode');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_periodeDet3 ($pilihan)
    {
        $this->db->where('id_periode', $pilihan);
        $this->db->order_by('urutan', 'asc');
        $query = $this->db->get('trans_periode');
        
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

	function get_periodeAktif () {
        $this->db->where('status', 1); 
        $query = $this->db->get('periode');

        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['id_periode'];
            return $val;
        }
        else return null;
    }

    function get_periodeAktifDet () {
        $this->db->where('status', 1); 
        $query = $this->db->get('trans_periode');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['nama_tes'];
            return $val;
        }
        else return null;
    }

	function insert_periode($data)
    {
        $this->db->set('status', 0);
        $this->db->insert('periode', $data);
    }

    function insert_periodeDet($data)
    {
        for ($i=0; $i < count($data['id_transPeriode']); $i++) { 
            $this->db->set('id_transPeriode', $data['id_transPeriode'][$i]); 
            $this->db->set('urutan', $data['urutan'][$i]); 
            $this->db->set('id_periode', $data['id_periode']); 
            $this->db->set('nama_tes', $data['nama_tes'][$i]); 
            $this->db->set('semester', $data['semester'][$i]); 
            $this->db->set('status', 0);
            $this->db->insert('trans_periode');
        }
    }

    function insert_kelas($data)
    {
        $this->db->where('id_periode', $data['periode_sebelum']);
        $query = $this->db->get('trans_kelas');

        if($query->result_array())
        {
            foreach ($query->result_array() as $row) {
                $this->db->set('id_tklsgeneralKey', $row['id_tklsgeneralKey']); 
                $this->db->set('id_guruWali', $row['id_guruWali']); 
                $this->db->set('id_periode', $data['id_periode']);
                $this->db->insert('trans_kelas');
            }
        }
    }

    function set_periodeAktif ($data) {
        $this->db->set('status', 0); 
        $this->db->where('id_periode', $this->session->userdata('periode_aktif')); 
        $this->db->update('periode'); 

        $this->db->set('status', 1); 
        $this->db->where('id_periode', $data['new_periode']); 
        $this->db->update('periode');

        $this->db->set('status', 0); 
        $this->db->where('id_transPeriode', $this->session->userdata('periode_aktifDet')); 
        $this->db->update('trans_periode'); 

        $this->db->set('status', 1); 
        $this->db->where('id_transPeriode', $data['new_periode_detail']); 
        $this->db->update('trans_periode');
    }

    function edit_periode($data)
    {
        $this->db->set('rapat_dewanGuru', $data['rapat_dewanGuru']); 
        $this->db->set('tanggal_rapor', $data['tanggal_rapor']); 
        $this->db->where('id_transPeriode', $data['id_transPeriode']);
        $this->db->update('trans_periode'); 
    }
}