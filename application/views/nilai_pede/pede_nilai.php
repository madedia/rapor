<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Pengembangan Diri
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('nilai_pede'); ?>"> Nilai Pengembangan Diri</a></li>
    <li class="active"> <?php echo $nama_pede; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
    <!-- Default box -->
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">
              <?php 
                if ($tipe_pede == "Ekstrakurikuler")
                  echo "Daftar Nilai Ekstrakurikuler ".$nama_pede;
                elseif ($tipe_pede == "Organisasi")
                  echo "Daftar Nilai Organisasi ".$nama_pede;
              ?>
            </h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_open(base_url().'nilai_pede/insert'); ?>
            <table id="tabel_nilai" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th style="text-align:center">No</th>
                    <th style="text-align:center">NIP</th>
                    <td style='display:none'></td>
                    <td style='display:none'></td>
                    <th style="text-align:center">Nama Siswa</th>
                    <td style='display:none'></td>
                    <th style="text-align:center" class="col-sm-1">Nilai</th>
                    <th style="text-align:center" class="col-sm-6">Keterangan</th>
                </tr>
              </thead>
              <tbody>
              <?php
              if ($siswa_pede) {
                $baris=1;
                foreach ($siswa_pede as $row) {
                  if ($row['id_transPedeSiswa']!=''){
                    echo "<tr>";
                    echo "<td style='text-align:center'>".$baris."</td>";
                    echo "<td>".$row['id_siswa']."</td>";
                    echo "<td style='display:none'><input type='text' class='form-control' name='isUpdate[]' value='".$row['isUpdate']."'></td>";
                    echo "<td style='display:none'><input type='text' class='form-control' name='id_transPedeSiswa[]' value='".$row['id_transPedeSiswa']."'></td>";
                    echo "<td>".$row['nama_siswa']."</td>";
                    if ($row['isUpdate'])
                    {
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_nilaiPede[]' value='".$row['id_nilaiPede']."'></td>";
                        echo "<td><input style='width:70px' type='text' class='form-control' name='nilai_pede[]' value='".$row['nilai_pede']."'></td>";
                        echo "<td><textarea style='width:100%' rows='2' type='text' class='form-control' name='keterangan[]' placeholder='jika tidak diisi maka kolom keterangan akan terisi secara otomatis saat proses submit'>".$row['keterangan']."</textarea></td>";
                    }
                    else
                    {
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_nilaiPede[]' value='".null."'></td>";
                        echo "<td><input style='width:70px' type='text' class='form-control' name='nilai_pede[]'></td>";
                        echo "<td><textarea style='width:100%' rows='2' type='text' class='form-control' name='keterangan[]' placeholder='jika tidak diisi maka kolom keterangan akan terisi secara otomatis saat proses submit'></textarea></td>";
                    }
                    echo "</tr>";
                    $baris++;
                  }
                }
              }
              ?>
              </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-danger'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_nilai").DataTable({
      scrollY: "300px",
      scrollX: true,
      "bPaginate": false,
      "scrollCollapse": true,
      "bAutoWidth" : false,
    });
  });
</script>