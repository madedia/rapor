<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Mata Pelajaran
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('nilai_mapel'); ?>"> Nilai Mata Pelajaran</a></li>
    <li class="active"> Upload Nilai</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title"><?php echo 'Upload Nilai Mata Pelajaran '.$nama_mapel." Kelas ".$nama_kelas; ?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
        	<?php echo form_open_multipart("nilai_mapel/impor_tabel/".$id_tAjar.'/'.$id_tKelasKey);?>
  				<?php
  					if (strpos($error, "You did not select a file to upload.") == true)
  						echo "<b>Anda belum memilih file untuk diunggah</b>";
  					else if (strpos($error,"The filetype you are attempting to upload is not allowed.") == true)
  						echo "<b> Maaf, tipe file yang Anda unggah tidak sesuai, baca ketentuan di bawah</b>";
  					else if (strpos($error,"The uploaded file exceeds the maximum allowed size in your PHP configuration file.") == true)
  						echo "<b>Maaf, ukuran file yang Anda unggah terlalu besar, baca ketentuan di bawah</b>";
  					else
  						echo $error;
  				?>
  				<input type="file" name="userfile" size="20" />

  				<br /><i>(Ekstensi file EXCEL yang diperbolehkan : *.xls, *.xlsx atau *.csv)</i>
  				<br /><i>(Ukuran file limit : 2MB)</i><br /><br />
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' value="upload" class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Upload</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->
