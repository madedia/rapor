<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Mata Pelajaran
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('nilai_mapel'); ?>"> Nilai Mata Pelajaran</a></li>
    <li class="active"><?php echo $nama_mapel.' Kelas '.$nama_kelas;?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Nilai Mata Pelajaran <?php echo $nama_mapel,' Kelas '.$nama_kelas;?></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
          <?php $isUpdate=1; echo form_open(base_url().'nilai_mapel/tambahNilai/'.$id_tAjar.'/'.$isUpdate); ?>
          <table id="tabel_nilai" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style='text-align:center; vertical-align:middle' rowspan='2'>No</th>
                <th style='text-align:center; vertical-align:middle' rowspan='2'>NIS</th>
                <th style='text-align:center; vertical-align:middle' rowspan='2'>Nama Siswa</th>
                <?php
                  if($statusUAS)
                    echo "<th style='text-align:center; vertical-align:middle' colspan='2'>Pengetahuan</th>
                      <th style='text-align:center; vertical-align:middle' colspan='2'>Keterampilan</th>
                      <th style='text-align:center; vertical-align:middle' colspan='2'>Sikap</th>";

                  else
                    echo "<th style='text-align:center; vertical-align:middle'>Pengetahuan</th>
                      <th style='text-align:center; vertical-align:middle'>Keterampilan</th>
                      <th style='text-align:center; vertical-align:middle'>Sikap</th>";
                ?>
                <th style='text-align:center; vertical-align:middle' rowspan='2'>Komentar</th>
                <th rowspan='2' style='display:none'></th>
                <th rowspan='2' style='display:none'></th>
                <th rowspan='2' style='display:none'></th>
              </tr>
              <tr>
                <th style='text-align:center; vertical-align:middle'>Nilai</th>
                <?php if($statusUAS) echo "<th style='text-align:center; vertical-align:middle'>Deskripsi</th>"; ?>
                <th>Nilai</th>
                <?php if($statusUAS) echo "<th style='text-align:center; vertical-align:middle'>Deskripsi</th>"; ?>
                <th>Nilai</th>
                <?php if($statusUAS) echo "<th style='text-align:center; vertical-align:middle'>Deskripsi</th>"; ?>
              </tr>
            </thead>
            <tbody>
              <?php
                if ($siswa_mapel) {
                  $baris=1;
                  foreach ($siswa_mapel as $row) {
                    if ($row['id_siswa']!=''){
                      echo "<tr>";
                      echo "<td>".$baris."</td>";
                      echo "<td>".$row['id_siswa']."</td>";
                      echo "<td>".$row['nama_siswa']."</td>";
                      
                      if ($row['isUpdate'])
                      {
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nPeng[]' value='".$row['pengetahuan']."'></td>";
                        if($statusUAS)
                        {
                          echo "<td><input class='form-control' name='des_peng[]'' list='kamus_peng' size='50' value='".$row['des_peng']."' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_peng)
                          {
                            echo "<datalist id='kamus_peng'>";
                            foreach ($kamus_peng as $row_temp) {
                              echo "<option value='".$row_temp['des_peng']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nKet[]' value='".$row['keterampilan']."'></td>";
                        if($statusUAS) 
                        {
                          echo "<td><input class='form-control' name='des_ket[]'' list='kamus_ket' size='50' value='".$row['des_ket']."' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_ket)
                          {
                            echo "<datalist id='kamus_ket'>";
                            foreach ($kamus_ket as $row_temp) {
                              echo "<option value='".$row_temp['des_ket']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nSik[]' value='".$row['sikap']."'></td>";
                        if($statusUAS) 
                        {
                          echo "<td><input class='form-control' name='des_sik[]'' list='kamus_sik' size='50' value='".$row['des_sik']."' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_sik)
                          {
                            echo "<datalist id='kamus_sik'>";
                            foreach ($kamus_sik as $row_temp) {
                              echo "<option value='".$row_temp['des_sik']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><textarea rows='1' type='text' class='form-control' name='komentar[]'>".$row['komentar']."</textarea></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_nilai[]' value='".$row['id_nilaiMapel']."'></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='isUpdate[]' value='".$row['isUpdate']."'></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_transSisKls[]' value='".$row['id_transSisKls']."'></td>";
                      }
                      else
                      {
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nPeng[]'></td>";
                        if($statusUAS)
                        {
                          echo "<td><input class='form-control' name='des_peng[]'' list='kamus_peng' size='50' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_peng)
                          {
                            echo "<datalist id='kamus_peng'>";
                            foreach ($kamus_peng as $row_temp) {
                              echo "<option value='".$row_temp['des_peng']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nKet[]'></td>";
                        if($statusUAS) 
                        {
                          echo "<td><input class='form-control' name='des_ket[]'' list='kamus_ket' size='50' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_ket)
                          {
                            echo "<datalist id='kamus_ket'>";
                            foreach ($kamus_ket as $row_temp) {
                              echo "<option value='".$row_temp['des_ket']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><input type='number' style='width:70px' min='0' max='100' class='form-control' name='nSik[]'></td>";
                        if($statusUAS) 
                        {
                          echo "<td><input class='form-control' name='des_sik[]'' list='kamus_sik' size='50' placeholder='otomatis bila tdk diisi'> </input></td>";
                          if($kamus_sik)
                          {
                            echo "<datalist id='kamus_sik'>";
                            foreach ($kamus_sik as $row_temp) {
                              echo "<option value='".$row_temp['des_sik']."'>";
                            }
                            echo "</datalist>";
                          }
                        }
                        echo "<td><textarea rows='1' type='text' class='form-control' name='komentar[]'></textarea></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_nilai[]' value='".null."'></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='isUpdate[]' value='".$row['isUpdate']."'></td>";
                        echo "<td style='display:none'><input type='text' class='form-control' name='id_transSisKls[]' value='".$row['id_transSisKls']."'></td>";
                      }
                      echo "</tr>";
                      $baris++;
                    }
                  }
                }
              ?>
            </tbody>
          </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_nilai").DataTable({
      scrollY: "300px",
      scrollX: true,
      "bPaginate": false,
      "scrollCollapse": true,
      "bAutoWidth" : false,
      "fixedColumns": {leftColumns: 3}
    });
  });
  
  $('body').addClass('sidebar-collapse');
</script>