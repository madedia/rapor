    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url() ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>CHIS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">RAPOR<b>CHIS</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                <a <?php if ($this->session->userdata('tipe') <= 1) echo "data-toggle='control-sidebar'"; ?>>
                  <span>Periode: <?php echo $this->session->userdata('periode_tes')." Tahun Ajaran ". $this->session->userdata('periode_aktif'); ?></span>
                </a>
              </li>
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url('assets/images/user7-128x128.jpg') ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $this->session->userdata('nama_pendek'); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url('assets/images/user7-128x128.jpg') ?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $this->session->userdata('nama_guru'); ?>
                      <small>di CHIS sejak Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="text-center">
                      <a>sebagai Guru</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url('user') ?>" class="btn btn-default btn-flat">Edit Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url('auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" <?php if ($this->session->userdata('tipe') <= 1) echo "data-toggle='control-sidebar'"; ?> ><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>