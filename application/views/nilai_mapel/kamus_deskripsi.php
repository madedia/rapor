<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Mata Pelajaran
    <small><?php echo "Daftar Deskripsi Nilai Kompetensi ".$nama_mapel." Kelas ".$nama_kelas; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('nilai_mapel'); ?>"> Nilai Mata Pelajaran</a></li>
    <li class="active"> <?php echo "KD ".$nama_mapel." Kelas ".$nama_kelas; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Kompetensi Pengetahuan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <?php echo form_open(base_url().'nilai_mapel/update_kamusDeskripsi/'.$id_tAjar); ?>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style='text-align:center; vertical-align:middle' class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Deskripsi Nilai</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($pengetahuan) {
                    $baris=1;
                    foreach ($pengetahuan as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_transPeng[]' value='".$row['id_transPeng']."'></td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$baris."</td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_peng[]'>".$row['des_peng']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Kompetensi Keterampilan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Deskripsi Nilai</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($keterampilan) {
                    $baris=1;
                    foreach ($keterampilan as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_transKet[]' value='".$row['id_transKet']."'></td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$baris."</td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_ket[]'>".$row['des_ket']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Kompetensi Sikap</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>No</th>
                      <th style="text-align:center; vertical-align:middle" class='col-sm-1'>Nilai Kompetensi</th>
                      <th style="text-align:center; vertical-align:middle">Deskripsi Nilai</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if ($sikap) {
                    $baris=1;
                    foreach ($sikap as $row) {
                      echo "<tr>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_tAjar[]' value='".$id_tAjar."'></td>";
                      echo "<td style='display:none'><input type='text' class='form-control' name='id_transSik[]' value='".$row['id_transSik']."'></td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$baris."</td>";
                      echo "<td style='text-align:center; vertical-align:middle'>".$row['sikap']."</td>";
                      echo "<td><textarea rows='2' type='text' class='form-control' name='des_sik[]'>".$row['des_sik']."</textarea></td>";
                      $baris ++;
                      echo "</tr>";
                    }
                  }
                  ?>
                  </tbody>
              </table>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="box-tools pull-right">
            <button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button>
            <?php echo form_close();?>
          </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->