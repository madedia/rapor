<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_pede extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();

		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }

		$this->load->model('model_pede');
		$this->load->model('model_nilaiPede');
	}

	function index()
	{
		$data['pede_enrol'] = $this->model_nilaiPede->get_pedeEnrol();

		$this->template->display('nilai_pede/pede_enrol', $data);
	}

	public function ekspor_tabel($id_transPedeGuru, $id_pembina)
	{
		$detail_pede= $this->model_pede->get_detail($id_transPedeGuru);
		$result= $this->model_nilaiPede->get_siswa2($id_transPedeGuru, $id_pembina);
		$result2= $this->model_nilaiPede->get_siswa($id_transPedeGuru, $id_pembina);

		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();

		if($detail_pede[0]['tipe_pede'] == "Ekstrakurikuler")
			$sheet->setCellValue('A1', 'Daftar Nilai Pengembangan Diri (Ekstrakurikuler) '.$detail_pede[0]['nama_pede']);
		else
			$sheet->setCellValue('A1', 'Daftar Nilai Pengembangan Diri (Organisasi) '.$detail_pede[0]['nama_pede']);

		$sheet->setCellValue('A2', 'Periode '.$this->session->userdata('periode_tes').' Tahun Ajaran '.$this->session->userdata('periode_aktif'));
    	$sheet->mergeCells('A1:D1');
    	$sheet->mergeCells('A2:D2');
    	$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    	$sheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    	$sheet->setCellValue('A4', 'Guru Pembina : '.$detail_pede[0]['nama_guru']);

		$sheet
			->setCellValue('A6', 'NIS')
			->setCellValue('B6', 'Nama Lengkap')
			->setCellValue('C6', 'Nilai')
			->setCellValue('D6', 'Keterangan');

		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(40);
		$sheet->getColumnDimension('C')->setWidth(10);
		$sheet->getColumnDimension('D')->setWidth(70);
		$sheet->getStyle('A6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$styleArray = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('argb' => '000000'),
		),
		),
		);

		$sheet->getStyle('A6:D6')->applyFromArray($styleArray);

		$baris=6;
		if($result)
		{
			foreach ($result as $res)
			{
				$temp = 0;
				if($result2)
				{
					foreach ($result2 as $res2) {
						if ($res['id_transPedeSiswa']==$res2['id_tPedeSiswaKey'])
						{
							$this->excel->getActiveSheet()
								->setCellValue('A'.($baris+1), $res2['id_siswa'])
								->setCellValue('B'.($baris+1), $res2['nama_siswa'])
								->setCellValue('C'.($baris+1), $res2['nilai_pede'])
								->setCellValue('D'.($baris+1), $res2['keterangan']);
							$temp = 1;				
							break;
						}
					}
					if($temp == 0) {
						$this->excel->getActiveSheet()
							->setCellValue('A'.($baris+1), $res['id_siswa'])
							->setCellValue('B'.($baris+1), $res['nama_siswa']);
					}
					$sheet->getStyle('A'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('B'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('C'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('D'.($baris+1))->applyFromArray($styleArray);
					$baris++;
				}
			}
		}
		
		$sheet->getDefaultRowDimension()->setRowHeight(20);

		$filename= 'Daftar Nilai PD '.$detail_pede[0]['nama_pede'].' periode '.$this->session->userdata('periode_aktifDet').'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function impor_tabel($id_transPedeGuru, $id_pembina)
	{
		$data_imp = array(
			'id_transPedeGuru' => $id_transPedeGuru,
			'id_pembina' => $id_pembina
		);
		$pede_detail= $this->model_pede->get_detail($id_transPedeGuru);
		foreach ($pede_detail as $row) {
			$data_imp['id_transPedeGuru'] = $row['id_transPedeGuru'];
			$data_imp['tipe_pede'] = $row['tipe_pede'];
			$data_imp['nama_pede'] = $row['nama_pede'];
		}

		$config['upload_path'] = './files/';
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$data_imp['error'] = $this->upload->display_errors();
			$this->template->display('nilai_pede/upload_nilaiPede', $data_imp);
		}
		else
		{
			$import_res['upload_data'] = $this->upload->data();

			$this->insert_nilaiImpor($id_transPedeGuru, $id_pembina, $import_res['upload_data']['file_name']);
		}
	}

	function insert_nilaiImpor($id_transPedeGuru, $id_pembina, $file_name)
	{
		$data = array();
		$data['id_input'] = $this->session->userdata('id_guru');
		$data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');

		$init = $this->model_nilaiPede->get_siswa2($id_transPedeGuru, $id_pembina);
		$result = $this->model_nilaiPede->get_siswa($id_transPedeGuru, $id_pembina);

		/*
		Pengecekan  apakah nilai yang akan di import apakah sebelunya telah terisi apa belum
		Jika sudah maka update
		Jika belum maka insert
		*/
		if($init)
		{
			$barisExcel = 0;
			foreach ($init as $row1)
			{
				$temp = 0;
				if ($result)
				{
					foreach ($result as $row2)
					{
						if($row1['id_transPedeSiswa'] == $row2['id_tPedeSiswaKey'])
						{
							$data['id_tPedeSiswaKey'][$barisExcel] = $row2['id_tPedeSiswaKey'];
							$data['id_nilaiPede'][$barisExcel] = $row2['id_nilaiPede'];
							$data['isUpdate'][$barisExcel] = 1;
							$temp = 1;				
							break;
						}
					}
				}
				if($temp == 0) {
					$data['id_tPedeSiswaKey'][$barisExcel] = $row1['id_transPedeSiswa'];
					$data['isUpdate'][$barisExcel] = 0;
				}
				$barisExcel++;
			}
		}

		$this->load->library('excel');
		$eksekusi = 0;

		$inputFileName = FCPATH . 'files/'. $file_name;
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		$objPHPExcel->setActiveSheetIndex(0);

		// Validasi format antara yang di-export dengan yang di-import
		$worksheet = $objPHPExcel->getActiveSheet();
		$jmlRowExcel = ($worksheet->getHighestRow())-6;
		if(count($init) == $jmlRowExcel)
		{
	        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

	        //remove header until row=6
	        array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);array_shift($sheetData);

	        $A = $worksheet->getCell( "A6" ) ->getValue();
	        $B = $worksheet->getCell( "B6" ) ->getValue();
	        $C = $worksheet->getCell( "C6" ) ->getValue();
	        $D = $worksheet->getCell( "D6" ) ->getValue();

			if(strcasecmp($A,"NIS")==0)
	            if(strcasecmp($B,"Nama Lengkap")==0)
	                if(strcasecmp($C,"Nilai")==0)
	                    if(strcasecmp($D,"Keterangan")==0)
	                        $eksekusi = 1;
		}

        if($eksekusi)
        {
        	$baris = 0;
        	foreach ($sheetData as $rowCol)
        	{
				//$data['id_siswa'][$baris] = $rowCol['A'];
				//$data['nama_siswa'][$baris] = $rowCol['B'];
				$data['nilai_pede'][$baris] = $rowCol['C'];
				$data['keterangan'][$baris] = $rowCol['D'];
        		$baris++;
        	}

        	unlink($inputFileName);

        	$this->insert($data, 1);
        }
	}

	function isi($id_transPedeGuru, $id_pembina)
	{
		$data['pede_detail']= $this->model_pede->get_detail($id_transPedeGuru);
		foreach ($data['pede_detail'] as $row) {
			$data['id_transPedeGuru'] = $row['id_transPedeGuru'];
			$data['tipe_pede'] = $row['tipe_pede'];
			$data['nama_pede'] = $row['nama_pede'];
		}

		$data['siswa_pede'] = array();
		$siswa_enrol= $this->model_nilaiPede->get_siswa2($id_transPedeGuru, $id_pembina);	
		$siswa_nilai= $this->model_nilaiPede->get_siswa($id_transPedeGuru, $id_pembina);
		if($siswa_enrol)
		{
			foreach ($siswa_enrol as $row1) {
				$temp=0;
				if ($siswa_nilai)
				{
					foreach ($siswa_nilai as $row2) {
						if($row1['id_transPedeSiswa'] == $row2['id_transPedeSiswa'])
						{
							$row2['isUpdate'] = 1;
							array_push($data['siswa_pede'], $row2);
							$temp = 1;				
							break;
						}
					}
				}
				if($temp == 0) {
					$row1['isUpdate'] = 0;
					array_push($data['siswa_pede'], $row1);
				}
			}
		}

		$this->template->display('nilai_pede/pede_nilai', $data);
	}

	function insert ($data=null, $impor=null)
	{
		if($impor==null)
		{
			$data = array(
				'isUpdate' => $this->input->post('isUpdate'),
				'id_tPedeSiswaKey' => $this->input->post('id_transPedeSiswa'),
				'nilai_pede' => $this->input->post('nilai_pede'),
				'keterangan' => $this->input->post('keterangan'),
				'id_nilaiPede' => $this->input->post('id_nilaiPede'),
			);
		}

		for ($i=0; $i < count($data['id_tPedeSiswaKey']); $i++) { 
			if ($data['nilai_pede'][$i] != null)
				if ($data['keterangan'][$i] == null)
					$data['keterangan'][$i]= $this->model_nilaiPede->get_keterangan($data['nilai_pede'][$i]);
		}

		$this->model_nilaiPede->insert($data);
		redirect("nilai_pede");
	}
}