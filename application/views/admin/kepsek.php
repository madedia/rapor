<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrator
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrator</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#tambahKepsek">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3><?php echo count($kepsek) ?></h3>
                    <p>Tambah Kepala Sekolah</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person-add"></i>
                  </div>
                </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Master Data Kepala Sekolah</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
          					<th style="text-align:center">NIP</th>
          					<th style="text-align:center">Nama Kepala Sekolah</th>
          					<th style="text-align:center">Tahun Aktif</th>
          					<th style="text-align:center">Tahun Non-Aktif</th>
                    <th style="text-align:center">Status</th>
          					<th style="text-align:center">Opsi</th>
                    <th style="display:none"></th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($kepsek) {
            $baris=1;
						foreach ($kepsek as $row) {
							if ($row['id_transKepsek']!=''){
								echo "<tr>";
                echo "<td>".($baris)."</td>";
								echo "<td>".$row['id_kepsek']."</td>";
								echo "<td>".$row['nama_guru']."</td>";
								echo "<td>".$row['tahun_mulai']."</td>";
								echo "<td>".$row['tahun_berakhir']."</td>";
                if($row['status_kepsek']) {
                  echo "<td>Aktif</td>";
                }
                else {
                  echo "<td>Non-Aktif</td>";
                }
								echo '<td style="text-align:center">&nbsp <a href="#editKepsek" class="edit-kepsek btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                echo '&nbsp <a href="'.base_url('admin/hapus_kepsek/'.$row['id_transKepsek']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
                echo "<td class='nr' style='display:none'>".$row['id_transKepsek']."</td>";
								echo "</tr>";
							}
              $baris++;
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal1 Tambah Kepsek-->
<div class="modal fade" id="tambahKepsek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kepala Sekolah</h4>
      </div>
      <div class="modal-body">
    <?php 
      echo form_open('admin/tambah_kepsek');?>
    <div class="form-group">
      <?php 
      echo form_label('NIP | Nama', 'id_guru', array(
          'class' => 'control-label',
      ));
      ?>

      <select name="id_guru" id="id_guru" class="form-control">
        <option value=''disabled selected>- Pilih -</option>
        <?php 
          if ($guru){
            foreach ($guru as $row) {
              echo "<option value='".$row['id_guru']."'";
              echo ">".$row['id_guru'].' | '.$row['nama_guru']."</option>";
            }
          } 
        ?>
      </select>
    </div>

    <div class="form-group">
      <?php 
      echo form_label('Periode', '', array(
          'class' => 'control-label',
      ));
      ?>
      <div class="row">
            <div class="col-sm-4">
              <select name="tahun_aktif" id="tahun_aktif" class="form-control" onChange=get_thnNonAktif()>
                <?php 
                  for ($i=2010; $i <= 2100; $i++) { 
                    if ($i==date("Y")){
                      echo "<option selected value='".$i."'";
                      echo ">".$i."</option>";
                    }
                    else
                    {
                      echo "<option value='".$i."'";
                      echo ">".$i."</option>";
                    }  
                  } 
                ?>
              </select>
            </div>
            <div class="col-sm-1">
              <b>s.d</b>
            </div>
            <div class="col-sm-4">
              <select name="tahun_nonAktif" id="tahun_nonAktif" class="form-control">
                <option value="<?php echo (date("Y")+4); ?>" selected><?php echo (date("Y")+4); ?></option>
              </select>
            </div>
      </div>
    </div>

    <!-- <div class="form-group">
      <input type="checkbox" class="minimal" name="status_kepsek" value="1">
        <b>Aktifkan*</b>
      </input>
      <i><br><br>*Dengan memilih menu aktivasi Kepala Sekolah baru maka Kepala Sekolah periode sebelumnya akan di non-aktifkan.</i>
    </div> -->
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal1-->

<!-- Modal2 Edit Kepsek-->
<div class="modal fade" id="editKepsek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Kepala Sekolah</h4>
      </div>
      <?php echo form_open('admin/do_edit_kepsek');?>
      <div class="modal-body" id="edit_kepsek">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end2 -->

<script type="text/javascript">
  function get_thnNonAktif(){
    var tahun_aktif =$('#tahun_aktif').val();
    //alert(tahun_aktif);
    var url = '<?php echo base_url('admin/get_thnNonAktif');?>'

    $.ajax
      ({
      type: "POST",
      url: url,
      data: {"tahun_aktif":tahun_aktif},
      cache: false,
      success: function(html)
      {
        $("#tahun_nonAktif").html(html);
      }
    });
  }

  $(".edit-kepsek").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_transKepsek = $row.find(".nr").text(); // Find the text
      var $url = '<?php echo base_url('admin/edit_kepsek');?>'

      $.ajax
      ({
        type: "POST",
        url: $url,
        data: {"id_transKepsek":$id_transKepsek},
        cache: false,
        success: function(html)
        {
          $("#edit_kepsek").html(html);
        }
    });

      $('#editKepsek').modal('show');
    });
</script>