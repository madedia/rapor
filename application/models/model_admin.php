<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_admin extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    function get_struktur($pilihan)
    {
        if ($pilihan == 'guru') {
            $query = $this->db->get('guru');
        }
        else if ($pilihan == 'user') {
            $this->db->where('id_guru = id_user');
            $this->db->where('tipe = id_tipeUser');
            $query = $this->db->get('guru, user, tipe_user');
        }
        else if ($pilihan == 'tipe_user') {
            $query = $this->db->get('tipe_user');
        }
        else if ($pilihan == 'periode') {
            $query = $this->db->get('periode');
        }
        else if ($pilihan == 'kelas_general') {
            $query = $this->db->get('kelas_general');
        }
        else if ($pilihan == 'siswa') {
            $this->db->where('status', 'Aktif');
            $query = $this->db->get('siswa');
        }
        else if ($pilihan == 'kepsek') {
            $this->db->where('id_kepsek = id_guru');
            $query = $this->db->get('trans_kepsek, guru');
        }
        else if ($pilihan == 'aturan_konversi') {
            $query = $this->db->get('konversi_nilai');
        }
        else if ($pilihan == 'log_backup') {
            $this->db->order_by('waktu', 'DESC');
            $query = $this->db->get('log_backupdb');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_tipeUser($id_user)
    {
        $this->db->where('id_guru', $id_user);
        $query = $this->db->get('guru');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['tipe'];
            return $val;
        }
        else return null;
    }

    function get_userDetail($id_user)
    {
        $this->db->where('id_guru', $id_user);
        $this->db->where('id_guru = id_user');
        $this->db->where('tipe = id_tipeUser');
        $query = $this->db->get('guru, user, tipe_user');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambah_user($data)
    {
        $this->db->insert('user', $data);
    }

    function tambah_kepsek($data)
    {
        /*if ($data['status_kepsek']) {
            $this->db->set('status_kepsek', 0);
            $this->db->set('tahun_berakhir', date("Y"));
            $this->db->where('status_kepsek', 1);
            $this->db->update('trans_kepsek');

            $this->db->set('tipe', 1);
            $this->db->update('trans_kepsek');

        }*/
        $this->db->insert('trans_kepsek', $data);
    }

    function update_user($data)
    {
        $this->db->set('tipe', $data['tipe']);
        $this->db->where('id_guru', $data['id_user']);
        $this->db->update('guru');

        $this->db->set('password', $data['password']);
        $this->db->where('id_user', $data['id_user']);
        $this->db->update('user');

        //echo $this->db->last_query() . '<br>';
    }

    /*function delete($pilihan, $key)
    {
        if($pilihan == "kepsek")
        {
            $this->db->where('id_transKepsek', $key);
        }
    }*/

    function get_detailKepsek($id_transKepsek)
    {
        $this->db->where('id_transKepsek', $id_transKepsek);
        $this->db->where('id_kepsek = id_guru');
        $query = $this->db->get('trans_kepsek,guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function delete_user($id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->delete('user');
    }

    function delete_kepsek($id_transKepsek)
    {
        $this->db->where('id_transKepsek', $id_transKepsek);
        $this->db->delete('trans_kepsek');
    }    

    function insert_logBackupDB($data)
    {
        $this->db->insert('log_backupdb', $data);
    }

    public function get_deskripsi($pilihan)
    {
        if ($pilihan ==1) {
            $query = $this->db->get('deskripsi_npengetahuan');
        }
        elseif ($pilihan ==2) {
            $query = $this->db->get('deskripsi_nketerampilan');
        }
        elseif ($pilihan ==3) {
            $query = $this->db->get('deskripsi_nsikap');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_ajarKelas($id_guru)
    {
        $this->db->where('id_guruWali', $id_guru);
        $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_kelas');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function get_ajarMapel($id_guru)
    {
        $this->db->where('trans_ajar.id_guru', $id_guru);
        $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_ajar, trans_kelas');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function get_ajarPede($id_guru)
    {
        $this->db->where('trans_pedeguru.id_pembina', $id_guru);
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_pedeguru');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function update_kepsek($data)
    {
        if($data['status_kepsek'])
        {
            $this->db->set('status_kepsek', 0);
            $this->db->where('status_kepsek', 1);
            $this->db->update('trans_kepsek');
        }

        $this->db->set('id_kepsek', $data['id_kepsekEdit']);
        $this->db->set('tahun_mulai', $data['tahun_mulai']);
        $this->db->set('tahun_berakhir', $data['tahun_berakhir']);
        $this->db->set('status_kepsek', $data['status_kepsek']);
        $this->db->where('id_transKepsek', $data['id_transKepsek']);
        $this->db->update('trans_kepsek');

        if ($data['id_kepsekEdit']!=$data['id_kepsekAwal'])
        {
            if($data['guru_awalKelas'] == null)
            {
                if($data['guru_awalMapel'] == null)
                {
                    if($data['guru_awalPede'] == null)
                    {
                        $this->db->set('tipe', 'NULL', false);
                        $this->db->where('id_guru', $data['id_kepsekAwal']);
                        $this->db->where('tipe > 0');
                        $this->db->update('guru');
                    }
                    else
                    {
                        $this->db->set('tipe', 4);
                        $this->db->where('id_guru', $data['id_kepsekAwal']);
                        $this->db->where('tipe > 0');
                        $this->db->update('guru');
                    }
                }
                else
                {
                    $this->db->set('tipe', 3);
                    $this->db->where('id_guru', $data['id_kepsekAwal']);
                    $this->db->where('tipe > 0');
                    $this->db->update('guru');
                }
            }
            else
            {
                $this->db->set('tipe', 2);
                $this->db->where('id_guru', $data['id_kepsekAwal']);
                $this->db->where('tipe > 0');
                $this->db->update('guru');
            }
        }  

        $this->db->set('tipe', 1);
        $this->db->where('id_guru', $data['id_kepsekEdit']);
        $where = "(`tipe` > 1 OR `tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function update_aturanKonversi($data)
    {
        for ($i=0; $i < count($data['id_predikat']); $i++) { 
            $this->db->set('id_predikat', $data['id_predikat'][$i]);
            $this->db->set('batas_atas', $data['batas_atas'][$i]);
            $this->db->set('sikap', $data['sikap'][$i]);
            $this->db->set('detail_sikap', $data['detail_sikap'][$i]);
            $this->db->set('nilai_ekskul', $data['nilai_ekskul'][$i]);
            $this->db->where('id_predikat', $data['id_predikat'][$i]);
            $this->db->update('konversi_nilai');
        }
    }

    function update_kiKd($data)
    {
        for ($i=0; $i < count($data['id_pengetahuan']); $i++) { 
            $this->db->set('des_peng', $data['des_peng'][$i]);
            $this->db->where('id_pengetahuan', $data['id_pengetahuan'][$i]);
            $this->db->update('deskripsi_npengetahuan');
        }
        for ($i=0; $i < count($data['id_keterampilan']); $i++) { 
            $this->db->set('des_ket', $data['des_ket'][$i]);
            $this->db->where('id_keterampilan', $data['id_keterampilan'][$i]);
            $this->db->update('deskripsi_nketerampilan');
        }
        for ($i=0; $i < count($data['id_sikap']); $i++) { 
            $this->db->set('des_sikap', $data['des_sikap'][$i]);
            $this->db->where('id_sikap', $data['id_sikap'][$i]);
            $this->db->update('deskripsi_nsikap');
        }
    }
}