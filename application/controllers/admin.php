<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 2) {
        	redirect ('home');
        }

		$this->load->model('model_admin');
	}

	function index ()
	{
		$data['user']= $this->model_admin->get_struktur('user');
		$data['guru']= $this->model_admin->get_struktur('guru');

		$this->template->display('admin/user', $data);
	}

	function tambah_user ()
	{
		$data = array(
			'id_user' => $this->input->post('id_user'),
			//'tipe' => $this->input->post('tipe_user'),
			'password' => $this->input->post('password'),
			//'password' => md5($this->input->post('password')),
		);

		$this->model_admin->tambah_user($data);
		redirect('admin');
	}

	function get_tipeUser ()
	{
		$tipe_user= $this->model_admin->get_struktur('tipe_user');
		$res=$this->model_admin->get_tipeUser($this->input->post('id_user'));
		if($res)
			echo "<option value=''disabled>- Pilih -</option>";
		else
			echo "<option value=''disabled selected>- Pilih -</option>";
		if ($tipe_user){
			foreach ($tipe_user as $row) {
				if($res == $row['id_tipeUser'])
					echo "<option selected value='".$row['id_tipeUser']."'>".$row['hak_akses']."</option>";
				else
					echo "<option value='".$row['id_tipeUser']."'>".$row['hak_akses']."</option>";
			}
		}	
	}

	function edit_user ()
	{
		$id_user=$this->input->post('id_user');
		$user = $this->model_admin->get_userDetail($id_user);
		$tipe_user= $this->model_admin->get_struktur('tipe_user');

		if($user)
		{
			echo "<input type='hidden' name='id_user' value='".$id_user."'>";
			echo "<table class='table'>";

			foreach ($user as $row) {
				echo "<tr>
					<th>ID user</th>
					<td>".$row['id_user']."</td>
					</tr>";
				echo "<tr>
					<th>Nama Guru</th>
					<td>".$row['nama_guru']."</td>
					</tr>";
				echo "<tr>
					<th>Hak Akses</th>

					<td><div class='form-group'>
					<select name='tipe_user' id='tipe_user' class='form-control'>";
				if ($tipe_user){
					foreach ($tipe_user as $row2) {
						if($row2['hak_akses'] == $row['hak_akses'])
						{
							echo "<option selected value='".$row2['id_tipeUser']."'";
							echo ">".$row2['hak_akses']."</option>";
						}	
						else
						{
							echo "<option value='".$row2['id_tipeUser']."'";
							echo ">".$row2['hak_akses']."</option>";
						}
					}
				}	
				echo "</select>
				</div>


					</td>
					</tr>";
				echo "<tr>
					<th>Password</th>
					<td>";
				echo form_input(array(
				  'class' => 'form-control',
	              'name'  => 'password',
	              'id'    => 'password',
	              'value' => $row['password'],
	            ));
				echo "</td>
					</tr>";
			}
			echo "</table>";
		}
	}

	function do_edit_user ()
	{
		$data = array(
			'id_user' => $this->input->post('id_user'),
			'tipe' => $this->input->post('tipe_user'),
			'password' => $this->input->post('password'),
			//'password' => md5($this->input->post('password')),
		);

		$this->model_admin->update_user($data);
		redirect('admin');
	}

	function hapus_user($id_user)
	{
		$this->model_admin->delete_user($id_user);
		redirect('admin');
	}

	function backup_db($action = null)
	{
		$data['log_backup']= $this->model_admin->get_struktur('log_backup');
		$this->template->display('admin/backup_db', $data);

		/*if($action)
		{
			$data = array(
				'id_user' => $this->session->userdata('id_guru'),
				'keterangan' => $this->input->post('keterangan'),
			);

			$this->model_admin->insert_logBackupDB($data);

			// Load the DB utility class
	        $this->load->dbutil();

	        // nyiapin aturan untuk file backup
	        $aturan = array(    
	                'format'      => 'zip',            
	                'filename'    => 'rapor_chis_backup'.date("Y-m-d-H-i-s").'.sql'
	              );

	        $backup =& $this->dbutil->backup($aturan);

	        $nama_database = 'rapor_chis_backup-on-'. date("Y-m-d-H-i-s") .'.zip';
	        $simpan = '/backup'.$nama_database;

	        $this->load->helper('file');
	        write_file($simpan, $backup);

	        $this->load->helper('download');
	        force_download($nama_database, $backup);
	        redirect('admin');
		}*/
	}

	function do_backupDB()
	{
		$data = array(
			'id_user' => $this->session->userdata('id_guru'),
			'keterangan' => $this->input->post('keterangan'),
		);

		$this->model_admin->insert_logBackupDB($data);

		// Load the DB utility class
        $this->load->dbutil();

        // nyiapin aturan untuk file backup
        $aturan = array(    
                'format'      => 'zip',            
                'filename'    => 'rapor_chis_backup'.date("Y-m-d-H-i-s").'.sql'
              );

        $backup =& $this->dbutil->backup($aturan);

        $nama_database = 'rapor_chis_backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        $simpan = '/backup'.$nama_database;

        $this->load->helper('file');
        write_file($simpan, $backup);

        $this->load->helper('download');
        force_download($nama_database, $backup);
        redirect('admin/backup_db, refresh');
	}

	function aturan_konversi($status_edit=null)
	{
		$data['aturan_konversi']= $this->model_admin->get_struktur('aturan_konversi');
		$data['status_edit'] = $status_edit;

		$this->template->display('admin/aturan_konversi', $data);
	}

	function edit_aturan_konversi()
	{
		$data = array(
			'id_predikat' => $this->input->post('id_predikat'),
			'batas_atas' => $this->input->post('batas_atas'),
			'batas_bawah' => $this->input->post('batas_bawah'),
			'sikap' => $this->input->post('sikap'),
			'detail_sikap' => $this->input->post('detail_sikap'),
			'nilai_ekskul' => $this->input->post('nilai_ekskul'),
		);
		$this->model_admin->update_aturanKonversi($data);
		redirect('admin/aturan_konversi');
	}

	function kepsek()
	{
		$data['guru']= $this->model_admin->get_struktur('guru');
		$data['kepsek']= $this->model_admin->get_struktur('kepsek');

		$this->template->display('admin/kepsek', $data);
	}

	function get_thnNonAktif()
	{
		$tahun_mulai = $this->input->post('tahun_aktif');
		echo "<option value='".($tahun_mulai+4)."' selected>".($tahun_mulai+4)."</option>";
		/*for ($i=$tahun_mulai; $i <= 2100; $i++) { 
             echo "<option value='".$i."'>".$i."</option>";
        }*/
	}

	function tambah_kepsek()
	{
		$data = array(
			'id_kepsek' => $this->input->post('id_guru'),
			'tahun_mulai' => $this->input->post('tahun_aktif'),
			'tahun_berakhir' => $this->input->post('tahun_nonAktif'),
			//'status_kepsek' => $this->input->post('status_kepsek'),
		);

		$this->model_admin->tambah_kepsek($data);
		redirect('admin/kepsek');
	}

	function ki_kd()
	{
		$data['pengetahuan'] = $this->model_admin->get_deskripsi(1);
		$data['keterampilan'] = $this->model_admin->get_deskripsi(2);
		$data['sikap'] = $this->model_admin->get_deskripsi(3);
		$this->template->display('admin/ki_kd', $data);
	}

	function edit_kepsek()
	{
		$id_transKepsek = $this->input->post('id_transKepsek');
		$detail = $this->model_admin->get_detailKepsek($id_transKepsek);
		$guru= $this->model_admin->get_struktur('guru');
	
		echo '<div class="form-group">';
	    echo form_label('NIP | Nama', '', array(
	          'class' => 'control-label',
	      ));
	    echo '<select name="id_kepsekEdit" id="id_kepsekEdit" class="form-control">';
	    if ($guru){
            foreach ($guru as $row) {
            	if($detail[0]['id_guru']==$row['id_guru'])
              		echo "<option selected value='".$row['id_guru']."'>".$row['id_guru'].' | '.$row['nama_guru']."</option>";
              	else
              		echo "<option value='".$row['id_guru']."'>".$row['id_guru'].' | '.$row['nama_guru']."</option>";
            }
          }
	    echo '</select></div>';

	    echo '<div class="form-group">';
	    echo form_label('Periode', '', array(
	          'class' => 'control-label',
	      ));
	    echo '<div class="row">
            	<div class="col-sm-4">';
            		echo '<select name="tahun_mulai" id="tahun_mulai" class="form-control">';
            		for ($i=2010; $i <= 2100; $i++) { 
				    	if($detail[0]['tahun_mulai'] == $i)
			            	echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            else
			            	echo '<option value="'.$i.'">'.$i.'</option>';
			        }
			        echo '</select>'; 
			    echo '</div>
			    <div class="col-sm-1">
	              <b>s.d</b>
	            </div>'; 
			    echo '<div class="col-sm-4">';
            		echo '<select name="tahun_berakhir" id="tahun_berakhir" class="form-control">';
            		for ($i=2010; $i <= 2100; $i++) { 
				    	if($detail[0]['tahun_berakhir'] == $i)
			            	echo '<option value="'.$i.'" selected>'.$i.'</option>';
			            else
			            	echo '<option value="'.$i.'">'.$i.'</option>';
			        }
			        echo '</select>'; 
			    echo '</div>'; 
        echo '	</div>
        	</div>';

	    echo '<div class="form-group">';
	    echo form_label('Status', '', array(
	          'class' => 'control-label',
	      ));
	    echo '<select name="status_kepsek" id="status_kepsek" class="form-control">';
	    if($detail[0]['status_kepsek']) {
	    	echo '<option value="1" selected>Aktif</option>';
	    	echo '<option value="0">Non Aktif</option>';
	    } else {
	    	echo '<option value="1">Aktif</option>';
	    	echo '<option value="0" selected>Non Aktif</option>';
	    }
	    echo '</select> <i>* Bila menu aktivasi ini dikatifkan maka status Kepala Sekolah yang aktif sebelumnya akan di non-aktifkan terlebih dahulu oleh sistem secara otomatis.</i>
	    	<input style="display:none" name="id_kepsekAwal" value="'.$detail[0]['id_kepsek'].'">
	    	<input style="display:none" name="id_transKepsek" value="'.$detail[0]['id_transKepsek'].'">
	    	</div>';
	}

	function do_edit_kepsek()
	{
		$data = array(
			'id_transKepsek' => $this->input->post('id_transKepsek'),
			'id_kepsekEdit' => $this->input->post('id_kepsekEdit'),
			'id_kepsekAwal' => $this->input->post('id_kepsekAwal'),
			'tahun_mulai' => $this->input->post('tahun_mulai'),
			'tahun_berakhir' => $this->input->post('tahun_berakhir'),
			'status_kepsek' => $this->input->post('status_kepsek'),
		);

		if ($data['id_kepsekEdit']!=$data['id_kepsekAwal'])
		{
			$data['guru_awalKelas'] = $this->model_admin->get_ajarKelas($data['id_kepsekAwal']);
			$data['guru_awalMapel'] = $this->model_admin->get_ajarMapel($data['id_kepsekAwal']);
	        $data['guru_awalPede'] = $this->model_admin->get_ajarPede($data['id_kepsekAwal']);
		}

		$this->model_admin->update_kepsek($data);
		redirect('admin/kepsek');
	}

	public function edit_KIKD()
	{
		$data = array(
			'id_pengetahuan' => $this->input->post('id_pengetahuan'),
			'id_keterampilan' => $this->input->post('id_keterampilan'),
			'id_sikap' => $this->input->post('id_sikap'),
			'des_peng' => $this->input->post('des_peng'),
			'des_ket' => $this->input->post('des_ket'),
			'des_sikap' => $this->input->post('des_sik'),
		);

		$this->model_admin->update_kiKd($data);
		
		redirect('admin/ki_kd');
	}

	function hapus_kepsek ($id_transKepsek)
	{
		$this->model_admin->delete_kepsek($id_transKepsek);

		redirect('admin/kepsek');
	}
}

?>