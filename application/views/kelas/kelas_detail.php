<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kelas
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?php echo base_url('kelas'); ?>">Kelas</a></li>
    <li class="active"><?php echo $detail[0]['nama_kelas'] ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  <!-- Default box -->
    <div class="box box-danger">
        <div class="box-header with-border">
			<h3 class="box-title">Data Siswa <?php echo "Kelas ".$detail[0]['nama_kelas'] ?></h3>
            <div class="box-tools pull-right">
                <!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button> -->
                <button type="button" class="btn btn-sm pull-right btn-danger btn-block" data-toggle="modal" data-target="#tambahSiswaKelas">Tambah Siswa</button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_siswaKelas" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
					<th style="text-align:center">NIS</th>
					<th style="text-align:center">Nama</th>
					<th style="text-align:center">Opsi</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($siswa) {
						$baris=1;
						foreach ($siswa as $row) {
							if ($row['id_siswa']!=''){
								echo "<tr>";
								echo "<td>".($baris)."</td>";
								echo "<td>".$row['id_siswa']."</td>";
								echo "<td>".$row['nama_siswa']."</td>";
								//echo "<td style='text-align:center'><a href='"; echo base_url('kelas/delete_siswa/'.$id_transKelas.'/'.$row['id_transSisKls']); echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-remove'></span>Delete</a></td>";
								echo '<td style="text-align:center"> <a href="'.base_url('kelas/delete_siswa/'.$id_transKelas.'/'.$row['id_transSisKls']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
								echo "</tr>";
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="tambahSiswaKelas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Siswa Kelas</h4>
      </div>
      <div class="modal-body">
		<?php echo form_open('kelas/tambah_siswa/'.$id_transKelas);?>
		<div class="form-group">
			<div class="table-responsive">
				<table id="tabel_tambahSiswa" class="table table-striped">
					<thead>
						<tr>
							<th>Nomor Induk</th>
							<th>Nama Siswa</th>
							<th>Pilih</th>
						</tr>
					</thead>
				<?php 
					$pilih = array();
					if ($siswa_available){
						echo "<tbody>";
						foreach ($siswa_available as $row) {
							/*if($siswa)
							{
								if (FALSE === array_search($row['id_siswa'], array_column($siswa, 'id_siswa'))) {
									echo "</tr>";
									echo "<td>".$row['id_siswa']."</td>";
									echo "<td>".$row['nama_siswa']."</td>";
									
										echo "<td>
										 <div class='checkbox'>
											  <label><input type='checkbox' name='pilih[]' value = ".$row['id_siswa']."></label>
										 </div>
										</td>";
								}
							}
							else*/
							echo "</tr>";
							echo "<td>".$row['id_siswa']."</td>";
							echo "<td>".$row['nama_siswa']."</td>";

							echo "<td>
							 <div class='checkbox'>
								  <label><input type='checkbox' name='pilih[]' value = ".$row['id_siswa']."></label>
							 </div>
							</td>";
						}
						echo "</tbody>";
					}
					else echo "--- Tidak Ada Siswa ---";
				?>
				</table>
			</div>
		</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-danger'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_siswaKelas").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    });
  });
</script>

<script>
  $(function () {
    $("#tabel_tambahSiswa").DataTable({
    	"deferRender": true,
    	"bLengthChange": false, //menghilangkan show entries drop down
    	"pagingType": "simple",
    	"columnDefs": [ {
		    "bSearchable" : false,
		    "orderable": false,
		    "targets": 2,
        } ]
    });
  });
</script>