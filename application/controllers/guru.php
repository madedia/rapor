<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guru extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 3) {
        	redirect ('');
        }

		$this->load->model('model_guru');
	}

	function index ()
	{
		$data['guru']= $this->model_guru->get_guru();

		$this->template->display('guru/guru', $data);
	}

	function tambah_guru ()
	{
		$data = array(
			'id_guru' => $this->input->post('id_guru'),
			'nama_guru' => $this->input->post('nama_guru'),
			'nama_pendek' => $this->input->post('nama_pendek'),
		);

		$this->model_guru->tambah_guru($data);
		redirect('guru');
	}

	function get_detail()
	{
		$id_guru=$this->input->post('id_guru');

		$data = $this->model_guru->detail_guru($id_guru);

		echo '<img class="profile-user-img img-responsive img-circle" src="'.base_url('assets/images/siswa2.png').'" alt="User profile picture">
              <h3 class="profile-username text-center">'.$data[0]['nama_guru'].'</h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>NIP</b> <a class="pull-right">'.$data[0]['id_guru'].'</a>
                </li>
              </ul>';
	}

	function edit_detail()
	{
		$id_guru=$this->input->post('id_guru');

		$data = $this->model_guru->detail_guru($id_guru);

		echo '<img class="profile-user-img img-responsive img-circle" src="'.base_url('assets/images/guru.png').'" alt="User profile picture">
              <input type="text" name="nama_guru" class="profile-username text-center form-control" value="'.$data[0]['nama_guru'].'"></input>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>NIP</b> <a class="pull-right">'.$data[0]['id_guru'].'</a>
                </li>
              </ul>
              <input style="display:none" name="id_guru" value="'.$data[0]['id_guru'].'">';
	}

	function do_edit_detail()
	{
		$data = array(
			'id_guru' => $this->input->post('id_guru'),
			'nama_guru' => $this->input->post('nama_guru'),
		);

		//var_dump($data);

		$this->model_guru->edit_detail($data);
		redirect('guru');
	}

	function delete($id_guru)
	{
		$this->model_guru->delete($id_guru);
		redirect('guru');
	}
}

?>