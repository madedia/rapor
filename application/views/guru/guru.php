<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Guru
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Guru</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#tambahGuru">
	              <!-- small box -->
	              <div class="small-box bg-green">
	                <div class="inner">
	                  <h3><?php echo count($guru) ?></h3>
	                  <p>Tambah Guru</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-person-add"></i>
	                </div>
	              </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
  <!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">Data Guru</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_siswa" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">No</th>
					<th style="text-align:center">NIP</th>
					<th style="text-align:center">Nama</th>
          <?php if ($this->session->userdata('tipe') == 0) { ?>
            <th style="text-align:center">Opsi</th>
          <?php } ?>
          <th style='display:none'></th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($guru) {
					$baris=1;
						foreach ($guru as $row) {
							if ($row['id_guru']!=''){
								echo "<tr>";
								echo "<td>".($baris)."</td>";
								echo "<td><a href='#detailGuru' class='detail-guru' href='#detailGuru'>".$row['id_guru']."</a></td>";
								echo "<td>".$row['nama_guru']."</td>";;
								
                if ($this->session->userdata('tipe') == 0) {
								  /*echo "<td><a href='#editGuru' class='edit-guru'>";
                  echo"<span style='margin-right:3px;' class='glyphicon glyphicon-info-sign' data-toggle='tooltip' data-placement='top' title='Edit'></span></a>";
                  echo "<a class='bootbox_alert' href='"; echo base_url('guru/delete/'.$row['id_guru']);
                  echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-remove-sign' data-toggle='tooltip' data-placement='top' title='Delete'></span></a>";*/
                  echo '<td style="text-align:center">&nbsp <a href="#editGuru" class="edit-guru btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a></span>';
                  echo '&nbsp <a href="'.base_url('guru/delete/'.$row['id_guru']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
                }

                echo "<td class='nr' style='display:none'>".$row['id_guru']."</td>";

								echo "</tr>";
								$baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="tambahGuru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Guru</h4>
      </div>
      <div class="modal-body">
	<?php 
		//$attributes = array('id' => 'konduite', 'name' => 'konduite');
		echo form_open('guru/tambah_guru');?>
		<div class="form-group">
			<?php 
			echo form_label('NIP', 'id_guru');
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'id_guru',
		              'id'    => 'id_guru',
		            ));
			?>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama', 'nama_guru', array(
			    'class' => 'control-label',
			));
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'nama_guru',
		              'id'    => 'nama_guru',
		            ));
			?>
		</div>			
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-success'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<!-- end -->

<!-- Modal2 -->
<div class="modal fade" id="detailGuru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Profil Guru</h4>
      </div>
      <div class="modal-body" id="detail_guru">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div> 
<!-- end2 -->

<!-- Modal3 -->
<div class="modal fade" id="editGuru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Profil Guru</h4>
      </div>
      <div class="modal-body">
        <?php echo form_open('guru/do_edit_detail'); ?>
        <!-- Profile Image -->
        <div class="box-body box-profile" id='edit_guru'>
        </div><!-- /.box-body -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> 
<!-- end3 -->

<script type="text/javascript">
  $(".detail-guru").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_guru = $row.find(".nr").text(); // Find the text
      var $url = '<?php echo base_url('guru/get_detail');?>'
      // Let's test it out
      //alert($id_guru);

      $.ajax
      ({
      type: "POST",
      url: $url,
      data: {"id_guru":$id_guru},
      cache: false,
      success: function(html)
      {
        $("#detail_guru").html(html);
      }
    });

      $('#detailGuru').modal('show');
    });

  $(".edit-guru").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_guru = $row.find(".nr").text(); // Find the text
      var $url = '<?php echo base_url('guru/edit_detail');?>'

      $.ajax
      ({
      type: "POST",
      url: $url,
      data: {"id_guru":$id_guru},
      cache: false,
      success: function(html)
      {
        $("#edit_guru").html(html);
      }
    });

      $('#editGuru').modal('show');
    });
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_siswa").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    });
  });
</script>