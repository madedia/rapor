<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Administrator
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Administrator</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
          </div><!-- /.row -->
          
  	<!-- Default box -->
    <div class="box box-success">
        <div class="box-header with-border">
        	<h3 class="box-title">
            <?php
              if ($status_edit == null)
                echo 'Tabel Aturan Konversi';
              else
                echo 'Edit Tabel Aturan Konversi';
            ?>         
          </h3>
            <div class="box-tools pull-right">
              <?php
                if ($status_edit == null) {
                  echo '<a href="'.base_url('admin/aturan_konversi/1').'" type="button" class="btn btn-sm pull-right btn-warning btn-block">Edit Aturan</a>';
                }
              ?>
                
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center">Predikat</th>
          					<th style="text-align:center">Nilai Batas Atas</th>
          					<th style="text-align:center">Nilai Batas Bawah</th>
          					<th style="text-align:center">Kode Sikap</th>
          					<th style="text-align:center">Detail</th>
          					<th style="text-align:center">Nilai Ekstrakurikuler</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($aturan_konversi) {
            if($status_edit != null)
              echo form_open('admin/edit_aturan_konversi');
						foreach ($aturan_konversi as $row) {
							if ($row['id_predikat']!=''){
								echo "<tr>";
                if($status_edit == null)
                {
                  echo "<td>".$row['id_predikat']."</td>";
                  echo "<td>".$row['batas_atas']."</td>";
                  echo "<td>".$row['batas_bawah']."</td>";
                  echo "<td>".$row['sikap']."</td>";
                  echo "<td>".$row['detail_sikap']."</td>";
                  echo "<td>".$row['nilai_ekskul']."</td>";  
                }
                else
                {
                  echo "<td><input type='text' class='form-control' name='id_predikat[]' value='".$row['id_predikat']."'></td>";
                  echo "<td><input type='number' min='0' max='4' step='0.01' class='form-control' name='batas_atas[]' value='".$row['batas_atas']."'></td>";
                  echo "<td><input type='number' min='0' max='4' step='0.01' class='form-control' name='batas_bawah[]' value='".$row['batas_bawah']."'></td>";
                  echo "<td><input type='text' class='form-control' name='sikap[]' value='".$row['sikap']."'></td>";
                  echo "<td><input type='text' class='form-control' name='detail_sikap[]' value='".$row['detail_sikap']."'></td>";
                  echo "<td><input type='text' class='form-control' name='nilai_ekskul[]' value='".$row['nilai_ekskul']."'></td>";
                }
								echo "</tr>";
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <?php
              if($status_edit != null)
              {
                echo '<a href="'.base_url('admin/aturan_konversi/').'" type="button" class="btn btn-warning pull-left">Cancel</a>';
                echo "<td><button type='submit' class='btn btn-success pull-right'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Perubahan</button></td>";
                echo form_close();
              }
            ?>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->