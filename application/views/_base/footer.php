		</div><!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
		      <b>Versi</b> 1.0
			</div>
		    <strong>Copyright &copy; 2015 <a href="http://itmbali.com">ITM BALI</a></strong> Pengrajin Software & Konsultan IT.
		</footer>

		 <!-- Control Sidebar -->
	      <aside class="control-sidebar control-sidebar-light">
	        <!-- Create the tabs -->
	        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
	          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
	        </ul>
	        <!-- Tab panes -->
	        <div class="tab-content">
	          <!-- Settings tab content -->
	          <div class="tab-pane active" id="control-sidebar-settings-tab">
	            <?php echo form_open(base_url('periode/aktivasi_periode/'.$this->uri->segment(1))); ?>
	              <h3 class="control-sidebar-heading">Ganti Periode Aktif</h3>
	              
	              	<div class="form-group">
	              		<?php echo "<input type='hidden' name='controller_aktif' value='".$this->uri->segment(1)."'></input>"; ?>
	              	</div>

	                <div class="form-group">
	                  <label>Tahun Ajaran</label>
	                  <select name='id_periodePilih' class="form-control">
	                  	<?php
	                  		if($this->session->userdata('footer')) {
	                  			foreach ($this->session->userdata('footer') as $row) {
	                  				if($row['id_periode'] == $this->session->userdata('periode_aktif'))
	                  					echo "<option selected value='".$row['id_periode']."'>".$row['id_periode']."</option>";
	                  				else
	                  					echo "<option value='".$row['id_periode']."'>".$row['id_periode']."</option>";
	                  			}
	                  		}
	                  	?>
	                  </select>
	                </div>

	                <!-- radio -->
	                <div class="form-group">
	                  <div class="radio">
	                    <label>
	                      <input type="radio" name="id_tesPilih" value="-uts1" required>
	                      UTS Semester Gasal
	                    </label>
	                  </div>
	                  <div class="radio">
	                    <label>
	                      <input type="radio" name="id_tesPilih" value="-uas1">
	                      UAS Semester Gasal
	                    </label>
	                  </div>
	                  <div class="radio">
	                    <label>
	                      <input type="radio" name="id_tesPilih" value="-uts2">
	                      UTS Semester Genap
	                    </label>
	                  </div>
	                  <div class="radio">
	                    <label>
	                      <input type="radio" name="id_tesPilih" value="-uas2">
	                      UAS Semester Genap
	                    </label>
	                  </div>
	                </div>

                <button type="submit" class="btn btn-info center-block">Aktifkan</button>
                <?php echo form_close(); ?>
	          </div><!-- /.tab-pane -->
	        </div>
	      </aside><!-- /.control-sidebar -->
	      
		  <!-- Add the sidebar's background. This div must be placed
	           immediately after the control sidebar -->
	      <div class="control-sidebar-bg"></div>

	    <!-- Modal Info Aturan Konversi -->
		<div class="modal modal-info fade" id="aturanKonversi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Aturan Konversi Nilai</h4>
		      </div>
		      <div class="modal-body">
		      	<?php
              		if($this->session->userdata('konversi')) {
              			echo '<table class="table table-bordered">
			                <thead>
			                  <tr>
			                    <th style="text-align:center">Predikat</th>
								<th style="text-align:center">Nilai Batas Atas</th>
								<th style="text-align:center">Nilai Batas Bawah</th>
								<th style="text-align:center">Kode Sikap</th>
								<th style="text-align:center">Detail</th>
								<th style="text-align:center">Nilai Ekstrakurikuler</th>
			                  </tr>
			                </thead>
			                <tbody>';
              			foreach ($this->session->userdata('konversi') as $row) {
							echo '<tr>';
							echo '<td>'.$row['id_predikat'].'</td>';
							echo '<td>'.$row['batas_atas'].'</td>';
							echo '<td>'.$row['batas_bawah'].'</td>';
							echo '<td>'.$row['sikap'].'</td>';
							echo '<td>'.$row['detail_sikap'].'</td>';
							echo '<td>'.$row['nilai_ekskul'].'</td>';
							echo '</tr>';
						}
						echo '</tbody></table>';
              		}
              	?>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-outline" data-dismiss="modal">Tutup</button>
		      </div>
		    </div>
		  </div>
		</div> <!-- end modal5 -->

		</div><!-- ./wrapper -->

	    <!-- datepicker -->
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
	    <!-- Slimscroll -->
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
	    <!-- FastClick -->
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/fastclick/fastclick.min.js') ?>"></script>
	    <!-- iCheck 1.0.1 -->
    	<script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js') ?>"></script>
	    <!-- AdminLTE App -->
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/dist/js/app.min.js') ?>"></script>

	    <!-- InputMask untuk Input tanggal Periode
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/input-mask/jquery.inputmask.js') ?>"></script>
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/input-mask/jquery.inputmask.date.extensions.js') ?>"></script>
	    <script src="<?php echo base_url('assets/AdminLTE-2.3.0/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>-->
	    
	</body>
</html>

<script type="text/javascript">
$(".bootbox_alert").on("click", function (e) {
    // Init
    var self = $(this);
    e.preventDefault();

    // Show Message        
    bootbox.confirm("Apakah Anda yakin?", function (result) {
        if (result) {                
            self.unbind("click");
            self.get(0).click();
        }
    }).addClass("modal-danger");
});
</script>