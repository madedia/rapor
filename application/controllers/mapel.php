<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapel extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 3) {
        	redirect ('');
        }

		$this->load->model('model_mapel');
	}

	function index ($sesi = null)
	{
		$data['mapel']= $this->model_mapel->get_struktur('mapel');
		$data['kelompok']= $this->model_mapel->get_struktur('kelompok');
		$data['guru']= $this->model_mapel->get_struktur('guru');
		$data['enrol']= $this->model_mapel->get_enrol();

		if($sesi == null)
		{
			$sesi = 'enrol';
		}
		$data['sesi'] = $sesi;

		$this->template->display('mapel/mapel', $data);
	}

    function tambah_kelompok()
    {
        $data = array(
            'id_kelompok' => $this->input->post('id_kelompok'),
            'nama_kelompok' => $this->input->post('nama_kelompok'),
            'detail' => $this->input->post('detail'),
        );

        $this->model_mapel->tambah_kelompok($data);
        redirect('mapel');
    }

	function tambah_mapel ()
	{
		$data = array(
			'id_kelompok' => $this->input->post('id_kelompok'), 
			'nama_mapel' => $this->input->post('nama_mapel'),
			'id_kelasGen' => $this->input->post('daftarKelas'),
			'kkm100' => $this->input->post('kkm'),
			'kkm_aksel100' => $this->input->post('kkm_aksel'),
		);

		$string = $data['nama_mapel'];

		/*
			Cek Mata Kuliah Muatan Lokal Kelas Bahasa Inggris
		*/
		$flagKategori = 0;
		if ($data['id_kelompok']=="E")
        	$flagKategori = 1;

        $string = strtoupper($string);

        $string = str_replace("DAN", "" , $string);
        $string = str_replace("BAHASA", "" , $string);
        $string = str_replace("PENDIDIKAN", "" , $string);
        $string = ltrim($string);
        $string = rtrim($string);

        $pjgChar = strlen($string)-1;
        $pjgStr = str_word_count($string);
        $subString = "";

        if (strpos($string, '('))
        {
            $string = str_replace(')', "" , $string);
            $subString = substr($string, strpos($string, '(') + 1);
            $string = substr($string, 0, strpos($string, "("));
        }

        $acronym = "";
        $words = explode(" ", $string);
        
        if ($pjgStr==1)
        {
            $acronym .= $string[0];
            $acronym .= $string[1];
            $acronym .= $string[2];
        }
        else if ($pjgStr==2)
        {
            foreach ($words as $w) {
            if (!empty($w))
                $acronym .= $w[0];
            }
            $acronym .= $string[$pjgChar];
        }
        else if ($pjgStr > 2)
        {
            foreach ($words as $w) {
            if (!empty($w))
                $acronym .= $w[0];
        	}
        }

        if ($flagKategori)
        {
            $acronym .= '_ENG';
        }

        if ($subString)
        {
            $acronym .= '_';
            $acronym .= $subString[0];
            $acronym .= $subString[1];
        }

        for ($baris=0; $baris<count($data['id_kelasGen']); $baris++)
        { 
        	$data['id_mapel'][$baris] = $acronym."_".$data['id_kelasGen'][$baris];
        }

		$this->model_mapel->tambah_mapel($data);

		redirect('mapel');
	}

	function enrol_mapel ()
	{
		$data = array(
			'id_guru' => $this->input->post('id_guru'),
			'id_tkelasKey' => $this->input->post('nama_kelas'),
		);

		$record = $this->input->post('id_mapel');
		$data['id_mapelKey'] = substr($record, 0, strpos($record, "~"));

        $temp_mapel = $this->model_mapel->get_mapelX($data['id_mapelKey']);
        $status = $this->model_mapel->get_statusKelas($data['id_tkelasKey']);
		if($status)
        {
            $data['kkm_ajar'] = $temp_mapel[0]['kkm_aksel'];
            $data['kkm_ajar100'] = $temp_mapel[0]['kkm_aksel100'];
        }         
        else
        {
            $data['kkm_ajar'] = $temp_mapel[0]['kkm'];
            $data['kkm_ajar100'] = $temp_mapel[0]['kkm100'];
        }

		$id_tAjar = $this->model_mapel->tambah_enrolPaket($data);
        $this->model_mapel->update_tipeEnrol($data['id_guru']);
        
        $kompetensi = $this->model_mapel->get_kompetensi(1);
        $this->model_mapel->insert_tdeskripsi($id_tAjar, $kompetensi, 1);
        $kompetensi = $this->model_mapel->get_kompetensi(2);
        $this->model_mapel->insert_tdeskripsi($id_tAjar, $kompetensi, 2);
        $kompetensi = $this->model_mapel->get_kompetensi(3);
        $this->model_mapel->insert_tdeskripsi($id_tAjar, $kompetensi, 3);
		redirect('mapel');
	}

	function get_kelas()
	{
		$record=$this->input->post('id_mapel');

		$id_mapel = substr($record, strpos($record, "~") + 1);

		echo '<option value="">- Pilih Kelas -</option>';

		$data = $this->model_mapel->get_kelas($id_mapel);
		if ($data){
			foreach ($data as $row) {
				echo "<option value='".$row['id_transKelas']."'";
				echo ">".$row['nama_kelas']."</option>";
			}
		}
	}

    function edit_enrol()
    {
        $id_transAjar = $this->input->post('id_transAjar');
        $detail=$this->model_mapel->get_detail($id_transAjar, 'enrol');
        $guru=$this->model_mapel->get_struktur('guru');

        echo '<div class="form-group">';
        echo form_label('Mata Pelajaran', 'id_mapelKey', array(
                'class' => 'control-label',
                ));
        echo '<select disabled required name="" id="" class="form-control">
                <option selected value="'.$detail[0]['nama_mapel'].'">'.$detail[0]['nama_mapel'].' | Kelas '.$detail[0]['id_kelasGen'].'</option>
                </select></div>';

        echo '<div class="form-group">';
        echo form_label('Kelas', 'id_mapelKey', array(
                'class' => 'control-label',
                ));
        echo '<select disabled required name="" id="" class="form-control">
                <option selected value="'.$detail[0]['id_tKelasKey'].'">'.$detail[0]['nama_kelas'].'</option>
                </select></div>';

        echo '<div class="form-group">';
        echo form_label('Guru Pengampu', 'id_guruEdit', array(
                'class' => 'control-label',
                ));
        echo '<select required name="id_guruEdit" id="id_guruEdit" class="form-control">
                <option disabled value="">- Pilih Guru -</option>';
        if ($guru){
            foreach ($guru as $row) {
                if($detail[0]['id_guru'] == $row['id_guru'])
                    echo '<option selected value="'.$row['id_guru'].'">'.$row['id_guru'].' | '.$row['nama_guru'].'</option>';
                else
                    echo '<option value="'.$row['id_guru'].'">'.$row['id_guru'].' | '.$row['nama_guru'].'</option>';
            }
        }
        echo '</select>
            <input style="display:none" name="id_transAjar" value="'.$detail[0]['id_transAjar'].'">
            <input style="display:none" name="id_guruAwal" value="'.$detail[0]['id_guru'].'">
            </div>';
    }

    function do_edit_enrol()
    {
        $data = array(
            'id_transAjar' => $this->input->post('id_transAjar'),
            'id_guruAwal' => $this->input->post('id_guruAwal'),
            'id_guruEdit' => $this->input->post('id_guruEdit'),
        );

        if ($data['id_guruEdit']!=$data['id_guruAwal'])
        {
            $data['guru_awalMapelLain'] = $this->model_mapel->get_mapelLain($data['id_guruAwal'], $data['id_transAjar']);
            $data['guru_awalPede'] = $this->model_mapel->get_ajarPede($data['id_guruAwal'], $data['id_transAjar']);
            $this->model_mapel->update_enrol($data);
        }
        redirect('mapel');
    }

    function edit_katalog()
    {
        $id_mapel = $this->input->post('id_mapel');
        $detail=$this->model_mapel->get_detail($id_mapel, 'katalog');
        $kelompok=$this->model_mapel->get_struktur('guru');

        echo '<div class="form-group">';
        echo form_label('ID Mapel', 'id_mapelKey', array(
                'class' => 'control-label',
                ));
        echo '<input disabled class="form-control" name="" value="'.$detail[0]['id_mapel'].'"></div>';

        echo '<div class="form-group">';
        echo form_label('Kelas', 'id_mapelKey', array(
                'class' => 'control-label',
                ));
        echo '<input disabled class="form-control" value="'.$detail[0]['id_kelasGen'].'"></div>';
        
        echo '<div class="form-group">';
        echo form_label('Nama Mapel', 'nama_mapel', array(
                'class' => 'control-label',
                ));
        echo '<input type="text" class="form-control" name="nama_mapel" value="'.$detail[0]['nama_mapel'].'"></div>';

        echo '<div class="form-group">';
        echo form_label('KKM Kelas Regular (skala 0-100)', 'kkm100', array(
                'class' => 'control-label',
                ));
        echo '<input type="number" min="0" class="form-control" name="kkm100" value="'.$detail[0]['kkm100'].'"></div>';

        echo '<div class="form-group">';
        echo form_label('KKM Kelas Akselerasi (skala 0-100)', 'kkm_aksel100', array(
                'class' => 'control-label',
                ));
        echo '<input type="number" min="0" class="form-control" name="kkm_aksel100" value="'.$detail[0]['kkm_aksel100'].'"></div>';
        echo '<input style="display:none" class="form-control" name="id_mapel" value="'.$detail[0]['id_mapel'].'">';
    }

    function do_edit_katalog()
    {
        $data = array(
            'id_mapel' => $this->input->post('id_mapel'),
            'kkm100' => $this->input->post('kkm100'),
            'kkm_aksel100' => $this->input->post('kkm_aksel100'),
        );
        $this->model_mapel->update_katalog($data);
        redirect('mapel');
    }

    function edit_kategori()
    {
        $id_kelompok = $this->input->post('id_kelompok');
        $detail=$this->model_mapel->get_detail($id_kelompok, 'kategori');

        echo '<div class="form-group">';
        echo form_label('ID Kelompok', 'id_kelompok', array(
                'class' => 'control-label',
                ));
        echo '<input disabled class="form-control" name="" value="'.$detail[0]['id_kelompok'].'"></div>';

        echo '<div class="form-group">';
        echo form_label('Nama Kelompok', 'nama_kelompok', array(
                'class' => 'control-label',
                ));
        echo '<input class="form-control" name="nama_kelompok" value="'.$detail[0]['nama_kelompok'].'"></div>';

        echo '<div class="form-group">';
        echo form_label('Detail', 'detail', array(
                'class' => 'control-label',
                ));
        echo '<input class="form-control" name="detail" value="'.$detail[0]['detail'].'"></div>';
        echo '<input style="display:none" class="form-control" name="id_kelompok" value="'.$detail[0]['id_kelompok'].'"></div>';
    }

    function do_edit_kategori()
    {
        $data = array(
            'id_kelompok' => $this->input->post('id_kelompok'),
            'nama_kelompok' => $this->input->post('nama_kelompok'),
            'detail' => $this->input->post('detail'),
        );
        $this->model_mapel->update_kategori($data);
        redirect('mapel');
    }

    function delete ($pilihan, $key)
    {
        $data['pilihan']=$pilihan;
        $data['key']=$key;

        $this->model_mapel->delete_key($data);

        redirect('mapel');
    }
}

?>