<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pengembangan Diri
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Pengembangan Diri</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
      <div class="row">
        <?php if ($this->session->userdata('tipe') <= 1) { ?>
          <div class="col-lg-3 col-xs-6">
            <a data-toggle="modal" href="#tambahPede">
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3><?php echo count($pede) ?></h3>
                    <p>Tambah Pengembangan Diri</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-ribbon-a"></i>
                  </div>
                </div>
            </a>
          </div><!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <a data-toggle="modal" href="#enrolPede">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3><?php echo count($enrol) ?></h3>
                    <p>Enrol Pengembangan Diri</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-map"></i>
                  </div>
                </div>
            </a>
          </div><!-- ./col -->
        <?php } ?>
      </div><!-- /.row -->
          
  <!-- Custom Tabs (Pulled to the right) -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
      <li class="active"><a href="#tab_1-1" data-toggle="tab">Enrol</a></li>
      <li><a href="#tab_2-2" data-toggle="tab">Katalog</a></li>
      <li class="pull-left header"><i class="fa fa-th"></i> </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1-1">
      	<table id="tabel_enrol" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center">No</th>
        				<th style="text-align:center">Nama</th>
        				<th style="text-align:center">Nama Pembina</th>
        				<th style="text-align:center">Opsi</th>
                <th style="display:none"></th>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($enrol) {
					$baris=1;
					foreach ($enrol as $row) {
						if ($row['id_pede']!=''){
							echo "</tr>";
							echo "<td style='text-align:center'>".($baris)."</td>";
							echo "<td>".$row['nama_pede']."</td>";
							echo "<td>".$row['nama_guru']."</td>";
							//echo "<td><a href='"; echo base_url('pede/detail/'.$row['id_transPedeGuru']); echo"'><span style='margin-right:3px;' class='glyphicon glyphicon-list-alt'></span></a></td>";
              echo '<td style="text-align:center">&nbsp <a href="'.base_url('pede/detail/'.$row['id_transPedeGuru']).'" class="btn btn-sm btn-info" data-toggle="tooltip" title="Daftar Siswa"><i class="fa fa-group"></i></a>';
              echo '&nbsp <a href="#editEnrol" class="edit-enrol btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
              echo '&nbsp <a href="'.base_url('pede/delete/enrol/'.$row['id_transPedeGuru']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
              echo "<td class='nr1' style='display:none'>".$row['id_transPedeGuru']."</td>";
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2-2">
        <table id="tabel_pede" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center">No</th>
        				<th style="text-align:center">Nama</th>
        				<th style="text-align:center">Tipe</th>
                <?php if ($this->session->userdata('tipe') <= 1) { ?>
                  <th style="text-align:center">Opsi</th>
                  <th style="display:none"></th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($pede) {
					$baris=1;
					foreach ($pede as $row) {
						if ($row['id_pede']!=''){
							echo "</tr>";
							echo "<td style='text-align:center'>".($baris)."</td>";
							echo "<td>".$row['nama_pede']."</td>";
							echo "<td>".$row['tipe_pede']."</td>";
              if ($this->session->userdata('tipe') <= 1) {
                echo '<td style="text-align:center">&nbsp <a href="#editKatalog" class="edit-katalog btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                echo '&nbsp <a href="'.base_url('pede/delete/katalog/'.$row['id_pede']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
                echo "<td class='nr2' style='display:none'>".$row['id_pede']."</td>";
              }
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->

</section><!-- /.content -->

<!-- Modal Tambah Pengembangan Diri-->
<div class="modal fade" id="tambahPede" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Katalog Pengembangan Diri</h4>
      </div>
      <div class="modal-body">
		<?php 
		echo form_open('pede/tambah_pede');?>
		<div class="form-group">
			<?php 
			echo form_label('Nama Pengembangan Diri', 'nama_pede');
			echo form_input(array(
					  'class' => 'form-control',
		              'name'  => 'nama_pede',
		              'id'    => 'nama_pede',
		            ));
			?>
		</div>

		<div class="form-group">
			<?php 
				echo form_label('Tipe', 'tipe_pede', array(
				    'class' => 'control-label',
				));
			?>
			<select name="tipe_pede" id="tipe_pede" class="form-control">
				<option selected disabled value=''>- Pilih -</option>
				<option value='Ekstrakurikuler'>Ekstrakurikuler</option>
				<option value='Organisasi'>Organisasi</option>
			</select>
		</div>
	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal -->

<!-- Modal Enrol Pengembangan Diri-->
<div class="modal fade" id="enrolPede" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Enrol Pengembangan Diri</h4>
      </div>
      <div class="modal-body">
		<?php 
		echo form_open('pede/enrol_pede');?>
		<div class="form-group">
			<?php 
				echo form_label('Nama Pengembangan Diri', '', array(
				    'class' => 'control-label',
				));
			?>
			<select name="nama_pede" id="nama_pede" class="form-control">
				<option selected disabled value=''>- Pilih -</option>
				<?php 
					if ($pede){
						foreach ($pede as $row) {
							echo "<option value='".$row['id_pede']."'";
							echo ">".$row['nama_pede']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
				echo form_label('Guru Pembina', 'id_guru', array(
				    'class' => 'control-label',
				));
			?>
			<select name="id_guru" id="id_guru" class="form-control">
				<option selected disabled value=''>- Pilih -</option>
				<?php 
					if ($guru){
						foreach ($guru as $row) {
							echo "<option value='".$row['id_guru']."'";
							echo ">".$row['id_guru'].' | '.$row['nama_guru']."</option>";
						}
					}	
				?>
			</select>
		</div>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal-->

<!-- Modal Edit Enrol Pengembangan Diri-->
<div class="modal fade" id="editEnrol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Pengembangan Diri Enrol</h4>
      </div>
      <?php echo form_open('pede/do_edit_enrol');?>
      <div class="modal-body" id="edit_enrol">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal -->

<!-- Modal Edit Katalog Pengembangan Diri-->
<div class="modal fade" id="editKatalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Katalog Pengembangan Diri</h4>
      </div>
      <?php echo form_open('pede/do_edit_katalog');?>
      <div class="modal-body" id="edit_katalog">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal -->

<!-- page script -->
<script type="text/javascript">

		function get_kelas(){
			var id_mapel =$('#id_mapel').val();
			var url = '<?php echo base_url('mapel/get_kelas');?>'
			//window.alert (id_mapel);

			$.ajax
				({
				type: "POST",
				url: url,
				data: {"id_mapel":id_mapel},
				cache: false,
				success: function(html)
				{
					$("#nama_kelas").html(html);
				}
			});
		}

    $(".edit-enrol").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_transPedeGuru = $row.find(".nr1").text(); // Find the text
      var $url = '<?php echo base_url('pede/edit_enrol');?>'

      $.ajax
      ({
        type: "POST",
        url: $url,
        data: {"id_transPedeGuru":$id_transPedeGuru},
        cache: false,
        success: function(html)
        {
          $("#edit_enrol").html(html);
        }
    });

      $('#editEnrol').modal('show');
    });

    $(".edit-katalog").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_pede = $row.find(".nr2").text(); // Find the text
      var $url = '<?php echo base_url('pede/edit_katalog');?>'

      $.ajax
      ({
        type: "POST",
        url: $url,
        data: {"id_pede":$id_pede},
        cache: false,
        success: function(html)
        {
          $("#edit_katalog").html(html);
        }
    });

      $('#editKatalog').modal('show');
    });

</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    });
  });

  $(function () {
    $("#tabel_pede").DataTable(/*{
      "columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    }*/);
  });
</script>