<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pede extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') == 3) {
        	redirect ('');
        }

		$this->load->model('model_pede');
	}


	function index ($sesi = null)
	{
		$data['pede']= $this->model_pede->get_struktur('pede');
		$data['enrol']= $this->model_pede->get_struktur('enrol');
		$data['guru']= $this->model_pede->get_struktur('guru');

		if($sesi == null)
		{
			$sesi = 'enrol';
		}
		$data['sesi'] = $sesi;

		$this->template->display('pede/pede', $data);
	}

	function tambah_pede()
	{
		$data = array(
			'nama_pede' => $this->input->post('nama_pede'),
			'tipe_pede' => $this->input->post('tipe_pede'),
		);

		$this->model_pede->tambah_pede($data);
		redirect('pede');
	}

	function enrol_pede()
	{
		$data = array(
			'id_tPede' => $this->input->post('nama_pede'),
			'id_pembina' => $this->input->post('id_guru'),
			'id_periode' => $this->session->userdata('periode_aktif'),
		);

		$this->model_pede->enrol_pede($data);
		$this->model_pede->update_tipeEnrol($data['id_pembina']);
		redirect('pede');
	}

	function detail($id_tPedeGuru)
	{
		$data['pede_detail']= $this->model_pede->get_detail($id_tPedeGuru);
		foreach ($data['pede_detail'] as $row) {
			$data['id_transPedeGuru'] = $row['id_transPedeGuru'];
			$data['tipe_pede'] = $row['tipe_pede'];
			$data['nama_pede'] = $row['nama_pede'];
		}
		$data['siswa']= $this->model_pede->get_struktur('siswa');

		$data['siswa_pede']= $this->model_pede->get_siswa($id_tPedeGuru);

		$this->template->display('pede/pede_detail', $data);
	}

	function tambah_siswa ($id_transPedeGuru)
	{
		$data = $this->input->post('pilih');

		$this->model_pede->tambah_siswa($data, $id_transPedeGuru);
		redirect("pede/detail/".$id_transPedeGuru);
	}

	function delete_siswa ($id_transPedeGuru, $id_transPedeSiswa)
	{
		$data['siswa']=$this->model_pede->delete_siswa($id_transPedeSiswa);

		redirect("pede/detail/".$id_transPedeGuru);
	}

	function delete ($pilihan, $key)
	{
		$data['pilihan']=$pilihan;
		$data['key']=$key;

		$this->model_pede->delete_pede($data);

		redirect('pede');
	}

	function edit_enrol()
	{
		$id_transPedeGuru = $this->input->post('id_transPedeGuru');
		$detail=$this->model_pede->get_detail($id_transPedeGuru);
		$guru=$this->model_pede->get_struktur('guru');

		/*Nama Pengembangan Diri*/
		echo '<div class="form-group">';
		echo form_label('Nama Pengembangan Diri', '', array(
			    'class' => 'control-label',
			));

		echo '<select disabled name="" id="" class="form-control">
				<option>'.$detail[0]['nama_pede'].'</option>
				</select>
		</div>';

		/*Guru Wali*/
		echo '<div class="form-group">';
		echo form_label('Guru Pembina', 'id_PembinaEdit', array(
			    'class' => 'control-label',
			));
		echo '<select name="id_PembinaEdit" id="id_PembinaEdit" class="form-control">';
		if ($guru){
			foreach ($guru as $row) {
				if($row['id_guru']==$detail[0]['id_pembina'])
					echo '<option selected value="'.$row['id_guru'].'">'.$row['nama_guru'].'</option>';
				else
					echo '<option value="'.$row['id_guru'].'">'.$row['nama_guru'].'</option>';
			}
		}

		echo '</select>
				<input style="display:none" name="id_PembinaAwal" value="'.$detail[0]['id_pembina'].'">
				<input style="display:none" name="id_transPedeGuru" value="'.$detail[0]['id_transPedeGuru'].'">
				</div>';
	}

	function do_edit_enrol()
	{
		$data = array(
			'id_transPedeGuru' => $this->input->post('id_transPedeGuru'),
			'id_PembinaAwal' => $this->input->post('id_PembinaAwal'),
			'id_PembinaEdit' => $this->input->post('id_PembinaEdit'),
		);

		if ($data['id_PembinaEdit']!=$data['id_PembinaAwal'])
		{
			$data['guru_awalPedeLain'] = $this->model_mapel->get_pedeLain($data['id_guruAwal'], $data['id_transPedeGuru']);
			$this->model_pede->update_enrol($data);
		}
		
		redirect('pede');
	}

	function edit_katalog()
	{
		$id_pede = $this->input->post('id_pede');
		$detail=$this->model_pede->get_detail2($id_pede);

		echo '<div class="form-group">';
		echo form_label('Nama Pengembangan Diri', 'nama_pede', array(
				    'class' => 'control-label',
				));
		echo '<input class="form-control" name="nama_pede" value="'.$detail[0]['nama_pede'].'"></input>';
		echo '</div>';

		echo '<div class="form-group">';
		echo form_label('Tipe', 'tipe_pede', array(
				    'class' => 'control-label',
				));
		echo '<select name="tipe_pede" id="tipe_pede" class="form-control">
				<option selected disabled value="">- Pilih -</option>';
		if($detail[0]['tipe_pede']=="Ekstrakurikuler")
		{
			echo '<option selected value="Ekstrakurikuler">Ekstrakurikuler</option>
					<option value="Organisasi">Organisasi</option>';
		}
		elseif($detail[0]['tipe_pede']=="Organisasi")
		{
			echo '<option value="Ekstrakurikuler">Ekstrakurikuler</option>
					<option selected value="Organisasi">Organisasi</option>';
		}
		echo '<input style="display:none" name="id_pede" value="'.$detail[0]['id_pede'].'"></select> </div>';
	}

	function do_edit_katalog()
	{
		$data = array(
			'id_pede' => $this->input->post('id_pede'),
			'nama_pede' => $this->input->post('nama_pede'),
			'tipe_pede' => $this->input->post('tipe_pede'),
		);

		$this->model_pede->update_katalog($data);
		redirect('pede');
	}
}