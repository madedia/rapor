<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_mapel extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_struktur($pilihan)
    {
        if ($pilihan == 'mapel') {
            if ($this->session->userdata('tipe') == 2) {
                $this->db->where('id_kelasGen', $this->session->userdata('kelas_gen'));
            }
            $this->db->order_by('id_kelasGen, nama_mapel', 'ASC');
            $query = $this->db->get('mapel');
        }
        elseif ($pilihan == 'kelompok') {
            $query = $this->db->get('kelompok_mapel');
        }
        elseif ($pilihan == 'trans_kelas') {
            $query = $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
            $query = $this->db->get('trans_kelas');
        }
        elseif ($pilihan == 'guru') {
            $query = $this->db->get('guru');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_mapelX($id_mapel)
    {
        $this->db->where('id_mapel', $id_mapel);
        $query = $this->db->get('mapel');
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_enrol()
    {
        if ($this->session->userdata('tipe')==2)
        {
            $query = $this->db->where('trans_kelas.id_guruWali', $this->session->userdata('id_guru'));
        }
        $query = $this->db->where('trans_ajar.id_guru = guru.id_guru');
        $query = $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $query = $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas');
        $query = $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        $query = $this->db->order_by('trans_klsgeneral.nama_kelas, mapel.nama_mapel', 'ASC');

        $query = $this->db->get('trans_ajar, guru, trans_klsgeneral, trans_kelas, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambah_mapel($data)
    {
        for ($baris=0; $baris<count($data['id_kelasGen']); $baris++) {
            $this->db->set('id_mapel', $data['id_mapel'][$baris]);
            $this->db->set('nama_mapel', $data['nama_mapel']);
            $this->db->set('id_kelompok', $data['id_kelompok']);
            $this->db->set('id_kelasGen', $data['id_kelasGen'][$baris]);
            $this->db->set('kkm', (($data['kkm100']/100)*4));
            $this->db->set('kkm_aksel', (($data['kkm_aksel100']/100)*4));
            $this->db->set('kkm100', $data['kkm100']);
            $this->db->set('kkm_aksel100', $data['kkm_aksel100']);
            $this->db->insert('mapel');
        }
    }

    function tambah_kelompok($data)
    {
        $this->db->set('id_kelompok', $data['id_kelompok']);
        $this->db->set('nama_kelompok', $data['nama_kelompok']);
        $this->db->set('detail', $data['detail']);
        $this->db->insert('kelompok_mapel');
    }

    function tambah_enrolPaket($data)
    {
        $this->db->insert('trans_ajar', $data);
        //echo $this->db->last_query() . '<br>';
        return $this->db->insert_id();
    }

    function update_tipeEnrol($id_guru)
    {
        $this->db->set('tipe', 3);
        $this->db->where('id_guru', $id_guru);
        $where = "(`tipe` > 3 OR `tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function get_desKompetensi($id_mapel, $id_kelasGen, $pilihan)
    {
        $query = $this->db->where('id_mapelKey', $id_mapel);
        if($pilihan == 'pengetahuan')
            $query = $this->db->get('trans_npengetahuan');
        else if($pilihan == 'keterampilan')        
            $query = $this->db->get('trans_nketerampilan');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_kompetensi($pilihan)
    {
        if($pilihan == 1)
            $query = $this->db->get('deskripsi_npengetahuan');
        else if($pilihan == 2)        
            $query = $this->db->get('deskripsi_nketerampilan');
        else if($pilihan == 3)        
            $query = $this->db->get('deskripsi_nsikap');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function insert_tdeskripsi($id_tAjar, $kompetensi, $pilihan)
    {
        foreach ($kompetensi as $row)
        {
            $this->db->set('id_tAjarKey', $id_tAjar);
            $this->db->set('sikap', $row['sikap']);
            if($pilihan == 1)
            {
                $this->db->set('id_npengetahuan', $row['id_pengetahuan']);
                $this->db->set('des_peng', $row['des_peng']);
                $this->db->insert('trans_npengetahuan');
            }
            else if($pilihan == 2)        
            {
                $this->db->set('id_nketerampilan', $row['id_keterampilan']);
                $this->db->set('des_ket', $row['des_ket']);
                $this->db->insert('trans_nketerampilan');
            }
            else if($pilihan == 3)        
            {
                $this->db->set('id_nsikap', $row['id_sikap']);
                $this->db->set('des_sik', $row['des_sikap']);
                $this->db->insert('trans_nsikap');
            }
            echo $this->db->last_query() . '<br>';
        }
        
    }

    function get_siswa($id_kelas)
    {
        $query = $this->db->where('id_kelas', $id_kelas);
        $query = $this->db->get('siswa');
        
        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_kelas($id_kelasGen)
    {
        $aksel = $id_kelasGen."-A";
        $from = "(SELECT * from trans_kelas, trans_klsgeneral where id_periode = '". $this->session->userdata('periode_aktif') ."' and id_tklsgeneralKey = id_trans_klsgeneral) a";
        $this->db->from($from);
        $this->db->where('id_kelasGen', $id_kelasGen);
        $this->db->or_where('id_kelasGen', $aksel);
        if ($this->session->userdata('tipe') == 2) {
                $query = $this->db->where('id_transKelas', $this->session->userdata('kelas_wali'));
            }
        $query = $this->db->get('');        

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function update_desKompetensi($data, $id_kelasGen )
    {
        for ($baris=0; $baris<count($data['id_transP']); $baris++) {
            $string = "UPDATE `trans_npengetahuan`
                SET `des_peng`= '".$data['des_peng'][$baris]."'
                WHERE `id_transPeng` = ".$data['id_transP'][$baris]."";
                $query = $this->db->query($string);
        }

        for ($baris=0; $baris<count($data['id_transK']); $baris++) {
            $string = "UPDATE `trans_nketerampilan`
                SET `des_ket`= '".$data['des_ket'][$baris]."'
                WHERE `id_transKet` = ".$data['id_transK'][$baris]."";
                $query = $this->db->query($string);
        }
    }

    function get_detail($key, $pilihan)
    {
        if($pilihan == 'enrol')
        {
            $this->db->where('trans_ajar.id_transAjar', $key);
            $this->db->where('trans_ajar.id_guru = guru.id_guru');
            $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
            $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas'); 
            $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral'); 
            $query = $this->db->get('trans_ajar, mapel, guru, trans_kelas, trans_klsgeneral');

        }
        elseif($pilihan == 'katalog')
        {
            $this->db->where('id_mapel', $key);
            $query = $this->db->get('mapel');
        }
        elseif($pilihan == 'kategori')
        {
            $this->db->where('id_kelompok', $key);
            $query = $this->db->get('kelompok_mapel');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_mapelLain($id_guru, $id_transAjar)
    {
        $this->db->where('trans_ajar.id_guru', $id_guru);
        $this->db->where('trans_ajar.id_transAjar !=', $id_transAjar);
        $this->db->where('trans_ajar.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_ajar, trans_kelas');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function get_ajarPede($id_guru)
    {
        $this->db->where('trans_pedeguru.id_pembina', $id_guru);
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_pedeguru');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function update_enrol($data)
    {
        $this->db->set('id_guru', $data['id_guruEdit']);
        $this->db->where('id_transAjar', $data['id_transAjar']);
        $this->db->update('trans_ajar');

        if($data['guru_awalMapelLain'] == null)
        {
            if($data['guru_awalPede'] == null)
            {
                $this->db->set('tipe', 'NULL', false);
                $this->db->where('id_guru', $data['id_guruAwal']);
                $this->db->where('tipe > 2');
                $this->db->update('guru');
                //echo $this->db->last_query() . '<br>';
            }
            else
            {
                $this->db->set('tipe', 4);
                $this->db->where('id_guru', $data['id_guruAwal']);
                $this->db->where('tipe > 2');
                $this->db->update('guru');
                //echo $this->db->last_query() . '<br>';
            }
        }

        $this->db->set('tipe', 3);
        $this->db->where('id_guru', $data['id_guruEdit']);
        $where = "(`tipe` > 3 OR `tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function update_katalog($data)
    {
        $this->db->set('kkm100', $data['kkm100']);
        $this->db->set('kkm_aksel100', $data['kkm_aksel100']);
        $this->db->set('kkm', (($data['kkm100']/100)*4));
        $this->db->set('kkm_aksel', (($data['kkm_aksel100']/100)*4));
        $this->db->where('id_mapel', $data['id_mapel']);
        $this->db->update('mapel');
    }

    function update_kategori($data)
    {
        $this->db->set('nama_kelompok', $data['nama_kelompok']);
        $this->db->set('detail', $data['detail']);
        $this->db->where('id_kelompok', $data['id_kelompok']);
        $this->db->update('kelompok_mapel');
    }

    function get_statusKelas($id_tKelasKey)
    {
        $this->db->where('id_transKelas', $id_tKelasKey);
        $this->db->where('id_tklsgeneralKey = id_trans_klsgeneral');
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');
        $query = $this->db->get('trans_kelas, trans_klsgeneral, kelas_general');

        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['status_aksel'];
            return $val;
        }
        else return null;
    }

    function delete_key($data)
    {
        if($data['pilihan']=='enrol')
        {
            $this->db->where('id_transAjar', $data['key']);
            $this->db->delete('trans_ajar');
        }
        elseif($data['pilihan']=='katalog')
        {
            $this->db->where('id_mapel', $data['key']);
            $this->db->delete('mapel');
        }
        elseif($data['pilihan']=='kategori')
        {
            $this->db->where('id_kelompok', $data['key']);
            $this->db->delete('kelompok_mapel');
        }
    }
}