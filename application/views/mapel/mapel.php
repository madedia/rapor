<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Mata Pelajaran
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Mata Pelajaran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">	
	<!-- Small boxes (Stat box) -->
      <div class="row">
      	<?php if ($this->session->userdata('tipe') <= 1) { ?>
	        <div class="col-lg-3 col-xs-6">
	          <a data-toggle="modal" href="#tambahKelompok">
	              <!-- small box -->
	              <div class="small-box bg-yellow">
	                <div class="inner">
	                  <h3><?php echo count($kelompok) ?></h3>
	                  <p>Tambah Kategori Mata Pelajaran</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-pricetags"></i>
	                </div>
	              </div>
	          </a>
	        </div><!-- ./col -->
	        <div class="col-lg-3 col-xs-6">
	          <a data-toggle="modal" href="#tambahMapel">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3><?php echo count($mapel) ?></h3>
	                  <p>Tambah Mata Pelajaran</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-quote"></i>
	                </div>
	              </div>
	          </a>
	        </div><!-- ./col -->
        <?php } ?>
        <div class="col-lg-3 col-xs-6">
          <a data-toggle="modal" href="#enrolMapel">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo count($enrol) ?></h3>
                  <p>Enrol Mata Pelajaran</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pin"></i>
                </div>
              </div>
          </a>
        </div><!-- ./col -->
      </div><!-- /.row -->
          
  <!-- Custom Tabs (Pulled to the right) -->
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
      <li class="active"><a href="#tab_1-1" data-toggle="tab">Enrol</a></li>
      <li><a href="#tab_2-2" data-toggle="tab">Katalog</a></li>
      <li><a href="#tab_3-2" data-toggle="tab">Kategori</a></li>
      <li class="pull-left header"><i class="fa fa-th"></i> </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1-1">
      	<table id="tabel_enrol" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center">No</th>
				<th style="text-align:center">Kode</th>
				<th style="text-align:center">Mata Pelajaran</th>
				<th style="text-align:center">Kelas</th>
				<th style="text-align:center">NIP</th>
				<th style="text-align:center">Guru Pengampu</th>
				<th style="text-align:center">Opsi</th>
				<th style="display:none"></th>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($enrol) {
					$baris=1;
					foreach ($enrol as $row) {
						if ($row['id_mapel']!=''){
							echo "</tr>";
							echo "<td>".($baris)."</td>";
							echo "<td>".$row['id_mapel']."</td>";
							echo "<td>".$row['nama_mapel']."</td>";
							echo "<td>".$row['nama_kelas']."</td>";
							echo "<td>".$row['id_guru']."</td>";
							echo "<td>".$row['nama_guru']."</td>";
							echo '<td style="text-align:center">&nbsp <a href="#editEnrol" class="edit-enrol btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
							echo '&nbsp <a href="'.base_url('mapel/delete/enrol/'.$row['id_transAjar']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							echo "<td class='nr3' style='display:none'>".$row['id_transAjar']."</td>";
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2-2">
        <table id="tabel_mapel" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center">No</th>
				<th style="text-align:center">Kode Mata Pelajaran</th>
				<th style="text-align:center">Nama</th>
				<th style="text-align:center">Kelompok</th>
				<th style="text-align:center">Kelas</th>
				<th style="text-align:center">KKM (100)</th>
				<th style="text-align:center">KKM Kelas Akselerasi (100)</th>
				<?php if ($this->session->userdata('tipe') <= 1) { ?>
					<th style="text-align:center">Opsi</th>
				<?php } ?>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($mapel) {
					$baris=1;
					foreach ($mapel as $row) {
						if ($row['id_mapel']!=''){
							echo "</tr>";
							echo "<td>".($baris)."</td>";
							echo "<td class='nr2'>".$row['id_mapel']."</td>";
							echo "<td>".$row['nama_mapel']."</td>";
							echo "<td>".$row['id_kelompok']."</td>";
							echo "<td>".$row['id_kelasGen']."</td>";
							echo "<td>".$row['kkm100']."</td>";
							echo "<td>".$row['kkm_aksel100']."</td>";
							if ($this->session->userdata('tipe') <= 1) {
								echo '<td style="text-align:center"> &nbsp <a href="#editKatalog" class="edit-katalog btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
								echo '&nbsp <a href="'.base_url('mapel/delete/katalog/'.$row['id_mapel']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							}
							$baris++;
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3-2">
        <table id="tabel_kategori" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align:center">Kelompok</th>
				<th style="text-align:center">Nama Kelompok</th>
				<th style="text-align:center">Detail</th>
				<?php if ($this->session->userdata('tipe') <= 1) { ?>
					<th style="text-align:center">Opsi</th>
				<?php } ?>
              </tr>
            </thead>
            <tbody>
        	<?php
				if ($kelompok) {
					$baris=1;
					foreach ($kelompok as $row) {
						if ($row['id_kelompok']!=''){
							echo "</tr>";
							echo "<td class='nr3'>".$row['id_kelompok']."</td>";
							echo "<td>".$row['nama_kelompok']."</td>";
							echo "<td>".$row['detail']."</td>";
							if ($this->session->userdata('tipe') <= 1) {
								echo '<td style="text-align:center"> &nbsp <a href="#editKategori" class="edit-kategori btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
								echo '&nbsp <a href="'.base_url('mapel/delete/kategori/'.$row['id_kelompok']).'" class="btn btn-sm btn-danger bootbox_alert" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></a></td>';
							}
						}
					}
				}
			?>
            </tbody>
        </table>
      </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->

</section><!-- /.content -->

<!-- Modal1 Tambah Mapel-->
<div class="modal fade" id="tambahKelompok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Kategori Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
		<?php 
			echo form_open('mapel/tambah_kelompok');?>

			<div class="form-group">
				<?php 
				echo form_label('ID Kelompok', 'id_kelompok', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
			              'name'  => 'id_kelompok',
			              'id'    => 'id_kelompok',
			            ));
				?>
			</div>

			<div class="form-group">
				<?php 
				echo form_label('Nama Kelompok', 'detail', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
			              'name'  => 'nama_kelompok',
			              'id'    => 'nama_kelompok',
			            ));
				?>
			</div>

			<div class="form-group">
				<?php 
				echo form_label('Detail', 'detail', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
			              'name'  => 'detail',
			              'id'    => 'detail',
			            ));
				?>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal1-->

<!-- Modal2 Tambah Mapel-->
<div class="modal fade" id="tambahMapel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
		<?php 
			echo form_open('mapel/tambah_mapel');?>

			<div class="form-group">
				<?php 
				echo form_label('Nama Mata Pelajaran', 'nama_mapel', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
			              'name'  => 'nama_mapel',
			              'id'    => 'nama_mapel',
			            ));
				?>
			</div>

			<div class="form-group">
				<?php 
				echo form_label('Kelompok Mata Pelajaran', 'id_kelompok', array(
				    'class' => 'control-label',
				)); ?>

				<select name="id_kelompok" id="id_kelompok" class="form-control">
					<option value=''>- Pilih Kelompok -</option>
					<?php 
						if ($kelompok){
							foreach ($kelompok as $row) {
								echo "<option value='".$row['id_kelompok']."'";
								if (!$row['detail'])
									echo ">".$row['nama_kelompok']."</option>";
								else
									echo ">".$row['nama_kelompok'].' | '.$row['detail']."</option>";
							}
						}	
					?>
				</select>
			</div>		

			<div class="form-group">
				<?php 
				echo form_label('KKM (skala 100)', 'kkm', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
						  'type' => 'number',
						  'min' => '0',
						  'max' => '100',
			              'name'  => 'kkm',
			              'id'    => 'kkm',
			            ));
				?>
			</div>

			<div class="form-group">
				<?php 
				echo form_label('KKM Kelas Akselerasi (skala 100)', 'kkm_aksel', array(
				    'class' => 'control-label',
				));
				echo form_input(array(
						  'class' => 'form-control',
						  'type' => 'number',
						  'min' => '0',
						  'max' => '100',
			              'name'  => 'kkm_aksel',
			              'id'    => 'kkm_aksel',
			            ));
				?>
			</div>

			<div class="form-group">
				<?php 
					echo form_label('Kelas', 'id_kelas', array(
					    'class' => 'control-label',
					)); ?>
					
				<div>
					<div>
						<input type="checkbox" name="daftarKelas[]" value="X" /> Kelas X </input>
					</div>
					<div>
						<input type="checkbox" name="daftarKelas[]" value="XI" /> Kelas XI </input>
					</div>
					<div>
						<input type="checkbox" name="daftarKelas[]" value="XII" /> Kelas XII </input>
					</div>
				</div>
			</div>	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal2-->

<!-- Modal3 Enrol Mapel-->
<div class="modal fade" id="enrolMapel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
		<?php echo form_open('mapel/enrol_mapel');?>
		<div class="form-group">
			<?php 
			echo form_label('Guru Pengampu', 'id_guru', array(
			    'class' => 'control-label',
			));
			?>

			<select required name="id_guru" id="id_guru" class="form-control">
				<option value=''>- Pilih Guru -</option>
				<?php 
					if ($guru){
						foreach ($guru as $row) {
							echo "<option value='".$row['id_guru']."'";
							echo ">".$row['id_guru'].' | '.$row['nama_guru']."</option>";
						}
					}	
				?>
			</select>
		</div>

		<div class="form-group">
			<?php 
			echo form_label('Nama Mata Pelajaran', 'id_mapel', array(
			    'class' => 'control-label',
			));
			?>

			<select required name="id_mapel" id="id_mapel" class="form-control" onChange=get_kelas()>
				<option value=''>- Pilih Mapel -</option>
				<?php 
					if ($mapel){
						foreach ($mapel as $row) {
							echo "<option value='".$row['id_mapel'].'~'.$row['id_kelasGen']."'";
							echo ">".$row['nama_mapel'].' | Kelas '.$row['id_kelasGen']."</option>";
						}
					}	
				?>
			</select>

		</div>			

		<div class="form-group">
		<?php 
		echo form_label('Kelas', 'nama_kelas', array(
		    'class' => 'control-label',
		));
		?>

		<select required name="nama_kelas" id="nama_kelas" class="form-control">
			<option value='' disabled selected>- Kosong -</option>
		</select>
		</div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
	    <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
	  </div>
      <?php echo form_close(); ?>
    </div>
  </div>
 </div>
</div> <!-- end modal3-->

<!-- Modal4 -->
<div class="modal fade" id="editEnrol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Enrol</h4>
      </div>
      <?php echo form_open('mapel/do_edit_enrol');?>
      <div class="modal-body" id="edit_enrol">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal4 -->

<!-- Modal5 -->
<div class="modal fade" id="editKatalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Katalog</h4>
      </div>
      <?php echo form_open('mapel/do_edit_katalog');?>
      <div class="modal-body" id="edit_katalog">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal5 -->

<!-- Modal6 -->
<div class="modal fade" id="editKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
      </div>
      <?php echo form_open('mapel/do_edit_kategori');?>
      <div class="modal-body" id="edit_kategori">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div> <!-- end modal6 -->

<!-- page script -->
<script type="text/javascript">
	function get_kelas(){
		var id_mapel =$('#id_mapel').val();
		var url = '<?php echo base_url('mapel/get_kelas');?>'
		//window.alert (id_mapel);

		$.ajax
			({
			type: "POST",
			url: url,
			data: {"id_mapel":id_mapel},
			cache: false,
			success: function(html)
			{
				$("#nama_kelas").html(html);
			}
		});
	}

	$(".edit-enrol").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_transAjar = $row.find(".nr3").text(); // Find the text
      var $url = '<?php echo base_url('mapel/edit_enrol');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_transAjar":$id_transAjar},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_enrol").html(html);
	      }
	  });
      $('#editEnrol').modal('show');
    });

    $(".edit-katalog").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_mapel = $row.find(".nr2").text(); // Find the text
      var $url = '<?php echo base_url('mapel/edit_katalog');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_mapel":$id_mapel},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_katalog").html(html);
	      }
	  });
      $('#editKatalog').modal('show');
    });

    $(".edit-kategori").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_kelompok = $row.find(".nr3").text(); // Find the text
      var $url = '<?php echo base_url('mapel/edit_kategori');?>'

      $.ajax
      ({
	      type: "POST",
	      url: $url,
	      data: {"id_kelompok":$id_kelompok},
	      cache: false,
	      success: function(html)
	      {
	        $("#edit_kategori").html(html);
	      }
	  });
      $('#editKategori').modal('show');
    });
</script>

<!-- DataTable page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 6
        } ]
    });
  });

  $(function () {
    $("#tabel_mapel").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 5
        } ]
    }*/);
  });

  $(function () {
    $("#tabel_kategori").DataTable(/*{
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": 3
        } ]
    }*/);
  });
</script>