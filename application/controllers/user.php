<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 2) {
        	redirect ('home');
        }

		//$this->load->model('model_user');
	}

	function index ()
	{
		$data = array();
		$this->template->display('user/user', $data);
	}

	function do_edit ()
	{
		
	}
}