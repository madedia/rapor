<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absensi_catatan extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();
		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }
        elseif ($this->session->userdata('tipe') >= 3) {
        	redirect ('');
        }

		$this->load->model('model_kelas');
		$this->load->model('model_absensiCatatan');
	}

	function index ()
	{
		if ($this->session->userdata('tipe') <= 1)
		{
			$data['kelas']=$this->model_kelas->get_struktur('kelas');
			$this->template->display('wali/absensi_catatan', $data);
		}
        else
        {
        	$id_transKelas = $this->model_absensiCatatan->get_kelasWali($this->session->userdata('id_guru'));
        	$this->kelas($id_transKelas);
        }
	}

	public function ekspor_tabel($id_transKelas)
	{
		$result = $this->model_kelas->get_siswa($id_transKelas);
		$result2 = $this->model_absensiCatatan->get_siswaCatatan($id_transKelas);
		$detail = $this->model_absensiCatatan->get_Detail($id_transKelas);

		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		$sheet = $this->excel->getActiveSheet();

		$styleArray = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('argb' => '000000'),
		),
		),
		);

		$sheet->setCellValue('A1', 'Daftar Absensi dan Catatan Wali Kelas '.$detail[0]['nama_kelas']);
		$sheet->setCellValue('A2', 'Periode '.$this->session->userdata('periode_tes').' Tahun Ajaran '.$this->session->userdata('periode_aktif'));
    	
    	$sheet->setCellValue('A4', 'Guru Wali : '.$detail[0]['nama_guru']);

		$sheet
			->setCellValue('A6', 'NIS')
			->setCellValue('B6', 'Nama Lengkap')
			->setCellValue('C6', 'Ketidakhadiran')
			->setCellValue('C7', 'Sakit')
			->setCellValue('D7', 'Izin')
			->setCellValue('E7', 'T. Ket.')
			->setCellValue('F6', 'Catatan Wali Kelas');
		$sheet->mergeCells('A6:A7');
		$sheet->mergeCells('B6:B7');
		$sheet->mergeCells('F6:F7');
		$sheet->mergeCells('C6:E6');

		if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false)
		{
			$sheet
				->setCellValue('G6', 'Kenaikan')
				->setCellValue('G7', 'Status')
				->setCellValue('H7', 'Ke Kelas');
			$sheet->mergeCells('G6:H6');
			$sheet->mergeCells('A1:H1');
			$sheet->mergeCells('A2:H2');
			$sheet->getStyle('A6:H7')->applyFromArray($styleArray);
		}
		else
		{
			$sheet->mergeCells('A1:F1');
			$sheet->mergeCells('A2:F2');
			$sheet->getStyle('A6:F7')->applyFromArray($styleArray);
		}
		$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$sheet->getColumnDimension('A')->setWidth(5);
		$sheet->getColumnDimension('B')->setWidth(40);
		$sheet->getColumnDimension('C')->setWidth(10);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->getColumnDimension('E')->setWidth(10);
		$sheet->getColumnDimension('F')->setWidth(50);
		$sheet->getColumnDimension('G')->setWidth(30);
		$sheet->getColumnDimension('H')->setWidth(20);
		$sheet->getStyle('A6:H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$baris=7;
		if($result)
		{
			foreach ($result as $res)
			{
				$temp = 0;
				if($result2)
				{
					foreach ($result2 as $res2) {
						if ($res['id_transSisKls']==$res2['id_tSisKlsKey'])
						{
							$this->excel->getActiveSheet()
								->setCellValue('A'.($baris+1), $res2['id_siswa'])
								->setCellValue('B'.($baris+1), $res2['nama_siswa'])
								->setCellValue('C'.($baris+1), $res2['jml_sakit'])
								->setCellValue('D'.($baris+1), $res2['jml_izin'])
								->setCellValue('E'.($baris+1), $res2['jml_tketerangan'])
								->setCellValue('F'.($baris+1), $res2['isi_catatan']);
							if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false)
							{
								if($res2['status_kenaikan']==1)
									$this->excel->getActiveSheet()->setCellValue('G'.($baris+1), 'Naik Kelas');
								elseif($res2['status_kenaikan']==2)
									$this->excel->getActiveSheet()->setCellValue('G'.($baris+1), 'Tidak Naik Kelas');
								
								$this->excel->getActiveSheet()->setCellValue('H'.($baris+1), $this->model_absensiCatatan->get_namaKlsLanjut($res2['kelas_lanjut']));
							}
							$temp = 1;				
							break;
						}
					}
					if($temp == 0) {
						$this->excel->getActiveSheet()
							->setCellValue('A'.($baris+1), $res['id_siswa'])
							->setCellValue('B'.($baris+1), $res['nama_siswa']);
					}
					$sheet->getStyle('A'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('B'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('C'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('D'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('E'.($baris+1))->applyFromArray($styleArray);
					$sheet->getStyle('F'.($baris+1))->applyFromArray($styleArray);
					if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false)
					{
						$sheet->getStyle('G'.($baris+1))->applyFromArray($styleArray);
						$sheet->getStyle('H'.($baris+1))->applyFromArray($styleArray);
					}
					$baris++;
				}
			}
		}
		
		$sheet->getDefaultRowDimension()->setRowHeight(20);

		$filename= 'Daftar Absensi dan Catatan Wali Kelas '.$detail[0]['nama_kelas'].'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	function kelas ($id_transKelas)
	{
		$data['id_transKelas'] = $id_transKelas;
		/*$data['siswa_kelas'] = null;
		$result = $this->model_absensiCatatan->get_siswaCatatan($id_transKelas);
		if($result)
		{
			$data['siswa_kelas']=$result;
			$data['isUpdate']=1;
			foreach ($data['siswa_kelas'] as $row) {
				$data['catatan'][$row['id_transSisKls']] = $this->model_absensiCatatan->get_catatanGuru($row['id_transSisKls']);
			}
		}
		else
		{
			$data['isUpdate']=0;
			$data['siswa_kelas2'] = $this->model_kelas->get_siswa($id_transKelas);
		}

		if(strpos($this->session->userdata('periode_aktifDet'),"uas2") !== false) 
		{
			$data['kenaikan']=1;
		}*/
		$data['siswa_kelas'] = array();
		$siswa_enrol= $this->model_kelas->get_siswa($id_transKelas);
		$siswa_catatan= $this->model_absensiCatatan->get_siswaCatatan($id_transKelas);

		if($siswa_enrol)
		{
			foreach ($siswa_enrol as $row1) {
				$temp=0;
				if ($siswa_catatan)
				{
					foreach ($siswa_catatan as $row2) {
						if($row1['id_transSisKls'] == $row2['id_tSisKlsKey'])
						{
							$row2['isUpdate'] = 1;
							array_push($data['siswa_kelas'], $row2);
							$temp = 1;				
							break;
						}
					}
				}
				if($temp == 0) {
					$row1['isUpdate'] = 0;
					array_push($data['siswa_kelas'], $row1);
				}
			}
		}

		$data_kelas = $this->model_absensiCatatan->get_angkaKelas($id_transKelas);
		$data['next_kelas'] = $this->model_absensiCatatan->get_nextKelas($data_kelas);

		$this->template->display('wali/absensi_catatanKelas', $data);
	}

	function catatanGuru ($id_transSisKls)
	{
		$data = $this->model_absensiCatatan->get_catatanGuru($id_transSisKls);

		echo "<tr>";
		echo "<th>No</th>";
		echo "<th>Catatan</th>";
		echo "<th>Mata Pelajaran</th>";
		echo "<th>Guru</th>";
		echo "</tr>";

		if ($data){
			$baris = 1;
			foreach ($data as $row) {
				if($row['komentar'])
				{
					echo "<tr>";
					echo "<td>".$baris."</td>";
					echo "<td>".$row['komentar']."</td>";
					echo "<td>".$row['nama_mapel']."</td>";
					echo "<td>".$row['nama_guru']."</td>";
					echo "</tr>";
					$baris++;
				}
			}
		}
	}

	function insert ()
	{
		$data = array(
			'id_tSisKlsKey' => $this->input->post('id_transSisKls'),
			'jml_sakit' => $this->input->post('jml_sakit'),
			'jml_izin' => $this->input->post('jml_izin'),
			'jml_tketerangan' => $this->input->post('jml_tketerangan'),
			'isi_catatan' => $this->input->post('isi_catatan'),
			'status_kenaikan' => $this->input->post('status_kenaikan'),
			'kelas_lanjut' => $this->input->post('kelas_lanjut'),
			'id_catatan' => $this->input->post('id_catatan'),
			'isUpdate' => $this->input->post('isUpdate'),
		);

		$this->model_absensiCatatan->insert($data);

		redirect('absensi_catatan');
	}
}
