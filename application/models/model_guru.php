<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_guru extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_guru()
    {
        $query = $this->db->get('guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambah_guru($data)
    {
        $this->db->insert('guru', $data);
    }

    function edit_detail($data)
    {
        $this->db->set('nama_guru', $data['nama_guru']);
        $this->db->where('id_guru', $data['id_guru']);
        $this->db->update('guru');
        echo $this->db->last_query() . '<br>';
    }

    function detail_guru($id_guru)
    {
        $this->db->where('id_guru', $id_guru);
        $query = $this->db->get('guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function delete($id_guru)
    {
        $this->db->where('id_guru', $id_guru);
        $this->db->delete('guru');
    }
}