<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nilai Pengembangan Diri
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"> Nilai Pengembangan Diri</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">          
  	<!-- Default box -->
    <div class="box box-danger">
        <div class="box-header with-border">
        	<h3 class="box-title">Daftar Pengembangan Diri Enrol</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <table id="tabel_enrol" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="text-align:center" class="col-sm-1">No</th>
                    <th style="text-align:center">Nama </th>
                    <th style="text-align:center" class="col-sm-2">Isi Nilai</th>
                    <th style="text-align:center" class="col-sm-4">Isi Nilai Via Excel</th>
                  </tr>
                </thead>
                <tbody>
            	<?php
					if ($pede_enrol) {
						$baris=1;
						foreach ($pede_enrol as $row) {
							if ($row['id_pede']!=''){
								echo "</tr>";
                                echo "<td style='text-align:center'>".$baris."</td>";
                                echo "<td>".$row['nama_pede']."</td>";                                
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('nilai_pede/isi/'.$row['id_transPedeGuru'].'/'.$row['id_pembina']).'" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i> Daftar Nilai</a></td>';
                                echo '<td style="text-align:center">&nbsp <a href="'.base_url('nilai_pede/ekspor_tabel/'.$row['id_transPedeGuru'].'/'.$row['id_pembina']).'" class="btn btn-sm btn-default"><i class="fa fa-download"></i> Unduh Format</a>';
                                echo '&nbsp <a href="'.base_url('nilai_pede/impor_tabel/'.$row['id_transPedeGuru'].'/'.$row['id_pembina']).'" class="btn btn-sm btn-default"><i class="fa fa-upload"></i> Unggah Nilai</a></td>';
                                $baris++;
							}
						}
					}
				?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- page script -->
<script>
  $(function () {
    $("#tabel_enrol").DataTable({
    	"columnDefs": [ {
        "bSearchable" : false,
        "orderable": false,
        "targets": [2, 3],
        } ]
    });
  });
</script>