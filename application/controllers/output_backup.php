<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Output extends CI_Controller
{ 
  function __construct()
  {
      parent:: __construct();
      
      $this->load->model('model_output');
  }

  function index ()
  {
        
  }

  function rapor ($id_siswa, $id_periode=null, $id_periodeDet=null, $pilihan=null)
  {
      $data['id_siswa'] = $id_siswa;
      $data['thn_ajaran'] = $this->model_output->get_thnAjaran($data['id_siswa']);

      if ($id_periode == null) {
          $data['id_periode'] = $this->session->userdata('periode_aktif');
          $data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');
      }
      else
      {
          $data['id_periode'] = $id_periode;
          $data['id_periodeDet'] = $id_periodeDet;
      }
      
      $data['nilai'] = $this->model_output->get_nilai($data);
      $data['total'] = $this->model_output->get_totalNilaiR($data);
      $data['pede'] = $this->model_output->get_pede($data);
      $data['catatan'] = $this->model_output->get_catatan($data);

      $data['identitas'] = $this->model_output->get_identitas($data);

      if ($data['identitas'])
      {
          foreach ($data['identitas'] as $row) {
              $data['id_siswa'] = $row['id_siswa'];
              $data['nama_siswa'] = $row['nama_siswa'];
              $data['nama_kelas'] = $row['nama_kelas'];
              $data['id_wali'] = $row['id_wali'];
              $data['nama_wali'] = $row['nama_wali'];
              $data['id_kepsek'] = $row['id_kepsek'];
              $data['nama_kepsek'] = $row['nama_kepsek'];
              $data['id_periode'] = $row['id_periode'];

              if((strpos($data['id_periodeDet'],"uts1") || strpos($data['id_periodeDet'],"uas1")) !== false)
                $data['semester'] = $row['smt_ganjil'];
              else if((strpos($data['id_periodeDet'],"uts2") || strpos($data['id_periodeDet'],"uas2")) !== false)
                $data['semester'] = $row['smt_genap'];

              $tanggal_rapor = strtotime ($row['tanggal_rapor']);
              $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
              $data['tanggal_rapor'] = date("j", $tanggal_rapor). " " . $bulan[date("n", $tanggal_rapor)] . " " . date("Y", $tanggal_rapor);
              //$data['tanggal_rapor'] = date("j  Y", $tanggal_rapor);
          }
      }
      else
      {
          $data['id_siswa'] = null;
          $data['nama_siswa'] = null;
          $data['nama_kelas'] = null;
          $data['id_wali'] = null;
          $data['nama_wali'] = null;
          $data['id_kepsek'] = null;
          $data['nama_kepsek'] = null;
          $data['id_periode'] = null;
          $data['tanggal_rapor'] = null;
      }

      if($data['catatan'])
      {
        foreach ($data['catatan'] as $row) {
          $data['jml_sakit'] = $row['jml_sakit'];
          $data['jml_izin'] = $row['jml_izin'];
          $data['jml_tketerangan'] = $row['jml_tketerangan'];
          $data['isi_catatan'] = $row['isi_catatan'];
          $data['status_kenaikan'] = $row['status_kenaikan'];
          $data['kelas_lanjut'] = $row['kelas_lanjut'];
          $rapat_dewanGuru = strtotime ($row['rapat_dewanGuru']);
          $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
          $data['rapat_dewanGuru'] = date("j", $rapat_dewanGuru). " " . $bulan[date("n", $rapat_dewanGuru)] . " " . date("Y", $rapat_dewanGuru);
          $data['kelas_lanjut'] = $row['nama_kelas'];
        }
      }
      else
      {
        $data['jml_sakit'] = null;
        $data['jml_izin'] = null;
        $data['jml_tketerangan'] = null;
        $data['isi_catatan'] = null;
      }

      if($pilihan)
      {
          $this->rapor_pdf($data);
      }

      if(strpos($data['id_periodeDet'],"uts") !== false) {
          $this->template->display('output/raporUTS', $data);
      } else if(strpos($data['id_periodeDet'],"uas1") !== false) {
          $this->template->display('output/raporUAS1', $data);
      } else if(strpos($data['id_periodeDet'],"uas2") !== false) {
          $this->template->display('output/raporUAS2', $data);
      }
  }

    function leger ($id_transKelas, $id_periodeDet=null, $pilihan=null)
    {

        $data['id_transKelas'] = $id_transKelas;
        $data['thn_ajaran'] = $this->model_output->get_thnAjaranL($id_transKelas);
        if ($id_periodeDet == null) {
          $data['id_periodeDet'] = $this->session->userdata('periode_aktifDet');
        }
        else
        {
            $data['id_periodeDet'] = $id_periodeDet;
        }

        $data['identitas'] = $this->model_output->get_identitasL($data);
        if ($data['identitas'])
        {
            foreach ($data['identitas'] as $row) {
                $data['nama_kelas'] = $row['nama_kelas'];
                $data['id_wali'] = $row['id_wali'];
                $data['nama_wali'] = $row['nama_wali'];
                $data['id_kepsek'] = $row['id_kepsek'];
                $data['nama_kepsek'] = $row['nama_kepsek'];
                $data['id_periode'] = $row['id_periode'];
                $data['nama_tes'] = $row['nama_tes'];
                if((strpos($data['id_periodeDet'],"uts1") || strpos($data['id_periodeDet'],"uas1")) !== false)
                  $data['semester'] = $row['smt_ganjil'];
                else if((strpos($data['id_periodeDet'],"uts2") || strpos($data['id_periodeDet'],"uas2")) !== false)
                  $data['semester'] = $row['smt_genap'];


                $tanggal_rapor = strtotime ($row['tanggal_rapor']);
                $bulan = array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "Sepetmber", "Oktober", "Nopember", "Desember");
                $data['tanggal_rapor'] = date("j", $tanggal_rapor). " " . $bulan[date("n", $tanggal_rapor)] . " " . date("Y", $tanggal_rapor);
            }
        }
        else
        {
            $data['nama_kelas'] = null;
            $data['id_wali'] = null;
            $data['nama_wali'] = null;
            $data['id_kepsek'] = null;
            $data['nama_kepsek'] = null;
            $data['id_periode'] = null;
            $data['nama_tes'] = null;
            $data['tanggal_rapor'] = null;
            $data['semester'] = null;
        }


        $data['mapel'] = $this->model_output->get_mapelL($data);
        $data['siswa'] = $this->model_output->get_siswaL($data);
        $data['total'] = $this->model_output->get_totalNilai($data);

        if($data['mapel'])
            foreach ($data['siswa'] as $row1) {
                foreach ($data['mapel'] as $row2) {
                     $result = $this->model_output->get_nilaiL($row1['id_transSisKls'], $row2['id_transAjar'], $data['id_periodeDet']);
                     $data['nilai'][$row1['id_siswa']][$row2['id_mapel']] = $result;
                }
            }

        if($pilihan)
        {
            $this->leger_excel($data);
        }
        $this->template->display('output/leger', $data);
    }

    function rapor_pdf($data)
    {
        $this->load->library('pdf');

        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('CHIS');
        $this->pdf->SetTitle('Rapor');
        $this->pdf->SetSubject('Rapor');
        //$this->pdf->SetKeywords('TCPDF, PDF, example, test, guide');
         
        // remove default header/footer
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(false);
         
        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         
        //set margins
        $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         
        //set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
         
        //set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         
        //set some language-dependent strings
        //$this->pdf->setLanguageArray(null);
         
        // ---------------------------------------------------------
         
        // set font
        $this->pdf->SetFont('times', '', 12);
         
        // add a page
        $this->pdf->AddPage();

        $header = null;
        $body = null;
        $footer = null;

        $header .= '<b><table width="100%"  border="0" cellpadding="2" cellspacing="1">
                  <tr>
                    <td width="25%" >&nbsp;</td>
                    <td width="28%" >&nbsp;</td>
                    <td width="23%" >&nbsp;</td>
                    <td width="25%" >&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td>Nama Peserta Didik</td>
                    <td>: '.$data['nama_siswa'].' </td>
                    <td>Kelas/Semester</td>
                    <td>: '.$data['nama_kelas'].' / '.$data['semester'].' </td>
                  </tr>
                  <tr>
                    <td>Nomor Induk</td>
                    <td>: '.$data['id_siswa'].' </td>
                    <td>Tahun Pelajaran</td>
                    <td>: '.$data['id_periode'].' </td>
                  </tr>
                  <tr>
                    <td>Nama Sekolah</td>
                    <td>: SMA CHIS Denpasar</td>
                    <td></td>
                    <td></td>
                  </tr>
                </table></b>';

        $footer .= '<b><table width="100%"  border="0" cellpadding="2" cellspacing="5">
                  <tr>
                    <td width="32%" >&nbsp;</td>
                    <td width="36%" >&nbsp;</td>
                    <td width="32%" >&nbsp;</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td> Denpasar, '.$data['tanggal_rapor'].'</td>
                  </tr>
                  <tr>
                    <td> Orang Tua/Wali <br> Peserta Didik
                    </td>
                    <td align="center"> Wali Kelas </td>
                    <td> Kepala Sekolah </td>
                  </tr>
                  <tr>
                    <td colspan="3" height="40px"></td>
                  </tr>
                  <tr>
                    <td> __________________</td>
                    <td> <u>'.$data['nama_wali'].'</u></td>
                    <td> <u>'.$data['nama_kepsek'].'</u></td>
                  </tr>
                  <tr>
                    <td> </td>
                    <td> NIP. '.$data['id_wali'].'</td>
                    <td> NIP. '.$data['id_kepsek'].'</td>
                  </tr>
                </table></b>';

        $sikap_antarMapel = null;
        $jml_nilai_pengetahuan = null;
        $jml_nilai_keterampilan = null;
        $rata2_nilai_pengetahuan = null;
        $rata2_nilai_keterampilan = null;
        $ranking = null;
        $sikap_antarMapelMuatan = null;
        $tabelA = null;
        $tabelB = null;
        $tabelC = null;
        $tabelD = null;
        $tabelE = null;

        if($data['total']) {
          foreach ($data['total'] as $row) {
              if ($row['status_muatan'] == 0) {
                  $jml_nilai_pengetahuan = $row['jml_nilai_pengetahuan'];
                  $jml_nilai_keterampilan = $row['jml_nilai_keterampilan'];
                  $rata2_nilai_pengetahuan = $row['rata2_nilai_pengetahuan'];
                  $rata2_nilai_keterampilan = $row['rata2_nilai_keterampilan'];
                  $ranking = $row['ranking'];
                  $sikap_antarMapel = $row['sikap_antarMapel'];
              }
              else {
                  $sikap_antarMapelMuatan = $row['sikap_antarMapel'];
              }
          }
        }
        $kolom_sikap = count($data['nilai'])+11;
        if (strpos($data['nama_kelas'],"IPA") !== false)
            $tabelC.= "<tr><td colspan='8'>Kelompok Peminatan Matematika dan Ilmu Alam</td></tr>";
        if (strpos($data['nama_kelas'],"IPS") !== false)
            $tabelC.= '<tr><td colspan="8">Kelompok Peminatan Ilmu Sosial</td></tr>';
        if (strpos($data['nama_kelas'],"Bahasa") !== false)
            $tabelC.= "<tr><td colspan='8'>Kelompok Peminatan Ilmu Bahasa</td></tr>";

        if ($data['nilai']) {
            $barisA = $barisB = $barisC = $barisD = $barisE = 1;
            foreach ($data['nilai'] as $row) {
                if ($row['id_kelompok']=='A'){
                  $tabelA .= "<tr>";
                  $tabelA .= "<td>".($barisA)."</td>";
                  $tabelA .= "<td>".$row['nama_mapel']."</td>";
                  
                  if (strpos($data['nama_kelas'],'Akselerasi') !== false)
                    $tabelA .= "<td>".$row['kkm_aksel']."</td>";
                  else
                    $tabelA .= "<td>".$row['kkm']."</td>";
                  
                  $tabelA .= "<td>".$row['nilai_pengetahuan']."</td>";
                  $tabelA .= "<td>".$row['predikat_pengetahuan']."</td>";
                  $tabelA .= "<td>".$row['nilai_keterampilan']."</td>";
                  $tabelA .= "<td>".$row['predikat_keterampilan']."</td>";
                  $tabelA .= "<td>".$row['predikat_sikap']."</td>";
                  $tabelA .= "</tr>";
                  $barisA++;
                }
                if ($row['id_kelompok']=='B'){
                  $tabelB .= "<tr>";
                  $tabelB .= "<td>".($barisB)."</td>";
                  $tabelB .= "<td>".$row['nama_mapel']."</td>";
                  
                  if (strpos($data['nama_kelas'],'Akselerasi') !== false)
                    $tabelB .= "<td>".$row['kkm_aksel']."</td>";
                  else
                    $tabelB .= "<td>".$row['kkm']."</td>";
                  
                  $tabelB .= "<td>".$row['nilai_pengetahuan']."</td>";
                  $tabelB .= "<td>".$row['predikat_pengetahuan']."</td>";
                  $tabelB .= "<td>".$row['nilai_keterampilan']."</td>";
                  $tabelB .= "<td>".$row['predikat_keterampilan']."</td>";
                  $tabelB .= "<td>".$row['predikat_sikap']."</td>";
                  $tabelB .= "</tr>";
                  $barisB++;
                }
                if (strpos($row['id_kelompok'],"C") !== false){
                  $tabelC .= "<tr>";
                  $tabelC .= "<td>".($barisC)."</td>";
                  $tabelC .= "<td>".$row['nama_mapel']."</td>";
                  
                  if (strpos($data['nama_kelas'],'Akselerasi') !== false)
                    $tabelC .= "<td>".$row['kkm_aksel']."</td>";
                  else
                    $tabelC .= "<td>".$row['kkm']."</td>";
                  
                  $tabelC .= "<td>".$row['nilai_pengetahuan']."</td>";
                  $tabelC .= "<td>".$row['predikat_pengetahuan']."</td>";
                  $tabelC .= "<td>".$row['nilai_keterampilan']."</td>";
                  $tabelC .= "<td>".$row['predikat_keterampilan']."</td>";
                  $tabelC .= "<td>".$row['predikat_sikap']."</td>";
                  $tabelC .= "</tr>";
                  $barisC++;
                }
                if (strpos($row['id_kelompok'],"D") !== false){
                  $tabelD .= "<tr>";
                  $tabelD .= "<td>".($barisD)."</td>";
                  $tabelD .= "<td>".$row['nama_mapel']."</td>";
                  
                  if (strpos($data['nama_kelas'],'Akselerasi') !== false)
                    $tabelD .= "<td>".$row['kkm_aksel']."</td>";
                  else
                    $tabelD .= "<td>".$row['kkm']."</td>";
                  
                  $tabelD .= "<td>".$row['nilai_pengetahuan']."</td>";
                  $tabelD .= "<td>".$row['predikat_pengetahuan']."</td>";
                  $tabelD .= "<td>".$row['nilai_keterampilan']."</td>";
                  $tabelD .= "<td>".$row['predikat_keterampilan']."</td>";
                  $tabelD .= "<td>".$row['predikat_sikap']."</td>";
                  $tabelD .= "</tr>";
                  $barisD++;
                }
                if (strpos($row['id_kelompok'],"E") !== false){
                  $tabelE .= "<tr>";
                  $tabelE .= "<td>".($barisE)."</td>";
                  $tabelE .= "<td>".$row['nama_mapel']."</td>";
                  
                  if (strpos($data['nama_kelas'],'Akselerasi') !== false)
                    $tabelE .= "<td>".$row['kkm_aksel']."</td>";
                  else
                    $tabelE .= "<td>".$row['kkm']."</td>";
                  
                  $tabelE .= "<td>".$row['nilai_pengetahuan']."</td>";
                  $tabelE .= "<td>".$row['predikat_pengetahuan']."</td>";
                  $tabelE .= "<td>".$row['nilai_keterampilan']."</td>";
                  $tabelE .= "<td>".$row['predikat_keterampilan']."</td>";
                  $tabelE .= "<td>".$row['predikat_sikap']."</td>";
                  $tabelE .= "</tr>";
                  $barisE++;
                }
              }
        }

        // Halaman nilai ranking
        $body .= '<br><b>&nbsp;&nbsp;Capaian Kompetensi Peserta Didik</b><br>
                    <table border="0.1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <td align="center" width="6%" rowspan="3"><b><br><br>No.</b></td>
                        <td align="center" rowspan="3" width="20%"><b><br><br>Mata Pelajaran</b></td>
                        <td align="center" colspan="7"><b>Nilai Hasil Belajar</b></td>
               
                    </tr>
                    <tr>
                        <td align="center" colspan="3"><b>Pengetahuan</b></td>
                        <td align="center" colspan="2"><b>Keterampilan</b></td>
                        <td align="center" colspan="2"><b>Sikap</b></td>
                    </tr>
                    <tr>
                        <td align="center"><b>KKM</b></td>
                        <td align="center"><b>Nilai</b></td>
                        <td align="center"><b>Predikat</b></td>
                        <td align="center"><b>Nilai</b></td>
                        <td align="center"><b>Predikat</b></td>
                        <td align="center"><b>Dalam Mapel</b></td>
                        <td align="center"><b>Antar Mapel</b></td>
                    </tr>
                    <tr>
                        <td colspan="8"><b>KELOMPOK A</b></td>
                        <td align="center" rowspan="'.$kolom_sikap.'">'.$sikap_antarMapel.'
                        </td>
                    </tr>
                    '.$tabelA.'
                    <tr>
                        <td colspan="8"><b>KELOMPOK B</b></td>
                    </tr>
                    '.$tabelB.'
                    <tr>
                        <td colspan="8"><b>KELOMPOK C Peminatan</b></td>
                    </tr>
                    '.$tabelC.'                    
                    <tr>
                        <td colspan="8"><b>KELOMPOK LINTAS PEMINATAN</b></td>
                    </tr>
                    '.$tabelD.'                    
                    <tr>
                        <td colspan="2"><b>JUMLAH</b></td>
                        <td></td>
                        <td>'.$jml_nilai_pengetahuan.'</td>
                        <td></td>
                        <td>'.$jml_nilai_keterampilan.'</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>RATA-RATA</b></td>
                        <td></td>
                        <td>'.$rata2_nilai_pengetahuan.'</td>
                        <td></td>
                        <td>'.$rata2_nilai_keterampilan.'</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>RANKING</b></td>
                        <td></td>
                        <td align="center" colspan="5">'.$ranking.'</td>
                    </tr>
                </table>
                <br>&nbsp;&nbsp;*) Muatan Lokal Provinsi Bali<br><br>';

        // output the HTML content
        $this->pdf->writeHTML($header, true, false, true, false, '');
        $this->pdf->writeHTML($body, true, false, true, false, '');
        $this->pdf->writeHTML($footer, true, false, true, false, '');
        $this->pdf->AddPage();

        // Halaman nilai Muatan Lokal
        $body = '&nbsp;&nbsp;
            <table border="0.1" cellspacing="0" cellpadding="5" width="100%">
                    <tr>
                        <td align="center" width="6%" rowspan="3"><b><br><br>No.</b></td>
                        <td align="center" rowspan="3" width="20%"><b><br><br>Mata Pelajaran</b></td>
                        <td align="center" colspan="7"><b>Nilai Hasil Belajar</b></td>
               
                    </tr>
                    <tr>
                        <td align="center" colspan="3"><b>Pengetahuan</b></td>
                        <td align="center" colspan="2"><b>Keterampilan</b></td>
                        <td align="center" colspan="2"><b>Sikap</b></td>
                    </tr>
                    <tr>
                        <td align="center"><b>KKM</b></td>
                        <td align="center"><b>Nilai</b></td>
                        <td align="center"><b>Predikat</b></td>
                        <td align="center"><b>Nilai</b></td>
                        <td align="center"><b>Predikat</b></td>
                        <td align="center"><b>Dalam Mapel</b></td>
                        <td align="center"><b>Antar Mapel</b></td>
                    </tr>
                     <tr>
                        <td colspan="8"><b>MUATAN LOKAL</b></td>
                        <td align="center" rowspan="'.$kolom_sikap.'">'.$sikap_antarMapelMuatan.'</td>
                    </tr>
                    '.$tabelE.'
                </table><br><br>';

        // output the HTML content
        $this->pdf->writeHTML($header, true, false, true, false, '');
        $this->pdf->writeHTML($body, true, false, true, false, '');
        $this->pdf->writeHTML($footer, true, false, true, false, '');
        $this->pdf->AddPage();

        if(strpos($data['id_periodeDet'],"uas") !== false) {
          $halA = $halB = $halC= $halD = $halE = null;
          if ($data['nilai']) {
          $barisA= $barisB= $barisC= $barisD= $barisE= 1;
            foreach ($data['nilai'] as $row) {
              if ($row['id_kelompok']=='A'){
                $halA .= "<tr>";
                $halA .= "<td rowspan=3>".($barisA)."</td>";
                $halA .= "<td rowspan='3'>".$row['nama_mapel']."</td>";
                $halA .= "<td>Pengetahuan</td>";
                $halA .= "<td>".$row['des_peng']."</td>";
                $halA .= "</tr>";

                $halA .= "<tr>";
                $halA .= "<td></td>";
                $halA .= "<td></td>";
                $halA .= "<td>Keterampilan</td>";
                $halA .= "<td>".$row['des_ket']."</td>";
                $halA .= "</tr>";

                $halA .= "<tr>";
                $halA .= "<td>Sikap sosial dan spiritual</td>";
                $halA .= "<td>".$row['des_sik']."</td>";
                $halA .= "</tr>";
                $barisA++;
              }
              if ($row['id_kelompok']=='B'){
                $halB .=  "<tr>";
                $halB .= "<td rowspan='3'>".($barisB)."</td>";
                $halB .= "<td rowspan='3'>".$row['nama_mapel']."</td>";
                $halB .= "<td>Pengetahuan</td>";
                $halB .= "<td>".$row['des_peng']."</td>";
                $halB .= "</tr>";

                $halB .= "<tr>";
                $halB .= "<td>Keterampilan</td>";
                $halB .= "<td>".$row['des_ket']."</td>";
                $halB .= "</tr>";

                $halB .= "<tr>";
                $halB .= "<td>Sikap sosial dan spiritual</td>";
                $halB .= "<td>".$row['des_sik']."</td>";
                $halB .= "</tr>";
                $barisB++;
              }
              if ($row['id_kelompok']=='C'){
                $halC .=  "<tr>";
                $halC .= "<td rowspan='3'>".($barisC)."</td>";
                $halC .= "<td rowspan='3'>".$row['nama_mapel']."</td>";
                $halC .= "<td>Pengetahuan</td>";
                $halC .= "<td>".$row['des_peng']."</td>";
                $halC .= "</tr>";

                $halC .= "<tr>";
                $halC .= "<td>Keterampilan</td>";
                $halC .= "<td>".$row['des_ket']."</td>";
                $halC .= "</tr>";

                $halC .= "<tr>";
                $halC .= "<td>Sikap sosial dan spiritual</td>";
                $halC .= "<td>".$row['des_sik']."</td>";
                $halc .= "</tr>";
                $barisC++;
              }
              if ($row['id_kelompok']=='D'){
                $halD .=  "<tr>";
                $halD .= "<td rowspan='3'>".($barisD)."</td>";
                $halD .= "<td rowspan='3'>".$row['nama_mapel']."</td>";
                $halD .= "<td>Pengetahuan</td>";
                $halD .= "<td>".$row['des_peng']."</td>";
                $halD .= "</tr>";

                $halD .= "<tr>";
                $halD .= "<td>Keterampilan</td>";
                $halD .= "<td>".$row['des_ket']."</td>";
                $halD .= "</tr>";

                $halD .= "<tr>";
                $halD .= "<td>Sikap sosial dan spiritual</td>";
                $halD .= "<td>".$row['des_sik']."</td>";
                $halD .= "</tr>";
                $barisD++;
              }
              if ($row['id_kelompok']=='E'){
                $halE .=  "<tr>";
                $halE .= "<td rowspan='3'>".($barisE)."</td>";
                $halE .= "<td rowspan='3'>".$row['nama_mapel']."</td>";
                $halE .= "<td>Pengetahuan</td>";
                $halE .= "<td>".$row['des_peng']."</td>";
                $halE .= "</tr>";

                $halE .= "<tr>";
                $halE .= "<td>Keterampilan</td>";
                $halE .= "<td>".$row['des_ket']."</td>";
                $halE .= "</tr>";

                $halE .= "<tr>";
                $halE .= "<td>Sikap sosial dan spiritual</td>";
                $halE .= "<td>".$row['des_sik']."</td>";
                $halE .= "</tr>";
                $barisE++;
              }
            }
          }
          // Halaman Kompetensi Mapel Umum
          $body = '<br><b>&nbsp;&nbsp;Deskripsi Kompetensi Dasar Peserta Didik</b><br>
                      <table border="0.1px" cellspacing="0" cellpadding="2">
                      <tr>
                          <td align="center" width="5%"><b>No.</b></td>
                          <td align="center" width="25%"><b>Mata Pelajaran</b></td>
                          <td align="center" width="25%"><b>Kompetensi</b></td>
                          <td align="center" width="45%"><b>Catatan</b></td>
                      </tr>
                      <tr>
                          <td colspan="4"><b>Kelompok A</b></td>
                      </tr>
                      '.$halA.'
                      <tr>
                          <td colspan="4"><b>Kelompok B</b></td>
                      </tr>
                      '.$halB.'
                      <tr>
                          <td colspan="4"><b>Kelompok C</b></td>
                      </tr>
                      '.$halC.'
                      <tr>
                          <td colspan="4"><b>Kelompok Lintas Peminatan</b></td>
                      </tr>
                      '.$halD.'
                  </table>
                  <br>&nbsp;&nbsp;*) Muatan Lokal Provinsi Bali<br><br>';

          // output the HTML content
          $this->pdf->writeHTML($header, true, false, true, false, '');
          $this->pdf->writeHTML($body, true, false, true, false, '');
          $this->pdf->writeHTML($footer, true, false, true, false, '');
          $this->pdf->AddPage();

          // Halaman Kompetensi Mapel Muatan
          $body = '&nbsp;&nbsp;<table border="0.1px" cellspacing="0" cellpadding="2">
                      <tr>
                          <td align="center" width="5%"><b>No.</b></td>
                          <td align="center" width="25%"><b>Mata Pelajaran</b></td>
                          <td align="center" width="25%"><b>Kompetensi</b></td>
                          <td align="center" width="45%"><b>Catatan</b></td>
                      </tr>
                      <tr>
                          <td colspan="4"><b>MUATAN LOKAL</b></td>
                      </tr>
                      '.$halE.'
                  </table>';

          // output the HTML content
          $this->pdf->writeHTML($header, true, false, true, false, '');
          $this->pdf->writeHTML($body, true, false, true, false, '');
          $this->pdf->writeHTML($footer, true, false, true, false, '');
          $this->pdf->AddPage();
        }

        // Halaman Pengembangan Diri
        $ekskul = $organisasi = null;
        if($data['pede'])
        {
          $barisE = 1;
          $barisO = 1;
          foreach ($data['pede'] as $row) {
            if ($row['tipe'] == 'Ekstrakurikuler') {
              $ekskul .= "<tr>";
              $ekskul .= "<td></td>";
              $ekskul .= "<td>".$barisE."</td>";
              $ekskul .= "<td>".$row['nama_pede']."</td>";
              $ekskul .= "<td>".$row['nilai_pede']."</td>";
              $ekskul .= "<td>".$row['keterangan']."</td>";
              $ekskul .= "</tr>";
              $barisE++;
            }
            if ($row['tipe'] == 'Organisasi') {
              $organisasi .= "<tr>";
              $organisasi .= "<td></td>";
              $organisasi .= "<td>".$barisO."</td>";
              $organisasi .= "<td>".$row['nama_pede']."</td>";
              $organisasi .= "<td>".$row['nilai_pede']."</td>";
              $organisasi .= "<td>".$row['keterangan']."</td>";
              $organisasi .= "</tr>";
              $barisO++;
            }
          }
        }

        $body = '<br><br><b>&nbsp;&nbsp;Pengembangan Diri</b><br>
                <table border="0.1px" cellspacing="0" cellpadding="2">
                    <tr>
                        <td width="5%">No.</td>
                        <td width="40%" colspan="2">Jenis Kegiatan</td>
                        <td width="10%">Nilai</td>
                        <td width="45%">Keterangan</td>
                    </tr>
                    <tr>
                        <td>A</td>
                        <td colspan="2">Kegiatan Ekstrakurikuler</td>
                        <td></td>
                        <td></td>
                    </tr>
                    '.$ekskul.'
                    <tr>
                        <td>B</td>
                        <td colspan="2">Keikutsertaan dalam Organisasi / Kegiatan Sekolah</td>
                        <td></td>
                        <td></td>
                    </tr>
                    '.$organisasi.'
                </table>';

        // Halaman Ketidakhadiran
        $body .= '<br><br><b>&nbsp;&nbsp;Ketidakhadiran</b><br>
                    <table border="0.1px" cellspacing="0" cellpadding="2">
                    <tr>
                        <td width="5%">No.</td>
                        <td width="45%">Alasan Ketidakhadiran</td>
                        <td width="50%">Keterangan</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Sakit</td>
                        <td>'.$data['jml_sakit'].'</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Izin</td>
                        <td>'.$data['jml_izin'].'</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Tanpa Keterangan</td>
                        <td>'.$data['jml_tketerangan'].'</td>
                    </tr>
                </table>';

        // Halaman Catatan Wali Kelas
        $body .= '<br><b>&nbsp;&nbsp;</b><br>
                <table border="0.1px" cellspacing="0" cellpadding="2">
                    <tr>
                        <td align="center">
                        <b>Catatan Wali Kelas</b>
                        <br>
                        <br>
                        '.$data['isi_catatan'].'
                        <br>
                        </td>
                    </tr>
                </table>';

        if(strpos($data['id_periodeDet'],"uas2") !== false) {
          if($data['catatan'])
            {
              $rapat_dewanGuru = $data['rapat_dewanGuru'];
              if($data['status_kenaikan'])
                $status_kenaikan = "Naik/<s>Tidak Naik</s>*)";
              else
                $status_kenaikan = "<s>Naik</s>/Tidak Naik*)";
              $kelas_lanjut = $data['kelas_lanjut'];
            }
          else
          {
            $rapat_dewanGuru =  $status_kenaikan = $kelas_lanjut = null;
          }

          $kenaikan = "Dengan memperhatikan persyaratan kenaikan kelas dan hasil keputusan rapat dewan guru pada tanggal ".$rapat_dewanGuru." Peserta Didik dinyatakan : ".$status_kenaikan." ke kelas: ".$kelas_lanjut;
          // Halaman Kenaikan khusus untuk UAS Semester Genap
          $body .= '<br><b>&nbsp;&nbsp;</b><br>
                  <table border="0.1px" cellspacing="0" cellpadding="2">
                      <tr>
                          <td align="center">
                          <br><br>
                          '.$kenaikan.'
                          <br>
                          </td>
                      </tr>
                  </table>';
        }

        // output the HTML content
        $this->pdf->writeHTML($header, true, false, true, false, '');
        $this->pdf->writeHTML($body, true, false, true, false, '');
        $this->pdf->writeHTML($footer, true, false, true, false, '');
         
        //Close and output PDF document
        $this->pdf->Output('example_002.pdf', 'I');
         
        //============================================================+
        // END OF FILE
        //============================================================+
    }

    function leger_excel ($data)
    {
      $this->load->library('excel');

      //activate worksheet number 1
      $this->excel->setActiveSheetIndex(0);
      $sheet = $this->excel->getActiveSheet();

      //name the worksheet
      $sheet->setTitle('Mata Pelajaran Reguler');

      $sheet
        ->getPageSetup()
        ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      $sheet
        ->getPageSetup()
        ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

      /*
        Inisialisasi variabel dan mencari jumlah kolom mapel
      */
      $kel_a = $kel_b = $kel_c = $kel_d = $kel_e = 0; $max_row = 1; $max_col = 1;
      if($data['mapel'])
      foreach ($data['mapel'] as $row)
      {
        if ($row['id_kelompok'] == 'A') $kel_a++;
        if ($row['id_kelompok'] == 'B') $kel_b++;
        if ($row['id_kelompok'] == 'C_IPA' || $row['id_kelompok'] == 'C_IPS' || $row['id_kelompok'] == 'C_IB') $kel_c++;
        if ($row['id_kelompok'] == 'D') $kel_d++;
        if ($row['id_kelompok'] == 'E') $kel_e++;
      }
      if($kel_a) $kel_a = ($kel_a*5)-1; if($kel_b) $kel_b = ($kel_b*5)-1; if($kel_c) $kel_c = ($kel_c*5)-1; if($kel_d) $kel_d = ($kel_d*5)-1; if($kel_e) $kel_e = ($kel_e*5)-1;
      
      /*
        Mencetak Header leger
      */
      $sheet->setCellValue('A1', 'DAFTAR NILAI RAPOR '. $data['nama_tes']. ' SEMESTER '.$data['semester'].' TAHUN AJARAN '.$data['id_periode']);
      $sheet->setCellValue('A2', 'SMA CHIS DENPASAR');

      $sheet->setCellValue('A4', 'Kelas');

      $sheet
        ->setCellValue('A8', 'No')
        ->setCellValue('B8', 'NIS')
        ->setCellValue('C8', 'NISN')
        ->setCellValue('D8', 'Nama');

      $sheet->mergeCells('A8:A10');
      $sheet->mergeCells('B8:B10');
      $sheet->mergeCells('C8:C10');
      $sheet->mergeCells('D8:D10');

      $temp='D';
      if($kel_a) {
        $cel_a = ++$temp;
        $temp = chr(ord($temp) + $kel_a);
        $cel_a1 = $temp;
        
        $sheet->setCellValue($cel_a.'8', 'MAPEL WAJIB KELOMPOK A');
        $sheet->mergeCells($cel_a.'8:'.$cel_a1.'8');
      }
      if($kel_b) {
        $cel_b = ++$temp;
        $temp = chr(ord($temp) + $kel_b);
        $cel_c1 = $temp;

        $sheet->setCellValue($cel_b.'8', 'MAPEL WAJIB KELOMPOK B');
        $sheet->mergeCells($cel_b.'8:'.$cel_b1.'8');
      }
      if($kel_c) {
        $cel_c = ++$temp;
        $temp = chr(ord($temp) + $kel_c);
        $cel_c1 = $temp;

        $sheet->setCellValue($cel_c.'8', 'MAPEL KELOMPOK PEMINATAN');
        $sheet->mergeCells($cel_c.'8:'.$cel_c1.'8');
      } 
      if($kel_d) {
        $cel_d = ++$temp;
        $temp = chr(ord($temp) + $kel_d);
        $cel_d1 = $temp;

        $sheet->setCellValue($cel_d.'8', 'MAPEL KELOMPOK LINTAS PEMINATAN');
        $sheet->mergeCells($cel_d.'8:'.$cel_d1.'8');
      } 

      $jml = ++$temp;
      //$temp = chr(ord($temp) + 1);
      //$jml1 = $temp;
      $sheet->setCellValue($jml.'8', 'JUMLAH');
      $sheet->setCellValue($temp.'10', 'P');
      $sheet->setCellValue(++$temp.'10', 'K');
      $jml1 = $temp;
      $sheet->mergeCells($jml.'8:'.$jml1.'9');

      $rt2 = ++$temp;
      //$temp = chr(ord($temp) + 1);
      //$rt22 = $temp;
      $sheet->setCellValue($rt2.'8', 'RATA-RATA');
      $sheet->setCellValue($temp.'10', 'P');
      $sheet->setCellValue(++$temp.'10', 'K');
      $rt22 = $temp;
      $sheet->mergeCells($rt2.'8:'.$rt22.'9');

      $rk = ++$temp;
      $sheet->setCellValue($rk.'8', 'RK');
      $sheet->mergeCells($rk.'8:'.$rk.'10');

      $sheet->getStyle('E8:'.$rk.'8')->getAlignment()->setWrapText(true);
      $sheet->getStyle('E9:'.$rk.'9')->getAlignment()->setWrapText(true);

      $kol = 'D'; $bar = 9; $pindah_e=0;
      if($data['mapel'])
      foreach ($data['mapel'] as $row)
      {
        if ($row['id_kelompok'] != 'E')
        {
          $kiri = ++$kol;
          //$kol = chr(ord($kol) + 4);
          //$kanan = $kol;
          $sheet->setCellValue($kiri.'9', $row['nama_mapel']);
          $sheet->setCellValue($kol.'10', 'P');
          $sheet->setCellValue(++$kol.'10', 'pr');
          $sheet->setCellValue(++$kol.'10', 'K');
          $sheet->setCellValue(++$kol.'10', 'pr');
          $sheet->setCellValue(++$kol.'10', 'S');
          $kanan=$kol;
          $sheet->mergeCells($kiri.$bar.':'.$kanan.$bar);
        }
          
        /*if (($row['id_kelompok'] == 'E') && ($pindah_e == 1)) {
          $aa = chr(ord($kol) + 5);
          $q = ++$kol;
          $pindah_e = 0;
        }
        else {
          $aa = chr(ord($kol) + 5);
          $q = chr(ord($kol) + 4);
        }
        
        
        $q1 = $aa;
        $kol = $aa;
        $sheet->setCellValue($q.$bar, $row['nama_mapel']); 
        $sheet->mergeCells($q.$bar.':'.$q1.$bar);*/
      }

      $kol = 'A'; $bar = 11;
      $baris = 1;
      if ($data['siswa']) {
        foreach ($data['siswa'] as $row) {
          $sheet->setCellValue($kol++.$bar, $baris); 
          $sheet->setCellValue($kol++.$bar, $row['id_siswa']); 
          $sheet->setCellValue($kol++.$bar, ""); 
          $sheet->setCellValue($kol++.$bar, $row['nama_siswa']); 
        
          if($data['mapel']) {
            foreach ($data['mapel'] as $row1) {
              if ($row1['id_kelompok'] != 'E') 
              {
                if($data['nilai'])
                {
                  $nilai = $data['nilai'];
                  if($nilai[$row['id_siswa']][$row1['id_mapel']])
                  {
                    foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                      $sheet->setCellValue($kol++.$bar, $row2['nilai_pengetahuan']); 
                      $sheet->setCellValue($kol++.$bar, $row2['predikat_pengetahuan']); 
                      $sheet->setCellValue($kol++.$bar, $row2['nilai_keterampilan']); 
                      $sheet->setCellValue($kol++.$bar, $row2['predikat_keterampilan']); 
                      $sheet->setCellValue($kol++.$bar, $row2['predikat_sikap']); 
                    }
                  }
                  else
                  {
                    $sheet->setCellValue($kol++.$bar, ""); 
                    $sheet->setCellValue($kol++.$bar, ""); 
                    $sheet->setCellValue($kol++.$bar, ""); 
                    $sheet->setCellValue($kol++.$bar, ""); 
                    $sheet->setCellValue($kol++.$bar, ""); 
                  }
                }
              }
            }
          }
          $flag_total=1;
          if($data['total'])
          {
            foreach ($data['total'] as $row3) {
              if ($row3['id_tSisKlsKey'] == $row['id_transSisKls'] && $row3['status_muatan'] == 0)
              {
                $sheet->setCellValue($kol++.$bar, $row3['jml_nilai_pengetahuan']); 
                $sheet->setCellValue($kol++.$bar, $row3['jml_nilai_keterampilan']); 
                $sheet->setCellValue($kol++.$bar, $row3['rata2_nilai_pengetahuan']); 
                $sheet->setCellValue($kol++.$bar, $row3['rata2_nilai_keterampilan']); 
                $sheet->setCellValue($kol++.$bar, $row3['ranking']); 
                $flag_total=0;
              }
            }
            if($flag_total)
              {
                $sheet->setCellValue($kol++.$bar, ""); 
                $sheet->setCellValue($kol++.$bar, ""); 
                $sheet->setCellValue($kol++.$bar, ""); 
                $sheet->setCellValue($kol++.$bar, ""); 
                $sheet->setCellValue($kol++.$bar, ""); 
              }
          }

          /*if($data['mapel']) {
            foreach ($data['mapel'] as $row1) {
              if ($row1['id_kelompok'] == 'E')
              {
                foreach ($nilai[$row['id_siswa']][$row1['id_mapel']] as $row2) {
                  $sheet->setCellValue($kol++.$bar, $row2['nilai_pengetahuan']); 
                  $sheet->setCellValue($kol++.$bar, $row2['predikat_pengetahuan']); 
                  $sheet->setCellValue($kol++.$bar, $row2['nilai_keterampilan']); 
                  $sheet->setCellValue($kol++.$bar, $row2['predikat_keterampilan']); 
                  $sheet->setCellValue($kol++.$bar, $row2['predikat_sikap']); 
                }
              }
            }
          }*/
          $kol = 'A';
          $bar++;
          $baris++;
        }
        $max_row = $bar - 1; 
      }
      
      // Merge judul Leger agar di tengah
      $sheet->mergeCells('A1:'.$temp.'1');
      $sheet->getStyle('A1:'.$temp.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      $max_col = $temp;

      /*
      Footer Excel untuk Tanda tangan
      */
      $foot_col = chr(ord($temp) - 10);
      $foot_row = $max_row + 2;
      // Bagian Wali Kelas
      $sheet->setCellValue('C'.($foot_row+1), 'Wali Kelas'); 
      $sheet->setCellValue('C'.($foot_row+4), $data['nama_wali']); 
      $sheet->setCellValue('C'.($foot_row+5), 'NIP. '.$data['id_wali']); 


      // Bagian Kepala Sekolah
      $sheet->setCellValue($foot_col.($foot_row), 'Denpasar, '.$data['tanggal_rapor']); 
      $sheet->setCellValue($foot_col.($foot_row+1), 'Kepala Sekolah'); 
      $sheet->setCellValue($foot_col.($foot_row+4), $data['nama_kepsek']); 
      $sheet->setCellValue($foot_col.($foot_row+5), 'NIP. '.$data['id_kepsek']); 


      $sheet->getStyle('A8:CK10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
      $sheet->getStyle('A8:CK10')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
      $styleArray = array(
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
          ),
        ),
      );

      $sheet->getStyle('A8:'.$max_col.$max_row)->applyFromArray($styleArray);

      $sheet->getDefaultRowDimension()->setRowHeight(15);
      $sheet->getDefaultColumnDimension()->setWidth(3.75);

      $sheet->getRowDimension('8')->setRowHeight(50);
      $sheet->getRowDimension('9')->setRowHeight(40);
      $sheet->getColumnDimension('A')->setWidth(5);
      $sheet->getColumnDimension('B')->setWidth(7);
      $sheet->getColumnDimension('C')->setWidth(7);
      $sheet->getColumnDimension('D')->setWidth(30);
      
      $filename='leger.xls'; //save our workbook as this file name
      header('Content-Type: application/vnd.ms-excel'); //mime type
      header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
      header('Cache-Control: max-age=0'); //no cache

      //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
      //if you want to save it as .XLSX Excel 2007 format
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
      //force user to download the Excel file without writing it to server's HD
      $objWriter->save('php://output');
    }


}

?>