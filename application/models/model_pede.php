<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_pede extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

    function get_struktur($pilihan)
    {
        if ($pilihan == 'pede') {
            $this->db->order_by('nama_pede', 'ASC');
            $query = $this->db->get('pede');
        }
        else if ($pilihan == 'enrol') {
            $this->db->where('id_pede = id_tPede');
            $this->db->where('id_pembina = id_guru');
            $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
            $query = $this->db->get('pede, trans_pedeguru, guru');
        }
        else if ($pilihan == 'guru') {
            $query = $this->db->get('guru');
        }
        else if ($pilihan == 'siswa') {
            $this->db->where('status_siswa', '1');
            $query = $this->db->get('siswa');
        }

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

	function get_detail($id_tPedeGuru)
    {
        $this->db->where('id_transPedeGuru', $id_tPedeGuru);
        $this->db->where('id_tPede = id_pede');
        $this->db->where('id_pembina = id_guru');
        
        $query = $this->db->get('pede, trans_pedeguru, guru');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_detail2($id_pede)
    {
        $this->db->where('id_pede', $id_pede);
        
        $query = $this->db->get('pede');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswa($id_tPedeGuru)
    {
        $this->db->where('id_tPedeGuru', $id_tPedeGuru);
        $this->db->where('id_tPedeGuru = id_transPedeGuru');
        $this->db->where('trans_pedesiswa.id_siswa = siswa.id_siswa');
        $this->db->where('trans_pedeguru.id_periode', $this->session->userdata('periode_aktif'));

        $query = $this->db->get('trans_pedeguru, trans_pedesiswa, siswa');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function tambah_pede($data)
    {
        $this->db->insert('pede', $data);
    }

    function enrol_pede($data)
    {
        $this->db->insert('trans_pedeguru', $data);
    }

    function update_tipeEnrol($id_guru)
    {
        $this->db->set('tipe', 4);
        $this->db->where('id_guru', $id_guru);
        $where = "(`tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function tambah_siswa($data, $id_transPedeGuru)
    {
        foreach ($data as $row) {
            $this->db->set('id_siswa', $row);
            $this->db->set('id_tPedeGuru', $id_transPedeGuru);
            $this->db->insert('trans_pedesiswa');
        }
    }

    function get_pedeLain($id_guru, $id_transPedeGuru)
    {
        $this->db->where('id_pembina', $id_guru);
        $this->db->where('id_transPedeGuru !=', $id_transPedeGuru);
        $this->db->where('id_periode', $this->session->userdata('periode_aktif'));
        $query = $this->db->get('trans_pedeguru');

        if($query->num_rows()>0)
        {
            return 1;
        }
        else return null;
    }

    function update_enrol($data)
    {
        $this->db->set('id_pembina', $data['id_PembinaEdit']);
        $this->db->where('id_transPedeGuru', $data['id_transPedeGuru']);
        $this->db->update('trans_pedeguru');

        if($data['guru_awalPedeLain'] == null)
        {
            $this->db->set('tipe', 'NULL', false);
            $this->db->where('id_guru', $data['id_PembinaAwal']);
            $this->db->where('tipe > 3');
            $this->db->update('guru');
        }

        $this->db->set('tipe', 4);
        $this->db->where('id_guru', $data['id_PembinaEdit']);
        $where = "(`tipe` IS NULL)";
        $this->db->where($where);
        $this->db->update('guru');
    }

    function update_katalog($data)
    {
        $this->db->set('nama_pede', $data['nama_pede']);
        $this->db->set('tipe_pede', $data['tipe_pede']);
        $this->db->where('id_pede', $data['id_pede']);
        $this->db->update('pede');
    }
    
    function delete_siswa($id_transPedeSiswa)
    {
        $this->db->where('id_transPedeSiswa', $id_transPedeSiswa);

        $query = $this->db->delete('trans_pedesiswa');
    }

    function delete_pede($data)
    {
        if($data['pilihan']=='enrol')
        {
            $this->db->where('id_transPedeGuru', $data['key']);
            $this->db->delete('trans_pedeguru');
        }
        elseif($data['pilihan']=='katalog')
        {
            $this->db->where('id_pede', $data['key']);
            $this->db->delete('pede');
        }
    }
}