<?php

class Template {
	
	protected $_ci;
	
	function __construct()
	{
		$this->_ci = &get_instance();
	}

	function display($page, $data)
	{
		$data['_content']=$this->_ci->load->view($page, $data, true);
		$data['_head']=$this->_ci->load->view('_base/head', $data, true);
		$data['_navbar_top']=$this->_ci->load->view('_base/navbar_top', $data, true);
		$data['_sidebar']=$this->_ci->load->view('_base/sidebar', $data, true);
		$data['_footer']=$this->_ci->load->view('_base/footer', $data, true);

		/*$parts = explode("/",$page);
		$data['page'] = $parts['0'];
		$data['_nav']=$this->_ci->load->view('base/navigation', $data, true);*/

		$this->_ci->load->view('_base/template.php',$data);
	}

	/*function displayLogin($page, $data)
	{
		$data['_content']=$this->_ci->load->view($page, $data, true);
		$data['_header']=$this->_ci->load->view('base/header-login', $data, true);
		$data['_footer']=$this->_ci->load->view('base/footer', $data, true);
		$this->_ci->load->view('base/template-login.php',$data);
	}*/
}

?>