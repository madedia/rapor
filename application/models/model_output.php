<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_output extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_mapelAgama($kelas, $id_siswa, $agama)
	{
        $this->db->order_by('id_kelasGen, id_mapel');
        $this->db->where('id_kelasGen', $kelas);
        $this->db->like('mapel.nama_mapel', $agama);
        $this->db->where('siswa.id_siswa', $id_siswa);

        $query = $this->db->get('mapel, siswa');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
	}

    function get_identitas($data)
    {
        $this->db->select('*, wali.nama_guru AS nama_wali, wali.id_guru AS id_wali, kepsek.nama_guru AS nama_kepsek, kepsek.id_guru AS id_kepsek');
        $this->db->where('siswa.id_siswa', $data['id_siswa']);
        $this->db->where('siswa.id_siswa = trans_siswa.id_siswa');
        $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        
        $this->db->where('periode.id_periode', $data['id_periode']);
        $this->db->where('trans_periode.id_transPeriode', $data['id_periodeDet']);
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');

        $this->db->where('periode.id_periode = trans_kelas.id_periode');
        $this->db->where('periode.tahun_awal >= trans_kepsek.tahun_mulai');
        $this->db->where('periode.tahun_akhir <= trans_kepsek.tahun_berakhir');
        //$or_where = "(`periode`.`tahun_akhir` <= `trans_kepsek`.`tahun_berakhir` OR `trans_kepsek`.`tahun_berakhir` IS NULL)";
        //$this->db->where($or_where);
        //$this->db->where('periode.tahun_akhir <= trans_kepsek.tahun_berakhir');
        
        $this->db->join('guru AS wali', 'wali.id_guru = trans_kelas.id_guruWali', 'left');

        $this->db->join('guru AS kepsek', 'trans_kepsek.id_kepsek = kepsek.id_guru', 'left');

        $query = $this->db->get('siswa, trans_siswa, periode, kelas_general, trans_klsgeneral, trans_kelas, trans_kepsek, trans_periode');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_nilai($data)
    {
        $this->db->where('trans_siswa.id_siswa', $data['id_siswa']);
        $this->db->where('trans_siswa.id_transSisKls = nilai_mapel.id_tSisKlsKey');
        //$this->db->where('nilai_mapel.id_tSisKlsKey', $data['id_transSisKls']);

        $this->db->where('nilai_mapel.id_periodeDet', $data['id_periodeDet']);        

        // data mapel
        $this->db->where('nilai_mapel.id_tAjarKey = trans_ajar.id_transAjar');
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');

        $query = $this->db->get('nilai_mapel, trans_ajar, mapel, trans_siswa');

        //echo $this->db->last_query() . '<br>';


        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_catatan($data)
    {
        $this->db->where('trans_siswa.id_siswa', $data['id_siswa']);

        // get periode
        $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode', $data['id_periode']);
        
        $this->db->where('trans_siswa.id_transSisKls = catatan_walikelas.id_tSisKlsKey');
        $this->db->where('catatan_walikelas.id_periodeDet', $data['id_periodeDet']);
        $this->db->where('catatan_walikelas.id_periodeDet = trans_periode.id_transPeriode');
        $this->db->where('trans_periode.id_periode = periode.id_periode');

        //get nama kelas
        if(strpos($data['id_periodeDet'],"uas2") !== false)
        {
            $this->db->where('catatan_walikelas.kelas_lanjut = trans_klsgeneral.id_trans_klsgeneral');
            $query = $this->db->get('catatan_walikelas, trans_kelas, trans_siswa, periode, trans_periode, trans_klsgeneral');
        }
        else
            $query = $this->db->get('catatan_walikelas, trans_kelas, trans_siswa, periode, trans_periode');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_pede($data)
    {
        $this->db->where('trans_pedesiswa.id_siswa', $data['id_siswa']);
        $this->db->where('trans_pedesiswa.id_tPedeGuru = trans_pedeguru.id_transPedeGuru');
        $this->db->where('trans_pedeguru.id_periode', $data['id_periode']);
        $this->db->where('trans_pedeguru.id_tPede = pede.id_pede');

        $this->db->where('trans_pedesiswa.id_transPedeSiswa = nilai_pede.id_tPedeSiswaKey');        
        $this->db->where('nilai_pede.id_periodeDet', $data['id_periodeDet']);

        $query = $this->db->get('trans_pedesiswa, trans_pedeguru, nilai_pede, pede');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_thnAjaran($id_siswa)
    {
        $this->db->where('trans_siswa.id_siswa', $id_siswa);
        $this->db->where('trans_siswa.id_tKelasKey = trans_kelas.id_transKelas');
        $this->db->where('trans_kelas.id_periode = trans_periode.id_periode');
        $this->db->order_by('trans_periode.urutan', 'asc');
        $query = $this->db->get('trans_siswa, trans_kelas, trans_periode');


        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

	function get_mapel($kelas, $id_siswa, $kelompok)
	{
        $this->db->order_by('id_kelasGen, mapel.id_mapel');
        $this->db->where('id_kelasGen', $kelas);
        $this->db->where('siswa.id_siswa', $id_siswa);

		$this->db->where('mapel.id_kelompok', $kelompok);

        $query = $this->db->get('mapel, siswa');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
	}

	function get_thnAjaranL($id_transKelas)
	{
		$this->db->where('id_transKelas', $id_transKelas);
        $this->db->where('trans_kelas.id_periode = trans_periode.id_periode');
        $this->db->order_by('trans_periode.urutan', 'asc');
		$query = $this->db->get('trans_kelas, trans_periode');

		if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
	}

    function get_mapelL($data)
    {
        //$this->db->where('trans_kelas.id_kelas', $data['id_kelas']);
        $this->db->where('trans_kelas.id_transKelas', $data['id_transKelas']);

        $this->db->where('trans_kelas.id_transKelas = trans_ajar.id_tKelasKey');

        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->order_by('mapel.id_kelompok', 'ASC');

        $query = $this->db->get('trans_kelas, trans_ajar, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_klpMapelL($data)
    {
        //$this->db->where('trans_kelas.id_kelas', $data['id_kelas']);
        $this->db->where('trans_kelas.id_transKelas', $data['id_transKelas']);

        $this->db->where('trans_kelas.id_transKelas = trans_ajar.id_tKelasKey');

        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->group_by('mapel.id_kelompok');

        $query = $this->db->get('trans_kelas, trans_ajar, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_siswaL($data)
    {
        //$this->db->where('trans_kelas.id_kelas', $data['id_kelas']);
        $this->db->where('trans_kelas.id_transKelas', $data['id_transKelas']);

        $this->db->where('trans_kelas.id_transKelas = trans_siswa.id_tKelasKey');
        $this->db->where('trans_siswa.id_siswa = siswa.id_siswa');

        $query = $this->db->get('trans_siswa, siswa, trans_kelas');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_nilaiL($id_transSisKls, $id_transAjar, $id_periodeDet)
    {
        //$this->db->select('nilai_pengetahuan, predikat_pengetahuan, nilai_keterampilan, predikat_keterampilan, predikat_sikap, id_kelompok');
        $this->db->where('nilai_mapel.id_tSisKlsKey', $id_transSisKls);
        $this->db->where('nilai_mapel.id_tAjarKey', $id_transAjar);
        $this->db->where('nilai_mapel.id_tAjarKey', $id_transAjar);
        $this->db->where('nilai_mapel.id_tAjarKey = trans_ajar.id_transAjar');
        $this->db->where('trans_ajar.id_mapelKey = mapel.id_mapel');
        $this->db->where('nilai_mapel.id_periodeDet', $id_periodeDet);

        $query = $this->db->get('nilai_mapel, trans_ajar, mapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_totalNilai($data)
    {
        $this->db->where('id_tKelasKey', $data['id_transKelas']);
        $this->db->where('id_periodeDet', $data['id_periodeDet']);

        $query = $this->db->get('total_nilaimapel');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_totalNilaiR($data)
    {
        $this->db->where('trans_siswa.id_siswa', $data['id_siswa']);
        $this->db->where('trans_siswa.id_transSisKls = total_nilaimapel.id_tSisKlsKey');
        //$this->db->where('total_nilaimapel.id_tSisKlsKey', $data['id_transSisKls']);

        $this->db->where('total_nilaimapel.id_tSisKlsKey = trans_siswa.id_transSisKls');
        $this->db->where('trans_siswa.id_tKelasKey = total_nilaimapel.id_tKelasKey');
        $this->db->where('id_periodeDet', $data['id_periodeDet']);

        $query = $this->db->get('total_nilaimapel, trans_siswa');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_identitasL($data)
    {
        $this->db->select('*, wali.nama_guru AS nama_wali, wali.id_guru AS id_wali, kepsek.nama_guru AS nama_kepsek, kepsek.id_guru AS id_kepsek');

        $this->db->where('trans_kelas.id_transKelas', $data['id_transKelas']);
        $this->db->where('trans_kelas.id_tklsgeneralKey = trans_klsgeneral.id_trans_klsgeneral');
        $this->db->where('trans_klsgeneral.id_kelasGen = kelas_general.id_kelasGen');
        $this->db->where('trans_periode.id_transPeriode', $data['id_periodeDet']);

        $this->db->where('trans_periode.id_periode = periode.id_periode');
        $this->db->where('periode.tahun_awal >= trans_kepsek.tahun_mulai');
        $this->db->where('periode.tahun_akhir <= trans_kepsek.tahun_berakhir');

        $this->db->join('guru AS wali', 'wali.id_guru = trans_kelas.id_guruWali', 'left');
        $this->db->join('guru AS kepsek', 'trans_kepsek.id_kepsek = kepsek.id_guru AND trans_kepsek.status_kepsek = 1', 'left');

        $query = $this->db->get('periode, kelas_general, trans_klsgeneral, trans_kelas, trans_kepsek, trans_periode');

        //echo $this->db->last_query() . '<br>';

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }
}