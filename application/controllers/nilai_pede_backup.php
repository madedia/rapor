<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai_pede extends CI_Controller
{	
	function __construct()
	{
		parent:: __construct();

		if ($this->session->userdata('logged_in') == null)
		{
            $this->session->sess_destroy();
            redirect ('login');
        }

		$this->load->model('model_pede');
		$this->load->model('model_nilaiPede');
	}

	function index()
	{
		$data['pede_enrol'] = $this->model_nilaiPede->get_pedeEnrol();

		$this->template->display('nilai_pede/pede_enrol', $data);
	}

	function isi($id_transPedeGuru, $id_pembina)
	{
		$data['pede_detail']= $this->model_pede->get_detail($id_transPedeGuru);
		foreach ($data['pede_detail'] as $row) {
			$data['id_transPedeGuru'] = $row['id_transPedeGuru'];
			$data['nama_pede'] = $row['nama_pede'];
		}

		$data['isUpdate'] = 1;
		$data['siswa_pede']= $this->model_nilaiPede->get_siswa($id_transPedeGuru, $id_pembina);
		if($data['siswa_pede'] == null) {
			$data['isUpdate'] = 0;
			$data['siswa_pede']= $this->model_nilaiPede->get_siswa2($id_transPedeGuru, $id_pembina);	
		}

		$this->template->display('nilai_pede/pede_nilai', $data);
	}

	function insert ()
	{
		$data = array(
			'isUpdate' => $this->input->post('isUpdate'),
			'id_tPedeSiswaKey' => $this->input->post('id_transPedeSiswa'),
			'nilai_pede' => $this->input->post('nilai_pede'),
			'keterangan' => $this->input->post('keterangan'),
		);
		//$data['id_pede'] = $id_pede;	
		if($data['isUpdate'])
		{
			$data['id_nilaiPede'] = $this->input->post('id_nilaiPede');
		}

		$this->model_nilaiPede->insert($data);
		redirect("nilai_pede");
	}

	public function ekspor_tabel($id_tAjar)
	{
		$result = $this->model_nilaiMapel->get_siswaKelas($id_tAjar);
		$this->load->library('excel');

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//$sheet = $this->excel->getActiveSheet();
		$this->excel->getActiveSheet()->setCellValue('A1', 'DAFTAR NILAI');
    	$this->excel->getActiveSheet()->mergeCells('A1:E1');
    	$this->excel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    	//$this->excel->getActiveSheet()->setCellValue('A3', 'KELAS ');

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('Daftar Nilai Mapel ........');

				$this->excel->getActiveSheet()
					->setCellValue('A6', 'NIS')
					->setCellValue('B6', 'Nama Lengkap')
					->setCellValue('C6', 'Nilai Pengetahuan')
					->setCellValue('D6', 'Nilai Keterampilan')
					->setCellValue('E6', 'Nilai Sikap');

		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$this->excel->getActiveSheet()->getStyle('A6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$styleArray = array(
		'borders' => array(
		'allborders' => array(
		'style' => PHPExcel_Style_Border::BORDER_THIN,
		'color' => array('argb' => '000000'),
		),
		),
		);

		$this->excel->getActiveSheet()->getStyle('A6:E6')->applyFromArray($styleArray);

		/*$baris=6;
		foreach ($result as $res) {
			if ($res['id_siswa']!=''){
				$this->excel->getActiveSheet()
							//->getStyle('A'.($baris+1))->applyFromArray($styleArray)
							->setCellValue('A'.($baris+1), $res['id_siswa'])
							->setCellValue('B'.($baris+1), $res['nama_siswa']);
			}
			$this->excel->getActiveSheet()->getStyle('A'.($baris+1))->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('B'.($baris+1))->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('C'.($baris+1))->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('D'.($baris+1))->applyFromArray($styleArray);
			$this->excel->getActiveSheet()->getStyle('E'.($baris+1))->applyFromArray($styleArray);
			$baris++;
		}*/
		
		$this->excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(20);


		$filename='test.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

}