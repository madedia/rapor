<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Periode
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Periode</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <a data-toggle="modal" href="#tambahPeriode">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3><?php echo count($periode) ?></h3>
	                  <p>Tambah Periode</p>
	                </div>
	                <div class="icon">
	                  <i class="ion ion-calendar"></i>
	                </div>
	              </div>
              </a>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
  	<!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
        	<h3 class="box-title">Data Periode</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
          <div class="dropdown">
            <label>Tahun Ajaran </label>
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $id_periode;?><span class="caret"></span></button>
            <ul class="dropdown-menu">
              <?php
                if ($periode){
                  foreach ($periode as $row) {
                    if($row['id_periode'] == $id_periode)
                      echo "<li class='active'><a href='".base_url('periode/index/'.$row['id_periode'])."'>". $row['id_periode']. "</a></li>";
                    else
                      echo "<li><a href='".base_url('periode/index/'.$row['id_periode'])."'>". $row['id_periode']. "</a></li>";
                  }
                }
              ?>
              
            </ul>
          </div>

          <br/>
          <div>
            <!--<label>Tahun Ajaran ".$id_periode."</label>-->
            <table class='table table-bordered table-striped'>
              <thead>
                <tr>
                  <th style="text-align:center">Nama Periode</th>
                  <th style="text-align:center">Tanggal Rapat Dewan Guru</th>
                  <th style="text-align:center">Tanggal Pembagian Rapor</th>
                  <th style="text-align:center">Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if ($periodeDet) {
                    foreach ($periodeDet as $row) {
                      echo "<tr>";
                      echo "<td class='nr' style='display:none'>".$row['id_transPeriode']."</td>";
                      echo "<td>".$row['nama_tes']."</td>";
                      echo "<td>".$row['rapat_dewanGuru']."</td>";
                      echo "<td>".$row['tanggal_rapor']."</td>";
                      //echo "<td><a class='edit-periode'><span style='margin-right:3px;' class='glyphicon glyphicon-edit'></span></a></td>";
                      echo '<td style="text-align:center">&nbsp <a class="edit-periode btn btn-sm btn-warning" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a></td>';
                      echo "</tr>";
                    }
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
  <div class="modal fade" id="tambahPeriode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Tahun Ajaran</h4>
        </div>
        <div class="modal-body">
          <?php 
            echo form_open('periode/tambah_periode');?>

            <div class="form-group">
              <?php 
              echo form_label('Tahun Mulai', 'nama_mapel', array(
                  'class' => 'control-label',
              ));
              echo form_input(array(
                    'class' => 'form-control',
                          'name'  => 'tahun_mulai',
                          'id'    => 'nama_mulai',
                        ));
              ?>
            </div>

            <div class="form-group">
              <?php 
              echo form_label('Tahun Berakhir', 'tahun_akhir', array(
                  'class' => 'control-label',
              ));
              echo form_input(array(
                    'class' => 'form-control',
                          'name'  => 'tahun_akhir',
                          'id'    => 'tahun_akhir',
                        ));
              ?>
            </div>

      
          <!-- checkbox -->
          <div class="form-group">
            <label>
              Enrol Kelas dan Wali berdasarkan Tahun Ajaran Sebelumnya?
            </label>
            <br>
            <input type="checkbox" class="minimal" name="statusKelasOtomatis" value="1">
              Setuju
            </input>
          </div>
    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div> <!-- end modal -->


<!-- Modal -->
  <div class="modal fade" id="editPeriode2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Periode</h4>
        </div>
        <?php echo form_open(base_url('periode/do_edit/')); ?>
        <div class="modal-body" id="detail_tPeriode">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <?php echo "<td><button type='submit' class='btn btn-info'><span style='margin-right:3px;' class='glyphicon glyphicon-saved'></span>Simpan Data</button></td>"; ?>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div> <!-- end modal -->

<script type="text/javascript">
	function get_periodeData(){
    var id_periode =$('#select_periode').val();
    var url = '<?php echo base_url('periode/data_periode');?>'
    //window.alert (id_periode);

    $.ajax
      ({
      type: "POST",
      url: url,
      data: {"id_periode":id_periode},
      cache: false,
      success: function(html)
      {
        $("#sdata_periode").html(html);
      }
    });
  }

  $(".edit-periode").click(function() {
      var $row = $(this).closest("tr");    // Find the row
      var $id_transPeriode = $row.find(".nr").text(); // Find the text
      var $url = '<?php echo base_url('periode/edit');?>'
      // Let's test it out
      //alert($id_transPeriode);

      $.ajax
      ({
        type: "POST",
        url: $url,
        data: {"id_transPeriode":$id_transPeriode},
        cache: false,
        success: function(html)
        {
          $("#detail_tPeriode").html(html);
        }
    });

      $('#editPeriode2').modal('show');
    });
</script>

<!-- page script -->
<script>
  $(function () {
    $("#tabel_periode").DataTable();
  });
</script>