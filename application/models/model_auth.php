<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_auth extends CI_Model {
    
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_user($data){
        if($data['id_user']!='')
            $this->db->where('user.id_user', $data['id_user']); 

        if($data['password'])
            $this->db->where('password', $data['password']); 

        $this->db->from('user');
        $this->db->join('guru', 'guru.id_guru = user.id_user', 'left');

        $query = $this->db->get();

        //echo $this->db->last_query();

        if($query->num_rows()>0){
            return $query->result_array();
        }
        else return null;
    }

    function get_periodeAktif () {
        $this->db->where('status', 1); 
        $query = $this->db->get('periode');

        //echo $this->db->last_query();
        if($query->num_rows()>0){
            $res = $query->row_array();
            $val = $res['id_periode'];
            return $val;
        }
        else return null;
    }

    function get_periodeAktifDet () {
        $this->db->where('status', 1); 
        $query = $this->db->get('trans_periode');

        if($query->num_rows()>0){
            return $query->result_array();
        }
        else return null;
    }

    function get_kelasWali ($id_guru, $id_periode) {
        $this->db->where('id_guruWali', $id_guru); 
        $this->db->where('id_periode', $id_periode); 
        $this->db->where('id_tklsgeneralKey = id_trans_klsgeneral'); 
        $query = $this->db->get('trans_kelas, trans_klsgeneral');

        if($query->num_rows()>0){
            return $query->result_array();
        }
        else return null;
    }

    function get_footer()
    {
        $query = $this->db->get('periode');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }

    function get_konversi()
    {
        $query = $this->db->get('konversi_nilai');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else return null;
    }
}
